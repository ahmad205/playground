///Custom jQuery
/*jQuery(document).ready(function(){
    jQuery(".open-noti .wholepage").click(function(){
        jQuery(".notification-dropdown").hide();
        jQuery("body").removeClass('open-noti');
    });
    jQuery(".notification-alert").click(function(){
        jQuery(".notification-dropdown").slideToggle();
        jQuery("body").addClass('open-noti');
    });
});*/
/*notification*/
jQuery(document).ready(function(){

    var running = false;
    $(".notification-alert").off('click').on('click', function () {
        if(running === false) {
            running = true;

            if ($(this).hasClass('active')) {
                $('.notification-dropdown').slideToggle(function () {
                    $(".notification-alert").removeClass('active');
                    running = false;
                });
            } else {
                $('.notification-dropdown').slideToggle(function () {
                    $(".notification-alert").addClass('active');
                    running = false;
                });
            }
        }
    });

    $(document).on('click', function(e) {
        var container = $(".notification-alert");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('.notification-dropdown').slideUp();
            container.removeClass('active');
        }
    });
});
/* rocket icon*/
/*jQuery(document).ready(function(){
    jQuery(".rocket-icon").click(function(){
        jQuery(".rocket-section-content").toggleClass('open-rocket-content');
    });
});*/
jQuery(document).ready(function(){
    $(function(){               
                
        var $rocketwin = $(window); // or $box parent container
        var $rocketbtn = $(".rocket-icon");
                
        $rocketwin.on("click.Bst", function(event){      
        if ( 
            $rocketbtn.has(event.target).length == 0 //checks if descendants of $box was clicked
            &&
            !$rocketbtn.is(event.target) //checks if the $box itself was clicked
          ){
                jQuery(".rocket-section-content").removeClass('open-rocket-content');
            } else {
                jQuery(".rocket-section-content").toggleClass('open-rocket-content');
            }
        });
  
    });
});
/* Do small icon */
jQuery(document).ready(function () {
    jQuery(".dosm-visi").off('click').on('click', function () {
        jQuery(".dosm-visi").not(this).parents(".edit-listing-div").find('.dosmall-action-div').slideUp();
        jQuery(".dosm-visi").not(this).parents(".edit-listing-div").removeClass('edit-listing-div');
        jQuery(this).parents(".listing-div").toggleClass('edit-listing-div');
        jQuery(this).parents(".listing-div").find('.dosmall-action-div').slideToggle();
    });
});
jQuery(document).ready(function(){
    $(function(){               
                
        var $doactwin = $(window); // or $box parent container
        var $dosmbtn = $(".dosm-visi");
                
        $doactwin.on("click.Bst", function(event){      
        if ( 
            $dosmbtn.has(event.target).length == 0 //checks if descendants of $box was clicked
            &&
            !$dosmbtn.is(event.target) //checks if the $box itself was clicked
          ){
                jQuery(".listing-div").removeClass('edit-listing-div');
                jQuery(".listing-div").find('.dosmall-action-div').slideUp();
            }
        });
  
    });
});




jQuery(document).ready(function(){
    jQuery(".text-line").click(function(){
        jQuery(this).parent().find(".dosmall-action-ul").slideToggle();
    });
});

jQuery(document).ready(function(){
    jQuery(".remove-img").click(function(){
        jQuery(this).parents('.up-singl-img').remove();
    });
});
jQuery(document).ready(function(){
    jQuery(".alert-close").click(function(){
        jQuery(this).parent(".alert").fadeOut();
    });
});
jQuery(document).ready(function(){
setTimeout(function() {
 jQuery('.alertmsg-10').fadeOut();
}, 10000 );
});

jQuery(document).ready(function(){
    jQuery(".selfinput").keyup(function(){
    	var min = 0;
    	var len = jQuery(this).val().length;
    	if(len > min){
    		jQuery(this).addClass('changestate');
            jQuery(this).parent('label').addClass('filled-input');
            jQuery(this).parent('label').removeClass('empty-input');
    	}
    	else{
        jQuery(this).removeClass('changestate');
    		jQuery(this).parent('label').addClass('empty-input');
            jQuery(this).parent('label').removeClass('filled-input');
    	}
    });
});

jQuery(document).ready(function(){
    jQuery(".nice-select").click(function(){
        if (jQuery(".nice-select .list li").hasClass("selected")){
            jQuery(this).parent('.select-box').addClass('selecte-done');
        }
        else{
            jQuery(this).parents('.select-box').removeClass('selecte-done');
        }
    });
});
jQuery(document).ready(function(){
    jQuery(".close-popup").click(function(){
        jQuery(this).parents(".selfpopup").fadeOut();
        $(".notification-alert").removeClass('active');
        $('.notification-dropdown').slideUp();
    });

    jQuery(".nice-select .list").mCustomScrollbar();
});
///add parent class for image upload 
