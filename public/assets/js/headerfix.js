jQuery(document).ready(function(){
   $('.mobile-dropdown-menu li.has-sub > a').on('click', function (e) {
        if($('.mobile-dropdown-menu li.has-sub ul').children('li').length  > 1 ){
            e.preventDefault();
            e.stopImmediatePropagation();
        }      
        $(this).parent().toggleClass('opened');
        $(this).parent().find('ul').slideToggle(200);
    });
    $('.mobile-dropdown-menu li.has-sub ul li a').on('click',function (e) {
        e.stopPropagation();	   
    });
})
