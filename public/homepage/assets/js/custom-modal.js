jQuery(document).ready(function($){
    //cache some jQuery objects
    var modalTrigger = $('.custom-modal-trigger'),
        transitionLayer = $('.custom-modal-layer'),
        transitionBackground = transitionLayer.children(),
        modalWindow = $('.custom-modal'),
        bodyClass = $('.custom-modal-transition');

    var frameProportion = 1.78, //png frame aspect ratio
        frames = transitionLayer.data('frame'), //number of png frames
        resize = false;

    //set transitionBackground dimentions
    setLayerDimensions();
    $(window).on('resize', function(){
        if( !resize ) {
            resize = true;
            (!window.requestAnimationFrame) ? setTimeout(setLayerDimensions, 300) : window.requestAnimationFrame(setLayerDimensions);
        }
    });

    //open modal window
    modalTrigger.on('click', function(event){
        event.preventDefault();
        var modalId = $(event.target).attr('href');
        transitionLayer.addClass('visible opening');
        var delay = ( $('.no-cssanimations').length > 0 ) ? 0 : 800;
        setTimeout(function(){
            modalWindow.filter(modalId).addClass('visible');
            transitionLayer.removeClass('opening');
            bodyClass.addClass('custom-modal-transition--active');
        }, delay);

        setTimeout(function(){
            transitionLayer.removeClass('visible');
        }, delay*2);
    });

    //close modal window
    modalWindow.on('click', '.custom-modal__close, .custom-modal-close', function(event){
        event.preventDefault();
        transitionLayer.addClass('visible closing');
        modalWindow.removeClass('visible');
        bodyClass.removeClass('custom-modal-transition--active');
        transitionBackground.on('animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd', function(){
            transitionLayer.removeClass('closing opening visible');
            transitionBackground.off('animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd');
        });
    });

    function setLayerDimensions() {
        var windowWidth = $(window).width(),
            windowHeight = $(window).height(),
            layerHeight, layerWidth;

        if( windowWidth/windowHeight > frameProportion ) {
            layerWidth = windowWidth;
            layerHeight = layerWidth/frameProportion;
        } else {
            layerHeight = windowHeight*1.2;
            layerWidth = layerHeight*frameProportion;
        }

        transitionBackground.css({
            'width': layerWidth*frames+'px',
            'height': layerHeight+'px'
        });

        resize = false;
    }
});