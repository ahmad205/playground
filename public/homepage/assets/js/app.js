jQuery(document).ready(function () {
    // Check page direction
    var direction = $('html').attr('dir') == 'rtl' ? 'left' : 'right';

    // Ahmed ElShahat Changes
    $('.control-panel__images').cardslider({
        showCards: 3,
        loop: true,
        direction: direction,
        nav: false,
        dots: false
    });

    var cardSlider = $('.control-panel__images').data('cardslider');
    var newList = $("<ul />");

    for(var i = 0; $('.control-panel__images ul li').length > i ; i++){
        var className = (i == 0) ? 'active': '';
        $('<li/>', {
            'data-index' : i,
            class : className
        }).appendTo(newList);
    }
    newList.appendTo(".vertical-dots");

    var dotsList = $('.vertical-dots ul li');
    dotsList.each(function(){
        var _self = $(this);
        $(this).on('click', function(){
            if (_self.hasClass('active')){
                return false;
            }
            dotsList.removeClass('active');
            var index = _self.attr('data-index');
            _self.addClass('active');
            cardSlider.changeCardTo(parseInt(index));
            $('.panel-features .intro-section').hide().eq(index).fadeIn(800);
        });
    });

    $('.panel-features .intro-section').first().show();

    $('#gateways-canvas').tagcanvas({
        textColour : '#000',
        outlineColour: 'transparent',
        outlineThickness : 1,
        depth : 0.99,
        shape: "sphere",
        wheelZoom: false,
        dragControl: false,
        shuffleTags: true,
        maxSpeed: 0.01,
        minSpeed: 0.01,
        initial: [0,0.1],
        clickToFront: 600
    },'gateWaysList');

    $('#gateWaysList li').each(function () {
        $(this).find('a').on('click', function (e) {
            e.preventDefault();
        })
    });

    var menuButton = $('.menu > a');
    var menuItems = $('.menu__items');
    menuCalc();

    $(window).resize(function() {
        menuCalc();
    });

    menuButton.on('click', function () {
        $(this).next().slideToggle();
        menuButton.toggleClass('active');
        menuItems.find('ul').toggleClass('shown');
    });

    function menuCalc() {
        var leftCalc = menuButton.offset().left - ($('.menu__items').outerWidth()/2);
        menuItems.css('left', leftCalc);
    }

    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('active');
        if(!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('active');
    });

    $('[data-toggle="tooltip"]').tooltip();
});

$(window).on('scroll load', function () {
    $('.animated[data-animate-in][data-animate-out]').each(function () {
        var y = $(window).scrollTop(),
            x = $(this).offset().top - ($(window).height() - 200);

        var animateIn = $(this).data('animate-in'),
            animateOut = $(this).data('animate-out');
        if (y > x) {
            $(this).addClass(animateIn).removeClass(animateOut);
        } else {
            $(this).removeClass(animateIn).addClass(animateOut);
        }
    });
});