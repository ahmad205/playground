Rails.application.configure do
  # Verifies that versions and hashed value of the package contents in the project's package.json
  config.webpacker.check_yarn_integrity = true

  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false
 config.action_mailer.default_url_options = { host: 'http://dev001.payers.local' }
 #config.action_mailer.delivery_method = :smtp
 #config.action_mailer.perform_deliveries = true
 
  #config.action_mailer.delivery_method = :mailgun
#  config.action_mailer.mailgun_settings = {
#      api_key: 'key-1173f0da990305a7bcf06f4d2ef45d09',
#      domain: 'sandboxd398e72047544c559f4ba47aebd39f7a.mailgun.org'
#  }

config.action_mailer.delivery_method = :smtp
# SMTP settings for mailgun
config.action_mailer.smtp_settings = {
port:                       587,
address:               'smtp.mailgun.org',
domain:              'clients.payers.net',
user_name:         'postmaster@clients.payers.net',
password:             'd888e71e8e40c1eb2befa44a04eff80e',
authentication:  :plain,
}

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # config.cache_store = :redis_store, {
  #   host: "<%= format_link(request) %>",
  #   port: 6379,
  #   db: 0,
  #   password: "",
  #   namespace: "cache"
  #  }


  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log


  # Only use best-standards-support built into browsers
  # config.action_dispatch.best_standards_support = :builtin
  
  #   # Raise exception on mass assignment protection for Active Record models
  #   config.active_record.mass_assignment_sanitizer = :strict
  
  #   # Log the query plan for queries taking more than this (works
  #   # with SQLite, MySQL, and PostgreSQL)
  #   config.active_record.auto_explain_threshold_in_seconds = 0.5
    

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true
  
  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
  
  #IGNORE YARN
  config.webpacker.check_yarn_integrity = false
  
end
