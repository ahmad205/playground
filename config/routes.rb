Rails.application.routes.draw do
  resources :withdraws_banks


  resources :credit_cards do
    member do
      get :credit_card_image
    end
  end

  

 devise_for :userds ,:path_names => {
	:verify_authy => "/verify-token",
	:enable_authy => "/enable-two-factor",
	:verify_authy_installation => "/verify-installation",

},

controllers: { registrations: "userds/registrations",passwords:"userds/passwords", sessions: "userds/sessions"  ,  invitations: "invitations", two_factor_authentication: "userds/two_factor_authentication",checkga:"userds/checkga",confirmations:"userds/confirmations"  } do
  
  match 'userds/sign_out' => "devise/sessions#destroy"
end



  resources :users_wallets_balances,:except => [:edit,:show,:index]
  resources :users_wallets_balances_frozens,:except => [:index]

  resources :balances_withdraws_statuses do
    member do
      get :withdraw_image
    end
  end
 
  resources :notifications,:except => [:new]
  
  resources :users_groups
  resources :email_templates

  resources :carts,:except => [:edit,:index]
  resources :company_balances
  resources :balances_withdraws
  resources :roles
  resources :cards
  
  resources :watchdogs
  resources :settings


  resources :tickets do
    member do
      get :ticket_image
    end
  end

  
  resources :users_banks
  
   resources :onlinebanks, only: [:show] 


  resources :customsdata
  resources :user_verifications
  resources :transactions
  



   
get 'test_limits' => 'userds#test_limits'

get "come_soon" => "credit_cards#come_soon"

get "getcardtype" => "credit_cards#getcardtype"

get "testniceselect" => "homedevise#testniceselect"
  
get 'userwallet_frozen' => "users_wallets_balances_frozens#userwallet_frozen_get"
  
get 'check_expenses' => "users_wallets_balances_frozens#check_expenses"

get 'payers_welcome' => "userds#payers_welcome"

get 'error_page' => "frontendpages#error_page"

get 'contact' => "frontendpages#contact"


post 'userds/password/forgot', to: 'userds#forgot'

post 'userds/password/reset', to: 'userds#reset'

get "backup_status" => "userds#backup_status"

get "resend_tfa" => "userds#resend_tfa"
post "resend_tfa" => "userds#resend_tfa"
patch "resend_tfa" => "userds#resend_tfa"
get "uniquetel" => "userds#uniquetel"
get "offline_avatar" => "onlinebanks#offline_avatar"
get "exception_ticket" => "onlinebanks#exception_ticket"

get "user_sms_check" => "userds#user_sms_check"
post "user_sms_check" => "userds#user_sms_check_post"

get "balance_with_draw_sms_check" => "balances_withdraws#balance_with_draw_sms_check"
post "balance_with_draw_sms_check" => "balances_withdraws#balance_with_draw_sms_check_post"

  
post "changeprofile" => "userds#changeprofile"
post "edituserprofile" => "user_verifications#edituserprofile"
get "withdraw_status_image" => "balances_withdraws_statuses#withdraw_status_image"


get "mailconfirm" => "user_verifications#mailconfirm" 

get "get_attachment/:id" => "attachments#get_attachment"
get "admin_attach/:id" => "admin_attachments#admin_attach"


get "verify_image" => "user_verifications#verify_image"

get "unlockconfirm" => "user_verifications#unlockconfirm"

get "verify_image" => "user_verifications#verify_image"
post "balances_withdraws/:id" => "balances_withdraws#update_hint"
  
get"searchpost" => "transactions#search"
post"searchpost" => "transactions#searchpost"

get "types" => "currencies_withdraw_ratios#types"
post "types" => "currencies_withdraw_ratios#types"
patch "types" => "currencies_withdraw_ratios#types"

get "currencies" => "currencies_withdraw_types#currencies"
post "currencies" => "currencies_withdraw_types#currencies"
patch "currencies" => "currencies_withdraw_types#currencies"


get "buycart" => "carts#buycart"
get "payment_method" => "carts#payment_method"

get "payment_method_ratios" => "carts#payment_method_ratios"


get "get_expenses" => "balances_withdraws#get_expenses"
get "get_previous" => "balances_withdraws#get_previous"

get "review_balance_withdraw" => "balances_withdraws#review_balance_withdraw"
post "review_balance_withdraw" => "balances_withdraws#review_balance_withdraw"

post "review_western_gram" => "balances_withdraws#review_western_gram"
get "review_western_gram" => "balances_withdraws#review_western_gram_get"
post "review_egyptian_mail" => "balances_withdraws#review_egyptian_mail"

get "sendsms" => "user_verifications#sendsms"
post "sendsms" => "user_verifications#sendsms_post"

get "sendvoicesms" => "user_verifications#sendvoicesms"
post "sendvoicesms" => "user_verifications#sendvoicesms_post"



get "help_center" => "tickets#help_center"

get "freshdesk" => "tickets#freshdesk"
post "freshdesk" => "tickets#freshdesk_post"

get "send2fasms" => "user_verifications#send2fasms"

get "resendsms" => "user_verifications#resendsms"
get "verifysms" => "user_verifications#verifysms"
get "veriffysms" => "user_verifications#veriffysms" 
get "resendsmsa2" => "user_verifications#resendsmsa2"

post "changetel" => "user_verifications#changetel"

get "user_wallets_transactions" => "transactions#user_wallets_transactions"

get "pre_verification" => "user_verifications#pre_verification"
get "all_verifications" => "user_verifications#all_verifications"

get "visa_activate" => "user_verifications#visa_activate"
get "address_activate" => "user_verifications#address_activate"
get "selfie_activate" => "user_verifications#selfie_activate"

get "exceptionmessage" => "balances_withdraws#exceptionmessage"
post "exceptionmessage" => "balances_withdraws#exceptionmessage"
patch "exceptionmessage" => "balances_withdraws#exceptionmessage"

get "balances_withdraws_methods" => "balances_withdraws#methods"
post "balances_withdraws_methods" => "balances_withdraws#methods"
patch "balances_withdraws_methods" => "balances_withdraws#methods"

post "searchpostbank" => "users_banks#searchpost"

get "users_banks_methods" => "users_banks#methods"
post "users_banks_methods" => "users_banks#methods"
patch "users_banks_methods" => "users_banks#methods"

get "users_banks_types" => "users_banks#types"
post "users_banks_types" => "users_banks#types"
patch "users_banks_types" => "users_banks#types"

get "users_banks_currencies" => "users_banks#currencies"
post "users_banks_currencies" => "users_banks#currencies"
patch "users_banks_currencies" => "users_banks#currencies"

post "add_bank_confirm" => "users_banks#add_bank_confirm"

get "review_egyptian_mail_post" => "users_banks#review_egyptian_mail"
post "review_egyptian_mail_post" => "users_banks#review_egyptian_mail_post"

get "review_new_bank" => "users_banks#new_review"

post "review_new_bank" => "users_banks#new_review_post"
patch "review_new_bank" => "users_banks#new_review_post"

get "users_banks_egyptian_mail" => "users_banks#egyptian_mail"
get "edit_egyptian_mail_bank" => "users_banks#edit_egyptian_mail_bank"
post "edit_egyptian_mail_bank" => "users_banks#edit_egyptian_mail_bank"
get "users_banks_western_gram" => "users_banks#western_gram"

get "users_banks/:id/edit_egyptian_mail" => "users_banks#edit_egyptian_mail", as: "edit_egyptian_mail"

get "users_banks/:id/edit_western_gram" => "users_banks#edit_western_gram", as: "edit_western_gram"
get "users_banks/:id/edit_bank" => "users_banks#edit_bank", as: "edit_bank"

post  'users_banks_save_western_gram' => 'users_banks#save_western_gram'
post "edit_western" => "users_banks#post_edit_western"
patch "edit_western" => "users_banks#post_edit_western"

post "users_banks_save_bank" => "users_banks#post_edit_western"

post "edit_egyptian_mail" => "users_banks#post_edit_egyptianmail"
patch "edit_egyptian_mail" => "users_banks#post_edit_egyptianmail"

post  'users_banks_save_egyptian_mail' => 'users_banks#save_egyptian_mail'

post  'users_bankssave' => 'users_banks#save'
get  'users_bankssave' => 'users_banks#getsave'

###########################################

post "id_activate" => "user_verifications#update"
patch "id_activate" => "user_verifications#update"

get 'homepages/home'

root 'userds#account_settings' 
get "welcome" => "welcome#welcome"
   
get "account_settings" => "userds#account_settings"
get "user_profile" => "userds#user_profile"
get "log_operations" => "userds#log_operations"
get "fail_signin" => "userds#fail_signin"
get "failed_operations" => "userds#failed_operations"
get "userstatistics" => "userds#userstatistics"
get "allusers" => "userds#index"

put "account_settings" => "userds#set_account_info"

get "useractive" => "userds#uactive"

get "change_locale" => "userds#change_locale"

get "sms_deactivate" => "user_verifications#sms_deactivate"
get "sms_activate" => "user_verifications#sms_activate"

post "sms_activate" => "user_verifications#sms_activate_post"

post "email_activate_post" => "user_verifications#email_activate_post"

get "active_otp_secret" => "user_verifications#active_otp_secret"

get "previousattachments" => "user_verifications#previous_attachments"

get "closeticket" => "tickets#closeticket"

get "listticket" => "tickets#index"

get "ticketreply" => "tickets#replay"
post "ticketreply" => "tickets#create"

get "ubankaccount" => "users_banks#useraccounts"

get "users_banks_main" => "users_banks#main_bank"

post "users_banks_main" => "users_banks#main_bank_post"

get "balances_withdraws_main" => "balances_withdraws#main"

get "egyptian_mail" => "balances_withdraws#egyptian_mail"

get "edit_bank_conversion" => "users_banks#edit_bank_conversion"

get "western_gram" => "balances_withdraws#western_gram"

post  'save_western_gram' => 'balances_withdraws#save_western_gram'

post  'save_egyptian_mail' => 'balances_withdraws#save_egyptian_mail'

get 'operations_details' => 'users_wallets_balances#operations_details'

post  'balancewithdrawsave' => 'balances_withdraws#save'
get 'userbankdids' => 'users_banks#for_balanceop'
get 'userwallets' => 'users_wallets_balances#walletselection'
get 'walletfrozen' => 'users_wallets_balances_frozens#walletfrozen'

get 'mailstatus' => 'users_wallets_balances_frozens#mailstatus'
get 'namestatus' => 'users_wallets_balances_frozens#namestatus'

post 'savetransfer' => 'users_wallets_balances_frozens#savetransfer'

get "approvedbalance" => "users_wallets_balances_frozens#approvedbalance"

get "refund_request" => "users_wallets_balances_frozens#refund_request"

get "frozenbalance" => "users_wallets_balances#frozenbalance"

get "deletewallet" => "users_wallets_balances_#deletewallet"

get "wallet_transfer" => "users_wallets_balances#get_transfer"

post "walletstransfer" => "users_wallets_balances#walletstransferpost"

get 'showtransfer/:id' , to: 'users_wallets_balances#showtransfer' , as: 'showtransfer'
get 'addbalance_show/:id' , to: 'carts#addbalance_show' , as: 'addbalance_show'
get 'edit_balance/:id' , to: 'carts#edit_balance' , as: 'edit_balance'

get "balancereports" => "users_wallets_balances#balancereports"
get "search" => "users_wallets_balances#search"
post "searchposts" => "users_wallets_balances#searchposts"

get "search_wallet_transfer" => "users_wallets_balances#search_wallet_transfer"

get "search_balance_transfer" => "users_wallets_balances#search_balance_transfer"
get "search_cart_charge" => "users_wallets_balances#search_cart_charge"

get "search_balance_withdraw" => "users_wallets_balances#search_balance_withdraw"

get "search1" => "users_wallets_balances#search1"
post "searchposts1" => "users_wallets_balances#searchposts1"
get "searchcart" => "users_wallets_balances#searchcart"
post "searchcartpost" => "users_wallets_balances#searchcartpost"
get "withdrawsearch" => "users_wallets_balances#withdrawsearch" 
post "withdrawsearchpost" => "users_wallets_balances#withdrawsearchpost"

get "dollar_wallet" => "users_wallets_balances#dollar_wallet"

get "egp_wallet" => "users_wallets_balances#egp_wallet"
get "sar_wallet" => "users_wallets_balances#sar_wallet"

get "check_limit" => "balances_withdraws#check_limit"

get "approvewithdraw" => "balances_withdraws#approvewithdraw"
get "disapprovewithdraw" => "balances_withdraws#disapprovewithdraw"
post "disapprovewithdraw" => "balances_withdraws#disappwithdraw"

get "canceloperation" => "balances_withdraws#canceloperation"
post "canceloperation" => "balances_withdraws#canceloperation"

get "changstatus" => "balances_withdraws#changstatus"
post "changstatus" => "balances_withdraws#changstatuspost"
patch "changstatus" => "balances_withdraws#changstatuspost"

get "cartbalance" => "carts#cartbalance"
post "cartbalance" => "carts#cartbalancetouser"
get "usercarts" => "userds#usercarts"
get 'cartgen/:id', to: 'carts#pcart', as: 'cartgen'
get "cart_preview" => "carts#cart_preview"

get "search" => "company_balances#search"
post "search" => "company_balances#searchpost"

get "notificationsearch" => "notifications#search"
post "notificationsearch" => "notifications#searchpost"

get "allread" => "notifications#allread"


get "ticketsearch" => "tickets#search"
post "ticketsearch" => "tickets#searchpost"

get "watchdogserarch" => "watchdogs#search"
post "watchdogserarch" => "watchdogs#searchpost"
get '/watchdogs', to: 'watchdogs#show'

get "usernotification" => "notifications#usernotification"

#online payment
get "paypal" => "onlinebanks#paypal"

post "paypal" => "onlinebanks#paypalpost"
get "paypalurl" => "onlinebanks#paypalurl"

get "paypalret" => "onlinebanks#paypalconfirm"

get "visa" => "onlinebanks#mastervisa"
post "visa" => "onlinebanks#mastervisapost"

get "online_image" => "onlinebanks#online_image"

get "coinmoney" => "onlinebanks#coinmoney"
post "coinmoney" => "onlinebanks#coinmoney_post"

post "call_coinmoney" => "onlinebanks#call_coinmoney"
post "paypal_ipn" => "onlinebanks#paypal_ipn"

post "584439346:AAGXxvOR3caHLUbJZPQQYwj5sNeP_yJSDl0" => "user_verifications#telegram_webhook"

#CPBL5HVNUI9AOYSETVXVBPGPLV

######################################################
get "perfectmoney" => "onlinebanks#perfectmoney"
post "perfectmoneyerror" => "onlinebanks#perfectmoneyerror"
post "perfectmoney" => "onlinebanks#perfectmoneypost"


post "perfectmoney_toapi" => "onlinebanks#perfectmoney_toapi"


#####################################################
get "webmoney" => "onlinebanks#webmoney"
post "webmoney" => "onlinebanks#webmoneypost"
get "webmoneydone" => "onlinebanks#webmoneydone"
get "webmoneyfail" => "onlinebanks#webmoneyfail"
get "success" => "onlinebanks#success"
get "fail" => "onlinebanks#fail"

get "onecard" => "onlinebanks#onecard"
post "onecard" => "onlinebanks#onecardpost"

get "webmoneyfail" => "onlinebanks#webmoneyfail"

#####################################################
post "egyptian_post" => "onlinebanks#egyptian_post"
post "banktransfer_post" => "onlinebanks#banktransfer_post"

##########################################################
get "uploadefile" => "onlinebanks#uploadefile"
post "uploadefile" => "onlinebanks#uploadefile"
get "user_reply" => "onlinebanks#user_reply"
post "user_reply" => "onlinebanks#user_reply"
get "cancel_payment" => "onlinebanks#cancel_payment"

post "/onlinebanks/:id" => "onlinebanks#show"
post "/hook" => "onlinebanks#hook"
 
match "/mesgall" => "userinboxtos#mesgall", via: [:get, :post]

###########################################################

#page has no route

if Rails.env.production?
 match '*a', :to => 'frontendpages#routing',via: [:get, :post]

end



end
