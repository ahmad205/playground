# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180626134700) do

  create_table "user_tests", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "balance",    limit: 255
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  # create_table "admin_attachments", force: :cascade do |t|
  #   t.string   "uuid",       limit: 255
  #   t.string   "file_ext",   limit: 255
  #   t.string   "file_name",  limit: 255
  #   t.binary   "content",    limit: 4294967295
  #   t.datetime "created_at",                    null: false
  #   t.datetime "updated_at",                    null: false
  # end

  # create_table "admingroups_roles", force: :cascade do |t|
  #   t.integer  "admins_group_id", limit: 4
  #   t.integer  "role_id",         limit: 4
  #   t.datetime "created_at",                null: false
  #   t.datetime "updated_at",                null: false
  # end

  # add_index "admingroups_roles", ["admins_group_id"], name: "index_admingroups_rules_on_admins_group_id", using: :btree
  # add_index "admingroups_roles", ["role_id"], name: "index_admingroups_rules_on_role_id", using: :btree

  # create_table "admins", force: :cascade do |t|
  #   t.string   "email",                  limit: 255, default: "", null: false
  #   t.string   "username",               limit: 255
  #   t.integer  "role_id",                limit: 4,   default: 1,  null: false
  #   t.string   "encrypted_password",     limit: 255, default: "", null: false
  #   t.string   "reset_password_token",   limit: 255
  #   t.datetime "reset_password_sent_at"
  #   t.datetime "remember_created_at"
  #   t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
  #   t.datetime "current_sign_in_at"
  #   t.datetime "last_sign_in_at"
  #   t.string   "current_sign_in_ip",     limit: 255
  #   t.string   "last_sign_in_ip",        limit: 255
  #   t.datetime "created_at",                                      null: false
  #   t.datetime "updated_at",                                      null: false
  #   t.string   "avatar_file_name",       limit: 255
  #   t.string   "avatar_content_type",    limit: 255
  #   t.integer  "avatar_file_size",       limit: 4
  #   t.datetime "avatar_updated_at"
  #   t.string   "admin_id",               limit: 255
  # end

  # add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  # add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  # create_table "admins_groups", force: :cascade do |t|
  #   t.string   "name",       limit: 255
  #   t.datetime "created_at",             null: false
  #   t.datetime "updated_at",             null: false
  # end

  # create_table "assignments", force: :cascade do |t|
  #   t.integer  "admin_id",   limit: 4
  #   t.integer  "role_id",    limit: 4
  #   t.datetime "created_at",           null: false
  #   t.datetime "updated_at",           null: false
  # end

  # create_table "attachments", force: :cascade do |t|
  #   t.string   "uuid",       limit: 255
  #   t.binary   "content",    limit: 4294967295
  #   t.datetime "created_at",                    null: false
  #   t.datetime "updated_at",                    null: false
  #   t.integer  "user_id",    limit: 4
  #   t.string   "file_ext",   limit: 255
  #   t.string   "file_name",  limit: 255
  # end

  # create_table "balances_withdraws", force: :cascade do |t|
  #   t.integer  "user_id",        limit: 4
  #   t.string   "status",         limit: 50,    default: "created", null: false
  #   t.string   "transaction_id", limit: 200,                       null: false
  #   t.integer  "user_bank_id",   limit: 4
  #   t.integer  "wallet_id",      limit: 4
  #   t.float    "thesum",         limit: 24,                        null: false
  #   t.float    "netsum",         limit: 24
  #   t.text     "hint",           limit: 65535
  #   t.datetime "created_at"
  #   t.datetime "updated_at",                                       null: false
  #   t.date     "expect_date"
  #   t.boolean  "approved",                     default: false
  # end

  # create_table "balances_withdraws_ratios", force: :cascade do |t|
  #   t.integer  "users_group_id",               limit: 4
  #   t.integer  "currencies_withdraw_ratio_id", limit: 4
  #   t.float    "ratio",                        limit: 24, default: 0.0, null: false
  #   t.float    "fees",                         limit: 24, default: 0.0, null: false
  #   t.datetime "created_at",                                            null: false
  #   t.datetime "updated_at",                                            null: false
  # end

  # create_table "balances_withdraws_statuses", force: :cascade do |t|
  #   t.integer  "balances_withdraws_id",    limit: 4
  #   t.string   "status",                   limit: 50,  default: "created"
  #   t.integer  "user_id",                  limit: 4
  #   t.integer  "user_bank_id",             limit: 4
  #   t.string   "hint",                     limit: 255
  #   t.integer  "attachments",              limit: 4
  #   t.string   "user_hint",                limit: 200
  #   t.datetime "created_at",                                               null: false
  #   t.datetime "updated_at",                                               null: false
  #   t.string   "attachments_file_name",    limit: 255
  #   t.string   "attachments_content_type", limit: 255
  #   t.integer  "attachments_file_size",    limit: 4
  #   t.datetime "attachments_updated_at"
  # end

  # create_table "bank_account_infos", force: :cascade do |t|
  #   t.integer  "user_id",      limit: 4
  #   t.string   "bank_account", limit: 255
  #   t.string   "bank_type",    limit: 255
  #   t.datetime "created_at",               null: false
  #   t.datetime "updated_at",               null: false
  # end

  # create_table "cards", force: :cascade do |t|
  #   t.integer  "value",      limit: 4
  #   t.string   "note",       limit: 255
  #   t.integer  "display",    limit: 4,   default: 0
  #   t.datetime "created_at",                         null: false
  #   t.datetime "updated_at",                         null: false
  # end

  # create_table "carts", force: :cascade do |t|
  #   t.integer  "onlinebank_id", limit: 4
  #   t.string   "prefix",        limit: 100,                   null: false
  #   t.string   "cart_number",   limit: 100,                   null: false
  #   t.float    "value",         limit: 24,                    null: false
  #   t.float    "real_value",    limit: 24,    default: 0.0,   null: false
  #   t.string   "currency",      limit: 100,   default: "USD", null: false
  #   t.date     "expire_date",                                 null: false
  #   t.integer  "user_id",       limit: 4
  #   t.text     "notes",         limit: 65535
  #   t.datetime "created_at",                                  null: false
  #   t.datetime "updated_at"
  #   t.integer  "status",        limit: 4,                     null: false
  # end

  # create_table "company_balances", force: :cascade do |t|
  #   t.integer  "user_id",      limit: 4
  #   t.integer  "operation_id", limit: 4
  #   t.integer  "trans_id",     limit: 4
  #   t.integer  "foreign_id",   limit: 4
  #   t.integer  "parent_id",    limit: 4
  #   t.text     "hint",         limit: 65535
  #   t.float    "balance",      limit: 24
  #   t.string   "currency",     limit: 50
  #   t.datetime "created_at"
  #   t.datetime "modified_at"
  #   t.boolean  "status"
  # end

  # create_table "company_banks", force: :cascade do |t|
  #   t.string   "bank_key",          limit: 200
  #   t.string   "bank_name",         limit: 250,               null: false
  #   t.string   "encryptkey",        limit: 100
  #   t.string   "currency",          limit: 20
  #   t.string   "country",           limit: 200
  #   t.string   "city",              limit: 200
  #   t.string   "branch",            limit: 200
  #   t.string   "branch_code",       limit: 200
  #   t.string   "phone",             limit: 50
  #   t.string   "name",              limit: 200
  #   t.string   "nationalid",        limit: 200
  #   t.string   "account_name",      limit: 200
  #   t.string   "account_email",     limit: 200
  #   t.string   "account_number",    limit: 100
  #   t.integer  "visa_cvv",          limit: 4
  #   t.string   "swiftcode",         limit: 100
  #   t.string   "ibancode",          limit: 100
  #   t.integer  "logo",              limit: 4
  #   t.date     "expire_date"
  #   t.datetime "created_at"
  #   t.float    "fees",              limit: 24,  default: 0.0, null: false
  #   t.float    "ratio",             limit: 24,  default: 0.0, null: false
  #   t.integer  "status",            limit: 4,   default: 0
  #   t.integer  "status_v",          limit: 4,   default: 0,   null: false
  #   t.string   "logo_file_name",    limit: 255
  #   t.string   "logo_content_type", limit: 255
  #   t.integer  "logo_file_size",    limit: 4
  #   t.datetime "logo_updated_at"
  #   t.string   "bank_category",     limit: 50
  #   t.string   "bank_subcategory",  limit: 50
  # end

  # create_table "credit_cards", force: :cascade do |t|
  #   t.integer  "user_id",             limit: 4
  #   t.string   "name",                limit: 255
  #   t.string   "card_no",             limit: 255
  #   t.string   "card_type",           limit: 255
  #   t.date     "expire_date"
  #   t.integer  "status",              limit: 4,   default: 2, null: false
  #   t.datetime "created_at",                                  null: false
  #   t.datetime "updated_at",                                  null: false
  #   t.string   "photo_file_name",     limit: 255
  #   t.string   "photo_content_type",  limit: 255
  #   t.integer  "photo_file_size",     limit: 4
  #   t.datetime "photo_updated_at"
  #   t.string   "selfie_file_name",    limit: 255
  #   t.string   "selfie_content_type", limit: 255
  #   t.integer  "selfie_file_size",    limit: 4
  #   t.datetime "selfie_updated_at"
  # end

  # create_table "currencies_withdraw_ratios", force: :cascade do |t|
  #   t.string   "name",             limit: 255
  #   t.integer  "currency_id",      limit: 4
  #   t.string   "hint",             limit: 255
  #   t.integer  "status",           limit: 4
  #   t.integer  "ima",              limit: 4
  #   t.datetime "created_at",                   null: false
  #   t.datetime "updated_at",                   null: false
  #   t.string   "ima_file_name",    limit: 255
  #   t.string   "ima_content_type", limit: 255
  #   t.integer  "ima_file_size",    limit: 4
  #   t.datetime "ima_updated_at"
  # end

  # create_table "currencies_withdraw_types", force: :cascade do |t|
  #   t.string   "name",              limit: 255
  #   t.string   "currency_code",     limit: 50
  #   t.string   "country_code",      limit: 50
  #   t.binary   "logo",              limit: 4294967295
  #   t.datetime "created_at",                           null: false
  #   t.datetime "updated_at",                           null: false
  #   t.string   "logo_file_name",    limit: 255
  #   t.string   "logo_content_type", limit: 255
  #   t.integer  "logo_file_size",    limit: 4
  #   t.datetime "logo_updated_at"
  # end

  create_table "dummy_models", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  # create_table "email_templates", force: :cascade do |t|
  #   t.string   "key",         limit: 255
  #   t.string   "greeting_en", limit: 255
  #   t.string   "greeting_ar", limit: 255
  #   t.string   "subject_en",  limit: 255
  #   t.string   "subject_ar",  limit: 255
  #   t.text     "en_content",  limit: 65535
  #   t.text     "ar_content",  limit: 65535
  #   t.string   "class_name",  limit: 255
  #   t.string   "action_name", limit: 255
  #   t.datetime "created_at",                null: false
  #   t.datetime "updated_at",                null: false
  # end

  # create_table "errors_logs", force: :cascade do |t|
  #   t.integer  "userd_id",   limit: 4
  #   t.text     "error_msg",  limit: 65535
  #   t.text     "error_code", limit: 65535
  #   t.datetime "created_at",               null: false
  #   t.datetime "updated_at",               null: false
  # end

  # create_table "frontendpages", force: :cascade do |t|
  #   t.string  "pagename",       limit: 150,   null: false
  #   t.string  "title",          limit: 350
  #   t.text    "pagecontent",    limit: 65535, null: false
  #   t.string  "pagename_en",    limit: 200
  #   t.string  "title_en",       limit: 200
  #   t.text    "pagecontent_en", limit: 65535
  #   t.boolean "status"
  # end

  # create_table "notifications", force: :cascade do |t|
  #   t.string   "user_id",      limit: 150
  #   t.integer  "trans_id",     limit: 4
  #   t.string   "addeduser_id", limit: 36
  #   t.string   "added_by",     limit: 11
  #   t.string   "foreign_id",   limit: 100
  #   t.string   "controller",   limit: 200
  #   t.text     "stats",        limit: 65535
  #   t.integer  "read",         limit: 4,     default: 0, null: false
  #   t.string   "notetype",     limit: 200
  #   t.datetime "created_at",                             null: false
  # end

  # create_table "onlinebanks", force: :cascade do |t|
  #   t.integer  "user_id",             limit: 4
  #   t.integer  "service_id",          limit: 4
  #   t.integer  "user_bank_id",        limit: 4
  #   t.integer  "company_bank_id",     limit: 4
  #   t.string   "transaction_id",      limit: 100
  #   t.string   "bank_name",           limit: 120
  #   t.string   "address",             limit: 255
  #   t.string   "status_url",          limit: 255
  #   t.string   "branch",              limit: 255
  #   t.string   "account_number",      limit: 255
  #   t.string   "national_id",         limit: 255
  #   t.string   "cardNumber",          limit: 100
  #   t.string   "cardCVV",             limit: 20
  #   t.string   "cardExpiryMonth",     limit: 10
  #   t.string   "cardExpiryYear",      limit: 10
  #   t.string   "responseCode",        limit: 100
  #   t.string   "responseMessage",     limit: 300
  #   t.string   "tansaction",          limit: 255
  #   t.string   "currency",            limit: 255
  #   t.float    "value",               limit: 24
  #   t.float    "netsum",              limit: 24,  default: 0.0, null: false
  #   t.string   "refund",              limit: 255
  #   t.boolean  "approved"
  #   t.string   "status",              limit: 120
  #   t.datetime "transfer_at"
  #   t.datetime "created_at",                                    null: false
  #   t.datetime "purchased_at"
  #   t.datetime "updated_at"
  #   t.string   "avatar_file_name",    limit: 255
  #   t.string   "avatar_content_type", limit: 255
  #   t.integer  "avatar_file_size",    limit: 4
  #   t.datetime "avatar_updated_at"
  #   t.integer  "attachments",         limit: 4
  # end

  # create_table "partners", force: :cascade do |t|
  #   t.string   "email",                  limit: 255, default: "",  null: false
  #   t.string   "encrypted_password",     limit: 255, default: "",  null: false
  #   t.string   "reset_password_token",   limit: 255
  #   t.datetime "reset_password_sent_at"
  #   t.datetime "remember_created_at"
  #   t.integer  "sign_in_count",          limit: 4,   default: 0,   null: false
  #   t.datetime "current_sign_in_at"
  #   t.datetime "last_sign_in_at"
  #   t.string   "current_sign_in_ip",     limit: 255
  #   t.string   "last_sign_in_ip",        limit: 255
  #   t.datetime "created_at",                                       null: false
  #   t.datetime "updated_at",                                       null: false
  #   t.string   "name",                   limit: 255
  #   t.float    "ratio",                  limit: 24,  default: 0.0, null: false
  #   t.integer  "sign_status",            limit: 4,   default: 1
  #   t.integer  "profit_status",          limit: 4,   default: 1
  #   t.integer  "profit_infinite",        limit: 4,   default: 0
  #   t.float    "withdraw_balance",       limit: 24,  default: 0.0
  #   t.float    "withdraw_limit",         limit: 24,  default: 0.0
  #   t.float    "usd_balance",            limit: 24,  default: 0.0
  #   t.float    "sar_balance",            limit: 24,  default: 0.0
  #   t.float    "egp_balance",            limit: 24,  default: 0.0
  #   t.string   "f_name",                 limit: 255
  #   t.string   "l_name",                 limit: 255
  #   t.string   "country",                limit: 255
  #   t.string   "city",                   limit: 255
  #   t.string   "first_address",          limit: 255
  #   t.string   "second_address",         limit: 255
  #   t.string   "postal_code",            limit: 255
  #   t.string   "tel",                    limit: 255
  #   t.string   "emergency_tel",          limit: 255
  #   t.string   "note",                   limit: 255
  # end

  # add_index "partners", ["email"], name: "index_partners_on_email", unique: true, using: :btree
  # add_index "partners", ["reset_password_token"], name: "index_partners_on_reset_password_token", unique: true, using: :btree

  # create_table "partners_reports", force: :cascade do |t|
  #   t.integer  "partner_id",         limit: 4
  #   t.integer  "company_balance_id", limit: 4
  #   t.integer  "current_ratio",      limit: 4
  #   t.float    "balance_before",     limit: 24
  #   t.float    "balance_after",      limit: 24
  #   t.string   "currency",           limit: 255
  #   t.string   "operation_type",     limit: 50,  default: "regular", null: false
  #   t.string   "details",            limit: 255
  #   t.datetime "created_at",                                         null: false
  #   t.datetime "updated_at",                                         null: false
  # end

  # create_table "partners_settings", force: :cascade do |t|
  #   t.text     "name",        limit: 65535
  #   t.text     "key",         limit: 65535
  #   t.text     "value",       limit: 65535
  #   t.text     "description", limit: 65535
  #   t.datetime "created_at",                null: false
  #   t.datetime "updated_at",                null: false
  # end

  # create_table "partners_withdraws", force: :cascade do |t|
  #   t.integer  "partner_id", limit: 4
  #   t.float    "value",      limit: 24,                null: false
  #   t.text     "details",    limit: 65535
  #   t.integer  "status",     limit: 4,     default: 2
  #   t.string   "currency",   limit: 255
  #   t.datetime "created_at",                           null: false
  #   t.datetime "updated_at",                           null: false
  # end

  # create_table "roles", force: :cascade do |t|
  #   t.string   "name",    limit: 200, null: false
  #   t.string   "caption", limit: 200, null: false
  #   t.datetime "created",             null: false
  #   t.integer  "status",  limit: 1,   null: false
  # end

  # create_table "sessions", force: :cascade do |t|
  #   t.string   "session_id", limit: 255,   null: false
  #   t.text     "data",       limit: 65535
  #   t.datetime "created_at"
  #   t.datetime "updated_at"
  # end

  # add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  # add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  # create_table "settings", force: :cascade do |t|
  #   t.text    "name",        limit: 65535
  #   t.text    "key",         limit: 65535
  #   t.text    "value",       limit: 65535
  #   t.text    "description", limit: 65535
  #   t.integer "parent_id",   limit: 4
  # end

  # create_table "tickets", force: :cascade do |t|
  #   t.integer  "foreign_id",              limit: 4
  #   t.integer  "ticket_number",           limit: 4,                            null: false
  #   t.integer  "userd_id",                limit: 4,     default: 0
  #   t.text     "title",                   limit: 65535
  #   t.text     "body",                    limit: 65535
  #   t.boolean  "approved"
  #   t.datetime "created_at"
  #   t.datetime "updated_at"
  #   t.string   "updated_reply",           limit: 200
  #   t.text     "hints",                   limit: 65535
  #   t.integer  "department_id",           limit: 4
  #   t.integer  "ticket_id",               limit: 4,     default: 0
  #   t.integer  "predefined_reply_id",     limit: 4
  #   t.string   "priority",                limit: 150
  #   t.string   "status",                  limit: 200,   default: "new ticket"
  #   t.boolean  "closed",                                default: false,        null: false
  #   t.string   "attachment_file_name",    limit: 255
  #   t.string   "attachment_content_type", limit: 255
  #   t.integer  "attachment_file_size",    limit: 4
  #   t.datetime "attachment_updated_at"
  #   t.integer  "reply_attachment",        limit: 4
  # end

  # create_table "transactions", force: :cascade do |t|
  #   t.integer  "foreign_id",            limit: 4
  #   t.string   "transaction_id",        limit: 200,                 null: false
  #   t.integer  "user_id",               limit: 4
  #   t.integer  "user_id_to",            limit: 4
  #   t.float    "value_from",            limit: 24,    default: 0.0, null: false
  #   t.string   "w_from_name",           limit: 200
  #   t.float    "w_from_balance_before", limit: 24,    default: 0.0, null: false
  #   t.float    "w_from_balance_after",  limit: 24,    default: 0.0, null: false
  #   t.float    "value_to",              limit: 24,    default: 0.0, null: false
  #   t.string   "w_to_name",             limit: 200
  #   t.float    "w_to_balance_before",   limit: 24,    default: 0.0, null: false
  #   t.float    "w_to_balance_after",    limit: 24,    default: 0.0, null: false
  #   t.float    "beforecharge",          limit: 24,    default: 0.0, null: false
  #   t.string   "controller",            limit: 200,                 null: false
  #   t.text     "description",           limit: 65535,               null: false
  #   t.string   "company_bank_data",     limit: 2000
  #   t.string   "user_bank_data",        limit: 200
  #   t.datetime "created_at",                                        null: false
  #   t.datetime "updated_at"
  #   t.integer  "ticket_opend",          limit: 4,     default: 0,   null: false
  # end

  # create_table "user_lists", force: :cascade do |t|
  #   t.integer  "user_id",     limit: 4
  #   t.string   "description", limit: 255
  #   t.datetime "created_at",              null: false
  #   t.datetime "updated_at",              null: false
  # end

  # create_table "user_notes", force: :cascade do |t|
  #   t.integer  "user_id",    limit: 4
  #   t.integer  "added_by",   limit: 4,   null: false
  #   t.string   "note",       limit: 255
  #   t.string   "role_id",    limit: 150
  #   t.integer  "important",  limit: 4,   null: false
  #   t.datetime "created_at",             null: false
  #   t.datetime "updated_at",             null: false
  # end

  

  # create_table "user_verifications", force: :cascade do |t|
  #   t.integer  "user_id",                         limit: 4
  #   t.integer  "nationalid_type",                 limit: 4
  #   t.string   "nationalid_no",                   limit: 255
  #   t.string   "nationalid_country",              limit: 255
  #   t.string   "nationalid_issued_by",            limit: 255
  #   t.date     "nationalid_start_date",                         default: '2018-01-01'
  #   t.date     "nationalid_expire_date",                        default: '2018-01-01'
  #   t.date     "nationalid_birth_date",                         default: '2018-01-01'
  #   t.string   "address_document_type",           limit: 200
  #   t.string   "address_line1",                   limit: 255
  #   t.string   "address_line2",                   limit: 255
  #   t.string   "address_document_city",           limit: 255
  #   t.string   "address_document_country",        limit: 255
  #   t.integer  "address_document_postal_code",    limit: 4
  #   t.integer  "credit_card_id",                  limit: 4
  #   t.integer  "nationalid_verified",             limit: 4,     default: 0
  #   t.integer  "nationalid_back_verified",        limit: 4,     default: 0,            null: false
  #   t.integer  "address_document_verified",       limit: 4,     default: 0
  #   t.integer  "credit_card_verified",            limit: 4,     default: 0
  #   t.integer  "selfie_verified",                 limit: 4,     default: 0
  #   t.text     "nationalid_refuse_messege",       limit: 65535
  #   t.text     "nationalid_back_refuse_messege",  limit: 65535
  #   t.text     "address_document_refuse_messege", limit: 65535
  #   t.text     "selfie_refuse_messege",           limit: 65535
  #   t.integer  "all_verified",                    limit: 4,     default: 0,            null: false
  #   t.datetime "created_at",                                                           null: false
  #   t.datetime "updated_at",                                                           null: false
  #   t.string   "nationalid_front_file_name",      limit: 255
  #   t.string   "nationalid_front_content_type",   limit: 255
  #   t.integer  "nationalid_front_file_size",      limit: 4
  #   t.datetime "nationalid_front_updated_at"
  #   t.string   "nationalid_back_file_name",       limit: 255
  #   t.string   "nationalid_back_content_type",    limit: 255
  #   t.integer  "nationalid_back_file_size",       limit: 4
  #   t.datetime "nationalid_back_updated_at"
  #   t.string   "address_document_file_name",      limit: 255
  #   t.string   "address_document_content_type",   limit: 255
  #   t.integer  "address_document_file_size",      limit: 4
  #   t.datetime "address_document_updated_at"
  #   t.string   "selfie_file_name",                limit: 255
  #   t.string   "selfie_content_type",             limit: 255
  #   t.integer  "selfie_file_size",                limit: 4
  #   t.datetime "selfie_updated_at"
  #   t.integer  "day_withdraw_count",              limit: 4,     default: 0,            null: false
  #   t.integer  "nationalid_front",                limit: 4
  #   t.integer  "nationalid_back",                 limit: 4
  #   t.integer  "address_document",                limit: 4
  #   t.integer  "selfie",                          limit: 4
  # end

  # add_index "user_verifications", ["user_id"], name: "index_user_verifications_on_user_id", unique: true, using: :btree

  # create_table "userds", force: :cascade do |t|
  #   t.string   "email",                         limit: 255, default: "",   null: false
  #   t.string   "f_name",                        limit: 255
  #   t.string   "l_name",                        limit: 255
  #   t.integer  "is_company",                    limit: 4,   default: 0
  #   t.string   "encrypted_password",            limit: 255, default: "",   null: false
  #   t.string   "reset_password_token",          limit: 255
  #   t.datetime "reset_password_sent_at"
  #   t.datetime "remember_created_at"
  #   t.datetime "password_changed_at"
  #   t.integer  "security_question_id",          limit: 4
  #   t.string   "security_question_answer",      limit: 255
  #   t.integer  "sign_in_count",                 limit: 4,   default: 0,    null: false
  #   t.datetime "current_sign_in_at"
  #   t.datetime "last_sign_in_at"
  #   t.string   "current_sign_in_ip",            limit: 255
  #   t.string   "last_sign_in_ip",               limit: 255
  #   t.integer  "failed_attempts",               limit: 4,   default: 0,    null: false
  #   t.string   "unlock_token",                  limit: 255
  #   t.string   "confirmation_token",            limit: 255
  #   t.datetime "confirmed_at"
  #   t.datetime "confirmation_sent_at"
  #   t.string   "unconfirmed_email",             limit: 255
  #   t.datetime "locked_at"
  #   t.datetime "created_at",                                               null: false
  #   t.datetime "updated_at",                                               null: false
  #   t.datetime "deleted_at"
  #   t.datetime "last_activity_at"
  #   t.datetime "expired_at"
  #   t.string   "country",                       limit: 100
  #   t.string   "nationality",                   limit: 200
  #   t.string   "city",                          limit: 200
  #   t.string   "postal_code",                   limit: 11
  #   t.string   "facebook_id",                   limit: 100
  #   t.string   "username",                      limit: 100
  #   t.string   "tel",                           limit: 20
  #   t.float    "current_family_limit",          limit: 24,  default: 0.0,  null: false
  #   t.float    "current_max_family_recieve",    limit: 24,  default: 0.0,  null: false
  #   t.integer  "role_id",                       limit: 4
  #   t.integer  "group_id",                      limit: 4
  #   t.string   "nationalid",                    limit: 200
  #   t.integer  "nationalid_type",               limit: 4
  #   t.string   "locale",                        limit: 200, default: "ar", null: false
  #   t.integer  "active",                        limit: 4,   default: 0
  #   t.string   "active_code",                   limit: 50
  #   t.string   "avatar_file_name",              limit: 255
  #   t.string   "avatar_content_type",           limit: 255
  #   t.integer  "avatar_file_size",              limit: 4
  #   t.datetime "avatar_updated_at"
  #   t.string   "attachments_file_name",         limit: 255
  #   t.string   "attachments_content_type",      limit: 255
  #   t.integer  "attachments_file_size",         limit: 4
  #   t.datetime "attachments_updated_at"
  #   t.string   "unique_session_id",             limit: 20
  #   t.string   "gauth_secret",                  limit: 255
  #   t.string   "gauth_token",                   limit: 255
  #   t.string   "gauth_enabled",                 limit: 255, default: "f"
  #   t.string   "gauth_tmp",                     limit: 255
  #   t.datetime "gauth_tmp_datetime"
  #   t.integer  "second_factor_attempts_count",  limit: 4,   default: 0
  #   t.string   "encrypted_otp_secret_key",      limit: 255
  #   t.string   "encrypted_otp_secret_key_iv",   limit: 255
  #   t.string   "encrypted_otp_secret_key_salt", limit: 255
  #   t.string   "otp_secret_key",                limit: 255
  #   t.integer  "active_otp_secret",             limit: 4,   default: 0,    null: false
  #   t.integer  "sms_active",                    limit: 4,   default: 0,    null: false
  #   t.integer  "hard_lock",                     limit: 4,   default: 0,    null: false
  #   t.string   "pin_id",                        limit: 255
  #   t.string   "backup_code",                   limit: 255
  #   t.string   "account_number",                limit: 255
  #   t.integer  "all_verified",                  limit: 4,   default: 0,    null: false
  #   t.string   "invitation_token",              limit: 255
  #   t.datetime "invitation_created_at"
  #   t.datetime "invitation_sent_at"
  #   t.datetime "invitation_accepted_at"
  #   t.integer  "invitation_limit",              limit: 4
  #   t.integer  "invited_by_id",                 limit: 4
  #   t.string   "invited_by_type",               limit: 255
  #   t.integer  "invitations_count",             limit: 4,   default: 0
  #   t.string   "time_zone",                     limit: 255
  #   t.integer  "test",                          limit: 4
  #   t.integer  "month_withdraw_count",          limit: 4
  #   t.float    "month_withdraw_value",          limit: 24
  #   t.integer  "day_withdraw_count",            limit: 4
  #   t.float    "day_withdraw_value",            limit: 24
  #   t.integer  "d_withdraw_count",              limit: 4,   default: 0
  #   t.float    "d_withdraw_value",              limit: 24,  default: 0.0
  #   t.integer  "m_withdraw_count",              limit: 4,   default: 0
  #   t.float    "m_withdraw_value",              limit: 24,  default: 0.0
  #   t.string   "telegram_code",                 limit: 255
  #   t.string   "telegram_id",                   limit: 255
  #   t.integer  "day_withdraw_counts",           limit: 4
  #   t.integer  "month_withdraw_counts",         limit: 4
  #   t.float    "day_withdraw_values",           limit: 24
  #   t.float    "month_withdraw_values",         limit: 24
  #   t.integer  "avatar",                        limit: 4
  #   t.string   "pin_voice_id",                  limit: 255
  # end

  # add_index "userds", ["email"], name: "index_userds_on_email", unique: true, using: :btree
  # add_index "userds", ["encrypted_otp_secret_key"], name: "index_userds_on_encrypted_otp_secret_key", unique: true, using: :btree
  # add_index "userds", ["invitation_token"], name: "index_userds_on_invitation_token", unique: true, using: :btree
  # add_index "userds", ["invitations_count"], name: "index_userds_on_invitations_count", using: :btree
  # add_index "userds", ["invited_by_id"], name: "index_userds_on_invited_by_id", using: :btree
  # add_index "userds", ["otp_secret_key"], name: "index_userds_on_otp_secret_key", unique: true, using: :btree
  # add_index "userds", ["reset_password_token"], name: "index_userds_on_reset_password_token", unique: true, using: :btree

  # create_table "usergroups_banks", force: :cascade do |t|
  #   t.integer  "users_group_id",  limit: 4
  #   t.integer  "company_bank_id", limit: 4
  #   t.integer  "status",          limit: 4
  #   t.datetime "created_at",                null: false
  #   t.datetime "updated_at",                null: false
  # end

  # create_table "users_banks", force: :cascade do |t|
  #   t.integer  "userd_id",                      limit: 4,                        null: false
  #   t.string   "bank_type",                     limit: 10,  default: "withdraw", null: false
  #   t.integer  "currencies_withdraw_ratios_id", limit: 4
  #   t.string   "personal_name",                 limit: 200
  #   t.string   "business_name",                 limit: 200
  #   t.string   "nationalid",                    limit: 50
  #   t.date     "birth_date"
  #   t.string   "relation",                      limit: 200
  #   t.string   "nickname",                      limit: 200
  #   t.string   "bank_name",                     limit: 200
  #   t.string   "country",                       limit: 200
  #   t.string   "country_code",                  limit: 50
  #   t.string   "branch",                        limit: 200
  #   t.string   "branchnumber",                  limit: 100
  #   t.string   "phone",                         limit: 50
  #   t.string   "from_mobile",                   limit: 50
  #   t.string   "to_mobile",                     limit: 50
  #   t.string   "account_name",                  limit: 200
  #   t.string   "account_email",                 limit: 200
  #   t.string   "account_number",                limit: 100
  #   t.integer  "visa_cvv",                      limit: 4
  #   t.string   "swiftcode",                     limit: 100
  #   t.string   "valid_swift_code",              limit: 11
  #   t.date     "expire_date"
  #   t.datetime "created_at"
  #   t.datetime "updated_at",                                                     null: false
  #   t.boolean  "status"
  #   t.string   "city",                          limit: 100
  #   t.string   "address",                       limit: 250
  #   t.integer  "postal_code",                   limit: 4
  #   t.string   "iban",                          limit: 200
  #   t.string   "bban",                          limit: 100
  #   t.integer  "checksum",                      limit: 4
  #   t.integer  "bank_code",                     limit: 4
  #   t.integer  "check_digit",                   limit: 4
  #   t.boolean  "sepa"
  #   t.string   "iban_validity",                 limit: 50
  #   t.string   "iban_checksum",                 limit: 50
  #   t.string   "iban_length",                   limit: 50
  #   t.string   "iban_structure",                limit: 50
  #   t.string   "account_checksum",              limit: 50
  #   t.string   "currency",                      limit: 50
  # end

  # create_table "users_groups", force: :cascade do |t|
  #   t.string  "name",                   limit: 200,                     null: false
  #   t.text    "description",            limit: 65535,                   null: false
  #   t.float   "family_limit",           limit: 24,    default: 0.0
  #   t.float   "max_family_recieve",     limit: 24,    default: 10000.0
  #   t.float   "minimum_value",          limit: 24,    default: 0.0,     null: false
  #   t.float   "minimum_withdraw",       limit: 24,                      null: false
  #   t.integer "month_withdraws_count",  limit: 4,                       null: false
  #   t.float   "month_max_withdraw",     limit: 24,                      null: false
  #   t.integer "day_withdraws_count",    limit: 4,                       null: false
  #   t.float   "day_max_withdraw",       limit: 24,                      null: false
  #   t.float   "max_withdraw_each_time", limit: 24,    default: 0.0,     null: false
  #   t.float   "add_balance_ratio",      limit: 24,                      null: false
  # end

  # create_table "users_wallets_balances", force: :cascade do |t|
  #   t.integer  "user_id",     limit: 4
  #   t.string   "currency",    limit: 255
  #   t.string   "wallet_name", limit: 255
  #   t.float    "balance",     limit: 24,  default: 0.0
  #   t.integer  "is_deleted",  limit: 4,   default: 0,   null: false
  #   t.datetime "created_at",                            null: false
  #   t.datetime "updated_at",                            null: false
  # end

  # create_table "users_wallets_balances_frozens", force: :cascade do |t|
  #   t.integer  "user_id",           limit: 4
  #   t.integer  "user_id_to",        limit: 4
  #   t.integer  "purpose_id",        limit: 4
  #   t.string   "currency",          limit: 255
  #   t.string   "wallet_name",       limit: 255
  #   t.float    "balance",           limit: 24
  #   t.float    "netbalance",        limit: 24,              null: false
  #   t.float    "ratio_company",     limit: 24
  #   t.string   "note",              limit: 255
  #   t.integer  "approve",           limit: 1,   default: 0, null: false
  #   t.integer  "deleted",           limit: 1,   default: 0, null: false
  #   t.integer  "Suspension_period", limit: 4
  #   t.datetime "created_at",                                null: false
  #   t.datetime "updated_at",                                null: false
  # end

  # create_table "users_wallets_frozens_purposes", force: :cascade do |t|
  #   t.string   "name",              limit: 255
  #   t.integer  "logo",              limit: 4
  #   t.string   "logo_file_name",    limit: 255
  #   t.string   "logo_content_type", limit: 255
  #   t.integer  "logo_file_size",    limit: 4
  #   t.datetime "logo_updated_at"
  #   t.datetime "created_at",                    null: false
  #   t.datetime "updated_at",                    null: false
  # end

  # create_table "users_wallets_frozens_ratios", force: :cascade do |t|
  #   t.integer  "users_group_id",               limit: 4
  #   t.integer  "user_wallet_purpose_ratio_id", limit: 4
  #   t.float    "ratio",                        limit: 24, default: 0.0, null: false
  #   t.float    "fees",                         limit: 24, default: 0.0, null: false
  #   t.datetime "created_at",                                            null: false
  #   t.datetime "updated_at",                                            null: false
  # end

  # create_table "watchdogs", force: :cascade do |t|
  #   t.integer  "user_id",      limit: 4,     default: 0
  #   t.integer  "user_to",      limit: 4,     default: 0
  #   t.string   "added_by",     limit: 255,   default: "0"
  #   t.integer  "foreign_id",   limit: 4,     default: 0
  #   t.string   "controller",   limit: 100
  #   t.string   "action_page",  limit: 150
  #   t.text     "details",      limit: 65535
  #   t.datetime "created_at"
  #   t.string   "device_name",  limit: 200
  #   t.integer  "ticket_opend", limit: 4,     default: 0
  # end

  # create_table "watchdogs_partners", force: :cascade do |t|
  #   t.integer  "partner_id",  limit: 4,     default: 0
  #   t.string   "added_by",    limit: 255,   default: "0"
  #   t.integer  "foreign_id",  limit: 4,     default: 0
  #   t.string   "controller",  limit: 100
  #   t.string   "action_page", limit: 150
  #   t.text     "details",     limit: 65535
  #   t.datetime "created_at"
  #   t.string   "device_name", limit: 200
  # end

  # create_table "withdraws_banks", force: :cascade do |t|
  #   t.integer  "user_id",                       limit: 4,   null: false
  #   t.integer  "bank_id",                       limit: 4,   null: false
  #   t.integer  "withdraw_id",                   limit: 4,   null: false
  #   t.integer  "currencies_withdraw_ratios_id", limit: 4
  #   t.string   "personal_name",                 limit: 255
  #   t.string   "business_name",                 limit: 255
  #   t.string   "nationalid",                    limit: 255
  #   t.date     "birth_date"
  #   t.string   "relation",                      limit: 255
  #   t.string   "nickname",                      limit: 255
  #   t.string   "bank_name",                     limit: 255
  #   t.string   "country",                       limit: 255
  #   t.string   "country_code",                  limit: 255
  #   t.string   "branch",                        limit: 255
  #   t.string   "branchnumber",                  limit: 255
  #   t.string   "phone",                         limit: 255
  #   t.string   "account_name",                  limit: 255
  #   t.string   "account_email",                 limit: 255
  #   t.string   "account_number",                limit: 255
  #   t.integer  "visa_cvv",                      limit: 4
  #   t.string   "swiftcode",                     limit: 255
  #   t.string   "valid_swift_code",              limit: 255
  #   t.date     "expire_date"
  #   t.boolean  "status"
  #   t.string   "city",                          limit: 255
  #   t.string   "address",                       limit: 255
  #   t.integer  "postal_code",                   limit: 4
  #   t.string   "iban",                          limit: 255
  #   t.string   "bban",                          limit: 255
  #   t.integer  "checksum",                      limit: 4
  #   t.integer  "bank_code",                     limit: 4
  #   t.integer  "check_digit",                   limit: 4
  #   t.boolean  "sepa"
  #   t.string   "iban_validity",                 limit: 255
  #   t.string   "iban_checksum",                 limit: 255
  #   t.string   "iban_length",                   limit: 255
  #   t.string   "iban_structure",                limit: 255
  #   t.string   "account_checksum",              limit: 255
  #   t.string   "currency",                      limit: 255
  #   t.datetime "created_at",                                null: false
  #   t.datetime "updated_at",                                null: false
  # end

end
