class CreateBankAccountInfos < ActiveRecord::Migration
  def change
    create_table :bank_account_infos do |t|
      t.integer :user_id
      t.string :bank_account
      t.string :bank_type

      t.timestamps null: false
    end
  end
end
