class AddUserIdToAttachments < ActiveRecord::Migration
  def change
    add_column :attachments, :user_id, :integer
    add_column :attachments, :file_ext, :string
    add_column :attachments, :file_name, :string


  end
end
