class AddAllToUserverification < ActiveRecord::Migration
  def change
    add_column :user_verifications, :nationalid_front, :binary
    add_column :user_verifications, :nationalid_back, :binary
    add_column :user_verifications, :address_document, :binary
    add_column :user_verifications, :selfie, :binary


  end
end
