class AddTelegramToUserds < ActiveRecord::Migration
  def change
    add_column :userds, :telegram_code, :string
    add_column :userds, :telegram_id, :string
  end
end
