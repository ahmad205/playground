class CreateTableAdminAttachments < ActiveRecord::Migration

  def change
    create_table :admin_attachments do |t|
      t.string :uuid
      t.string :file_ext
      t.string :file_name
      t.binary :content, limit: 2000.megabytes
      t.timestamps null: false
    end
  end

end
