class CreateUserTests < ActiveRecord::Migration
  def change
    create_table :user_tests do |t|
      t.string :name
      t.integer :balance

      t.timestamps null: false
    end
  end
end
