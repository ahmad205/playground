class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.string :uuid
      t.binary :content, limit: 2000.megabytes
      t.timestamps null: false
    end
  end
end
