require 'rails_helper'


# RSpec.describe UserTest, type: :model do
#   pending "add some examples to (or delete) #{__FILE__}"
# end

describe UserTest, '#transferbalance' do
 
  it 'returns one item, the balance of the sender and receiver  ' do
  # Setup
    @transferbalance = 100
    @sender = 'ahmad'
    @reciever = 'khaled'
    @balanceuser1 = UserTest.where(:name => @sender).first
    @balanceuser2 = UserTest.where(:name => @reciever ).first
    @transfer_function = UserTest.transferbalance(@sender,@reciever,@transferbalance)

  # Exercise  
   @senderbalance = @balanceuser1.balance - @transferbalance
   @receiverbalance = @balanceuser2.balance + @transferbalance
   
  # Verify
    expect(@transfer_function[0]).to eq @senderbalance
    expect(@transfer_function[1]).to eq @receiverbalance
 
  # Teardown is for now mostly handled by RSpec itself
  end
 
end

