require 'rails_helper'

RSpec.describe DummyModel, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end

describe DummyModel, '#favorite_gadget' do
 
  it 'returns one item, the favorite gadget of the agent ' do
  # Setup
    agent = DummyModel.create(name: 'James Bond')
    q = DummyModel.create(name: 'Q') 
 
  # Exercise    
    favorite_gadget = agent.name
   
  # Verify
    expect(favorite_gadget).not_to eq 'Walther PPK'
 
  # Teardown is for now mostly handled by RSpec itself
  end
 
end
