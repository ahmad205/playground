class CustomFailure < Devise::FailureApp
    def redirect_url
       new_userd_session_url(:subdomain => 'secure')
    end

    # You need to override respond to eliminate recall
    def respond
      if http_auth?
        # Notification.create(:user_id => @userd.id,:trans_id => 100,:controller => 'failure',:foreign_id => 18,:stats => ' عملية ناجحه ')
        
        http_auth
        
      else
        if(params[:userd])
          @userd = Userd.where(:email => params[:userd][:email]).first
      if (@userd)

        $ee=request.env['HTTP_USER_AGENT']
        @client = DeviceDetector.new($ee)                     
        @full =@client.device_name.to_s + '/' + @client.device_type + '/' + @client.os_name.to_s + '/' + @client.os_full_version.to_s + '/' + @client.name.to_s + '/' + @client.full_version.to_s
       @stats = 'failed signin'+'/'+ request.remote_ip + @full.to_s
        Watchdog.create(:added_by => @userd.id , :user_id => @userd.id,:controller => 'userds',:action_page => 'failed signin',:details=> request.remote_ip,:device_name=> @full)
        Notification.create(:user_id => @userd.id,:controller => 'failed signin',:stats => @stats)

        Emailcode.failed_signin(@userd , @stats).deliver
      end
    end

    store_location!
    flash[:alert] = i18n_message unless flash[:notice]

    redirect_to new_userd_session_path

   # redirect_to root_url

    
  end
    end
  end