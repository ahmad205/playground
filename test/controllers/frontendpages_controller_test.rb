require 'test_helper'

class FrontendpagesControllerTest < ActionController::TestCase
  setup do
    @frontendpage = frontendpages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:frontendpages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create frontendpage" do
    assert_difference('Frontendpage.count') do
      post :create, frontendpage: {  }
    end

    assert_redirected_to frontendpage_path(assigns(:frontendpage))
  end

  test "should show frontendpage" do
    get :show, id: @frontendpage
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @frontendpage
    assert_response :success
  end

  test "should update frontendpage" do
    patch :update, id: @frontendpage, frontendpage: {  }
    assert_redirected_to frontendpage_path(assigns(:frontendpage))
  end

  test "should destroy frontendpage" do
    assert_difference('Frontendpage.count', -1) do
      delete :destroy, id: @frontendpage
    end

    assert_redirected_to frontendpages_path
  end
end
