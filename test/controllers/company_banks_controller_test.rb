require 'test_helper'

class CompanyBanksControllerTest < ActionController::TestCase
  setup do
    @company_bank = company_banks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:company_banks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create company_bank" do
    assert_difference('CompanyBank.count') do
      post :create, company_bank: { account_email: @company_bank.account_email, account_name: @company_bank.account_name, account_number: @company_bank.account_number, bank_key: @company_bank.bank_key, bank_name: @company_bank.bank_name, branch: @company_bank.branch, country: @company_bank.country, expire_date: @company_bank.expire_date, phone: @company_bank.phone, swiftcode: @company_bank.swiftcode, visa_cvv: @company_bank.visa_cvv }
    end

    assert_redirected_to company_bank_path(assigns(:company_bank))
  end

  test "should show company_bank" do
    get :show, id: @company_bank
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @company_bank
    assert_response :success
  end

  test "should update company_bank" do
    patch :update, id: @company_bank, company_bank: { account_email: @company_bank.account_email, account_name: @company_bank.account_name, account_number: @company_bank.account_number, bank_key: @company_bank.bank_key, bank_name: @company_bank.bank_name, branch: @company_bank.branch, country: @company_bank.country, expire_date: @company_bank.expire_date, phone: @company_bank.phone, swiftcode: @company_bank.swiftcode, visa_cvv: @company_bank.visa_cvv }
    assert_redirected_to company_bank_path(assigns(:company_bank))
  end

  test "should destroy company_bank" do
    assert_difference('CompanyBank.count', -1) do
      delete :destroy, id: @company_bank
    end

    assert_redirected_to company_banks_path
  end
end
