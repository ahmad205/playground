require 'test_helper'

class CompanyBalancesControllerTest < ActionController::TestCase
  setup do
    @company_balance = company_balances(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:company_balances)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create company_balance" do
    assert_difference('CompanyBalance.count') do
      post :create, company_balance: { balance: @company_balance.balance, balance_operation_id: @company_balance.balance_operation_id, balance_withdraw_id: @company_balance.balance_withdraw_id, balances_operation_id: @company_balance.balances_operation_id, balances_withdraw_id: @company_balance.balances_withdraw_id, hint: @company_balance.hint, status: @company_balance.status, user_id: @company_balance.user_id }
    end

    assert_redirected_to company_balance_path(assigns(:company_balance))
  end

  test "should show company_balance" do
    get :show, id: @company_balance
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @company_balance
    assert_response :success
  end

  test "should update company_balance" do
    patch :update, id: @company_balance, company_balance: { balance: @company_balance.balance, balance_operation_id: @company_balance.balance_operation_id, balance_withdraw_id: @company_balance.balance_withdraw_id, balances_operation_id: @company_balance.balances_operation_id, balances_withdraw_id: @company_balance.balances_withdraw_id, hint: @company_balance.hint, status: @company_balance.status, user_id: @company_balance.user_id }
    assert_redirected_to company_balance_path(assigns(:company_balance))
  end

  test "should destroy company_balance" do
    assert_difference('CompanyBalance.count', -1) do
      delete :destroy, id: @company_balance
    end

    assert_redirected_to company_balances_path
  end
end
