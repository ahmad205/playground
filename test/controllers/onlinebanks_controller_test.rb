require 'test_helper'

class OnlinebanksControllerTest < ActionController::TestCase
  setup do
    @onlinebank = onlinebanks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:onlinebanks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create onlinebank" do
    assert_difference('Onlinebank.count') do
      post :create, onlinebank: { approved: @onlinebank.approved, created_at: @onlinebank.created_at, currency: @onlinebank.currency, refund: @onlinebank.refund, service_id: @onlinebank.service_id, tansaction: @onlinebank.tansaction, user_id: @onlinebank.user_id, value: @onlinebank.value }
    end

    assert_redirected_to onlinebank_path(assigns(:onlinebank))
  end

  test "should show onlinebank" do
    get :show, id: @onlinebank
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @onlinebank
    assert_response :success
  end

  test "should update onlinebank" do
    patch :update, id: @onlinebank, onlinebank: { approved: @onlinebank.approved, created_at: @onlinebank.created_at, currency: @onlinebank.currency, refund: @onlinebank.refund, service_id: @onlinebank.service_id, tansaction: @onlinebank.tansaction, user_id: @onlinebank.user_id, value: @onlinebank.value }
    assert_redirected_to onlinebank_path(assigns(:onlinebank))
  end

  test "should destroy onlinebank" do
    assert_difference('Onlinebank.count', -1) do
      delete :destroy, id: @onlinebank
    end

    assert_redirected_to onlinebanks_path
  end
end
