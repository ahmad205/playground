require 'test_helper'

class OrlesControllerTest < ActionController::TestCase
  setup do
    @orle = orles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:orles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create orle" do
    assert_difference('Orle.count') do
      post :create, orle: { user_id: @orle.user_id }
    end

    assert_redirected_to orle_path(assigns(:orle))
  end

  test "should show orle" do
    get :show, id: @orle
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @orle
    assert_response :success
  end

  test "should update orle" do
    patch :update, id: @orle, orle: { user_id: @orle.user_id }
    assert_redirected_to orle_path(assigns(:orle))
  end

  test "should destroy orle" do
    assert_difference('Orle.count', -1) do
      delete :destroy, id: @orle
    end

    assert_redirected_to orles_path
  end
end
