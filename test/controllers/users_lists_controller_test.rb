require 'test_helper'

class UsersListsControllerTest < ActionController::TestCase
  setup do
    @users_list = users_lists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users_lists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create users_list" do
    assert_difference('UsersList.count') do
      post :create, users_list: { description: @users_list.description, user_id: @users_list.user_id }
    end

    assert_redirected_to users_list_path(assigns(:users_list))
  end

  test "should show users_list" do
    get :show, id: @users_list
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @users_list
    assert_response :success
  end

  test "should update users_list" do
    patch :update, id: @users_list, users_list: { description: @users_list.description, user_id: @users_list.user_id }
    assert_redirected_to users_list_path(assigns(:users_list))
  end

  test "should destroy users_list" do
    assert_difference('UsersList.count', -1) do
      delete :destroy, id: @users_list
    end

    assert_redirected_to users_lists_path
  end
end
