require 'test_helper'

class BalancesWithdrawsControllerTest < ActionController::TestCase
  setup do
    @balances_withdraw = balances_withdraws(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:balances_withdraws)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create balances_withdraw" do
    assert_difference('BalancesWithdraw.count') do
      post :create, balances_withdraw: { bank_sum: @balances_withdraw.bank_sum, company_sum: @balances_withdraw.company_sum, expect_date: @balances_withdraw.expect_date, hint: @balances_withdraw.hint, netsum: @balances_withdraw.netsum, status: @balances_withdraw.status, thesum: @balances_withdraw.thesum, user_bank_id: @balances_withdraw.user_bank_id, user_id: @balances_withdraw.user_id, users_bank_id: @balances_withdraw.users_bank_id }
    end

    assert_redirected_to balances_withdraw_path(assigns(:balances_withdraw))
  end

  test "should show balances_withdraw" do
    get :show, id: @balances_withdraw
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @balances_withdraw
    assert_response :success
  end

  test "should update balances_withdraw" do
    patch :update, id: @balances_withdraw, balances_withdraw: { bank_sum: @balances_withdraw.bank_sum, company_sum: @balances_withdraw.company_sum, expect_date: @balances_withdraw.expect_date, hint: @balances_withdraw.hint, netsum: @balances_withdraw.netsum, status: @balances_withdraw.status, thesum: @balances_withdraw.thesum, user_bank_id: @balances_withdraw.user_bank_id, user_id: @balances_withdraw.user_id, users_bank_id: @balances_withdraw.users_bank_id }
    assert_redirected_to balances_withdraw_path(assigns(:balances_withdraw))
  end

  test "should destroy balances_withdraw" do
    assert_difference('BalancesWithdraw.count', -1) do
      delete :destroy, id: @balances_withdraw
    end

    assert_redirected_to balances_withdraws_path
  end
end
