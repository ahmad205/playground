require 'test_helper'

class UsersSearchesControllerTest < ActionController::TestCase
  setup do
    @users_search = users_searches(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users_searches)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create users_search" do
    assert_difference('UsersSearch.count') do
      post :create, users_search: { description: @users_search.description, user_id: @users_search.user_id }
    end

    assert_redirected_to users_search_path(assigns(:users_search))
  end

  test "should show users_search" do
    get :show, id: @users_search
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @users_search
    assert_response :success
  end

  test "should update users_search" do
    patch :update, id: @users_search, users_search: { description: @users_search.description, user_id: @users_search.user_id }
    assert_redirected_to users_search_path(assigns(:users_search))
  end

  test "should destroy users_search" do
    assert_difference('UsersSearch.count', -1) do
      delete :destroy, id: @users_search
    end

    assert_redirected_to users_searches_path
  end
end
