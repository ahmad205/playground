class TickettPreview  < ActionMailer::Preview
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.wallet.activate_deactivate.subject
  #
  def ticket_ar

    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'
    @ticket_number = 'رقم التذكرة'
    Tickett.ticket(@user,@ticket_number)

  end

  def ticket_en

    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'
    @ticket_number = 'ticket number'
    Tickett.ticket(@user,@ticket_number)

  end



  def ticket_reply_ar

    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'
    @ticket_number = 'رقم التذكرة'
    Tickett.ticket_reply(@user,@ticket_number)

  end

  def ticket_reply_en

    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'
    @ticket_number = 'ticket number'
    Tickett.ticket_reply(@user,@ticket_number)

  end


  

  def ticket_close_ar

    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'
    @ticket_number = 'رقم التذكرة'
    Tickett.ticket_close(@user,@ticket_number)

  end

  def ticket_close_en

    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'
    @ticket_number = 'ticket number'
    Tickett.ticket_close(@user,@ticket_number)

  end




 
 
end
 