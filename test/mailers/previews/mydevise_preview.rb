# class Mydevise < Devise::Mailer   

    class MydevisePreview  < ActionMailer::Preview



    # helper :application # gives access to all helpers defined within `application_helper`.
    # include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`
    # default template_path: 'devise/mailer' # to make sure that your mailer uses the devise views


    def reset_password_instructions_ar

        @user = Userd.new
        @user.username = 'اسم المستخدم'
        @user.email = 'ايميل المستخدم'



        Mydevise.reset_password_instructions(@user, {})
    end

        # mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'
        # message_params = {:from    => 'payerz@payerz.com',
        #                   :to      => userd['email'],
        #                   :subject => 'Welcome To Payerz',
        #                   :text    => "<a href='#{edit_userd_password_url(:reset_password_token => token)}'>Change My Password</a>"}
        # mg_client.send_message 'sandboxd398e72047544c559f4ba47aebd39f7a.mailgun.org', message_params


        # mandrill = Mandrill::API.new("#{MandrillConfig.api_key}")
        # mandrill.messages 'send-template',
        #         { 
        #           :template_name => 'Forgot Password', 
        #           :template_content => "",
        #           :message => {
        #             :subject => "Forgot Password",
        #             :from_email => "test@test123.com",
        #             :from_name => "Company Support",
        #             :to => [
        #               {
        #                 :email => record.email
        #               }
        #             ],
        #             :global_merge_vars => [
        #               {
        #                 :name => "FIRST_NAME",
        #                 :content => record.first_name
        #               },
        #               {
        #                 :name => "FORGOT_PASSWORD_URL",
        #                 :content => "<a href='#{edit_user_password_url(:reset_password_token => record.reset_password_token)}'>Change My Password</a>"
        #               }
        #             ]
        #           }
        #         }
        #   We need to call super because Devise doesn't think we have sent any mail 
          #super


      def reset_password_instructions_en

        @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'


        Mydevise.reset_password_instructions(@user, {})

      end

      def unlock_instructions_en

        @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'


        Mydevise.unlock_instructions(@user, {})

      end




    def unlock_instructions_ar

        @user = Userd.new
        @user.username = 'اسم المستخدم'
        @user.email = 'ايميل المستخدم'
        @user.locale = 'ar'



        Mydevise.unlock_instructions(@user, {})
    end



    def confirmation_instructions_en

        @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'


        Mydevise.confirmation_instructions(@user, {})

      end




    def confirmation_instructions_ar

        @user = Userd.new
        @user.username = 'اسم المستخدم'
        @user.email = 'ايميل المستخدم'
        @user.locale = 'ar'



        Mydevise.confirmation_instructions(@user, {})
    end



    def password_change_en

        @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'


        Mydevise.password_change(@user, {})

      end




    def password_change_ar

        @user = Userd.new
        @user.username = 'اسم المستخدم'
        @user.email = 'ايميل المستخدم'
        @user.locale = 'ar'



        Mydevise.password_change(@user, {})
    end




end
