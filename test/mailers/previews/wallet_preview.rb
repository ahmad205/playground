class WalletPreview   < ActionMailer::Preview

  
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.wallet.activate_deactivate.subject
  #


  # def deactivate
    
  #   @user = Userd.where(:id => 1).first
  #   @users_wallets_balances = UsersWalletsBalance.where(:id => 1).first

  #   Wallet.activate_deactivate(@user,@walletbalance,0)

  # end


  
  def activate_ar


    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'

    @walletname = 'اسم المحفظة'
    
    Wallet.activate(@user,@walletname)

  
  end


  def activate_en
    


    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'

    @walletname = 'wallet name'
    
    

    Wallet.activate(@user,@walletname)

  
  end




  def deactivate_ar


    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'

    @walletname = 'اسم المحفظة'
    
    Wallet.deactivate(@user,@walletname)

  
  end


  def deactivate_en
    


    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'

    @walletname = 'wallet name'
    
    

    Wallet.deactivate(@user,@walletname)

  
  end



   def deletewallet_en


    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'


    @walletname = 'wallet name'

    Wallet.deletewallet(@user,@walletname)

    
  end


  def deletewallet_ar

    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'
    @walletname = 'اسم المحفظة'



    Wallet.deletewallet(@user,@walletname)

    
  end




  def balancetransfer_en

    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'
    @value = 'value'
    @netvalue = 'netvalue'
    Wallet.balancetransfer(@user,@value,'FromCurrency','ToCurrency',@netvalue)

  end


  def balancetransfer_ar
    
    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'
    @value = 'القيمة'
    @netvalue = 'القيمة النهائية'
    Wallet.balancetransfer(@user,@value,'العملة المحول منها','العملة المحول اليها',@netvalue)

  end


end
