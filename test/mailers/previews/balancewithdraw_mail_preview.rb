class BalancewithdrawMailPreview < ActionMailer::Preview
  # default from: "Payers <no-reply@payers.net>"
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.balancewithdraw_mail.withdraw.subject
  #
  def withdraw_en

    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'
   
    @b = BalancesWithdraw.new
    @b.thesum = 0
    @b.transaction_id = 'transaction id'

    BalancewithdrawMail.withdraw(@user,@b,"currency","create")
  end


  def withdraw_ar

    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'
   
    @b = BalancesWithdraw.new
    @b.thesum = 0
    @b.transaction_id = 'رقم السحب'

    BalancewithdrawMail.withdraw(@user,@b,"العملة","create")
  end


end
