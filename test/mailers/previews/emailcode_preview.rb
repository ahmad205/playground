# Preview all emails at http://localhost:3000/rails/mailers/emailcode
class EmailcodePreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/emailcode/sendcode
  def sendcode_en
 
    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'

    @otp_code = 'Code'
    Emailcode.sendcode(@user,@otp_code)
  end


  def sendcode_ar
 
    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'
    
    @otp_code = 'الكود'
    Emailcode.sendcode(@user,@otp_code)
  end


  def backupcode_en
 
    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'

    @otp_code = 'BackupCode'
    Emailcode.backupcode(@user,@otp_code)
  end


  def backupcode_ar
 
    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'
    
    @otp_code = 'رمز الاستعادة الاحتياطي'
    Emailcode.backupcode(@user,@otp_code)
  end



  def failed_signin_en
 
    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'

    @otp_code = 'failed signin/41.69.218.114/desktop/Windows/10/Chrome/63.0.3239.132'
    Emailcode.failed_signin(@user,@otp_code)
  end


  def failed_signin_ar
 
    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'
    
    @otp_code = 'failed signin/41.69.218.114/desktop/Windows/10/Chrome/63.0.3239.132'
    Emailcode.failed_signin(@user,@otp_code)
  end
  


end
