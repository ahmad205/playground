class ChargebalancePreview  < ActionMailer::Preview

 # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.chargebalance.charge.subject
  #
  
  def payforcart_ar

    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'
    @currentbalance='الرصيد الحالي'

    @cartvalue=Cart.new
    @cartvalue.value=0
    @cartvalue.expire_date =Time.now

    
    @cartvalue.prefix = 'Fm+zGx4c5LQLOXNRKmH0iA=='
    @cartvalue.cart_number = 'kMCQLdjo4bge+KnaFnv3FA=='

    Chargebalance.payforcart(@user,@cartvalue,@currentbalance)

  end


  def payforcart_en

    @user = Userd.new
    @user.username = 'user name'
    @user.email = 'user email'
    @user.locale = 'en'
    @currentbalance='current balance'

    @cartvalue=Cart.new
    @cartvalue.value=0
    @cartvalue.expire_date =Time.now

    
    @cartvalue.prefix = 'Fm+zGx4c5LQLOXNRKmH0iA=='
    @cartvalue.cart_number = 'kMCQLdjo4bge+KnaFnv3FA=='

    Chargebalance.payforcart(@user,@cartvalue,@currentbalance)

  end



def charge_ar
  
  @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'
    @currentbalance='الرصيد الحالي'

    @cartvalue=Cart.new
    @cartvalue.value=0
    @cartvalue.expire_date =Time.now

    
    @cartvalue.prefix = 'Fm+zGx4c5LQLOXNRKmH0iA=='
    @cartvalue.cart_number = 'kMCQLdjo4bge+KnaFnv3FA=='

    Chargebalance.charge(@user,@cartvalue,@currentbalance)
    
  end


  def charge_en

    @user = Userd.new
    @user.username = 'user name'
    @user.email = 'user email'
    @user.locale = 'en'
    @currentbalance='current balance'

    @cartvalue=Cart.new
    @cartvalue.value=0
    @cartvalue.expire_date =Time.now

    
    @cartvalue.prefix = 'Fm+zGx4c5LQLOXNRKmH0iA=='
    @cartvalue.cart_number = 'kMCQLdjo4bge+KnaFnv3FA=='

    Chargebalance.charge(@user,@cartvalue,@currentbalance)

  end


end