class AddUserbankMailerPreview < ActionMailer::Preview
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.add_userbank_mailer.userbank.subject
  #
  

  def userbank_ar

    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'
    @bankname = 'اسم البنك'
    AddUserbankMailer.userbank(@user, @bankname)
    
  end


  def userbank_en

    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'
    @bankname = 'bankname'
    AddUserbankMailer.userbank(@user, @bankname)
    
  end


end
