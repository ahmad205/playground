# Preview all emails at http://localhost:3000/rails/mailers/usertouser
class UsertouserPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/usertouser/transfer
  def transfer_en
   
    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'

    @userto = Userd.new
    @userto.username = 'test_user_to'

    
    @users_wallets_balances_frozen = UsersWalletsBalancesFrozen.new
    @users_wallets_balances_frozen.balance = 0.00
    @users_wallets_balances_frozen.currency = 'Currency'

    Usertouser.transfer(@user,@userto,@users_wallets_balances_frozen)
  end


  def transfer_ar
   
    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'

    @userto = Userd.new
    @userto.username = 'المرسل اليه'

    
    @users_wallets_balances_frozen = UsersWalletsBalancesFrozen.new
    @users_wallets_balances_frozen.balance = 0.00
    @users_wallets_balances_frozen.currency = 'العملة'

    Usertouser.transfer(@user,@userto,@users_wallets_balances_frozen)
  end



  def transferto_en
   
    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'

    @userto = Userd.new
    @userto.username = 'test_user_to'

    
    @users_wallets_balances_frozen = UsersWalletsBalancesFrozen.new
    @users_wallets_balances_frozen.balance = 0.00
    @users_wallets_balances_frozen.currency = 'Currency'

    Usertouser.transferto(@user,@userto,@users_wallets_balances_frozen)
  end


  def transferto_ar
   
    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'

    @userto = Userd.new
    @userto.username = 'المرسل اليه'

    
    @users_wallets_balances_frozen = UsersWalletsBalancesFrozen.new
    @users_wallets_balances_frozen.balance = 0.00
    @users_wallets_balances_frozen.currency = 'العملة'

    Usertouser.transferto(@user,@userto,@users_wallets_balances_frozen)
  end




  def confirmtransfer_en
   

    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'

    @userto = Userd.new
    @userto.username = 'test_user_to'

    
    @users_wallets_balances_frozen = UsersWalletsBalancesFrozen.new
    @users_wallets_balances_frozen.balance = 0.00
    @users_wallets_balances_frozen.currency = 'Currency'

    @status = 'status'

    Usertouser.confirmtransfer(@user,@userto,@users_wallets_balances_frozen,@status)
  end


  def confirmtransfer_ar
   
    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'

    @userto = Userd.new
    @userto.username = 'المرسل اليه'

    @status = 'حالة التحويل'

    @users_wallets_balances_frozen = UsersWalletsBalancesFrozen.new
    @users_wallets_balances_frozen.balance = 0.00
    @users_wallets_balances_frozen.currency = 'العملة'

    Usertouser.confirmtransfer(@user,@userto,@users_wallets_balances_frozen,@status)
  end




  def confirmtransfertransferto_en
   

    @user = Userd.new
    @user.username = 'test_user'
    @user.email = 'test_email'
    @user.locale = 'en'

    @userto = Userd.new
    @userto.username = 'test_user_to'

    
    @users_wallets_balances_frozen = UsersWalletsBalancesFrozen.new
    @users_wallets_balances_frozen.balance = 0.00
    @users_wallets_balances_frozen.currency = 'Currency'

    @status = 'status'

    Usertouser.confirmtransfertransferto(@user,@userto,@users_wallets_balances_frozen,@status)
  end


  def confirmtransfertransferto_ar
   
    @user = Userd.new
    @user.username = 'اسم المستخدم'
    @user.email = 'ايميل المستخدم'
    @user.locale = 'ar'

    @userto = Userd.new
    @userto.username = 'المرسل اليه'

    @status = 'حالة التحويل'

    @users_wallets_balances_frozen = UsersWalletsBalancesFrozen.new
    @users_wallets_balances_frozen.balance = 0.00
    @users_wallets_balances_frozen.currency = 'العملة'

    Usertouser.confirmtransfertransferto(@user,@userto,@users_wallets_balances_frozen,@status)
  end








  

end
