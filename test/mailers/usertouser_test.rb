require 'test_helper'

class UsertouserTest < ActionMailer::TestCase
  test "transfer" do
    mail = Usertouser.transfer
    assert_equal "Transfer", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
