require 'test_helper'

class EmailcodeTest < ActionMailer::TestCase
  test "sendcode" do
    mail = Emailcode.sendcode
    assert_equal "Sendcode", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
