-- phpMyAdmin SQL Dump
-- version 4.0.10.20
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 04, 2018 at 06:33 AM
-- Server version: 5.1.73
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hightservice_development`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` int(11) NOT NULL DEFAULT '1',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `avatar_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_file_size` int(11) DEFAULT NULL,
  `avatar_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_admins_on_email` (`email`),
  UNIQUE KEY `index_admins_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `email`, `username`, `role_id`, `encrypted_password`, `reset_password_token`, `reset_password_sent_at`, `remember_created_at`, `sign_in_count`, `current_sign_in_at`, `last_sign_in_at`, `current_sign_in_ip`, `last_sign_in_ip`, `created_at`, `updated_at`, `avatar_file_name`, `avatar_content_type`, `avatar_file_size`, `avatar_updated_at`) VALUES
(1, 'boshkash1993@gmail.com', NULL, 1, '$2a$11$bVOkDmHrqd0SQnGR/MDx0uogQr9JFxySQ4Jf7H2IFMu.yAohlxv0C', NULL, NULL, NULL, 18, '2018-04-04 08:40:23', '2018-04-03 14:59:27', '41.44.46.196', '197.45.219.97', '2018-04-02 09:59:25', '2018-04-04 08:40:23', NULL, NULL, NULL, NULL),
(2, 'khalidkhalil1993@outlook.com', NULL, 1, '$2a$11$GWWd6mjTCQM8YW6U8G0Oo.qWxV6PCE3oeLt68mD0byv8.c0vGNXzC', NULL, NULL, NULL, 12, '2018-04-04 10:05:32', '2018-04-04 09:52:17', '196.221.41.202', '196.221.41.202', '2018-04-02 13:15:28', '2018-04-04 10:05:32', NULL, NULL, NULL, NULL),
(3, 'smsm4web@yahoo.com', NULL, 1, '$2a$11$yMWCElmzcMGMZq5Kf48pQe9l7/ciApFmNEnr.yg1cMrUTRda72lE6', NULL, NULL, NULL, 2, '2018-04-02 13:18:58', '2018-04-02 13:18:01', '41.44.46.196', '41.44.46.196', '2018-04-02 13:18:01', '2018-04-02 13:18:58', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `balances_withdraws`
--

CREATE TABLE IF NOT EXISTS `balances_withdraws` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'created',
  `transaction_id` varchar(200) NOT NULL,
  `user_bank_id` int(11) DEFAULT NULL,
  `wallet_id` int(11) DEFAULT NULL,
  `thesum` float NOT NULL,
  `netsum` float DEFAULT NULL,
  `hint` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expect_date` date DEFAULT NULL,
  `approved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `balances_withdraws`
--

INSERT INTO `balances_withdraws` (`id`, `user_id`, `status`, `transaction_id`, `user_bank_id`, `wallet_id`, `thesum`, `netsum`, `hint`, `created_at`, `updated_at`, `expect_date`, `approved`) VALUES
(1, 3, 'created', 'WBT84XRQ7WASJL6ZBV', 1, 6, 100, 90, NULL, '2018-03-30 14:34:02', '2018-03-30 14:34:02', '2018-04-05', 0),
(2, 3, 'created', 'WBCXJUAKWE8094YVLZ', 1, 6, 200, 90, NULL, '2018-03-30 15:19:36', '2018-03-30 15:19:36', '2018-04-05', 0),
(3, 3, 'refunded', 'WBJIC06A84DTEX2HYW', 1, 6, 20, 9, NULL, '2018-03-30 17:17:12', '2018-03-30 18:04:00', '2018-04-05', 0),
(4, 3, 'rejected', 'WBD2UK37OQM5XSR9L0', 1, 6, 20, 9, NULL, '2018-03-30 17:26:29', '2018-03-30 17:40:43', '2018-04-05', 0),
(5, 3, 'completed', 'WBP3XH10Q46AEWRVSL', 1, 6, 20, 9, NULL, '2018-03-30 17:32:35', '2018-03-30 17:39:37', '2018-04-05', 0),
(6, 3, 'canceled', 'WBQEW0P7UZ8VYK5FJ1', 1, 6, 20, 9, NULL, '2018-03-30 17:33:44', '2018-03-30 17:34:20', '2018-04-05', 0),
(7, 3, 'exception', 'WBBOVHLFQM7C9X4YIG', 1, 6, 20, 9, NULL, '2018-03-30 19:28:30', '2018-03-30 19:34:12', '2018-04-05', 0),
(8, 3, 'created', 'WB2PMX0CLAT3GD4OQE', 1, 6, 22, 10.9, NULL, '2018-03-30 20:04:01', '2018-03-30 20:04:01', '2018-04-05', 0),
(9, 3, 'completed', 'WBDL492NY61KTJSIPE', 1, 6, 200, 180, NULL, '2018-03-31 11:55:02', '2018-03-31 12:04:53', '2018-04-06', 0),
(10, 3, 'created', 'WBP0ARUVJGYHD2C98X', 1, 6, 23, 11.85, NULL, '2018-03-31 12:04:23', '2018-03-31 12:04:23', '2018-04-06', 0),
(11, 3, 'canceled', 'WB82XO1MIP4YTV6UK7', 1, 6, 30, 18.5, NULL, '2018-03-31 13:57:12', '2018-03-31 13:57:18', '2018-04-06', 0),
(12, 3, 'exception', 'WBSJM5X0AN1GFOPDWH', 1, 6, 22, 10.9, NULL, '2018-04-01 16:05:37', '2018-04-01 16:08:47', '2018-04-07', 0),
(13, 5, 'created', 'WBH28T0VXPSZNO9MJG', 4, 11, 93, 68.35, NULL, '2018-04-02 19:54:09', '2018-04-02 19:54:09', '2018-04-08', 0);

-- --------------------------------------------------------

--
-- Table structure for table `balances_withdraws_ratios`
--

CREATE TABLE IF NOT EXISTS `balances_withdraws_ratios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_group_id` int(11) DEFAULT NULL,
  `currencies_withdraw_ratio_id` int(11) DEFAULT NULL,
  `ratio` float NOT NULL DEFAULT '0',
  `fees` float NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `balances_withdraws_ratios`
--

INSERT INTO `balances_withdraws_ratios` (`id`, `users_group_id`, `currencies_withdraw_ratio_id`, `ratio`, `fees`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 2, 1, '2017-12-30 10:53:08', '2018-01-30 13:59:27'),
(2, 1, 5, 2, 3, '2017-12-30 12:22:28', '2017-12-30 12:22:28'),
(3, 1, 6, 1, 2, '2017-12-30 13:33:38', '2017-12-30 13:33:38'),
(4, 1, 12, 6, 4, '2017-12-30 13:35:02', '2017-12-30 13:35:02'),
(5, 1, 13, 6, 3, '2017-12-30 13:35:40', '2017-12-30 13:35:40'),
(6, 1, 14, 2, 1, '2017-12-30 13:35:56', '2017-12-30 13:35:56'),
(7, 1, 15, 5, 1, '2017-12-30 13:41:07', '2017-12-30 13:41:07'),
(8, 5, 5, 2, 2, '2017-12-31 11:53:57', '2017-12-31 11:53:57'),
(9, 5, 6, 5, 20, '2017-12-31 11:54:13', '2017-12-31 11:54:13'),
(10, 5, 12, 2, 2, '2017-12-31 11:54:34', '2017-12-31 11:54:34'),
(11, 5, 13, 2, 2, '2017-12-31 11:54:52', '2017-12-31 11:54:52'),
(12, 5, 14, 5, 20, '2017-12-31 11:55:04', '2017-12-31 11:55:04'),
(13, 5, 4, 5, 10, '2017-12-31 18:15:03', '2017-12-31 18:15:03'),
(14, 5, 15, 2, 2, '2017-12-31 18:15:36', '2017-12-31 18:15:36'),
(15, 2, 4, 1, 1, '2018-01-06 09:55:29', '2018-01-06 09:55:29'),
(16, 2, 5, 1, 1, '2018-01-06 09:56:00', '2018-01-06 09:56:00'),
(17, 2, 6, 1, 1, '2018-01-06 09:56:38', '2018-01-06 09:56:38'),
(18, 2, 12, 1, 1, '2018-01-06 09:56:50', '2018-01-06 09:56:50'),
(19, 2, 13, 0, 0, '2018-01-06 09:57:27', '2018-01-06 09:57:27'),
(20, 2, 14, 0, 0, '2018-01-06 09:57:43', '2018-01-06 09:57:43'),
(21, 2, 15, 0, 0, '2018-01-06 09:58:26', '2018-01-06 09:58:26'),
(22, 3, 4, 0, 0, '2018-01-06 10:02:34', '2018-01-06 10:02:34'),
(23, 3, 5, 0, 0, '2018-01-06 10:02:51', '2018-01-06 10:02:51'),
(24, 3, 6, 0, 0, '2018-01-06 10:03:09', '2018-01-06 10:03:09'),
(25, 3, 12, 0, 0, '2018-01-06 10:03:29', '2018-01-06 10:03:29'),
(26, 3, 13, 0, 0, '2018-01-06 10:03:49', '2018-01-06 10:03:49'),
(27, 3, 14, 0, 0, '2018-01-06 10:04:23', '2018-01-06 10:04:23'),
(28, 3, 15, 0, 0, '2018-01-06 10:05:31', '2018-01-06 10:05:31'),
(29, 4, 4, 5, 100, '2018-01-06 10:06:41', '2018-03-27 18:12:03'),
(30, 4, 5, 0, 0, '2018-01-06 10:07:00', '2018-01-06 10:07:00'),
(31, 4, 6, 0, 0, '2018-01-06 10:07:18', '2018-01-06 10:07:18'),
(32, 4, 12, 0, 0, '2018-01-06 10:07:48', '2018-01-06 10:07:48'),
(33, 4, 13, 0, 0, '2018-01-06 10:08:08', '2018-01-06 10:08:08'),
(34, 4, 14, 0, 0, '2018-01-06 10:08:27', '2018-01-06 10:08:27'),
(35, 4, 15, 0, 0, '2018-01-06 10:09:19', '2018-01-06 10:09:19'),
(50, 8, 4, 5, 100, '2018-01-07 18:02:58', '2018-03-29 19:26:37'),
(51, 8, 5, 0, 0, '2018-01-07 18:02:58', '2018-01-07 18:02:58'),
(52, 8, 6, 0, 0, '2018-01-07 18:02:58', '2018-01-07 18:02:58'),
(53, 8, 12, 0, 0, '2018-01-07 18:02:58', '2018-01-07 18:02:58'),
(54, 8, 13, 0, 0, '2018-01-07 18:02:58', '2018-01-07 18:02:58'),
(55, 8, 14, 0, 0, '2018-01-07 18:02:58', '2018-01-07 18:02:58'),
(56, 8, 15, 0, 0, '2018-01-07 18:02:58', '2018-01-07 18:02:58');

-- --------------------------------------------------------

--
-- Table structure for table `balances_withdraws_statuses`
--

CREATE TABLE IF NOT EXISTS `balances_withdraws_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `balances_withdraws_id` int(11) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'created' COMMENT '0:cancel,1 :create , 2:deliver ,3:work On ,4:exception ,5:done ,6:rejected ,7:refunded'';',
  `user_id` int(11) DEFAULT NULL,
  `user_bank_id` int(11) DEFAULT NULL,
  `hint` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `user_hint` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `attachments_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachments_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachments_file_size` int(11) DEFAULT NULL,
  `attachments_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `balances_withdraws_statuses`
--

INSERT INTO `balances_withdraws_statuses` (`id`, `balances_withdraws_id`, `status`, `user_id`, `user_bank_id`, `hint`, `user_hint`, `created_at`, `updated_at`, `attachments_file_name`, `attachments_content_type`, `attachments_file_size`, `attachments_updated_at`) VALUES
(1, 1, 'created', 3, 1, NULL, NULL, '2018-03-30 10:34:02', '2018-03-30 10:34:02', NULL, NULL, NULL, NULL),
(2, 2, 'created', 3, 1, NULL, NULL, '2018-03-30 11:19:36', '2018-03-30 11:19:36', NULL, NULL, NULL, NULL),
(3, 3, 'created', 3, 1, NULL, NULL, '2018-03-30 13:17:12', '2018-03-30 13:17:12', NULL, NULL, NULL, NULL),
(4, 4, 'created', 3, 1, NULL, NULL, '2018-03-30 13:26:29', '2018-03-30 13:26:29', NULL, NULL, NULL, NULL),
(5, 5, 'created', 3, 1, NULL, NULL, '2018-03-30 13:32:35', '2018-03-30 13:32:35', NULL, NULL, NULL, NULL),
(6, 6, 'created', 3, 1, NULL, NULL, '2018-03-30 13:33:44', '2018-03-30 13:33:44', NULL, NULL, NULL, NULL),
(7, 6, 'canceled', 3, 1, NULL, NULL, '2018-03-30 13:34:20', '2018-03-30 13:34:20', NULL, NULL, NULL, NULL),
(8, 5, 'completed', 3, 1, 'تم اكتمال الطلب', NULL, '2018-03-30 13:39:37', '2018-03-30 13:39:37', NULL, NULL, NULL, NULL),
(9, 4, 'rejected', 3, 1, 'تم رفض  الطلب', NULL, '2018-03-30 13:40:11', '2018-03-30 13:40:11', NULL, NULL, NULL, NULL),
(11, 3, 'refunded', 3, 1, 'تم خصم 3دولار مصاريف البنك', NULL, '2018-03-30 14:04:00', '2018-03-30 14:04:00', NULL, NULL, NULL, NULL),
(12, 7, 'created', 3, 1, NULL, NULL, '2018-03-30 15:28:30', '2018-03-30 15:28:30', NULL, NULL, NULL, NULL),
(13, 7, 'recieved', 3, 1, '', NULL, '2018-03-30 15:31:39', '2018-03-30 15:31:39', NULL, NULL, NULL, NULL),
(14, 7, 'workon', 3, 1, '', NULL, '2018-03-30 15:32:48', '2018-03-30 15:32:48', NULL, NULL, NULL, NULL),
(15, 7, 'exception', 3, 1, '', NULL, '2018-03-30 15:34:12', '2018-03-30 15:34:12', NULL, NULL, NULL, NULL),
(16, 8, 'created', 3, 1, NULL, NULL, '2018-03-30 16:04:02', '2018-03-30 16:04:02', NULL, NULL, NULL, NULL),
(17, 9, 'created', 3, 1, NULL, NULL, '2018-03-31 07:55:03', '2018-03-31 07:55:03', NULL, NULL, NULL, NULL),
(18, 10, 'created', 3, 1, NULL, NULL, '2018-03-31 08:04:23', '2018-03-31 08:04:23', NULL, NULL, NULL, NULL),
(19, 9, 'completed', 3, 1, '', NULL, '2018-03-31 08:04:53', '2018-03-31 08:04:53', NULL, NULL, NULL, NULL),
(20, 11, 'created', 3, 1, NULL, NULL, '2018-03-31 09:57:12', '2018-03-31 09:57:12', NULL, NULL, NULL, NULL),
(21, 11, 'canceled', 3, 1, NULL, NULL, '2018-03-31 09:57:18', '2018-03-31 09:57:18', NULL, NULL, NULL, NULL),
(22, 12, 'created', 3, 1, NULL, NULL, '2018-04-01 12:05:37', '2018-04-01 12:05:37', NULL, NULL, NULL, NULL),
(23, 12, 'exception', 3, 1, 'استثنااااااااااااااااااااااااااااااااااااااااااء', NULL, '2018-04-01 12:08:47', '2018-04-01 12:08:47', 'Penguins.jpg', 'image/jpeg', 777835, '2018-04-01 12:08:47'),
(24, 13, 'created', 5, 4, NULL, NULL, '2018-04-02 15:54:09', '2018-04-02 15:54:09', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE IF NOT EXISTS `carts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `onlinebank_id` int(11) DEFAULT NULL,
  `prefix` varchar(100) NOT NULL,
  `cart_number` varchar(100) NOT NULL,
  `value` float NOT NULL,
  `real_value` float NOT NULL DEFAULT '0',
  `currency` varchar(100) NOT NULL DEFAULT 'USD',
  `expire_date` date NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `notes` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(1) NOT NULL COMMENT '0-->charged , 1-->notcharged ,2-->disabled , 3-->refunded',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `onlinebank_id`, `prefix`, `cart_number`, `value`, `real_value`, `currency`, `expire_date`, `user_id`, `notes`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 'cGC7glHvDtPBtPieZcY8rQ==\n', 'EUFv39kYpRLIMMnVC4NWug==\n', 450, 500, 'USD', '2019-03-31', 3, '', '2018-03-31 17:20:43', '2018-04-01 17:31:15', 0),
(2, 2, 'lBpOY82EG83QyePAaLbM0g==\n', '5nXKtrX+ettvncsLC3OHtA==\n', 450, 500, 'USD', '2019-03-31', 3, '', '2018-03-31 17:30:30', '2018-03-31 17:48:45', 0),
(3, 3, 'ooAf4RFbYOKgU/7oH9Uj0Q==\n', 'Vu0U98O1BeF1ZG9JJHxKGw==\n', 90, 100, 'USD', '2019-04-01', 3, '', '2018-04-01 18:05:38', '2018-04-03 15:41:37', 3),
(4, NULL, 'iBiVAcGv9h0WhgadA/N0LA==\n', '0ye2ys8eTXtzyrclBd9OjA==\n', 85, 85, 'USD', '2019-04-03', NULL, '412', '2018-04-03 17:42:40', '2018-04-03 17:42:40', 1),
(5, NULL, '3cY4L8Lg0BaWxkelGRUbfg==\n', '2teELV3T6XnlLIEcqwsQoA==\n', 81, 81, 'USD', '2019-04-03', NULL, '415', '2018-04-03 17:43:26', '2018-04-03 17:43:26', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_balances`
--

CREATE TABLE IF NOT EXISTS `company_balances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `operation_id` int(11) DEFAULT NULL COMMENT '1--> buy card  2--> Currency exchange   3 --> transfer 4-->withdraw 5---> partner balance with draw',
  `trans_id` int(11) DEFAULT NULL,
  `foreign_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `hint` text,
  `balance` float DEFAULT NULL,
  `currency` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT NULL COMMENT '1--> add 0--> subtract',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `company_balances`
--

INSERT INTO `company_balances` (`id`, `user_id`, `operation_id`, `trans_id`, `foreign_id`, `parent_id`, `hint`, `balance`, `currency`, `created_at`, `modified_at`, `status`) VALUES
(1, 21, 2, 41, NULL, NULL, NULL, 1.59, 'USD', '2018-04-01 10:08:24', '2018-04-01 10:08:24', 1),
(2, 9, 2, 42, NULL, NULL, NULL, 1.65, 'SAR', '2018-04-01 11:49:39', '2018-04-01 11:49:39', 1),
(3, 3, 4, 43, NULL, NULL, NULL, 11.1, 'USD', '2018-04-01 12:05:37', '2018-04-01 12:05:37', 1),
(4, 1, 2, 45, NULL, NULL, NULL, 11, 'USD', '2018-04-01 13:21:09', '2018-04-01 13:21:10', 1),
(5, 1, 2, 46, NULL, NULL, NULL, 53, 'USD', '2018-04-01 13:21:29', '2018-04-01 13:21:29', 1),
(6, 3, 1, 47, 3, NULL, NULL, 10, 'USD', '2018-04-01 14:05:38', '2018-04-01 14:05:38', 1),
(7, 1, 3, 49, 9, NULL, NULL, 7.9636, 'USD', '2018-04-02 09:49:33', '2018-04-02 09:49:33', 1),
(8, 5, 4, 50, NULL, NULL, NULL, 24.65, 'USD', '2018-04-02 15:54:09', '2018-04-02 15:54:09', 1),
(9, 3, 1, 54, 3, 6, NULL, 10, 'USD', '2018-04-03 09:48:58', '2018-04-03 09:48:58', 0);

-- --------------------------------------------------------

--
-- Table structure for table `company_banks`
--

CREATE TABLE IF NOT EXISTS `company_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_key` varchar(200) DEFAULT NULL,
  `bank_name` varchar(250) NOT NULL,
  `encryptkey` varchar(100) DEFAULT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `branch` varchar(200) DEFAULT NULL,
  `branch_code` varchar(200) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `nationalid` varchar(200) DEFAULT NULL,
  `account_name` varchar(200) DEFAULT NULL,
  `account_email` varchar(200) DEFAULT NULL,
  `account_number` varchar(100) DEFAULT NULL,
  `visa_cvv` int(11) DEFAULT NULL,
  `swiftcode` varchar(100) DEFAULT NULL,
  `ibancode` varchar(100) DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `fees` float NOT NULL DEFAULT '0',
  `ratio` float NOT NULL DEFAULT '0',
  `status` int(1) DEFAULT '0',
  `status_v` int(11) NOT NULL DEFAULT '0' COMMENT 'status for unverified users',
  `logo_file_name` varchar(255) DEFAULT NULL,
  `logo_content_type` varchar(255) DEFAULT NULL,
  `logo_file_size` int(11) DEFAULT NULL,
  `logo_updated_at` datetime DEFAULT NULL,
  `bank_category` varchar(50) DEFAULT NULL COMMENT 'online/offline/crypto',
  `bank_subcategory` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `company_banks`
--

INSERT INTO `company_banks` (`id`, `bank_key`, `bank_name`, `encryptkey`, `currency`, `country`, `city`, `branch`, `branch_code`, `phone`, `name`, `nationalid`, `account_name`, `account_email`, `account_number`, `visa_cvv`, `swiftcode`, `ibancode`, `expire_date`, `created_at`, `fees`, `ratio`, `status`, `status_v`, `logo_file_name`, `logo_content_type`, `logo_file_size`, `logo_updated_at`, `bank_category`, `bank_subcategory`) VALUES
(1, 'paypal', 'pay pal', 'AFcWxV21C7fd0v3bYYYRCpSSRl31AsLUQgwU72EveJenKmhZ76QSaobd', 'USD', 'Egypt', NULL, '', NULL, '', '', '', 'Pay Pal', 'payments@servicehigh.net', '', NULL, '', NULL, NULL, '2015-12-05 14:42:18', 5, 3, 1, 1, 'paypal.png', 'image/png', 3097, '2017-12-30 08:16:25', 'online', ''),
(2, 'cashu', 'cash u', 'servicehigh2015', 'usd', 'Egypt', NULL, '', NULL, '', '', '', 'servicehigh1', '', '', NULL, NULL, NULL, NULL, '2015-12-06 16:10:54', 10, 2, 0, 0, NULL, NULL, NULL, NULL, 'online', NULL),
(3, 'perfectmoney', 'perfect money', '', 'USD', 'Egypt', NULL, '', NULL, '', '', '', 'U10796535', '', '', NULL, NULL, NULL, NULL, '2015-12-06 16:11:20', 8, 10, 1, 1, 'pm.png', 'image/png', 5032, '2017-12-28 13:18:39', 'online', NULL),
(4, 'webmoney', 'web money', '', 'USD', 'Egypt', NULL, '', NULL, '', '', '', '', '', '', NULL, NULL, NULL, NULL, '2015-12-06 16:24:47', 3, 2, 1, 0, 'webmoney-logo.png', 'image/png', 5593, '2017-12-28 13:18:45', 'online', NULL),
(5, 'mastervisa', 'master OR visa card ', NULL, NULL, 'Egypt', NULL, '', NULL, '', '', '', '', '', '', NULL, NULL, NULL, '2015-12-06', '2015-12-06 16:25:35', 5, 2, 0, 0, NULL, NULL, NULL, NULL, 'online', NULL),
(6, 'onecard', 'one card', NULL, NULL, 'Egypt', NULL, '', NULL, '', '', '', 'ServiceHigh@one.com', '', '', NULL, NULL, NULL, '2015-12-06', '2015-12-06 16:27:05', 5, 2, 0, 0, NULL, NULL, NULL, NULL, 'online', NULL),
(7, 'AlAhliBank', 'البنك الاهلي', '', 'EGP', NULL, NULL, 'cairo', '123', NULL, NULL, NULL, 'البنك الاهلي', NULL, '123456', NULL, NULL, NULL, NULL, '2017-12-28 15:56:42', 5, 2, 1, 0, 'nbe.png', 'image/png', 4184, '2017-12-31 14:49:45', 'offline', 'localeg/bank'),
(8, 'EgyptPost', 'Egypt Post', '', 'EGP', '', NULL, 'damietta', NULL, '', 'SH', '111111', 'Egyptian post', NULL, '123456', NULL, NULL, NULL, NULL, '2017-12-28 16:05:32', 5, 2, 1, 0, 'Logo_egypt_post.png', 'image/png', 14568, '2017-12-31 14:52:50', 'offline', 'localeg/post'),
(9, 'BTC', 'Bitcoin', '', 'USD', '', NULL, '', NULL, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2017-12-28 15:31:01', 3, 2, 1, 1, 'bitcoin.png', 'image/png', 5236, '2017-12-31 14:51:16', 'crypto', NULL),
(10, 'LiteCoin', 'LiteCoin', '', 'USD', '', NULL, '', NULL, '', '', '', 'LiteCoin', NULL, '', NULL, NULL, NULL, NULL, '2017-12-28 15:54:21', 5, 2, 1, 0, 'litecoin.png', 'image/png', 7280, '2017-12-31 14:51:52', 'crypto', NULL),
(11, 'dash', 'dash', '', 'USD', '', NULL, '', NULL, '', '', '', 'dash', NULL, '', NULL, NULL, NULL, NULL, '2017-12-31 16:16:34', 0, 0, 1, 0, 'dash_logo.png', 'image/png', 3074, '2017-12-31 11:16:34', 'crypto', NULL),
(12, 'DGB', 'DGB', '', 'USD', '', NULL, '', NULL, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2017-12-31 16:27:38', 0, 0, 1, 0, 'DigiByte.png', 'image/png', 4644, '2017-12-31 13:31:37', 'crypto', NULL),
(13, 'ETH', 'ETH', '', 'USD', '', NULL, '', NULL, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2017-12-31 16:40:47', 0, 0, 1, 0, 'ethereum.png', 'image/png', 1795, '2017-12-31 13:01:22', 'crypto', NULL),
(14, 'ETC', 'ETC', '', 'USD', '', NULL, '', NULL, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2017-12-31 17:53:28', 0, 0, 1, 0, 'ethereum-classic.png', 'image/png', 3024, '2017-12-31 14:24:42', 'crypto', NULL),
(15, 'TetherUSD', 'TetherUSD', '', 'USD', '', NULL, '', NULL, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2017-12-31 17:14:03', 0, 0, 1, 0, 'TetherUSD.png', 'image/png', 7752, '2017-12-31 12:14:03', 'crypto', NULL),
(16, 'XMR', 'XMR', '', 'USD', '', NULL, '', NULL, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2017-12-31 17:21:22', 0, 0, 1, 0, 'XMR.png', 'image/png', 4279, '2017-12-31 12:21:22', 'crypto', NULL),
(17, 'XVG', 'XVG', '', 'USD', '', NULL, '', NULL, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2017-12-31 17:34:06', 0, 0, 1, 0, 'verge_logo.png', 'image/png', 5021, '2017-12-31 12:34:06', 'crypto', NULL),
(18, 'ZEC', 'ZEC', '', 'USD', '', NULL, '', NULL, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2017-12-31 17:36:36', 0, 0, 1, 0, 'zcash.png', 'image/png', 4162, '2017-12-31 12:36:36', 'crypto', NULL),
(19, 'XEM', 'XEM', '', 'USD', '', NULL, '', NULL, '', '', '', '', '', '', NULL, '', NULL, NULL, '2017-12-31 17:45:29', 0, 0, 1, 0, 'nem2.png', 'image/png', 3778, '2017-12-31 12:46:53', 'online', ''),
(20, 'Ripple', 'Ripple', '', 'USD', '', NULL, '', NULL, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2017-12-31 17:10:53', 0, 0, 1, 0, 'ripple.png', 'image/png', 5149, '2017-12-31 14:22:45', 'crypto', NULL),
(21, 'BCH', 'BCH', '', 'USD', '', NULL, '', NULL, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2017-12-31 20:00:04', 0, 0, 1, 0, 'png.jpg', 'image/jpeg', 3886, '2017-12-31 15:00:04', 'crypto', NULL),
(22, 'AlRajhiBank', 'Al Rajhi Bank', NULL, 'SAR', NULL, NULL, NULL, NULL, NULL, 'ahmed', NULL, NULL, NULL, NULL, NULL, NULL, 'SA0380000000608010167519', NULL, '2018-02-22 12:57:40', 2, 3, 1, 1, NULL, NULL, NULL, NULL, 'offline', 'localsar'),
(23, 'ABUDHABIISLAMICBANK', 'ABU DHABI ISLAMIC BANK - EGYPT', NULL, 'USD', 'Egypt', NULL, 'GARDEN CITY', '12345', NULL, NULL, NULL, 'service high', NULL, '123456', NULL, 'ABDIEGCA', NULL, NULL, '2018-02-22 19:54:14', 3, 1, 1, 1, NULL, NULL, NULL, NULL, 'offline', 'interusd/swift'),
(24, 'AHLI UNITED BANK', 'AHLI UNITED BANK (EGYPT) S.A.E.', '', 'USD', 'Egypt', NULL, 'HELIOPOLIS BRANCH', '032', '', NULL, NULL, 'ahmed', '', '123', NULL, 'DEIBEGCX032', NULL, NULL, '2018-02-22 19:54:14', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'online', 'localeg/bank'),
(25, 'western', 'western union', '', 'USD', 'USA', 'washington', '', NULL, '01010101010', 'western account', NULL, '', '', '', NULL, '', NULL, NULL, '2018-02-22 19:58:04', 2, 6, 1, 1, NULL, NULL, NULL, NULL, 'online', 'localeg/bank'),
(26, 'money gram', 'money gram', '', 'USD', 'britain', 'london', '', NULL, '01111111111', 'money account', NULL, '', '', '', NULL, '', NULL, NULL, '2018-02-22 20:00:58', 4, 2, 1, 1, NULL, NULL, NULL, NULL, 'online', 'localeg/bank'),
(27, 'vodafone cash', 'vodafone cash', '54646', 'EGP', 'ث4ف', NULL, 'غت', NULL, '01020304050', NULL, NULL, '54', '43654', '4563', 545, 'لاى', NULL, '2018-01-17', '2018-02-24 19:21:12', 3, 1, 1, 1, '2018-03-26_12h59_41.png', 'image/png', 8611, '2018-03-26 13:12:02', 'online', 'localeg/bank'),
(31, '', '', '', '', '', NULL, '', NULL, '', NULL, NULL, '', '', '', NULL, '', NULL, NULL, '2018-04-04 14:00:18', 0, 0, 0, 0, NULL, NULL, NULL, NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_number` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_ids` varchar(150) NOT NULL,
  `service_ids` varchar(150) DEFAULT NULL,
  `value` float NOT NULL,
  `expiration_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `credit_cards`
--

CREATE TABLE IF NOT EXISTS `credit_cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `card_no` varchar(255) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '2' COMMENT '2 -> pending  1->approved 0-> not approved',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `photo_file_name` varchar(255) DEFAULT NULL,
  `photo_content_type` varchar(255) DEFAULT NULL,
  `photo_file_size` int(11) DEFAULT NULL,
  `photo_updated_at` datetime DEFAULT NULL,
  `selfie_file_name` varchar(255) DEFAULT NULL,
  `selfie_content_type` varchar(255) DEFAULT NULL,
  `selfie_file_size` int(11) DEFAULT NULL,
  `selfie_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `credit_cards`
--

INSERT INTO `credit_cards` (`id`, `user_id`, `name`, `card_no`, `card_type`, `expire_date`, `status`, `created_at`, `updated_at`, `photo_file_name`, `photo_content_type`, `photo_file_size`, `photo_updated_at`, `selfie_file_name`, `selfie_content_type`, `selfie_file_size`, `selfie_updated_at`) VALUES
(1, 3, 'amira', '123', 'American Express', '2022-05-01', 2, '2018-04-03 11:55:00', '2018-04-03 11:55:00', '17553419_1749947761963372_7859631055446362296_n.jpg', 'image/jpeg', 29413, '2018-04-03 11:55:00', 'Penguins.jpg', 'image/jpeg', 777835, '2018-04-03 11:55:00'),
(4, 21, 'khalidkhalil', '6011111111111117', 'American Express', '2018-05-01', 2, '2018-04-04 09:27:13', '2018-04-04 09:27:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 21, 'khalidkhalil', '5610591081018250', 'maestro', '2018-04-01', 2, '2018-04-04 10:09:09', '2018-04-04 10:09:09', 'hires_ok.png', 'image/png', 9753, '2018-04-04 10:09:09', 'green.png', 'image/png', 11244, '2018-04-04 10:09:09');

-- --------------------------------------------------------

--
-- Table structure for table `currencies_withdraw_ratios`
--

CREATE TABLE IF NOT EXISTS `currencies_withdraw_ratios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `hint` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ima_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ima_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ima_file_size` int(11) DEFAULT NULL,
  `ima_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `currencies_withdraw_ratios`
--

INSERT INTO `currencies_withdraw_ratios` (`id`, `name`, `currency_id`, `hint`, `status`, `created_at`, `updated_at`, `ima_file_name`, `ima_content_type`, `ima_file_size`, `ima_updated_at`) VALUES
(4, 'Western Union', 1, '', 1, '2017-07-24 11:57:37', '2017-10-30 08:33:50', 'logo2.jpg', 'image/jpeg', 5733, '2017-10-30 08:33:50'),
(5, 'Visa', 1, '', 0, '2017-07-24 12:06:48', '2018-01-14 13:11:17', 'logo3.png', 'image/png', 7394, '2017-10-30 08:34:09'),
(6, 'Egyptian Mail', 3, '', 1, '2017-07-25 12:49:48', '2017-07-31 08:31:30', '080316155931271.jpg', 'image/jpeg', 9618, '2017-07-31 08:31:30'),
(12, 'Money Gram', 1, '', 1, '2017-07-31 08:03:03', '2017-10-30 08:34:26', 'logo4.png', 'image/png', 4340, '2017-10-30 08:34:26'),
(13, 'تحويل بنكي جنية مصري', 3, '', 1, '2017-07-31 09:28:15', '2017-07-31 09:28:15', 'Bank.png', 'image/png', 33635, '2017-07-31 09:28:15'),
(14, 'تحويل بنكي دولار امريكي', 1, '', 1, '2017-07-31 09:32:21', '2017-07-31 09:33:00', 'blue-bank-icon-in-flat-style-with-the-building-facade-with-three--26.png', 'image/png', 8414, '2017-07-31 09:33:00'),
(15, 'تحويل بنكي ريال سعودي', 6, '', 1, '2017-07-31 13:19:16', '2017-07-31 13:19:16', 'Bank.png', 'image/png', 33635, '2017-07-31 13:19:16');

-- --------------------------------------------------------

--
-- Table structure for table `currencies_withdraw_types`
--

CREATE TABLE IF NOT EXISTS `currencies_withdraw_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `logo_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_file_size` int(11) DEFAULT NULL,
  `logo_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `currencies_withdraw_types`
--

INSERT INTO `currencies_withdraw_types` (`id`, `name`, `currency_code`, `country_code`, `created_at`, `updated_at`, `logo_file_name`, `logo_content_type`, `logo_file_size`, `logo_updated_at`) VALUES
(1, 'American Dollar', 'USD', 'US', '2017-07-24 11:31:17', '2018-04-03 09:19:30', 'world-white.png', 'image/png', 2655, '2018-04-03 09:19:30'),
(3, 'Egyptian Pound', 'EGP', 'EG', '2017-07-24 11:46:41', '2018-04-03 09:19:53', 'wicon3.png', 'image/png', 1809, '2018-04-03 09:19:53'),
(6, 'Saudi Riyal', 'SAR', 'SA', '2017-07-31 09:02:08', '2018-04-03 09:20:02', 'wicon2.png', 'image/png', 2425, '2018-04-03 09:20:02');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `caption` varchar(200) NOT NULL,
  `about` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `caption`, `about`, `status`) VALUES
(1, 'Technical Support', 'Technical Support', 'Technical Support', 1),
(2, 'Hosting', 'hosting', '', 1),
(3, '', '', '', 0),
(4, '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `greeting_en` varchar(255) DEFAULT NULL,
  `greeting_ar` varchar(255) DEFAULT NULL,
  `subject_en` varchar(255) DEFAULT NULL,
  `subject_ar` varchar(255) DEFAULT NULL,
  `en_content` text,
  `ar_content` text,
  `class_name` varchar(255) DEFAULT NULL,
  `action_name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `key`, `greeting_en`, `greeting_ar`, `subject_en`, `subject_ar`, `en_content`, `ar_content`, `class_name`, `action_name`, `created_at`, `updated_at`) VALUES
(1, 'userbank', 'Hello', 'مرحبًا', 'Bank account addition confirmation', 'تأكيد إضافة حساب بنكي', 'Your bank account has been added\r\n', 'تم تأكيد إضافة حسابك البنكي بنجاح الآن', 'add_userbank_mailer', 'userbank', '2018-02-05 14:41:08', '2018-02-14 11:31:18'),
(2, 'wallet_activate', 'Hello', 'مرحبًا', 'Wallet activation confirmation', 'تأكيد تنشيط محفظة', 'Your wallet has been activated successfully.', 'تم تنشيط محفظتك بنجاح.', 'wallet', 'activate', '2018-02-05 14:55:39', '2018-02-08 09:59:46'),
(3, 'wallet_deactivate', 'Hello', 'مرحبًا', 'Wallet deactivation confirmation', 'تأكيد تعطيل محفظة', 'Your wallet has been deactivated successfully', 'تم تأكيد تعطيل المحفظة بنجاح', 'wallet', 'deactivate', '2018-02-05 15:07:36', '2018-02-08 10:44:54'),
(4, 'wallet_delete', 'Hello', 'مرحبًا', 'Wallet deletion confirmation', 'تأكيد حذف محفظة', 'Your wallet has been deleted successfully', 'تم تأكيد حذف المحفظة بنجاح', 'wallet', 'deletewallet', '2018-02-05 15:10:52', '2018-02-08 10:51:49'),
(5, 'balancetransfer', 'Hello', 'مرحبًا', 'Confirm balance transfer between wallets', 'تأكيد تحويل رصيد بين المحافظ', 'This is a balance transfer confirmation', 'هذا تنبيه بتأكيد عملية تحويل الرصيد', 'wallet', 'balancetransfer', '2018-02-06 08:49:44', '2018-02-08 11:01:07'),
(6, 'balancewithdraw', 'Hello', 'مرحبًا', 'Balance withdraw request confirmation', 'تأكيد عملية سحب رصيد', 'This is a confirmation of balance withdraw request', 'هذا تأكيد لطلب سحب الرصيد', 'balancewithdraw_mail', 'withdraw', '2018-02-06 12:00:52', '2018-02-08 11:10:04'),
(7, 'payforcart', 'Hello', 'مرحبًا', 'Voucher purchase confirmation', 'تأكيد شراء قسيمة', 'This is a confirmation for voucher purchase process', 'هذا تأكيد لعملية شراء القسيمة', 'chargebalance', 'payforcart', '2018-02-06 12:07:41', '2018-02-08 11:47:31'),
(8, 'charge', 'Hello', 'مرحبًا', 'Voucher charge confirmation', 'تأكيد شحن قسيمة', 'This is a confirmation for voucher usage process', 'هذا تأكيد لعملية إستخدام القسيمة', 'chargebalance', 'charge', '2018-02-06 12:13:19', '2018-02-08 11:57:15'),
(9, 'sendcode', 'Hello', 'مرحبًا', 'Confirmation Code', 'رمز تأكيد العملية', 'This code is one time usage only\r\n', 'هذا الرمز للإستخدام مرة واحده فقط', 'emailcode', 'sendcode', '2018-02-06 12:23:39', '2018-02-22 21:48:41'),
(10, 'backupcode', 'Hello', 'مرحبا بكم', 'Notice about use of the backup code', 'إشعار إستخدام رمز الدخول الإحتياطي', 'This is a notification that you have used your backup access code.', 'هذا إشعار لتنبيهك بأنك قد قمت بإستخدام كود الدخول الإحتياطي الخاص بك.', 'emailcode', 'backupcode', '2018-02-06 12:24:23', '2018-02-08 14:55:00'),
(11, 'failed_signin', 'Hello', 'مرحبًا', 'Failed login notification', 'تنبيه بعملية دخول فاشلة', 'This is a notification about failed login process.', 'هذا تنبية حول عملية دخول فاشلة.', 'emailcode', 'failed_signin', '2018-02-06 12:25:00', '2018-02-10 11:16:20'),
(12, 'ticket', 'Hello', 'مرحبًا', 'New support ticket opened', 'تم إنشاء تذكرة دعم فني جديدة', 'A new support ticket has been opened', 'تذكرة دعم فني جديدة قد تم إنشاءها', 'tickett', 'ticket', '2018-02-06 12:31:46', '2018-02-10 11:18:33'),
(13, 'ticket_reply', 'Hello', 'مرحبًا', 'New support ticket response', 'تحديث جديد لطلب الدعم الفني', 'Your support ticket has been updated.', 'تم تحديث تذكرة الدعم الفني الخاصة بك.', 'tickett', 'ticket_reply', '2018-02-06 12:32:26', '2018-02-10 11:21:27'),
(14, 'ticket_close', 'Hello', 'مرحبًا', 'Your support ticket has been closed', 'تم إغلاق تذكرة الدعم الفني', 'Your support ticket marked as resolved.', 'تم إغلاق تذكرة الدعم الفني الخاصة بك.', 'tickett', 'ticket_close', '2018-02-06 12:33:06', '2018-02-10 11:58:45'),
(15, 'transfer', 'Hello', 'مرحبًا', 'Balance transfer notification', 'تنبيه بعملية تحويل رصيد', 'This is a notification about balance transfer process', 'هذا تنبيه بعملية تحويل رصيد', 'usertouser', 'transfer', '2018-02-06 12:41:15', '2018-02-10 12:03:18'),
(16, 'transferto', 'Hello', 'مرحبًا', 'Notification about payment has been received', 'إشعار بتلقي مدفوعات', 'You have received a payment', 'لقد تلقيت مدفوعات', 'usertouser', 'transferto', '2018-02-06 12:43:38', '2018-02-10 13:35:38'),
(17, 'confirmtransfer', 'Hello', 'مرحبًا', 'Your payment has been confirmed', 'تم تأكيد التحويل الخاص بك', 'This is a notification that your payment has been confirmed ', 'هذا تنبيه بتأكيد التحويل الخاص بك', 'usertouser', 'confirmtransfer', '2018-02-06 12:57:09', '2018-02-10 14:05:51'),
(18, 'confirmtransfertransferto', 'Hello', 'مرحبًا', 'Your received payments has been confirmed', 'تم تأكيد مدفوعاتك المُستلمة', 'This is a notification that your received payments has been confirmed.', 'هذا إشعار بأنه تم تأكيد مدفوعاتك المستلمة.', 'usertouser', 'confirmtransfertransferto', '2018-02-06 12:57:56', '2018-02-10 14:23:42'),
(19, 'reset_password_instructions', 'Hello', 'مرحبًا', 'Payers password change request', 'طلب تغير كلمة مرور حساب بايرز', 'Please click on the below button to reset your password.', 'برجاء الضغط على الزر بالأسفل لإستعادة كلمة المرور الخاصة بك.', 'mydevise', 'reset_password_instructions', '2018-02-06 13:01:34', '2018-02-10 14:31:13'),
(20, 'confirmation_instructions', 'Hello', 'مرحبًا', 'Confirm your registrerd email with Payers', 'تأكيد حسابك البريدي المسجل على بايرز', 'Please click on the below button to confirm your email.', 'برجاء الضغط على الزر بالأسفل لتأكيد حسابك البريدي.', 'mydevise', 'confirmation_instructions', '2018-02-06 13:05:51', '2018-02-10 14:51:41'),
(21, 'password_change', 'Hello', 'مرحبًا', 'Payers password change confirmation', 'تأكيد تغير كلمة مرور حسابك على بايرز', 'The password for your Payers account was recently changed.', 'كلمة المرور لحسابك على بايرز قد تم تغييرها مؤخرًا.', 'mydevise', 'password_change', '2018-02-06 13:12:01', '2018-02-11 11:42:44'),
(22, 'unlock_instructions', 'Hello', 'مرحبًا', 'Your Payers account has been locked', 'تم إيقاف حسابك على بايرز', 'Your account was recently locked, Please click on the button below to reactivate it again.', 'تم إيقاف حسابك على بايرز مؤخرًا، يرجى الضغط على الزر بالأسفل لإستعادة الحساب مرة أخرى.', 'mydevise', 'unlock_instructions', '2018-02-06 13:12:33', '2018-02-11 11:46:09'),
(23, 'mass_mail', 'Hello', 'مرحبا بكم', '... payers is one of the biggest payment gate in the middle East', '... payers is one of the biggest payment gate in the middle East', '<p><em>We Would Like to inform you about our opening </em></p>\r\n\r\n<p><strong>... payers is one of the biggest payment gate in the middle East</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n', '<blockquote>\r\n<p><em>مرحباً بكم عملينا العزيز&nbsp;</em><br />\r\n<br />\r\n<strong>يرجي العلم بآن هذه الرسائل هي رسائل تجريبة لتجربة النظام لدينا&nbsp;</strong><br />\r\n<br />\r\nنعتذر على&nbsp;&nbsp;الازعاج ،&nbsp;</p>\r\n</blockquote>\r\n', 'mass_mailer', 'mass_mail', '2018-02-08 10:38:55', '2018-04-03 10:59:56'),
(24, 'wallet_create', 'Hello', 'مرحبًا', 'Wallet creation confirmation', 'تأكيد إنشاء محفظة', 'Your wallet has been created successfully.', 'تم إنشاء محفظتك بنجاح.', 'wallet', 'create', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, NULL, '', '', '', '', '', '', NULL, NULL, '2018-03-10 09:26:10', '2018-03-10 09:26:10'),
(26, NULL, '', '', '', '', '', '', NULL, NULL, '2018-03-28 13:27:42', '2018-03-28 13:27:42'),
(27, NULL, '', '', '', '', '', '', NULL, NULL, '2018-03-28 14:41:59', '2018-03-28 14:41:59'),
(28, NULL, '', '', '', '', '', '', NULL, NULL, '2018-03-29 14:49:48', '2018-03-29 14:49:48'),
(29, NULL, '', '', '', '', '', '', NULL, NULL, '2018-04-01 13:34:45', '2018-04-01 13:34:45'),
(30, NULL, '', '', '', '', '', '', NULL, NULL, '2018-04-01 13:39:26', '2018-04-01 13:39:26'),
(31, NULL, '', '', '', '', '', '', NULL, NULL, '2018-04-03 11:08:45', '2018-04-03 11:08:45');

-- --------------------------------------------------------

--
-- Table structure for table `frontendpages`
--

CREATE TABLE IF NOT EXISTS `frontendpages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pagename` varchar(150) NOT NULL,
  `title` varchar(350) DEFAULT NULL,
  `pagecontent` text NOT NULL,
  `pagename_en` varchar(200) DEFAULT NULL,
  `title_en` varchar(200) DEFAULT NULL,
  `pagecontent_en` text,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `frontendpages`
--

INSERT INTO `frontendpages` (`id`, `pagename`, `title`, `pagecontent`, `pagename_en`, `title_en`, `pagecontent_en`, `status`) VALUES
(1, 'عنا', 'عنا', 'عنا', 'about ', 'about', 'jhgjhg j j j jh j', NULL),
(2, 'دعم', 'دعم', 'دعم', 'support', 'support', 'support', NULL),
(3, 'المصاريف', 'المصاريف', 'المصاريف', 'fees', 'fees', 'fees', NULL),
(4, 'اتصل بنا', 'اتصل بنا', '-\r\n\r\n-\r\n\r\n-\r\n\r\n-\r\n\r\nأسعار الصرف الرسمية 1.5 يتم تطبيق مصاريف إدارية عند استبدال العمالت المختلفة لذلك قد يختلف معدل التحويل عن\r\n\r\nالرئيسية المعتمدة حاليا ( الدوالر األمريكي، الريال السعودي، الجنيه المصري) 1.3 يمكن تحويل الرصيد الخاص بك من عملة إلى أخرى باستخدام المحافظ بالثالث عمالت\r\n\r\nبعملة مختلفة يحق لبايرز تسوية الرصيد باستخدام الرصيد المتاح 1.1 في حالة كان رصيد أحد المحافظ مدين بالسالب ولديك رصيد متاح على محفظة أخرى\r\n\r\nعن رأس مال الشركة. 1.1 بايرز ال تستخدم رصيد حسابك في أي أغراض تجارية حيث تكون أموالك محفوظة بمنعزل\r\n\r\n3.1\r\n\r\nالرصيد المتاح بالحساب ال تتلقى عليه فوائد أو عوائد خالل\r\n\r\nأي فترة زمنية\r\n\r\nرصيد الحساب\r\n\r\nأي مسؤولية قانونية نتيجة سوء استغالل الحساب عليه التواصل مع إدارة الموقع عند حدوث أي محاولة دخول غير مصرح به إلى الحساب لتجنب 1.1 مسؤولية الحفاظ على خصوصية وسرية معلومات الدخول تقع على عاتق المستخدم، وان\r\n\r\nالهاتف، بطاقة الهوية، البطاقة الضريبية، أي مستندات إضافية)\r\n\r\n1\r\n\r\n.\r\n\r\n1\r\n\r\nبايرز\r\n\r\nيحق لها التأكد\r\n\r\nمن الهوية الخاصة بالمستخدم من خالل طرق مختلفة ( البريد،\r\n\r\n1.1 االتصال بين المستخدم والموقع مشفر\r\n\r\nHTTPS\r\n\r\nلحماية معلومات المستخدم\r\n\r\nأمان الحساب والمعلومات\r\n\r\n1.1 يجب أن يكون المستخدم هو المستفيد الفعلي من الخدمة وال يجوز االنابة عنه\r\n\r\n1.1 ضرورة دقة المعلومات الشخصية التي يزودنا بها المستخدم\r\n\r\n1.1 السن 11 سنة على األقل\r\n\r\nأ\r\n\r\nهلية فتح الحساب\r\n\r\nأو خدمات تحويل األموال والتأمين المالي أو إدارة رؤوس األموال وال يمكن اعتبارها بنكا بايرز هي نظام الكتروني لتسهيل المدفوعات االلكترونية، بايرز ال توفر حلول بنكية أو مصرفية\r\n\r\nعالقة المستخدم بنظام بايرز\r\n\r\nاتفاقية استخدام خدمات بايرز Payers -\r\n\r\n-\r\n\r\n-\r\n\r\n-\r\n\r\nحماية البائع\r\n\r\n( جاري النقاش حولها )\r\n\r\nلالستعادة أو اإللغاء المدفوعات التي يتم تأكيدها من خالل الراسل أو النظام بعد مرور فترة التعليق غير قابلة\r\n\r\nأو حتى يتم تأكيد العملية من خالل الراسل، أيهما أقرب عند إرسال مدفوعات لتاجر أو فرد آخر يتم تعليق المعاملة تلقائيا بحساب المستقبل لمدة 7 أيام\r\n\r\nحماية المشتري\r\n\r\nالمستغرق إلتمام التحويل نتيجة العطالت األسبوعية واإلجازات الرسمية 5.1 عند إنشاء طلب سحب يتم توضيح الوقت التقديري لوصول المبلغ وقد يختلف الوقت\r\n\r\nللمستخدم 5.1 وسائل السحب المتاحة قد تختلف حسب العملة الخاصة بالمحفظة أيضا بلد اإلقامة\r\n\r\nالحساب، تفعيل الحساب الشخصي أو التجاري يتيح لنا زيادة حدود االستخدام الخاصة بك 5.1 يحق لنا تحديد حجم المبالغ التي يمكن سحبها من حسابك بناء على نشاطك وحالة\r\n\r\nسحب رصيد الحساب\r\n\r\nالدفع المصدرة من بايرز 3.1 لتتمكن من إرسال مدفوعات يجب أوال إضافة رصيد إلى حسابك من خالل إستخدام بطاقات\r\n\r\n3.1 يمكن إرسال المدفوعات باستخدام العمالت الرئيسية المعتمدة بالموقع فقط\r\n\r\nالحساب، تفعيل الحساب الشخصي أو التجاري يتيح لنا زيادة حدود االستخدام الخاصة بك 3.1 يحق لنا تحديد حجم المدفوعات التي يمكن ارسالها من حسابك بناء على نشاطك وحالة\r\n\r\nإرسال المدفوعات\r\n\r\nإلى رصيدك المتاح لالستخدام 1.7 الرصيد المعلق بالحساب ال يمكن سحبه أو استخدامه إلرسال مدفوعات حتى يتم تحريره\r\n\r\nعلى نظام بايرز 1.6 يتم إضافة الرصيد إلى حسابك من خالل شراء كروت شحن الرصيد بوسائل الدفع المعتمدة الخدمة وانتهاك شروط االستخدام وتعويض بايرز وكافة األطراف المتضررة عن أية خسائر المستخدم ملزم بدفع أي تعويضات أو مصاريف قانونية أو غرامات نتيجة إساءة استخدام\r\n\r\n- المسؤولية القانونية\r\n\r\n-\r\n\r\n-\r\n\r\n-\r\n\r\n-\r\n\r\nأنشطة تتطلب موافقة\r\n\r\nمسبقة\r\n\r\nالتأمين الربح السريع والتسويق الهرمي، خدمات تسوية الديون والبطاقات المدينة، خدمات وأنشطة الكراهية أو العنصرية، معلومات غير مصرح بمشاركتها، المواد والمنتجات الجنسية، أنظمة النشر والعالمات التجارية، البرمجيات الضارة والفيروسات، محتوى أو منتجات تحض على الكحوليات، مستلزمات التعاطي، العقاقير، البضائع المسروقة، المنتجات التي تنتهك حقوق يحظر أي مدفوعات لها عالقة ببيع أو ترويج ( المخدرات، األسلحة، القمار، التبغ والسجائر،\r\n\r\nيحظر بيع أي منتجات أو خدمات تصنف كاحتيال من قبل القانون الدولي أو المحلي\r\n\r\nيحظر أي نشاط ينتهك القانون أو التشريعات المحلية\r\n\r\nاألنشطة المحظورة\r\n\r\nالحصول على موافقته المسبقة يحق لبايرز تعديل نسبة الرسوم، الضرائب، المصاريف اإلدارية بدون الرجوع إلى المستخدم أو\r\n\r\nبايرز يتم خصم رسوم قدرها 11 بالمئة عند استقبال مدفوعات من خالل مستخدم آخر عبر نظام\r\n\r\nالرسوم\r\n\r\nالنزاعات و\r\n\r\nاستعادة األموال\r\n\r\n( جاري النقاش حولها ) إلى أي قوانين سيتم االحتكام إليها حال وجود أي نزاعات قانونية\r\n\r\n- الخالف مع بايرز ( جاري النقاش حولها )\r\n\r\n-\r\n\r\n-\r\n\r\nللخدمة مسؤولية المستخدم أن يقوم بالدخول دوريا إلى الموقع ومراجعة الشروط والقوانين المنظمة\r\n\r\nبالتعامل عبر النظام بعد هذا التحديث بمثابة موافقة ضمنية موافقته، ويتم وضع تنبيه يوضح أي تعديالت على اتفاقية االستخدام ويعتبر استمرار المستخدم يحق لنا التعديل على شروط االستخدام في أي وقت دون الرجوع للمستخدم أو الحصول على\r\n\r\nالتعديالت\r\n\r\nغرامات أو خسائر ناتجة عن ذلك ال يحق للعميل المطالبة بأي أرصدة تتعلق بأنشطة غير قانونية ويلزم العميل بأي تعويضات أو\r\n\r\nالحساب قبل أن نتمكن من إصدار أمر بسحب الرصيد يحق لبايرز تجميد أي أرصدة حتى االنتهاء من فض كافة النزاعات والتحقيقات حول أنشطة\r\n\r\nبمخالفات قانونية حول هذه المدفوعات المصاريف اإلدارية والحدود الدنيا لوسيلة الدفع المتاحة و عدم وجود أي تحقيقات أو االشتباه يحق للمستخدم طلب سحب األموال المتاحة في الحساب قبل غلقه شرط أن يتخطي المبلغ\r\n\r\nمن شروط االستخدام يحق لبايرز في أي وقت وبدون الرجوع إلى المستخدم بتقييد استخدام حسابك حال انتهاك أي\r\n\r\nيلزم المستخدم بتسوية األرصدة الخاصة به ودفع أي مبالغ مستحقة قبل غلق حسابه نهائيا\r\n\r\nالحساب ال يلغي عمليات الدفع التي تم إرسالها أو استقبالها يحق للمستخدم طلب إغالق الحساب الخاص به وذلك بالتواصل مع خدمة العمالء، طلب غلق\r\n\r\nغ\r\n\r\nلق الحساب', 'contact', 'contact', 'contact', NULL),
(5, 'تعرف علينا', 'تعرف علينا', 'تعرف علينا', 'know about us', 'know about us', 'know about us', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `helpcomments`
--

CREATE TABLE IF NOT EXISTS `helpcomments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `helpfaq_id` int(11) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `helpcomments`
--

INSERT INTO `helpcomments` (`id`, `helpfaq_id`, `subject`, `comment`, `user_id`, `created_at`) VALUES
(1, 1, 'fhtf', 'fhftfhdh', 1, '2015-09-15 15:08:02'),
(2, 1, 'lalala', 'derfd', 7, '2015-09-15 15:25:08'),
(3, 1, 'gshg', 'sfret', 7, '2015-09-15 15:27:49'),
(4, 1, 'dsdff', 'dsff', 7, '2015-09-15 15:28:34'),
(5, 1, 'jfyui', 'thfgjkyk', 10, '2015-10-01 12:40:30'),
(6, 1, 'مرحبا\r\n', 'شكراً لكم ', 8, '2017-07-05 17:40:38'),
(7, 2, '1', 'test', 7, '2017-08-08 16:57:10');

-- --------------------------------------------------------

--
-- Table structure for table `helpfaqs`
--

CREATE TABLE IF NOT EXISTS `helpfaqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(300) NOT NULL,
  `body` text NOT NULL,
  `yes_count` int(11) DEFAULT NULL,
  `no_count` int(11) DEFAULT NULL,
  `attachment` varchar(200) DEFAULT NULL,
  `attachment_file_name` varchar(200) DEFAULT NULL,
  `attachment_content_type` varchar(100) DEFAULT NULL,
  `attachment_file_size` int(11) DEFAULT NULL,
  `attachment_updated_at` date DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `helpfaqs`
--

INSERT INTO `helpfaqs` (`id`, `subject`, `body`, `yes_count`, `no_count`, `attachment`, `attachment_file_name`, `attachment_content_type`, `attachment_file_size`, `attachment_updated_at`, `department_id`, `service_id`, `created_at`, `status`) VALUES
(1, 'fdh', 'jhgk السحب', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, '2015-09-15 14:50:56', 0),
(2, 'طريقة السحب', 'طريقة السحب طريقة السحب طريقة السحب طريقة السحب طريقة السحب طريقة السحب طريقة السحب ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-02-05 17:49:34', 1);

-- --------------------------------------------------------

--
-- Table structure for table `helpfaqs_categories`
--

CREATE TABLE IF NOT EXISTS `helpfaqs_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `helpfaqs_categories`
--

INSERT INTO `helpfaqs_categories` (`id`, `name`, `caption`, `about`) VALUES
(1, 'الدفع', 'الدفع', 'الدفع'),
(2, 'السحب', 'السحب', 'السحب');

-- --------------------------------------------------------

--
-- Table structure for table `homepage`
--

CREATE TABLE IF NOT EXISTS `homepage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blockname` varchar(250) NOT NULL,
  `blockcontents` text NOT NULL,
  `blockname_en` varchar(200) DEFAULT NULL,
  `blockcontents_en` text,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `homepage`
--

INSERT INTO `homepage` (`id`, `blockname`, `blockcontents`, `blockname_en`, `blockcontents_en`, `status`) VALUES
(1, 'Test', 'test', NULL, NULL, NULL),
(2, 'Test', 'Test Payerz', NULL, NULL, NULL),
(3, '', '', NULL, NULL, NULL),
(4, '', '', NULL, NULL, NULL),
(5, '', '', NULL, NULL, NULL),
(6, '', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(150) DEFAULT NULL,
  `trans_id` int(200) DEFAULT NULL,
  `addeduser_id` varchar(36) DEFAULT NULL,
  `added_by` varchar(11) DEFAULT NULL,
  `foreign_id` varchar(100) DEFAULT NULL,
  `controller` varchar(200) DEFAULT NULL,
  `stats` text,
  `read` int(11) NOT NULL DEFAULT '0',
  `notetype` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `trans_id`, `addeduser_id`, `added_by`, `foreign_id`, `controller`, `stats`, `read`, `notetype`, `created_at`) VALUES
(1, '21', 1, NULL, '21', NULL, 'User_wallet_ballence/walletstransferpost', 'you have replaced/1000.0 SAR /with a value of/260.7 USD /Transaction ID/WAVXNHECGJPD5IO1B2', 1, NULL, '2018-03-30 10:08:40'),
(2, '3', NULL, NULL, NULL, '1', 'users_bank', 'you have added new bank for/Western Union', 1, NULL, '2018-03-30 10:34:02'),
(3, '3', 2, NULL, NULL, '1', 'balances_withdraws', 'withdraw request/100.0 USD /Transaction ID/WBT84XRQ7WASJL6ZBV', 1, NULL, '2018-03-30 10:34:02'),
(4, '3', 3, NULL, NULL, '2', 'balances_withdraws', 'withdraw request/200.0 USD /Transaction ID/WBCXJUAKWE8094YVLZ', 1, NULL, '2018-03-30 11:19:36'),
(5, '1', 4, '2', '1', '1', 'User_wallet_ballence_frozen', 'balance was transferred with value of/1000.0 USD/From User/linkOnline/to the user/alm7madi', 1, NULL, '2018-03-30 12:35:13'),
(6, '1', 5, '2', '1', '1', 'User_wallet_ballence_frozen', 'Balance Delivered/1000.0 USD/From User/linkOnline/to the user/alm7madi', 1, NULL, '2018-03-30 12:40:19'),
(7, '3', 6, NULL, NULL, '3', 'balances_withdraws', 'withdraw request/20.0 USD /Transaction ID/WBJIC06A84DTEX2HYW', 1, NULL, '2018-03-30 13:17:12'),
(8, '3', 7, NULL, NULL, '4', 'balances_withdraws', 'withdraw request/20.0 USD /Transaction ID/WBD2UK37OQM5XSR9L0', 1, NULL, '2018-03-30 13:26:29'),
(9, '3', 8, NULL, NULL, '5', 'balances_withdraws', 'withdraw request/20.0 USD /Transaction ID/WBP3XH10Q46AEWRVSL', 1, NULL, '2018-03-30 13:32:35'),
(10, '3', 9, NULL, NULL, '6', 'balances_withdraws', 'withdraw request/20.0 USD /Transaction ID/WBQEW0P7UZ8VYK5FJ1', 1, NULL, '2018-03-30 13:33:44'),
(11, '3', 10, NULL, '3', '6', 'balances_withdraws', 'balancewithdraw canceled/20.0  USD/Transaction ID/WBQEW0P7UZ8VYK5FJ1', 1, NULL, '2018-03-30 13:34:20'),
(12, '21', 11, NULL, '21', NULL, 'User_wallet_ballence/walletstransferpost', 'you have replaced/2000.0 SAR /with a value of/521.4 USD /Transaction ID/WALIPYHVW6SDFQO8J0', 0, NULL, '2018-03-30 13:38:28'),
(13, '3', NULL, NULL, '10', '5', 'balances_withdraws', 'completed withdraw request/20.0 USD /Transaction ID/WBP3XH10Q46AEWRVSL', 1, NULL, '2018-03-30 13:39:37'),
(14, '3', 13, NULL, '10', '4', 'balances_withdraws', 'rejected withdraw request/20.0 USD /Transaction ID/WBD2UK37OQM5XSR9L0', 1, NULL, '2018-03-30 13:40:11'),
(15, '3', NULL, NULL, '10', '4', 'balances_withdraws', 'completed withdraw request/20.0 USD /Transaction ID/WBD2UK37OQM5XSR9L0', 1, NULL, '2018-03-30 13:40:43'),
(16, '3', 14, NULL, '10', '3', 'balances_withdraws', 'refunded withdraw request/20.0 USD /Transaction ID/WBJIC06A84DTEX2HYW', 1, NULL, '2018-03-30 14:04:00'),
(17, '3', 15, NULL, '3', NULL, 'User_wallet_ballence/walletstransferpost', 'you have replaced/100.0 USD /with a value of/367.5 SAR /Transaction ID/WACKV907NBA81DTL3W', 1, NULL, '2018-03-30 14:58:59'),
(18, '3', 16, NULL, NULL, '7', 'balances_withdraws', 'withdraw request/20.0 USD /Transaction ID/WBBOVHLFQM7C9X4YIG', 1, NULL, '2018-03-30 15:28:30'),
(19, '3', NULL, NULL, '10', '7', 'balances_withdraws', 'recieved withdraw request/20.0 USD /Transaction ID/WBBOVHLFQM7C9X4YIG', 1, NULL, '2018-03-30 15:31:39'),
(20, '3', NULL, NULL, '10', '7', 'balances_withdraws', 'workon withdraw request/20.0 USD /Transaction ID/WBBOVHLFQM7C9X4YIG', 1, NULL, '2018-03-30 15:32:48'),
(21, '3', NULL, NULL, '10', '7', 'balances_withdraws', 'exception withdraw request/20.0 USD /Transaction ID/WBBOVHLFQM7C9X4YIG', 1, NULL, '2018-03-30 15:34:12'),
(22, '3', 17, NULL, NULL, '8', 'balances_withdraws', 'withdraw request/22.0 USD /Transaction ID/WB2PMX0CLAT3GD4OQE', 1, NULL, '2018-03-30 16:04:02'),
(23, '3', 18, '20', '3', '2', 'User_wallet_ballence_frozen', 'balance was transferred with value of/20.0 USD/From User/EngineeraAmiraa/to the user/engytoma', 1, NULL, '2018-03-31 07:48:40'),
(24, '3', 19, NULL, NULL, '9', 'balances_withdraws', 'withdraw request/200.0 USD /Transaction ID/WBDL492NY61KTJSIPE', 1, NULL, '2018-03-31 07:55:03'),
(25, '3', 20, NULL, NULL, '10', 'balances_withdraws', 'withdraw request/23.0 USD /Transaction ID/WBP0ARUVJGYHD2C98X', 1, NULL, '2018-03-31 08:04:23'),
(26, '3', NULL, NULL, '10', '9', 'balances_withdraws', 'completed withdraw request/200.0 USD /Transaction ID/WBDL492NY61KTJSIPE', 1, NULL, '2018-03-31 08:04:53'),
(27, '1', NULL, NULL, NULL, NULL, 'failed signin', 'failed signin/41.68.76.215/desktop/Mac/10.12.6/Chrome/65.0.3325.181', 1, NULL, '2018-03-31 08:23:43'),
(28, '3', NULL, NULL, NULL, NULL, 'failed signin', 'failed signin/196.221.117.199/desktop/Windows/7/Chrome/65.0.3325.181', 1, NULL, '2018-03-31 08:25:23'),
(29, '3', NULL, NULL, NULL, NULL, 'failed signin', 'failed signin/196.221.117.199/desktop/Windows/7/Chrome/65.0.3325.181', 1, NULL, '2018-03-31 08:26:14'),
(30, '3', NULL, NULL, NULL, NULL, 'failed signin', 'failed signin/196.221.117.199/desktop/Windows/7/Chrome/65.0.3325.181', 1, NULL, '2018-03-31 08:27:41'),
(31, '1', 22, '2', '10', '1', 'User_wallet_ballence_frozen', 'balance refunded/1000.0 USD/from user/linkOnline/to user/alm7madi', 1, NULL, '2018-03-31 09:39:25'),
(32, '3', 23, NULL, NULL, '11', 'balances_withdraws', 'withdraw request/30.0 USD /Transaction ID/WB82XO1MIP4YTV6UK7', 1, NULL, '2018-03-31 09:57:12'),
(33, '3', 24, NULL, '3', '11', 'balances_withdraws', 'balancewithdraw canceled/30.0  USD/Transaction ID/WB82XO1MIP4YTV6UK7', 1, NULL, '2018-03-31 09:57:18'),
(34, '21', NULL, NULL, NULL, NULL, 'failed signin', 'failed signin/156.198.234.146/desktop/Windows/10/Chrome/65.0.3325.181', 1, NULL, '2018-03-31 10:00:50'),
(35, '9', 25, '3', '9', '3', 'User_wallet_ballence_frozen', 'balance was transferred with value of/200.0 USD/From User/Amira/to the user/EngineeraAmiraa', 0, NULL, '2018-03-31 11:54:30'),
(36, '9', 26, '3', '9', '4', 'User_wallet_ballence_frozen', 'balance was transferred with value of/20.0 USD/From User/Amira/to the user/EngineeraAmiraa', 0, NULL, '2018-03-31 11:55:08'),
(37, '9', 27, '3', '9', '5', 'User_wallet_ballence_frozen', 'Balance Delivered/30.0 USD/From User/Amira/to the user/EngineeraAmiraa/Transaction ID/BTR1YA6XNSQ8W5BPK7', 0, NULL, '2018-03-31 11:55:52'),
(38, '3', 28, '9', '3', '6', 'User_wallet_ballence_frozen', 'balance was transferred with value of/200.0 USD/From User/EngineeraAmiraa/to the user/Amira', 1, NULL, '2018-03-31 11:56:58'),
(39, '3', 29, '9', '3', '7', 'User_wallet_ballence_frozen', 'balance was transferred with value of/200.0 USD/From User/EngineeraAmiraa/to the user/Amira', 1, NULL, '2018-03-31 11:58:46'),
(40, '9', 30, '3', '3', '3', 'User_wallet_ballence_frozen', 'balance refunded/200.0 USD/from user/Amira/to user/EngineeraAmiraa', 0, NULL, '2018-03-31 11:59:20'),
(41, '3', 31, '9', '3', '6', 'User_wallet_ballence_frozen', 'Balance Delivered/200.0 USD/From User/EngineeraAmiraa/to the user/Amira', 1, NULL, '2018-03-31 11:59:44'),
(42, '9', 32, '3', '9', '8', 'User_wallet_ballence_frozen', 'balance was transferred with value of/40.0 USD/From User/Amira/to the user/EngineeraAmiraa', 0, NULL, '2018-03-31 12:02:37'),
(43, '9', 33, '3', '10', '8', 'User_wallet_ballence_frozen', 'balance refunded/40.0 USD/from user/Amira/to user/EngineeraAmiraa', 0, NULL, '2018-03-31 12:03:13'),
(44, '3', 34, NULL, NULL, '1', 'carts/paypalconfirm', 'A credit card has been purchased/450.0 USD', 1, NULL, '2018-03-31 13:20:43'),
(45, '3', 35, NULL, NULL, '1', 'carts/cartbalancetouser', 'you have added balance with a value of/450.0 USD /Your balance has become/9363.9 USD ', 1, NULL, '2018-03-31 13:21:44'),
(46, '3', 36, NULL, NULL, '2', 'carts/paypalconfirm', 'A credit card has been purchased/450.0 USD', 1, NULL, '2018-03-31 13:30:30'),
(47, '3', 37, NULL, NULL, '2', 'carts/cartbalancetouser', 'you have added balance with a value of/450.0 USD /Your balance has become/9813.9 USD ', 1, NULL, '2018-03-31 13:30:57'),
(48, '3', 38, NULL, '3', NULL, 'User_wallet_ballence/walletstransferpost', 'you have replaced/2000.0 EGP /with a value of/415.6 SAR /Transaction ID/WA04DZXTHCJANKSUPG', 1, NULL, '2018-03-31 13:41:48'),
(49, '3', NULL, NULL, '10', '2', 'users_bank', 'you have added new bank for/Egypt Post', 1, NULL, '2018-03-31 14:38:57'),
(50, '2', 39, NULL, '2', NULL, 'User_wallet_ballence/walletstransferpost', 'you have replaced/1000.0 USD /with a value of/3675.0 SAR /Transaction ID/WAQZP1UB3GHVTYR7OW', 1, NULL, '2018-03-31 23:52:17'),
(51, '2', 40, NULL, '2', NULL, 'User_wallet_ballence/walletstransferpost', 'you have replaced/1000.0 USD /with a value of/17267.6 EGP /Transaction ID/WASMTZH7XVU39L2F8R', 1, NULL, '2018-03-31 23:53:01'),
(52, '21', 41, NULL, '21', NULL, 'User_wallet_ballence/walletstransferpost', 'you have replaced/300.0 SAR /with a value of/78.21 USD /Transaction ID/WAEDKBWSOQ21GM0YAC', 0, NULL, '2018-04-01 10:08:24'),
(53, '9', 42, NULL, '9', NULL, 'User_wallet_ballence/walletstransferpost', 'you have replaced/22.0 USD /with a value of/80.85 SAR /Transaction ID/WATB82VZFIMGH9CJUN', 0, NULL, '2018-04-01 11:49:39'),
(54, '3', NULL, NULL, NULL, '1', 'Ticket', 'new ticket has been added with number/ 793190', 1, NULL, '2018-04-01 11:55:50'),
(55, '3', 43, NULL, NULL, '12', 'balances_withdraws', 'withdraw request/22.0 USD /Transaction ID/WBSJM5X0AN1GFOPDWH', 1, NULL, '2018-04-01 12:05:37'),
(56, '3', NULL, NULL, '10', '12', 'balances_withdraws', 'exception withdraw request/22.0 USD /Transaction ID/WBSJM5X0AN1GFOPDWH', 1, NULL, '2018-04-01 12:08:47'),
(57, '3', 44, NULL, '3', '3', 'Onlinebanks/banktransfer_post', 'payment request/140238/addvalue/100.0', 1, NULL, '2018-04-01 13:01:50'),
(58, '3', 44, NULL, '3', '3', 'Onlinebanks/add_payment_data', 'payment information added/140238/addvalue/100.0', 1, NULL, '2018-04-01 13:02:13'),
(59, '3', 44, NULL, '10', '3', 'Onlinebanks/exception_payment', 'excepion payment request/ 140238/addvalue/100.0', 1, NULL, '2018-04-01 13:15:35'),
(60, '1', 45, NULL, '1', NULL, 'User_wallet_ballence/walletstransferpost', 'you have replaced/10000.0 EGP /with a value of/555.0 USD /Transaction ID/WACINKE59OAX17LYPH', 1, NULL, '2018-04-01 13:21:09'),
(61, '1', 46, NULL, '1', NULL, 'User_wallet_ballence/walletstransferpost', 'you have replaced/10000.0 SAR /with a value of/2607.0 USD /Transaction ID/WAOSHCD83KI7XJ2MRY', 1, NULL, '2018-04-01 13:21:29'),
(62, '3', 44, NULL, '3', '3', 'Onlinebanks/exception_reply', 'reply to exception/ 140238/addvalue/100.0', 1, NULL, '2018-04-01 13:42:09'),
(63, '3', 47, NULL, '10', '3', 'carts/offline_payment', 'A credit card has been purchased/90.0USD', 1, NULL, '2018-04-01 14:05:38'),
(64, '3', 44, NULL, '10', '3', 'Onlinebanks/complete_payment', 'complete payment request/ 140238/addvalue/100.0', 1, NULL, '2018-04-01 14:05:38'),
(65, '3', 48, NULL, '3', '4', 'Onlinebanks/banktransfer_post', 'payment request/653801/addvalue/50.0', 1, NULL, '2018-04-01 23:08:54'),
(66, '7', NULL, NULL, NULL, NULL, 'failed signin', 'failed signin/156.198.234.146/desktop/Windows/10/Chrome/65.0.3325.181', 1, NULL, '2018-04-02 08:48:02'),
(67, '7', NULL, NULL, NULL, NULL, 'failed signin', 'failed signin/156.198.234.146/desktop/Windows/10/Chrome/65.0.3325.181', 1, NULL, '2018-04-02 08:48:20'),
(68, '2', 49, '1', '2', '9', 'User_wallet_ballence_frozen', 'balance was transferred with value of/99.09 USD/From User/alm7madi/to the user/linkOnline', 1, NULL, '2018-04-02 09:49:33'),
(69, '5', NULL, NULL, NULL, '4', 'users_bank', 'you have added new bank for/HOUSING BANK FOR TRADE AND FINANCE, THE', 0, NULL, '2018-04-02 15:54:09'),
(70, '5', 50, NULL, NULL, '13', 'balances_withdraws', 'withdraw request/93.0  USD/Transaction ID/WBH28T0VXPSZNO9MJG', 0, NULL, '2018-04-02 15:54:09'),
(71, '3', 51, '20', NULL, '2', 'User_wallet_ballence_frozen', 'Balance Delivered/20.0 USD/From User/EngineeraAmiraa/to the user/engytoma', 0, NULL, '2018-04-02 16:00:05'),
(72, '9', 52, '3', NULL, '4', 'User_wallet_ballence_frozen', 'Balance Delivered/20.0 USD/From User/Amira/to the user/EngineeraAmiraa', 0, NULL, '2018-04-02 16:00:11'),
(73, '3', 48, NULL, '2', '4', 'Onlinebanks/exception_payment', 'excepion payment request/ 653801/addvalue/50.0', 0, NULL, '2018-04-03 09:17:40'),
(74, '3', 53, NULL, '3', '5', 'Onlinebanks/banktransfer_post', 'payment request/736459/addvalue/500.0', 0, NULL, '2018-04-03 09:28:40'),
(75, '3', NULL, NULL, '2', '3', 'cart', 'A credit card has been disabled/90.0USD', 0, NULL, '2018-04-03 09:48:40'),
(76, '3', NULL, NULL, '2', '3', 'cart', 'A credit card has been refunded/90.0USD', 0, NULL, '2018-04-03 09:48:58'),
(77, '9', 55, '3', '1', '12', 'User_wallet_ballence_frozen', 'balance refunded/20.0 USD/from user/Amira/to user/EngineeraAmiraa', 0, NULL, '2018-04-03 10:34:32'),
(78, '3', 56, '9', '1', '7', 'User_wallet_ballence_frozen', 'balance refunded/200.0 USD/from user/EngineeraAmiraa/to user/Amira', 0, NULL, '2018-04-03 10:35:03'),
(79, '1', 59, '2', '1', '48', 'User_wallet_ballence_frozen', 'Balance Transfer has been confirmed with value/10.0 USD/from user/linkOnline/to user/alm7madi', 1, NULL, '2018-04-03 15:13:16'),
(80, '3', NULL, NULL, NULL, '2', 'users_bank', 'Bank data has been modified/Egypt Post', 0, NULL, '2018-04-04 09:43:42');

-- --------------------------------------------------------

--
-- Table structure for table `onlinebanks`
--

CREATE TABLE IF NOT EXISTS `onlinebanks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `user_bank_id` int(11) DEFAULT NULL,
  `company_bank_id` int(11) DEFAULT NULL,
  `transaction_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `national_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cardNumber` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cardCVV` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cardExpiryMonth` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cardExpiryYear` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `responseCode` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `responseMessage` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tansaction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` float DEFAULT NULL,
  `netsum` float NOT NULL DEFAULT '0',
  `refund` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `status` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transfer_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `purchased_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `avatar_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_file_size` int(11) DEFAULT NULL,
  `avatar_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `onlinebanks`
--

INSERT INTO `onlinebanks` (`id`, `user_id`, `service_id`, `user_bank_id`, `company_bank_id`, `transaction_id`, `bank_name`, `address`, `status_url`, `branch`, `account_number`, `national_id`, `cardNumber`, `cardCVV`, `cardExpiryMonth`, `cardExpiryYear`, `responseCode`, `responseMessage`, `tansaction`, `currency`, `value`, `netsum`, `refund`, `approved`, `status`, `transfer_at`, `created_at`, `purchased_at`, `updated_at`, `avatar_file_name`, `avatar_content_type`, `avatar_file_size`, `avatar_updated_at`) VALUES
(1, 3, NULL, NULL, 1, 'BCCGVXD0E34R6LPF8J', 'paypal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '37966013TK1319618', 'USD', 520, 500, NULL, NULL, 'Completed', NULL, '2018-03-31 17:20:43', '2018-03-31 13:20:43', '2018-03-31 13:20:43', NULL, NULL, NULL, NULL),
(2, 3, NULL, NULL, 1, 'BCYBWJN6QP5TR2MHSC', 'paypal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '84K05621GB572474F', 'USD', 520, 500, NULL, NULL, 'Refunded', NULL, '2018-03-31 17:30:30', '2018-03-31 13:30:30', '2018-03-31 13:48:58', NULL, NULL, NULL, NULL),
(3, 3, NULL, 3, 23, '140238', 'ABU DHABI ISLAMIC BANK - EGYPT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', 104, 100, NULL, NULL, 'Completed', NULL, '2018-04-01 17:01:50', NULL, '2018-04-01 14:05:38', '17553419_1749947761963372_7859631055446362296_n.jpg', 'image/jpeg', 29413, '2018-04-01 13:02:13'),
(4, 3, NULL, NULL, 23, '653801', 'ABU DHABI ISLAMIC BANK - EGYPT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', 53.5, 50, NULL, NULL, 'Exception', NULL, '2018-04-02 03:08:54', NULL, '2018-04-03 09:17:40', NULL, NULL, NULL, NULL),
(5, 3, NULL, NULL, 23, '736459', 'ABU DHABI ISLAMIC BANK - EGYPT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', 508, 500, NULL, NULL, 'Pending', NULL, '2018-04-03 13:28:40', NULL, '2018-04-03 09:28:40', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE IF NOT EXISTS `partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ratio` float NOT NULL DEFAULT '0',
  `sign_status` int(11) DEFAULT '1',
  `profit_status` int(11) DEFAULT '1',
  `profit_infinite` int(11) DEFAULT '0',
  `withdraw_balance` float DEFAULT '0',
  `withdraw_limit` float DEFAULT '0',
  `usd_balance` float DEFAULT '0',
  `sar_balance` float DEFAULT '0',
  `egp_balance` float DEFAULT '0',
  `f_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `second_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emergency_tel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_partners_on_email` (`email`),
  UNIQUE KEY `index_partners_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `email`, `encrypted_password`, `reset_password_token`, `reset_password_sent_at`, `remember_created_at`, `sign_in_count`, `current_sign_in_at`, `last_sign_in_at`, `current_sign_in_ip`, `last_sign_in_ip`, `created_at`, `updated_at`, `name`, `ratio`, `sign_status`, `profit_status`, `profit_infinite`, `withdraw_balance`, `withdraw_limit`, `usd_balance`, `sar_balance`, `egp_balance`, `f_name`, `l_name`, `country`, `city`, `first_address`, `second_address`, `postal_code`, `tel`, `emergency_tel`, `note`) VALUES
(1, 'team@payerz.com', '$2a$11$yEYfrmotFE7pkFzWPnmus.w3mz3VqXw/gTqhIn8otMGX1mVCaITYG', NULL, NULL, NULL, 3, '2018-04-01 10:11:20', '2018-03-30 09:34:02', '156.198.234.146', '41.233.185.3', '2018-03-28 09:41:45', '2018-04-03 09:48:58', 'الشركة', 80, 1, 1, 0, 87.794, 0, 87.4429, 1.32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'support@payers.net', '$2a$11$Rl2UkUxxwfw2ZswVIZG0kel79IWy2.XJINM0DdP2qoxpZCG8JVVa.', NULL, NULL, NULL, 11, '2018-04-01 08:30:22', '2018-03-30 09:32:50', '156.198.234.146', '41.233.185.3', '2018-03-28 09:43:50', '2018-04-03 09:48:58', 'ddddds', 10, 1, 1, 1, 10.9742, 100, 10.9304, 0.165, 0, 'ddd', 'sdasd', 'dsds', 'ddsads', 'fsaf', 'fsaf', 'fsaf', 'fsafsaf', 'fsafsa', 'fsafsaf'),
(11, 'khalidkhalil1993@outlook.com', '$2a$11$SCTjc6iZa3Tw8Cjo2BnwBuBidKlTrzrGuQFMx6MaOyJeDU3S2ioKO', NULL, NULL, NULL, 1, '2018-03-29 13:03:11', '2018-03-29 13:03:11', '41.43.92.179', '41.43.92.179', '2018-03-29 12:55:38', '2018-04-03 09:48:58', NULL, 10, 1, 1, 0, 10.9742, 100, 10.9304, 0.165, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `partners_reports`
--

CREATE TABLE IF NOT EXISTS `partners_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) DEFAULT NULL,
  `company_balance_id` int(11) DEFAULT NULL,
  `current_ratio` int(11) DEFAULT NULL,
  `balance_before` float DEFAULT NULL,
  `balance_after` float DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `operation_type` varchar(50) NOT NULL DEFAULT 'regular',
  `details` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `partners_reports`
--

INSERT INTO `partners_reports` (`id`, `partner_id`, `company_balance_id`, `current_ratio`, `balance_before`, `balance_after`, `currency`, `operation_type`, `details`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 80, 0, 1.272, 'USD', 'regular', NULL, '2018-04-01 10:08:24', '2018-04-01 10:08:24'),
(2, 2, 1, 10, 0, 0.159, 'USD', 'regular', NULL, '2018-04-01 10:08:24', '2018-04-01 10:08:24'),
(3, 11, 1, 10, 0, 0.159, 'USD', 'regular', NULL, '2018-04-01 10:08:24', '2018-04-01 10:08:24'),
(4, 1, 2, 80, 0, 1.32, 'SAR', 'regular', NULL, '2018-04-01 11:49:39', '2018-04-01 11:49:39'),
(5, 2, 2, 10, 0, 0.165, 'SAR', 'regular', NULL, '2018-04-01 11:49:39', '2018-04-01 11:49:39'),
(6, 11, 2, 10, 0, 0.165, 'SAR', 'regular', NULL, '2018-04-01 11:49:39', '2018-04-01 11:49:39'),
(7, 1, 3, 80, 1.272, 10.152, 'USD', 'regular', NULL, '2018-04-01 12:05:37', '2018-04-01 12:05:37'),
(8, 2, 3, 10, 0.159, 1.269, 'USD', 'regular', NULL, '2018-04-01 12:05:37', '2018-04-01 12:05:37'),
(9, 11, 3, 10, 0.159, 1.269, 'USD', 'regular', NULL, '2018-04-01 12:05:37', '2018-04-01 12:05:37'),
(10, 1, 4, 80, 10.152, 18.952, 'USD', 'regular', NULL, '2018-04-01 13:21:10', '2018-04-01 13:21:10'),
(11, 2, 4, 10, 1.269, 2.369, 'USD', 'regular', NULL, '2018-04-01 13:21:10', '2018-04-01 13:21:10'),
(12, 11, 4, 10, 1.269, 2.369, 'USD', 'regular', NULL, '2018-04-01 13:21:10', '2018-04-01 13:21:10'),
(13, 1, 5, 80, 18.952, 61.352, 'USD', 'regular', NULL, '2018-04-01 13:21:29', '2018-04-01 13:21:29'),
(14, 2, 5, 10, 2.369, 7.669, 'USD', 'regular', NULL, '2018-04-01 13:21:29', '2018-04-01 13:21:29'),
(15, 11, 5, 10, 2.369, 7.669, 'USD', 'regular', NULL, '2018-04-01 13:21:29', '2018-04-01 13:21:29'),
(16, 1, 6, 80, 61.352, 69.352, 'USD', 'regular', NULL, '2018-04-01 14:05:38', '2018-04-01 14:05:38'),
(17, 2, 6, 10, 7.669, 8.669, 'USD', 'regular', NULL, '2018-04-01 14:05:38', '2018-04-01 14:05:38'),
(18, 11, 6, 10, 7.669, 8.669, 'USD', 'regular', NULL, '2018-04-01 14:05:38', '2018-04-01 14:05:38'),
(19, 1, 7, 80, 69.352, 75.7229, 'USD', 'regular', NULL, '2018-04-02 09:49:33', '2018-04-02 09:49:33'),
(20, 2, 7, 10, 8.669, 9.46536, 'USD', 'regular', NULL, '2018-04-02 09:49:33', '2018-04-02 09:49:33'),
(21, 11, 7, 10, 8.669, 9.46536, 'USD', 'regular', NULL, '2018-04-02 09:49:33', '2018-04-02 09:49:33'),
(22, 1, 8, 80, 75.7229, 95.4429, 'USD', 'regular', NULL, '2018-04-02 15:54:09', '2018-04-02 15:54:09'),
(23, 2, 8, 10, 9.46536, 11.9304, 'USD', 'regular', NULL, '2018-04-02 15:54:09', '2018-04-02 15:54:09'),
(24, 11, 8, 10, 9.46536, 11.9304, 'USD', 'regular', NULL, '2018-04-02 15:54:09', '2018-04-02 15:54:09'),
(25, 1, 9, 80, 95.4429, 87.4429, 'USD', 'regular', NULL, '2018-04-03 09:48:58', '2018-04-03 09:48:58'),
(26, 2, 9, 10, 11.9304, 10.9304, 'USD', 'regular', NULL, '2018-04-03 09:48:58', '2018-04-03 09:48:58'),
(27, 11, 9, 10, 11.9304, 10.9304, 'USD', 'regular', NULL, '2018-04-03 09:48:58', '2018-04-03 09:48:58');

-- --------------------------------------------------------

--
-- Table structure for table `partners_settings`
--

CREATE TABLE IF NOT EXISTS `partners_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `key` text,
  `value` text,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `partners_settings`
--

INSERT INTO `partners_settings` (`id`, `name`, `key`, `value`, `description`, `created_at`, `updated_at`) VALUES
(1, 'تعطيل نظام الارباح', 'profit_status', '1', 'تعطيل نظام الارباح', '2018-03-15 00:00:00', '0000-00-00 00:00:00'),
(2, 'تعطيل الدخول للشركاء', 'partner_signin', '1', 'تعطيل الدخول للشركاء', '2018-03-15 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `partners_withdraws`
--

CREATE TABLE IF NOT EXISTS `partners_withdraws` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) DEFAULT NULL,
  `value` float NOT NULL,
  `details` text,
  `status` int(11) DEFAULT '2' COMMENT '2 pending - 0 - refused -  1 accepted',
  `currency` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `tx` varchar(255) DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `user_id`, `amount`, `currency`, `item_name`, `tx`, `status`, `created_at`, `updated_at`) VALUES
(12, 19, 3, 'cc', 'params[:item_name]', 'params[:tx]', 1, '2017-12-30 07:59:31', '2017-12-30 07:59:31');

-- --------------------------------------------------------

--
-- Table structure for table `predefined_replies`
--

CREATE TABLE IF NOT EXISTS `predefined_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `predefined_replies`
--

INSERT INTO `predefined_replies` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'technical replay', 'fdssdfvvcfvvxcvxcvxcvcxxcxcxcvxcvxcxctrrrrvvcxvxcvxc', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'support replay', 'afdssddsvcxvcvjkiiiilooolovvvbvbvbfdsdd', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '', '', '2018-03-05 09:14:32', '2018-03-05 09:14:32'),
(4, '', '', '2018-03-10 09:24:59', '2018-03-10 09:24:59'),
(5, '', '', '2018-03-10 12:08:49', '2018-03-10 12:08:49'),
(6, '', '', '2018-03-10 12:12:51', '2018-03-10 12:12:51'),
(7, '', '', '2018-03-10 12:15:04', '2018-03-10 12:15:04'),
(8, '', '', '2018-03-12 11:11:55', '2018-03-12 11:11:55');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `caption` varchar(200) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `caption`, `created`, `status`) VALUES
(1, 'admin', 'admin', '2015-08-05 04:00:00', 1),
(2, 'client', 'client', '2015-08-05 04:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_privileges`
--

CREATE TABLE IF NOT EXISTS `role_privileges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `ticketadd` tinyint(1) NOT NULL,
  `ticketedit` tinyint(1) NOT NULL,
  `ticketdelete` tinyint(1) NOT NULL,
  `ticketclose` tinyint(1) NOT NULL,
  `ticketclosedreply` tinyint(1) NOT NULL,
  `department_id` varchar(150) NOT NULL DEFAULT '0',
  `setuserticket` tinyint(1) NOT NULL,
  `balanceadd` tinyint(1) NOT NULL,
  `balanceusersadd` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'allow user ato balance to other users',
  `balanceapprove` tinyint(1) NOT NULL DEFAULT '0',
  `serviceapprove` tinyint(1) DEFAULT NULL,
  `withdrawapprove` tinyint(1) NOT NULL DEFAULT '0',
  `cartsgenerate` tinyint(1) NOT NULL,
  `editinfo` tinyint(1) NOT NULL,
  `admincontact` tinyint(1) NOT NULL,
  `couponuse` tinyint(1) NOT NULL,
  `useractivate` tinyint(1) NOT NULL,
  `saleuse` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `role_privileges`
--

INSERT INTO `role_privileges` (`id`, `role_id`, `ticketadd`, `ticketedit`, `ticketdelete`, `ticketclose`, `ticketclosedreply`, `department_id`, `setuserticket`, `balanceadd`, `balanceusersadd`, `balanceapprove`, `serviceapprove`, `withdrawapprove`, `cartsgenerate`, `editinfo`, `admincontact`, `couponuse`, `useractivate`, `saleuse`, `created_at`, `modified_at`) VALUES
(5, 1, 1, 1, 1, 1, 1, '---- ''''- ''1''', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00'),
(6, 2, 1, 0, 0, 1, 1, '---- ''''- ''1''', 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, '2015-08-11 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `schema_migrations`
--

CREATE TABLE IF NOT EXISTS `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `schema_migrations`
--

INSERT INTO `schema_migrations` (`version`) VALUES
('20150810091733'),
('20150824104911'),
('20150919083020'),
('20170624091218'),
('20170624091350'),
('20170628074114'),
('20170703074552'),
('20170703110356'),
('20170704073258'),
('20170704074027'),
('20170704074032'),
('20170704074039'),
('20170704074045'),
('20170704074052'),
('20170705094152'),
('20170705123121'),
('20170705132831'),
('20170705132836'),
('20170705132839'),
('20170705132842'),
('20170705132845'),
('20170705132848'),
('20170709134321'),
('20170716115001'),
('20170716115504'),
('20170716115826'),
('20170723143835'),
('20170723144140'),
('20170724090025'),
('20170724103614'),
('20170724111700'),
('20170730134537'),
('20170731084327'),
('20170907115020'),
('20170912095222'),
('20170913075856'),
('20170913075937'),
('20170913075953'),
('20170918092651'),
('20170918093630'),
('20170919072429'),
('20170919075118'),
('20170920124753'),
('20170921093116'),
('20170921093332'),
('20170921094216'),
('20170921131004'),
('20170923090146'),
('20170923095215'),
('20170924142341'),
('20170924145345'),
('20170925075511'),
('20170925084722'),
('20170925085647'),
('20170925140739'),
('20171016141733'),
('20171118083415'),
('20171121103710'),
('20171121114841'),
('20171121120609'),
('20171223082700'),
('20171226122700'),
('20171226131830'),
('20171228094424'),
('20171230095008'),
('20180101091653'),
('20180102113116'),
('20180120134819'),
('20180120135325'),
('20180121113054'),
('20180205135616'),
('20180213091957'),
('20180213121744'),
('20180215110046'),
('20180220124714'),
('20180223014518'),
('20180311081225'),
('20180314130043'),
('20180315081602'),
('20180315082447'),
('20180327132259'),
('20180328090302'),
('20180328091735'),
('20180328094508'),
('20180329094413'),
('20180329095839'),
('20180329100040'),
('20180329100048'),
('20180329101358'),
('20180329101753'),
('20180329101801'),
('20180329101809'),
('20180329101817'),
('20180329103120'),
('20180329103128'),
('20180329103136'),
('20180329103143'),
('20180329131135'),
('20180402094937'),
('20180402143832');

-- --------------------------------------------------------

--
-- Table structure for table `security_questions`
--

CREATE TABLE IF NOT EXISTS `security_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `security_questions`
--

INSERT INTO `security_questions` (`id`, `locale`, `name`) VALUES
(1, 'en', 'What is your favorite team?\n'),
(2, 'en', 'What was your favorite food as a child?\n'),
(3, 'en', 'What school did you attend for sixth grade?\n'),
(4, 'en', 'What is your favorite movie?'),
(5, 'en', 'What was the name of the company where you had your first job?\n'),
(6, 'en', 'What was your childhood nickname?\n'),
(7, 'en', 'What was your favorite sport in high school?\n');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `caption` varchar(200) NOT NULL,
  `about` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `caption`, `about`, `status`, `price`) VALUES
(1, 'ask', 'gh', 'rtre', 0, 0),
(2, 'gsg', 'gg', 'gsrgr', 0, 0),
(3, 'sffg', 'fsf', 'fsg', 0, 20),
(5, 'oipup', 'upuipu', 'puip', 1, 40),
(6, 'test', 'test', 'test', 1, 12),
(7, 'test', 'test', 'test', 1, 12),
(8, 'test', 'test', 'test', 1, 12),
(9, 'test', 'test', 'test', 1, 12);

-- --------------------------------------------------------

--
-- Table structure for table `service_operations`
--

CREATE TABLE IF NOT EXISTS `service_operations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_number` varchar(200) DEFAULT NULL,
  `service_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `net_price` float NOT NULL,
  `hint` text NOT NULL,
  `approved` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=214 ;

--
-- Dumping data for table `service_operations`
--

INSERT INTO `service_operations` (`id`, `coupon_number`, `service_id`, `user_id`, `price`, `net_price`, `hint`, `approved`, `created_at`) VALUES
(126, 'zxcvbnm123', 3, 7, 20, 10, 'buy the service with coupon', '1', '2015-09-30 15:48:01'),
(129, 'zxcvbnm123', 3, 7, 20, 10, 'buy the service with coupon', '1', '2015-09-30 16:30:12'),
(130, '0', 2, 7, 0, 0, 'buy the service without coupon', '1', '2015-09-30 16:32:41'),
(131, '0', 3, 7, 20, 20, 'buy the service without coupon', '1', '2015-09-30 16:33:01'),
(132, '0', 3, 7, 20, 20, 'buy the service without coupon', '1', '2015-09-30 16:39:35'),
(193, 'zxcvbnm123', 3, 7, 20, 10, 'buy the service with coupon', '1', '2015-10-06 13:48:21'),
(204, 'zxcvbnm123', 3, 7, 20, 4, 'buy the service with coupon', '1', '2015-10-06 17:07:15'),
(205, '0', 3, 10, 20, 20, 'buy the service without coupon', '1', '2015-12-15 17:13:19'),
(206, '0', 1, 7, 0, 0, 'buy the service without coupon', '1', '2016-07-01 18:08:50'),
(207, '0', 1, 7, 0, 0, 'buy the service without coupon', '1', '2016-09-24 19:53:40'),
(208, '0', 1, 7, 0, 0, 'buy the service without coupon', '0', '2017-06-16 11:47:00'),
(209, '0', 1, 7, 0, 0, 'buy the service without coupon', '0', '2017-06-18 13:52:48'),
(210, '0', 1, 8, 0, 0, 'buy the service without coupon', '0', '2017-06-20 18:13:07'),
(211, 'zxcvbnm567', 3, 8, 20, 50, 'buy the service with coupon', '1', '2017-07-09 04:00:28'),
(212, '0', 1, 10, 0, 0, 'buy the service without coupon', '0', '2018-03-07 14:56:40'),
(213, '0', 1, 10, 0, 0, 'buy the service without coupon', '0', '2018-03-10 17:30:09');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `key` text,
  `value` text,
  `description` text,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `key`, `value`, `description`, `parent_id`) VALUES
(6, 'About Payerz', 'about', 'بايرز / Payerz', 'بايرز / Payerz', NULL),
(7, 'نسبة الشركة من التحويل الداخلي', 'transfer_ratio', '6', 'هنا يوضع نسبة التحويل من حساب لحساب داخل الموقع', NULL),
(8, 'اسم الموقع', 'sitename', 'بايرز', '', NULL),
(9, 'رابط Facebook', 'facebook', 'facebook', 'يمكنك التواصل معنا عبر هذا الهاتف . ', NULL),
(10, 'رابط twitter', 'twitter', 'twitter', '', NULL),
(11, 'رابط youtube', 'youtube', 'youtube', '', NULL),
(12, 'حقوق الموقع', 'copyrights', 'copyrights', '', NULL),
(13, 'meta decription', 'metadecription', 'meta decription', '', NULL),
(14, 'بريد الموقع 	', 'email', 'Admin@Payerz.Com', 'يمكنك التواصل معنا عبر هذه البريد . ', NULL),
(15, 'Terms ', 'Terms', 'Terms ', '', NULL),
(16, 'Phone ', 'phone', '201060000000', 'يمكنك التواصل معنا عبر هذه الهاتف . ', NULL),
(17, 'Address', 'adress ', 'Address ', '', NULL),
(18, 'اغلاق الموقع 	', 'siteclose', '1', 'شكراً لكم', NULL),
(19, 'رسالة إغلاق الموقع', 'msgclose', 'رسالة إغلاق الموقع', 'redfdfd', NULL),
(20, 'تفعيل العضوية ', 'useractivate', 'email', 'sms  Or email ', NULL),
(21, 'price of US dollar against Egyptian pound', 'USD TO EGP', '20', 'تحويل من دولار لجنية مصري', 27),
(22, 'price of US Dollar against Saudi Riyal', 'USD TO SAR', '3.6750', 'تحويل من دولار لريال سعودي', 27),
(23, 'price of Saudi Riyal against US Dollar', 'SAR TO USD', '0.2607', 'من ريال سعودي لدولار امريكي', 27),
(24, 'Price of Saudi Riyal against the Egyptian Pound', 'SAR TO EGP', '4.8608', 'من ريال سعودي لجنية مصري', 27),
(25, 'price of Egyptian pound against US dollar', 'EGP TO USD', '0.05', 'من جنية مصري لدولار امريكي', 27),
(26, 'Price of Egyptian Pound against Saudi Riyal', 'EGP TO SAR', '0.2078', 'من جنية مصري لريال سعودي', 27),
(27, 'currencies prices', 'currency_exchange', 'currencies prices', 'currencies prices', NULL),
(30, 'فترة تعليق الرصيد عند  المستلم', 'frozen balance', '2', 'يظل الرصيد معلقا عند المستلم لا يستطيع استلامه قبل مرور هذه المدة أو عمل كونفيرم من قبل المرسل', NULL),
(31, 'عرض التنبيهات لليوزر', 'notification', '1', '1 عرض التنبيهات - 0 اخفاء التنبيهات', NULL),
(32, 'تحدد  من سيقوم بتاكيد الطلب', 'Suspension_period', '1', '1--->  تحدد من قبل المستخدم\n2---> تحدد من قبل الادمن\n3---> المستخدم يؤكد الطلب بنفسه', NULL),
(33, 'حد التحويل العائلي', 'family_limit', '1000', 'الحد الاقصى المسموح به للمستخدم شهريا فى التحويل للاصدقاء والعائلة', NULL),
(34, 'الحد الادنى للتحويل', 'minimum_value', '10', 'الحد الادنى للمبلغ المسموح بتحويله فى المرة واحدة بالدولار  ', NULL),
(35, 'expected days to complete withdraw request', 'withdraw_date', '6', 'The number of days the withdraw request is expected to be completed', NULL),
(36, 'international dollars pounds', 'Universal USD TO EGP', '17.62', 'Convert from USD to EGP  ', 27),
(37, 'international dollars riyals', 'Universal USD TO SAR', '3.75', 'Convert from USD to SAR ', 27),
(38, 'international riyals dollar', 'Universal SAR TO USD', '0.266', 'Convert from SAR to USD', 27),
(39, 'international riyals pounds', 'Universal SAR TO EGP', '4.96', 'Convert from SAR to EGP', 27),
(40, 'international pounds dollar', 'Universal EGP TO USD', '0.0566', 'Convert from EGP to USD', 27),
(41, 'international Pound Riyal', 'Universal EGP TO SAR', '0.212 ', 'Convert from EGP to SAR', 27),
(42, 'price of US dollar against Egyptian pound for buying cart', 'Cart USD TO EGP', '17', 'تحويل من دولار لجنية مصري', NULL),
(43, 'price of US Dollar against Saudi Riyal for buying cart', 'Cart USD TO SAR', '4', 'تحويل من دولار لريال سعودي', NULL),
(44, 'number of days before payment request cancelled', 'cancel payment', '3', 'number of days before payment request cancelled', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foreign_id` int(11) DEFAULT NULL,
  `ticket_number` int(11) NOT NULL,
  `userd_id` int(11) DEFAULT '0',
  `title` text,
  `body` text,
  `approved` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `updated_reply` varchar(200) DEFAULT NULL,
  `hints` text,
  `department_id` int(11) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT '0',
  `predefined_reply_id` int(11) DEFAULT NULL COMMENT 'notification_id for payment request',
  `priority` varchar(150) DEFAULT NULL,
  `status` varchar(200) DEFAULT 'new ticket',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `attachment_file_name` varchar(255) DEFAULT NULL,
  `attachment_content_type` varchar(255) DEFAULT NULL,
  `attachment_file_size` int(11) DEFAULT NULL,
  `attachment_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `foreign_id`, `ticket_number`, `userd_id`, `title`, `body`, `approved`, `created_at`, `updated_at`, `updated_reply`, `hints`, `department_id`, `ticket_id`, `predefined_reply_id`, `priority`, `status`, `closed`, `attachment_file_name`, `attachment_content_type`, `attachment_file_size`, `attachment_updated_at`) VALUES
(1, NULL, 793190, 3, '222', '<p>14222<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/1f61b.svg);">&nbsp;</span>&nbsp;</p>', NULL, '2018-04-01 15:55:50', '2018-04-01 11:55:50', NULL, NULL, 1, 0, NULL, NULL, 'new ticket', 0, 'Tulips.jpg', 'image/jpeg', 620888, '2018-04-01 11:55:49'),
(2, 3, 244352, 3, NULL, 'تجربة استثناء', NULL, '2018-04-01 17:15:35', '2018-04-01 13:15:35', NULL, NULL, NULL, 0, 59, NULL, 'admin reply', 0, 'Chrysanthemum.jpg', 'image/jpeg', 879394, '2018-04-01 13:15:35'),
(3, 3, 233612, 3, NULL, 'vvvvvvvvvvvvvvvvvvvv', NULL, '2018-04-01 17:42:09', '2018-04-01 13:42:09', NULL, NULL, NULL, 2, 62, NULL, 'customer reply', 0, 'Koala.jpg', 'image/jpeg', 780831, '2018-04-01 13:42:09'),
(4, 4, 268226, 3, NULL, '', NULL, '2018-04-03 13:17:41', '2018-04-03 09:17:41', NULL, NULL, NULL, 0, 73, NULL, 'admin reply', 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tm_tasks`
--

CREATE TABLE IF NOT EXISTS `tm_tasks` (
  `id` mediumint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT 'Foreign key of "users"',
  `updated_by` int(11) DEFAULT NULL,
  `priority` varchar(30) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `message` text,
  `estimated_hours` decimal(6,1) DEFAULT NULL,
  `assign_to` int(11) DEFAULT NULL COMMENT 'Foreign Key of "users"',
  `due_date` date DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT '1' COMMENT '0-Inactive,1-Active',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tm_tasks_activities`
--

CREATE TABLE IF NOT EXISTS `tm_tasks_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tm_tasks_activities`
--

INSERT INTO `tm_tasks_activities` (`id`, `task_id`, `user_id`, `message`, `created`, `modified`) VALUES
(1, 1, 8, 'يسبسيبيسبسيبسبسيبسيبسيبسيب', '2016-10-01 11:03:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tm_tasks_completed`
--

CREATE TABLE IF NOT EXISTS `tm_tasks_completed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `intime` tinyint(1) NOT NULL,
  `latetime` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tm_tasks_completed`
--

INSERT INTO `tm_tasks_completed` (`id`, `task_id`, `intime`, `latetime`, `message`, `created`) VALUES
(1, 0, 0, 'asdasdsad', 'sadasdasdasdsa', '2016-10-01 12:33:04');

-- --------------------------------------------------------

--
-- Table structure for table `tm_tasks_notifications`
--

CREATE TABLE IF NOT EXISTS `tm_tasks_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `notify_in` date NOT NULL,
  `message` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foreign_id` int(11) DEFAULT NULL,
  `transaction_id` varchar(200) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_id_to` int(11) DEFAULT NULL,
  `value_from` float NOT NULL DEFAULT '0',
  `w_from_name` varchar(200) DEFAULT NULL,
  `w_from_balance_before` float NOT NULL DEFAULT '0',
  `w_from_balance_after` float NOT NULL DEFAULT '0',
  `value_to` float NOT NULL DEFAULT '0',
  `w_to_name` varchar(200) DEFAULT NULL COMMENT 'offline-pay  operation_id',
  `w_to_balance_before` float NOT NULL DEFAULT '0' COMMENT 'offline-pay user_must_paid',
  `w_to_balance_after` float NOT NULL DEFAULT '0' COMMENT 'offline-pay user_paid_value',
  `beforecharge` float NOT NULL DEFAULT '0' COMMENT 'refunded-withdraw',
  `controller` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `company_bank_data` varchar(2000) DEFAULT NULL,
  `user_bank_data` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `foreign_id`, `transaction_id`, `user_id`, `user_id_to`, `value_from`, `w_from_name`, `w_from_balance_before`, `w_from_balance_after`, `value_to`, `w_to_name`, `w_to_balance_before`, `w_to_balance_after`, `beforecharge`, `controller`, `description`, `company_bank_data`, `user_bank_data`, `created_at`, `updated_at`) VALUES
(1, NULL, 'WAVXNHECGJPD5IO1B2', 21, NULL, 1000, 'SAR', 10000, 9000, 260.7, 'USD', 10000, 10260.7, 0, 'users_wallets_balances/walletstransferpost', 'you have replaced/1000.0 SAR /with a value of/260.7 USD /Transaction ID/WAVXNHECGJPD5IO1B2', NULL, NULL, '2018-03-30 10:08:40', '2018-03-30 10:08:40'),
(2, 1, 'WBT84XRQ7WASJL6ZBV', 3, NULL, 100, 'USD', 10000, 9900, 90, 'Western Union', 0, 0, 0, 'balances_withdraws/created', 'withdraw request/100.0 USD /Transaction ID/WBT84XRQ7WASJL6ZBV', NULL, NULL, '2018-03-30 10:34:02', '2018-03-30 10:34:02'),
(3, 2, 'WBCXJUAKWE8094YVLZ', 3, NULL, 200, 'USD', 9900, 9700, 90, 'Western Union', 0, 0, 0, 'balances_withdraws/created', 'withdraw request/200.0 USD /Transaction ID/WBCXJUAKWE8094YVLZ', NULL, NULL, '2018-03-30 11:19:36', '2018-03-30 11:19:36'),
(4, 1, 'BTZFLU02IRA9MXQKV8', 1, 2, 1000, 'USD', 10000, 9000, 945, 'USD', 10000, 10000, 0, 'users_wallets_ballences_frozens/create', 'balance was transferred with value of/1000.0 USD/From User/linkOnline/to the user/alm7madi/Transaction ID/BTZFLU02IRA9MXQKV8', NULL, NULL, '2018-03-30 12:35:13', '2018-03-30 12:35:13'),
(5, 1, 'BTV0DXQWE76IBF3ZAO', 1, 2, 1000, 'USD', 9000, 9000, 945, 'USD', 10000, 10945, 0, 'users_wallets_ballences_frozens/approvedbalance', 'Balance Delivered/1000.0 USD/From User/linkOnline/to the user/alm7madi/Transaction ID/BTV0DXQWE76IBF3ZAO', NULL, NULL, '2018-03-30 12:40:19', '2018-03-30 12:40:19'),
(6, 3, 'WBJIC06A84DTEX2HYW', 3, NULL, 20, 'USD', 9700, 9680, 9, 'Western Union', 0, 0, 0, 'balances_withdraws/created', 'withdraw request/20.0 USD /Transaction ID/WBJIC06A84DTEX2HYW', NULL, NULL, '2018-03-30 13:17:12', '2018-03-30 13:17:12'),
(7, 4, 'WBD2UK37OQM5XSR9L0', 3, NULL, 20, 'USD', 9680, 9660, 9, 'Western Union', 0, 0, 0, 'balances_withdraws/created', 'withdraw request/20.0 USD /Transaction ID/WBD2UK37OQM5XSR9L0', NULL, NULL, '2018-03-30 13:26:29', '2018-03-30 13:26:29'),
(8, 5, 'WBP3XH10Q46AEWRVSL', 3, NULL, 20, 'USD', 9660, 9640, 9, 'Western Union', 0, 0, 0, 'balances_withdraws/created', 'withdraw request/20.0 USD /Transaction ID/WBP3XH10Q46AEWRVSL', NULL, NULL, '2018-03-30 13:32:35', '2018-03-30 13:32:35'),
(9, 6, 'WBQEW0P7UZ8VYK5FJ1', 3, NULL, 20, 'USD', 9640, 9620, 9, 'Western Union', 0, 0, 0, 'balances_withdraws/created', 'withdraw request/20.0 USD /Transaction ID/WBQEW0P7UZ8VYK5FJ1', NULL, NULL, '2018-03-30 13:33:44', '2018-03-30 13:33:44'),
(10, 6, 'WBDW0MHK2ZFG5QJCIN', 3, NULL, 20, 'USD', 9620, 9640, 9, NULL, 0, 0, 0, 'balances_withdraws/canceled', 'balancewithdraw canceled/20.0  USD/Transaction ID/ WBDW0MHK2ZFG5QJCIN', NULL, NULL, '2018-03-30 13:34:20', '2018-03-30 13:34:20'),
(11, NULL, 'WALIPYHVW6SDFQO8J0', 21, NULL, 2000, 'SAR', 9000, 7000, 521.4, 'USD', 10260.7, 10782.1, 0, 'users_wallets_balances/walletstransferpost', 'you have replaced/2000.0 SAR /with a value of/521.4 USD /Transaction ID/WALIPYHVW6SDFQO8J0', NULL, NULL, '2018-03-30 13:38:28', '2018-03-30 13:38:28'),
(12, 5, 'WB9KT437ZRPEQDAF8J', 3, NULL, 20, 'USD', 9640, 9640, 9, NULL, 0, 0, 0, 'balances_withdraws/completed', 'completed withdraw request/20.0  USD/Transaction ID/ WBP3XH10Q46AEWRVSL', NULL, NULL, '2018-03-30 13:39:37', '2018-03-30 13:39:37'),
(13, 4, 'WBXBLGDFY508MSIEQC', 3, NULL, 20, 'USD', 9640, 9660, 9, NULL, 0, 0, 0, 'balances_withdraws/rejected', 'rejected withdraw request/20.0  USD/Transaction ID/ WBD2UK37OQM5XSR9L0', NULL, NULL, '2018-03-30 13:40:11', '2018-03-30 13:40:11'),
(14, 3, 'WB2SLBPKC3NXUME5QZ', 3, NULL, 20, 'USD', 9660, 9677, 9, NULL, 0, 0, 3, 'balances_withdraws/refunded', 'refunded withdraw request/20.0  USD/Transaction ID/ WBJIC06A84DTEX2HYW', NULL, NULL, '2018-03-30 14:04:00', '2018-03-30 14:04:00'),
(15, NULL, 'WACKV907NBA81DTL3W', 3, NULL, 100, 'USD', 9677, 9577, 367.5, 'SAR', 10000, 10367.5, 0, 'users_wallets_balances/walletstransferpost', 'you have replaced/100.0 USD /with a value of/367.5 SAR /Transaction ID/WACKV907NBA81DTL3W', NULL, NULL, '2018-03-30 14:58:59', '2018-03-30 14:58:59'),
(16, 7, 'WBBOVHLFQM7C9X4YIG', 3, NULL, 20, 'USD', 9577, 9557, 9, 'Western Union', 0, 0, 0, 'balances_withdraws/created', 'withdraw request/20.0 USD /Transaction ID/WBBOVHLFQM7C9X4YIG', NULL, NULL, '2018-03-30 15:28:30', '2018-03-30 15:28:30'),
(17, 8, 'WB2PMX0CLAT3GD4OQE', 3, NULL, 22, 'USD', 9557, 9535, 10.9, 'Western Union', 0, 0, 0, 'balances_withdraws/created', 'withdraw request/22.0 USD /Transaction ID/WB2PMX0CLAT3GD4OQE', NULL, NULL, '2018-03-30 16:04:02', '2018-03-30 16:04:02'),
(18, 2, 'BTJUN78V6QY40P3ADG', 3, 20, 20, 'USD', 9535, 9515, 14, 'USD', 10000, 10000, 0, 'users_wallets_ballences_frozens/create', 'balance was transferred with value of/20.0 USD/From User/EngineeraAmiraa/to the user/engytoma/Transaction ID/BTJUN78V6QY40P3ADG', NULL, NULL, '2018-03-31 07:48:40', '2018-03-31 07:48:40'),
(19, 9, 'WBDL492NY61KTJSIPE', 3, NULL, 200, 'USD', 9515, 9315, 180, 'Western Union', 0, 0, 0, 'balances_withdraws/created', 'withdraw request/200.0 USD /Transaction ID/WBDL492NY61KTJSIPE', NULL, NULL, '2018-03-31 07:55:03', '2018-03-31 07:55:03'),
(20, 10, 'WBP0ARUVJGYHD2C98X', 3, NULL, 23, 'USD', 9315, 9292, 11.85, 'Western Union', 0, 0, 0, 'balances_withdraws/created', 'withdraw request/23.0 USD /Transaction ID/WBP0ARUVJGYHD2C98X', NULL, NULL, '2018-03-31 08:04:23', '2018-03-31 08:04:23'),
(21, 9, 'WBVX1805OT936ESWFU', 3, NULL, 200, 'USD', 9292, 9292, 180, NULL, 0, 0, 0, 'balances_withdraws/completed', 'completed withdraw request/200.0  USD/Transaction ID/ WBDL492NY61KTJSIPE', NULL, NULL, '2018-03-31 08:04:53', '2018-03-31 08:04:53'),
(22, 1, 'BT680MQ2TLZAXPCSRE', 1, 2, 1000, 'USD', 9000, 10000, 945, 'USD', 10945, 10000, 0, 'users_wallets_ballences_frozens/deleteOperation', 'balance refunded/1000.0 USD/from user/linkOnline/to user/alm7madi/Transactio ID/BT680MQ2TLZAXPCSRE', NULL, NULL, '2018-03-31 09:39:25', '2018-03-31 09:39:25'),
(23, 11, 'WB82XO1MIP4YTV6UK7', 3, NULL, 30, 'USD', 9292, 9262, 18.5, 'Western Union', 0, 0, 0, 'balances_withdraws/created', 'withdraw request/30.0 USD /Transaction ID/WB82XO1MIP4YTV6UK7', NULL, NULL, '2018-03-31 09:57:12', '2018-03-31 09:57:12'),
(24, 11, 'WBUTVH6OLCQ9NW1DMB', 3, NULL, 30, 'USD', 9262, 9292, 18.5, NULL, 0, 0, 0, 'balances_withdraws/canceled', 'balancewithdraw canceled/30.0  USD/Transaction ID/ WBUTVH6OLCQ9NW1DMB', NULL, NULL, '2018-03-31 09:57:18', '2018-03-31 09:57:18'),
(25, 3, 'BT7XAD5MPNW1JK0ZFY', 9, 3, 200, 'USD', 10000, 9800, 185, 'USD', 9292, 9292, 0, 'users_wallets_ballences_frozens/create', 'balance was transferred with value of/200.0 USD/From User/Amira/to the user/EngineeraAmiraa/Transaction ID/BT7XAD5MPNW1JK0ZFY', NULL, NULL, '2018-03-31 11:54:30', '2018-03-31 11:54:30'),
(26, 4, 'BT1MORGBZJW30Q756S', 9, 3, 20, 'USD', 9800, 9780, 15.2, 'USD', 9292, 9292, 0, 'users_wallets_ballences_frozens/create', 'balance was transferred with value of/20.0 USD/From User/Amira/to the user/EngineeraAmiraa/Transaction ID/BT1MORGBZJW30Q756S', NULL, NULL, '2018-03-31 11:55:08', '2018-03-31 11:55:08'),
(27, 5, 'BTR1YA6XNSQ8W5BPK7', 9, 3, 30, 'USD', 9780, 9750, 21.9, 'USD', 9292, 9313.9, 0, 'users_wallets_ballences_frozens/approvedbalance', 'Balance Delivered/30.0 USD/From User/Amira/to the user/EngineeraAmiraa/Transaction ID/BTR1YA6XNSQ8W5BPK7', NULL, NULL, '2018-03-31 11:55:52', '2018-03-31 11:55:52'),
(28, 6, 'BTM3850C7QXNWD2ORY', 3, 9, 200, 'USD', 9313.9, 9113.9, 180, 'USD', 9750, 9750, 0, 'users_wallets_ballences_frozens/create', 'balance was transferred with value of/200.0 USD/From User/EngineeraAmiraa/to the user/Amira/Transaction ID/BTM3850C7QXNWD2ORY', NULL, NULL, '2018-03-31 11:56:58', '2018-03-31 11:56:58'),
(29, 7, 'BTZF5QV27TXIG34BUS', 3, 9, 200, 'USD', 9113.9, 8913.9, 180, 'USD', 9750, 9750, 0, 'users_wallets_ballences_frozens/create', 'balance was transferred with value of/200.0 USD/From User/EngineeraAmiraa/to the user/Amira/Transaction ID/BTZF5QV27TXIG34BUS', NULL, NULL, '2018-03-31 11:58:46', '2018-03-31 11:58:46'),
(30, 3, 'BTOAP7V5BK2MRGX4Q0', 9, 3, 200, 'USD', 9750, 9950, 185, 'USD', 8913.9, 8913.9, 0, 'users_wallets_ballences_frozens/deleteOperation', 'balance refunded/200.0 USD/from user/Amira/to user/EngineeraAmiraa/Transactio ID/BTOAP7V5BK2MRGX4Q0', NULL, NULL, '2018-03-31 11:59:20', '2018-03-31 11:59:20'),
(31, 6, 'BTUI2XL0OG9A3S7RN1', 3, 9, 200, 'USD', 8913.9, 8913.9, 180, 'USD', 9950, 10130, 0, 'users_wallets_ballences_frozens/approvedbalance', 'Balance Delivered/200.0 USD/From User/EngineeraAmiraa/to the user/Amira/Transaction ID/BTUI2XL0OG9A3S7RN1', NULL, NULL, '2018-03-31 11:59:44', '2018-03-31 11:59:44'),
(32, 8, 'BT5LQUSCG8RHWIVTE9', 9, 3, 40, 'USD', 10130, 10090, 33, 'USD', 8913.9, 8913.9, 0, 'users_wallets_ballences_frozens/create', 'balance was transferred with value of/40.0 USD/From User/Amira/to the user/EngineeraAmiraa/Transaction ID/BT5LQUSCG8RHWIVTE9', NULL, NULL, '2018-03-31 12:02:37', '2018-03-31 12:02:37'),
(33, 8, 'BT3XFITR0C1HLSBKU8', 9, 3, 40, 'USD', 10090, 10130, 33, 'USD', 8913.9, 8913.9, 0, 'users_wallets_ballences_frozens/deleteOperation', 'balance refunded/40.0 USD/from user/Amira/to user/EngineeraAmiraa/Transactio ID/BT3XFITR0C1HLSBKU8', NULL, NULL, '2018-03-31 12:03:13', '2018-03-31 12:03:13'),
(34, 1, 'BCCGVXD0E34R6LPF8J', 3, NULL, 450, 'USD', 0, 0, 0, 'PayPal', 0, 0, 0, 'carts/paypalconfirm', 'A credit card has been purchased/450.0 USD', NULL, NULL, '2018-03-31 13:20:43', '2018-03-31 13:20:43'),
(35, 1, 'ABHK4LW9817XFZIVPT', 3, NULL, 450, 'USD', 0, 0, 0, 'PayPal', 0, 9363.9, 8913.9, 'carts/cartbalancetouser', 'you have added balance with a value of/450.0 USD', NULL, NULL, '2018-03-31 13:21:44', '2018-03-31 13:21:44'),
(36, 2, 'BCYBWJN6QP5TR2MHSC', 3, NULL, 450, 'USD', 0, 0, 0, 'PayPal', 0, 0, 0, 'carts/paypalconfirm', 'A credit card has been purchased/450.0 USD', NULL, NULL, '2018-03-31 13:30:30', '2018-03-31 13:30:30'),
(37, 2, 'ABY0JOTHL93WGM2XKS', 3, NULL, 450, 'USD', 0, 0, 0, 'PayPal', 0, 9813.9, 9363.9, 'carts/cartbalancetouser', 'you have added balance with a value of/450.0 USD', NULL, NULL, '2018-03-31 13:30:57', '2018-03-31 13:30:57'),
(38, NULL, 'WA04DZXTHCJANKSUPG', 3, NULL, 2000, 'EGP', 10000, 8000, 415.6, 'SAR', 10367.5, 10783.1, 0, 'users_wallets_balances/walletstransferpost', 'you have replaced/2000.0 EGP /with a value of/415.6 SAR /Transaction ID/WA04DZXTHCJANKSUPG', NULL, NULL, '2018-03-31 13:41:48', '2018-03-31 13:41:48'),
(39, NULL, 'WAQZP1UB3GHVTYR7OW', 2, NULL, 1000, 'USD', 10000, 9000, 3675, 'SAR', 10000, 13675, 0, 'users_wallets_balances/walletstransferpost', 'you have replaced/1000.0 USD /with a value of/3675.0 SAR /Transaction ID/WAQZP1UB3GHVTYR7OW', NULL, NULL, '2018-03-31 23:52:17', '2018-03-31 23:52:17'),
(40, NULL, 'WASMTZH7XVU39L2F8R', 2, NULL, 1000, 'USD', 9000, 8000, 17267.6, 'EGP', 10000, 27267.6, 0, 'users_wallets_balances/walletstransferpost', 'you have replaced/1000.0 USD /with a value of/17267.6 EGP /Transaction ID/WASMTZH7XVU39L2F8R', NULL, NULL, '2018-03-31 23:53:01', '2018-03-31 23:53:01'),
(41, NULL, 'WAEDKBWSOQ21GM0YAC', 21, NULL, 300, 'SAR', 7000, 6700, 78.21, 'USD', 10782.1, 10860.3, 0, 'users_wallets_balances/walletstransferpost', 'you have replaced/300.0 SAR /with a value of/78.21 USD /Transaction ID/WAEDKBWSOQ21GM0YAC', NULL, NULL, '2018-04-01 10:08:24', '2018-04-01 10:08:24'),
(42, NULL, 'WATB82VZFIMGH9CJUN', 9, NULL, 22, 'USD', 10130, 10108, 80.85, 'SAR', 10000, 10080.8, 0, 'users_wallets_balances/walletstransferpost', 'you have replaced/22.0 USD /with a value of/80.85 SAR /Transaction ID/WATB82VZFIMGH9CJUN', NULL, NULL, '2018-04-01 11:49:39', '2018-04-01 11:49:39'),
(43, 12, 'WBSJM5X0AN1GFOPDWH', 3, NULL, 22, 'USD', 9813.9, 9791.9, 10.9, 'Western Union', 0, 0, 0, 'balances_withdraws/created', 'withdraw request/22.0 USD /Transaction ID/WBSJM5X0AN1GFOPDWH', NULL, NULL, '2018-04-01 12:05:37', '2018-04-01 12:05:37'),
(44, 3, '140238', 3, NULL, 104, 'USD', 0, 0, 100, NULL, 104, 200, 0, 'Onlinebanks/banktransfer_post', 'payment request/140238/addvalue/100.0', 'interusd_swift/bank name/ABU DHABI ISLAMIC BANK - EGYPT/currency/USD/Fees/3.0/ratio/1.0/country/Egypt/Branch Name/GARDEN CITY/branch code/12345/account name/service high/account number/123456/swift code/ABDIEGCA', NULL, '2018-04-01 13:01:50', '2018-04-01 13:02:13'),
(45, NULL, 'WACINKE59OAX17LYPH', 1, NULL, 10000, 'EGP', 10000, 0, 555, 'USD', 10000, 10555, 0, 'users_wallets_balances/walletstransferpost', 'you have replaced/10000.0 EGP /with a value of/555.0 USD /Transaction ID/WACINKE59OAX17LYPH', NULL, NULL, '2018-04-01 13:21:09', '2018-04-01 13:21:09'),
(46, NULL, 'WAOSHCD83KI7XJ2MRY', 1, NULL, 10000, 'SAR', 10000, 0, 2607, 'USD', 10555, 13162, 0, 'users_wallets_balances/walletstransferpost', 'you have replaced/10000.0 SAR /with a value of/2607.0 USD /Transaction ID/WAOSHCD83KI7XJ2MRY', NULL, NULL, '2018-04-01 13:21:29', '2018-04-01 13:21:29'),
(47, 3, 'BCV0UM683GNHWLEPIC', 3, NULL, 100, NULL, 0, 0, 90, 'OfflinePayment', 0, 0, 0, 'carts/offline_payment', 'A credit card has been purchased/90.0 USD', NULL, NULL, '2018-04-01 14:05:38', '2018-04-01 14:05:38'),
(48, 4, '653801', 3, NULL, 53.5, 'USD', 0, 0, 50, NULL, 53.5, 0, 0, 'Onlinebanks/banktransfer_post', 'payment request/653801/addvalue/50.0', 'interusd_swift/bank name/ABU DHABI ISLAMIC BANK - EGYPT/currency/USD/Fees/3.0/ratio/1.0/country/Egypt/Branch Name/GARDEN CITY/branch code/12345/account name/service high/account number/123456/swift code/ABDIEGCA', NULL, '2018-04-01 23:08:54', '2018-04-01 23:08:54'),
(49, 9, 'BTM93FYO47R2AZLU5V', 2, 1, 99.09, 'USD', 8000, 7900.91, 91.1264, 'USD', 13162, 13162, 0, 'users_wallets_ballences_frozens/create', 'balance was transferred with value of/99.09 USD/From User/alm7madi/to the user/linkOnline/Transaction ID/BTM93FYO47R2AZLU5V', NULL, NULL, '2018-04-02 09:49:33', '2018-04-02 09:49:33'),
(50, 13, 'WBH28T0VXPSZNO9MJG', 5, NULL, 93, 'USD', 10000, 9907, 68.35, 'تحويل بنكي دولار امريكي', 0, 0, 0, 'balances_withdraws/created', 'withdraw request/93.0  USD/Transaction ID/WBH28T0VXPSZNO9MJG', NULL, NULL, '2018-04-02 15:54:09', '2018-04-02 15:54:09'),
(51, 2, 'BTLG4X38JBNC9T1AEK', 3, 20, 20, 'USD', 9791.9, 9791.9, 14, 'USD', 10000, 10014, 0, 'users_wallets_ballences_frozens/approvedbalance', 'Balance Delivered/20.0 USD/From User/EngineeraAmiraa/to the user/engytoma/Transaction ID/BTLG4X38JBNC9T1AEK', NULL, NULL, '2018-04-02 16:00:05', '2018-04-02 16:00:05'),
(52, 4, 'BT59ZAIR0CQE6BXOKP', 9, 3, 20, 'USD', 10108, 10108, 15.2, 'USD', 9791.9, 9807.1, 0, 'users_wallets_ballences_frozens/approvedbalance', 'Balance Delivered/20.0 USD/From User/Amira/to the user/EngineeraAmiraa/Transaction ID/BT59ZAIR0CQE6BXOKP', NULL, NULL, '2018-04-02 16:00:11', '2018-04-02 16:00:11'),
(53, 5, '736459', 3, NULL, 508, 'USD', 0, 0, 500, NULL, 508, 0, 0, 'Onlinebanks/banktransfer_post', 'payment request/736459/addvalue/500.0', 'interusd_swift/bank name/ABU DHABI ISLAMIC BANK - EGYPT/currency/USD/Fees/3.0/ratio/1.0/country/Egypt/Branch Name/GARDEN CITY/branch code/12345/account name/service high/account number/123456/swift code/ABDIEGCA', NULL, '2018-04-03 09:28:40', '2018-04-03 09:28:41'),
(54, 3, 'BC5XJSLOHEI4KY9GU1', NULL, NULL, 90, NULL, 0, 0, 0, 'adminpayment', 0, 0, 0, 'carts/change_status ', 'A credit card has been disabled/90.0 USD', NULL, NULL, '2018-04-03 09:48:58', '2018-04-03 09:48:58'),
(55, 12, 'BTU0XOB6EGWH5RYTVP', 9, 3, 20, 'USD', 10108, 10128, 15.2, 'USD', 9807.1, 9807.1, 0, 'users_wallets_ballences_frozens/deleteOperation', 'balance refunded/20.0 USD/from user/Amira/to user/EngineeraAmiraa/Transactio ID/BTU0XOB6EGWH5RYTVP', NULL, NULL, '2018-04-03 10:34:32', '2018-04-03 10:34:32'),
(56, 7, 'BT4G59TFYRHDAIZUOE', 3, 9, 200, 'USD', 9807.1, 10007.1, 180, 'USD', 10128, 10128, 0, 'users_wallets_ballences_frozens/deleteOperation', 'balance refunded/200.0 USD/from user/EngineeraAmiraa/to user/Amira/Transactio ID/BT4G59TFYRHDAIZUOE', NULL, NULL, '2018-04-03 10:35:03', '2018-04-03 10:35:03'),
(57, 4, 'BC1984TAJHVYSCX23W', NULL, NULL, 85, NULL, 0, 0, 0, 'adminpayment', 0, 0, 0, 'carts/adminpayment ', 'A credit card has been purchased/85.0 USD', NULL, NULL, '2018-04-03 13:42:40', '2018-04-03 13:42:40'),
(58, 5, 'BCUZ78S5AFD6N1HVWM', NULL, NULL, 81, NULL, 0, 0, 0, 'adminpayment', 0, 0, 0, 'carts/adminpayment ', 'A credit card has been purchased/81.0 USD', NULL, NULL, '2018-04-03 13:43:26', '2018-04-03 13:43:26'),
(59, 48, 'BT2XA6K0FYRP5DGHWZ', 1, 2, 10, 'USD', 13162, 13162, 4.5, 'USD', 7900.91, 7905.41, 0, 'users_wallets_ballences_frozens/approvedbalance', 'Balance Transfer has been confirmed with value/10.0 USD/from user/linkOnline/to user/alm7madi/Transactio ID/BT2XA6K0FYRP5DGHWZ', NULL, NULL, '2018-04-03 15:13:16', '2018-04-03 15:13:16');

-- --------------------------------------------------------

--
-- Table structure for table `userds`
--

CREATE TABLE IF NOT EXISTS `userds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `f_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `l_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `is_company` int(11) DEFAULT '0',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `password_changed_at` datetime DEFAULT NULL,
  `security_question_id` int(11) DEFAULT NULL,
  `security_question_answer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `failed_attempts` int(11) NOT NULL DEFAULT '0',
  `unlock_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `confirmation_sent_at` datetime DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locked_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `last_activity_at` datetime DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationality` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_family_limit` float NOT NULL DEFAULT '0',
  `current_max_family_recieve` float NOT NULL DEFAULT '0',
  `role_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `nationalid` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalid_type` int(2) DEFAULT NULL,
  `locale` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ar',
  `active` int(1) DEFAULT '0',
  `active_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_file_size` int(11) DEFAULT NULL,
  `avatar_updated_at` datetime DEFAULT NULL,
  `attachments_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachments_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachments_file_size` int(11) DEFAULT NULL,
  `attachments_updated_at` datetime DEFAULT NULL,
  `unique_session_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gauth_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gauth_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gauth_enabled` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'f',
  `gauth_tmp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gauth_tmp_datetime` datetime DEFAULT NULL,
  `second_factor_attempts_count` int(11) DEFAULT '0',
  `encrypted_otp_secret_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `encrypted_otp_secret_key_iv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `encrypted_otp_secret_key_salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `otp_secret_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active_otp_secret` int(255) NOT NULL DEFAULT '0',
  `sms_active` int(11) NOT NULL DEFAULT '0',
  `hard_lock` int(2) NOT NULL DEFAULT '0' COMMENT '0-active, 1-locked',
  `pin_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `backup_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `all_verified` int(2) NOT NULL DEFAULT '0',
  `invitation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invitation_created_at` datetime DEFAULT NULL,
  `invitation_sent_at` datetime DEFAULT NULL,
  `invitation_accepted_at` datetime DEFAULT NULL,
  `invitation_limit` int(11) DEFAULT NULL,
  `invited_by_id` int(11) DEFAULT NULL,
  `invited_by_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invitations_count` int(11) DEFAULT '0',
  `time_zone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `test` int(11) DEFAULT NULL,
  `month_withdraw_count` int(11) DEFAULT NULL,
  `month_withdraw_value` float DEFAULT NULL,
  `day_withdraw_count` int(11) DEFAULT NULL,
  `day_withdraw_value` float DEFAULT NULL,
  `d_withdraw_count` int(11) DEFAULT '0',
  `d_withdraw_value` float DEFAULT '0',
  `m_withdraw_count` int(11) DEFAULT '0',
  `m_withdraw_value` float DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_userds_on_email` (`email`),
  UNIQUE KEY `index_userds_on_reset_password_token` (`reset_password_token`),
  UNIQUE KEY `index_userds_on_encrypted_otp_secret_key` (`encrypted_otp_secret_key`),
  UNIQUE KEY `index_userds_on_otp_secret_key` (`otp_secret_key`),
  UNIQUE KEY `index_userds_on_invitation_token` (`invitation_token`),
  KEY `index_userds_on_invitations_count` (`invitations_count`),
  KEY `index_userds_on_invited_by_id` (`invited_by_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Dumping data for table `userds`
--

INSERT INTO `userds` (`id`, `email`, `f_name`, `l_name`, `is_company`, `encrypted_password`, `reset_password_token`, `reset_password_sent_at`, `remember_created_at`, `password_changed_at`, `security_question_id`, `security_question_answer`, `sign_in_count`, `current_sign_in_at`, `last_sign_in_at`, `current_sign_in_ip`, `last_sign_in_ip`, `failed_attempts`, `unlock_token`, `confirmation_token`, `confirmed_at`, `confirmation_sent_at`, `unconfirmed_email`, `locked_at`, `created_at`, `updated_at`, `deleted_at`, `last_activity_at`, `expired_at`, `country`, `nationality`, `city`, `postal_code`, `facebook_id`, `username`, `tel`, `current_family_limit`, `current_max_family_recieve`, `role_id`, `group_id`, `nationalid`, `nationalid_type`, `locale`, `active`, `active_code`, `avatar_file_name`, `avatar_content_type`, `avatar_file_size`, `avatar_updated_at`, `attachments_file_name`, `attachments_content_type`, `attachments_file_size`, `attachments_updated_at`, `unique_session_id`, `gauth_secret`, `gauth_token`, `gauth_enabled`, `gauth_tmp`, `gauth_tmp_datetime`, `second_factor_attempts_count`, `encrypted_otp_secret_key`, `encrypted_otp_secret_key_iv`, `encrypted_otp_secret_key_salt`, `otp_secret_key`, `active_otp_secret`, `sms_active`, `hard_lock`, `pin_id`, `backup_code`, `account_number`, `all_verified`, `invitation_token`, `invitation_created_at`, `invitation_sent_at`, `invitation_accepted_at`, `invitation_limit`, `invited_by_id`, `invited_by_type`, `invitations_count`, `time_zone`, `test`, `month_withdraw_count`, `month_withdraw_value`, `day_withdraw_count`, `day_withdraw_value`, `d_withdraw_count`, `d_withdraw_value`, `m_withdraw_count`, `m_withdraw_value`) VALUES
(1, 'ahmedjumaaobaid@gmail.com', 'Ahmed', 'Aly', 0, '$2a$11$lJfxqNrUH8QWRo0micxP1u9y4iLyfltpzXbLND5VltmX.kCyHDrEa', '2649d4130bc488233f013cb0a752e0c5a853f6f5e594fafa93884873a7fa5b22', '2018-03-28 11:31:55', NULL, '2018-01-23 19:01:08', NULL, NULL, 120, '2018-04-03 14:57:46', '2018-04-03 00:01:03', '197.45.219.97', '41.68.40.35', 0, NULL, 'xVznyZrfCs4HxXQUnedg', '2018-01-23 19:01:19', '2018-02-03 12:48:11', 'ahmedjumaaobaid@gmail.com', NULL, '2018-01-23 19:01:08', '2018-04-03 14:57:46', NULL, '2018-04-04 10:17:47', NULL, 'EG', 'Egyptian', 'Al Jizah', '2255577', NULL, 'linkOnline', '+201020822220', 0, 0, 2, 4, '1224533612', 1, 'ar', 0, NULL, '322072_269280246465107_563812545_o.jpg', 'image/jpeg', 87781, '2018-01-23 19:48:02', NULL, NULL, NULL, NULL, NULL, 'k6gdhywygmlmaoo4rlxevxg5pp4w7w4szpihu7b6t2z5s43qfld4dnrh7a4baroh', NULL, '0', 'es37p2bzd5dcdcbfoncfkp2m3hy5kah5', '2018-03-30 05:03:05', 0, NULL, NULL, NULL, '23aob5rrsqvzmknb', 0, 0, 0, 'A4D4379ECEEBD5F2959085001F7C66A3', '527694', 'PS808485381', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Cairo', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(2, 'alaa.mehmadi@gmail.com', 'Alaa', 'Almhmadi', 0, '$2a$11$5ZV31fSaDBO7HiIQuKo5Q.UigJ96B28Pidoe6JzS4O9mQfuBNKoAq', NULL, NULL, NULL, '2018-01-23 19:04:52', NULL, NULL, 36, '2018-04-03 08:40:42', '2018-04-02 09:44:34', '95.185.146.197', '176.47.48.91', 0, NULL, 'L4xbVsxYHfRjg6WqPLVQ', '2018-01-23 19:05:40', '2018-01-23 19:04:52', NULL, NULL, '2018-01-23 19:04:52', '2018-04-03 08:40:42', NULL, '2018-04-03 08:40:43', NULL, 'SA', 'Saudi', 'Jeddah', '21353', NULL, 'alm7madi', '+966569699979', 0, 0, 2, 5, NULL, 0, 'ar', 0, NULL, 'لقطة_الشاشة_2018-03-27_في_9.36.26 م.png', 'image/png', 12963, '2018-03-27 18:36:57', NULL, NULL, NULL, NULL, NULL, 'cxjjly4b44aifzekwyxval2lvpmyhl2nzl7akmbal3nzjo7s2pe6f3jus3cujszx', NULL, '0', '6y6noghru7piyns2bepzazlr4qbhktmq', '2018-02-12 13:14:55', 0, NULL, NULL, NULL, 'sdi6lx7uchteamtf', 0, 0, 0, '0A4870CCA5F879886BF39549B53C9729', '123456', 'PS281877104', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Riyadh', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(3, 'amira.elfayome@servicehigh.com', 'Eng', 'Amira', 0, '$2a$11$Lx9hvzxFlu37jDorNRGwVORZ5GmOzoXT1ZcljYaDzL7jN8Q1rw5AC', 'dfdba329cf26eec509c57878342314ef661c4c0fb5ba479f788f1841604ed5e0', '2018-03-24 11:15:35', NULL, '2018-01-23 19:23:13', NULL, NULL, 213, '2018-04-04 07:51:17', '2018-04-03 18:41:57', '196.221.41.202', '156.199.219.93', 0, NULL, 'VkeYb8GpAD8UXTppXpLt', '2018-01-23 19:24:37', '2018-02-04 12:13:19', 'amira.elfayome@servicehigh.com', NULL, '2018-01-23 19:23:13', '2018-04-04 08:00:04', NULL, '2018-04-04 10:11:50', NULL, 'EG', 'Austrian', 'دمياط', '22022', NULL, 'EngineeraAmiraa', '+201007460059', 0, 0, 2, 5, '21453617893214', 0, 'ar', 0, NULL, '17553419_1749947761963372_7859631055446362296_n.jpg', 'image/jpeg', 29413, '2018-02-08 14:20:12', NULL, NULL, NULL, NULL, NULL, '3pgvowa5wco3rzzqvz74ijeen2lz6aq5kehmlc2v3zknqmnovvmw7gewgtorgr3c', NULL, '0', 'rij64hofdmd3fmhq6iht4j2hxhc5wy5r', '2018-01-29 12:37:31', 0, NULL, NULL, NULL, 'xkkb4car66rujtrl', 0, 0, 0, 'F85FB6B7FFA9D468DF71E3B4732204CF', '832051', 'PS894679173', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Cairo', 24, NULL, NULL, 0, 0, 0, 0, 201, 30000),
(4, 'boshkash1993@gmail.com', 'Khalid', 'Khalil', 0, '$2a$11$xaGqNif4UlpB1OdDiw4rbOeS12jjszI/72pCj2P4E8XKtVmJ4gfoW', 'a444e187f5fbcd6d07c116313b74ccc9a4491f6b726c8405ca82b7d5cabe4e45', '2018-03-24 14:49:24', NULL, '2018-01-24 08:25:52', NULL, NULL, 192, '2018-03-28 08:54:36', '2018-03-27 14:51:53', '156.198.61.56', '156.198.61.56', 0, NULL, 'qf42UyiVxrUBrG1xtVH-', '2018-01-24 08:26:17', '2018-01-24 08:25:52', NULL, NULL, '2018-01-24 08:25:52', '2018-03-30 08:00:05', NULL, '2018-03-28 08:54:36', NULL, 'EG', 'Egyptian', 'دمياط', '1234589', NULL, 'khalidkhalil', '+201146700763', 0, 0, 2, 5, '19935478258551', 0, 'ar', 0, NULL, 'test-image.jpeg', 'image/jpeg', 60892, '2018-03-14 14:45:08', NULL, NULL, NULL, NULL, NULL, 'br6ydvevnytu5cxunrhpyspc3snxulogpft5qzil5kn3r7h2tna3b454s2s25haj', NULL, '0', 'dtuzlz3qzvrt7sdzruuro7anfxlozhah', '2018-03-13 14:47:52', 0, NULL, NULL, NULL, 'ip33oaapptrxerkc', 0, 0, 0, '80B2D980F69BD9FB5B19C671F477E3C8', '524706', 'PS107553645', 1, NULL, NULL, NULL, NULL, 3, NULL, NULL, 0, 'Hawaii', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(5, 'lawyer.amir@gmail.com', 'Amir', 'Zano', 0, '$2a$11$jnfUwyBdR8YqdmaCy4wfsO3XruFHehznb98eqdY2cEMdesd3MvbkS', '4e5ab43f08fe7b56c2d6f149934b5085fa2bb0fcc98d44fedd09a9b16f0cc1c4', '2018-01-27 20:14:39', NULL, '2018-01-24 09:28:53', NULL, NULL, 37, '2018-04-02 14:57:27', '2018-04-02 13:20:25', '41.44.46.196', '41.44.46.196', 0, NULL, 'ysz2KXydFjMGobrpv_9B', '2018-01-24 09:29:22', '2018-01-24 09:28:53', NULL, NULL, '2018-01-24 09:28:53', '2018-04-03 08:00:05', NULL, '2018-04-02 16:00:02', NULL, 'EG', 'Egyptian', 'دمياط', '25252', NULL, 'amir', '+201000254021', 0, 0, 2, 5, '254864125445', 1, 'ar', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'xgct53bws2qkthq6a3ceofxqwitqs6hwlqpca7ur4ofsf2dc555ck72nhxh2ug53', NULL, '0', '3xrhmzk7b2hgugt72noywqg34qmzdc3f', '2018-04-02 14:43:44', 0, NULL, NULL, NULL, 'dntihwqgd3txkplq', 0, 0, 0, '5391AD319BF56E67823357D44D0A3716', '123456', 'PS342516810', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, 93),
(6, 'sami@payers.net', 'Sami', 'Ali', 0, '$2a$11$y/0//mD18RihzGT6aFSLPu42JRvElbKhbeT8.pV74fWAWjwxyvBNu', NULL, NULL, '2018-04-04 09:30:31', '2018-01-24 09:28:57', NULL, NULL, 116, '2018-04-04 09:30:31', '2018-04-04 09:30:01', '196.221.41.202', '196.221.41.202', 0, NULL, 'pwQnNCjDZazPgj3KKh5x', '2018-01-24 09:29:33', '2018-03-10 14:35:56', 'payforme22@gmail.com', NULL, '2018-01-24 09:28:57', '2018-04-04 09:30:33', NULL, '2018-04-04 09:40:02', NULL, 'EG', 'Egyptian', 'رأس البر', '5151', NULL, 'smsmnet', '+201011226440', 0, 0, 2, 5, '12345698', 1, 'ar', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '32y6yja6rwdqdzrbrlrjmjfmoint26qzrkrlyffppr2w4kvcrom4ifdanwuwrfcg', NULL, '0', 'x63cidogbck3mcl322umgfy3ddgqkehn', '2018-02-01 14:41:28', 0, NULL, NULL, NULL, 'or6yaebrztrs6bqe', 0, 1, 0, '0C6A79346B3D718A8431C6D8D228CC6D', '324790', 'PS187637252', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Cairo', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(7, 'faten.alsaba2057@gmail.com', 'tona', 'mohamed', 0, '$2a$11$ZopJ6cIZi/7MCAEsVA721.nSxAgJUXlRoC1XJccMGQoqe/J3oUnRG', NULL, NULL, NULL, '2018-01-25 11:25:32', NULL, NULL, 17, '2018-04-03 07:45:32', '2018-04-02 08:49:02', '41.44.46.196', '156.198.234.146', 0, NULL, 'yVEKMn1J9zS8y3y1r7pz', '2018-01-25 11:26:10', '2018-03-05 13:57:29', 'faten.alsaba2057@gmail.com', NULL, '2018-01-25 11:25:32', '2018-04-04 10:30:14', NULL, '2018-04-03 14:27:33', NULL, 'SA', 'Egyptian', 'دمياط الجديدة', '123', NULL, 'Tona', '+01013781097', 310, 0, 2, 1, '78354612345678', 0, 'ar', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '75t5co6634ui7xzkqjz2lujsqnb4vlmfdmlv2awydii6a54ugnhjiw7z63tporlj', NULL, '0', 'rx25xk52vwdbnwkgrrdn3eqtkkl77i3e', '2018-02-14 12:47:57', 0, NULL, NULL, NULL, 'mska2fzfqosqqcx6', 0, 0, 0, 'D8D35011F941C252300AFE485212C4B7', '123456', 'PS710569595', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Hawaii', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(8, 'helal854@gmail.com', NULL, NULL, 0, '$2a$11$O3QmKLT0sxZeZtciFBLpKeV3keUX8ol0kql2i57WZ3y5Hsg4oyb9m', NULL, NULL, '2018-01-25 16:06:18', '2018-01-25 16:03:10', NULL, NULL, 1, '2018-01-25 16:06:18', '2018-01-25 16:06:18', '2.51.135.50', '2.51.135.50', 0, NULL, 'h5EuxHUGyyKEBbuTT8gB', '2018-01-25 16:05:51', '2018-01-25 16:03:10', NULL, NULL, '2018-01-25 16:03:10', '2018-03-30 08:00:05', NULL, '2018-01-26 06:45:26', NULL, 'EG', NULL, NULL, NULL, NULL, 'Helal854', '+971561150298', 0, 0, 2, 8, NULL, NULL, 'ar', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5pmoyeldilqsy5qcj5xkydpd4pqmmdinj76map644y52humzv2urn55j4bjd4gss', NULL, '0', NULL, NULL, 0, NULL, NULL, NULL, '2b27ly5jzldj2ntb', 0, 0, 0, '0C9FA05032987E86415FEA97671A13FF', '123456', 'PS325195184', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(9, 'eng.amira333@gmail.com', NULL, NULL, 0, '$2a$11$jsazVluHXXaUfyrIOUiRm.gn0phINdkBxA6JuJ3fUz4scK/myTmtW', NULL, NULL, NULL, '2018-01-25 19:37:56', NULL, NULL, 12, '2018-04-01 08:35:01', '2018-03-31 11:53:38', '196.221.117.199', '196.221.117.199', 0, NULL, 'iLDss6xbM2muUXoTfSsx', '2018-01-25 19:38:44', '2018-01-25 19:37:56', NULL, NULL, '2018-01-25 19:37:56', '2018-04-01 08:35:01', NULL, '2018-04-01 13:59:53', NULL, 'EG', NULL, NULL, NULL, NULL, 'Amira', '+201007460059', 0, 0, 2, 8, NULL, NULL, 'ar', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'cswh5pjqsz4fu73l4hxk5ttzl3p6gtuhzuilv3ou52dgrjzkkfyjo2kn7ihmpytb', NULL, '0', NULL, NULL, 0, NULL, NULL, NULL, 'h7pg3selpcb2qdz7', 0, 0, 0, '6D57003814CB58924F086CE0903DD9BF', '123456', 'PS756010485', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(10, 'khalidkhalil1993@outlook.com', 'Khalid', NULL, 0, '$2a$11$XBwPRIse8vE5BZXfi5XqW.I6P0duVNGOeB6sqLtbyX.t3eYrfl3QO', '21d52d9a08566f0914ee5ae70748dab5595b711152bf01a750ecb31bfcb50605', '2018-02-03 12:10:01', NULL, '2018-01-27 10:19:48', NULL, NULL, 413, '2018-04-02 09:45:30', '2018-04-02 08:59:17', '41.68.40.35', '156.198.234.146', 0, NULL, 'MvPWuZMuonvE2PKrM3jU', '2018-01-27 10:21:00', '2018-01-27 10:19:48', NULL, NULL, '2018-01-27 10:19:48', '2018-04-02 09:45:30', NULL, '2018-04-02 09:47:10', NULL, 'EG', 'Egyptian', NULL, NULL, NULL, 'khalid1993', '+201146700763', 0, 0, 1, 8, NULL, NULL, 'ar', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jwe4ewugtq2jw7pf7cajukfvkacsvd5mrrjj4pvnztp36mngvnqgpwalkkgwrjfc', NULL, '0', 'fj3xypuu22s2kpzeqah737b75stv4g6j', '2018-02-03 14:46:15', 0, NULL, NULL, NULL, 'maopvtkyr23qgw65', 0, 0, 0, '41DF144D185657BCD781E5B55EC4CFBB', '123456', 'PS500598468', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(11, 'smsm4web@yahoo.com', 'Sami', 'El-Shafei', NULL, '$2a$11$ZPhr640WKOo7ro51NC60TuDgqbNEzyH0.51TQwdVLG2w.vVXEe8AG', NULL, NULL, NULL, '2018-02-01 10:32:47', NULL, NULL, 63, '2018-04-01 10:15:58', '2018-03-31 11:11:03', '156.198.234.146', '156.198.234.146', 0, NULL, 'vGLDCgx3YoJUUAMKwGCT', '2018-02-01 10:34:16', '2018-02-01 10:32:47', NULL, NULL, '2018-02-01 10:32:47', '2018-04-04 10:27:54', NULL, '2018-04-01 10:31:39', NULL, 'SA', 'Egyptian', '', '', NULL, 'sami', '+201002914737', 0, 199.7, 1, 5, NULL, 0, 'ar', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gdng6nxhjspvuwpwmnywrwqrbarko52mtlujyydtwbznhbxnvzk3q5sxnrhp7ndr', NULL, '0', NULL, NULL, 0, NULL, NULL, NULL, 'cwi72obkbrupcgf6', 0, 0, 0, NULL, NULL, 'PS859138657', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(12, 'amir@payers.net', NULL, NULL, 0, '$2a$11$0zfTtSDl4AE9XIewazfqVeK6D7/kDxHSFNUYZALwcbytBwA3D9eUe', NULL, NULL, NULL, '2018-02-06 14:22:16', NULL, NULL, 11, '2018-02-21 13:44:33', '2018-02-18 13:41:48', '197.133.96.111', '197.133.34.16', 0, NULL, 'itmw5ikaFos2_jkyaLwQ', '2018-02-06 14:08:15', '2018-02-06 14:06:03', NULL, NULL, '2018-02-06 14:06:03', '2018-03-30 08:00:05', NULL, '2018-02-21 13:44:41', NULL, 'SA', NULL, NULL, NULL, NULL, 'amir2', NULL, 0, 0, 1, 8, NULL, NULL, 'ar', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fqs2oag2fkdym7nq653llmox2pryqfdp3a2bzyz5dt2irru6nafzpc43qq7fx7ap', NULL, '0', NULL, NULL, 0, NULL, NULL, NULL, 'vbwohmzhd3gl2cfj', 0, 0, 0, '6B4DC1A11BC896ADA1919A02B86767FB', NULL, 'PS391100884', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(15, 'ahmed@payers.net', 'Ahmed', 'Aly', 0, '$2a$11$iRw8HcR5b51IQVX9Xw2j9OuA2ZWhc2ej9u57l0Ge4xdhw471OwqZK', NULL, NULL, NULL, '2018-02-20 08:54:19', NULL, NULL, 1, '2018-02-20 08:55:43', '2018-02-20 08:55:43', '197.45.219.97', '197.45.219.97', 0, NULL, 'wDyMxQy3xHTZ-KzMck41', '2018-02-20 08:55:38', '2018-02-20 08:54:19', NULL, NULL, '2018-02-20 08:54:19', '2018-03-30 08:00:05', NULL, '2018-02-20 09:18:40', NULL, 'EG', 'Egyptian', 'Giza', '12786', NULL, 'Gomaa', '0110 100 0140', 0, 0, 2, 5, NULL, NULL, 'ar', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ske6pnogwxvcze4tghanb6bskkakv5alvasuq7aqo36h5mv3usp4rhrdttonvqdo', NULL, '0', NULL, NULL, 0, NULL, NULL, NULL, 'h2ouo5m2w2s7zcdw', 0, 0, 0, 'DFCBFD2E81F9748EFDBC059BD8184AEF', NULL, 'PS108043617', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Hawaii', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(17, 'yara.nasr@yallapay.net', NULL, NULL, 0, '$2a$11$L2594QVkocaUjdGJj4B9h.ndI64ZW6S04QGQAtn8HyfffqkQ27/ce', '17efe9addc1e82ce6393bb3d65e0fd1d3e23e589c8153604e4197aa20fd8d4a8', '2018-03-05 14:53:18', NULL, '2018-02-28 12:02:29', NULL, NULL, 0, NULL, NULL, NULL, NULL, 1, NULL, 'rYWv2jb5tVKjZ1Ch57mz', '2018-03-05 15:01:33', '2018-02-28 12:02:29', NULL, NULL, '2018-02-28 12:02:29', '2018-03-30 08:00:05', NULL, NULL, NULL, 'EG', NULL, NULL, NULL, NULL, 'yara nasr', '+201000709574', 0, 0, 2, 8, NULL, NULL, 'ar', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agrqg5lvxfqtu5pk6fbtwao2nk5w6mvzkvbxnrl3lpbkmtlqe73owguswjfkbsuq', NULL, '0', NULL, NULL, 0, NULL, NULL, NULL, 'xyqptztkmy76amdu', 0, 0, 0, 'F4658AE517BA242E74F91A15AD774987', NULL, 'PS941067658', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(18, 'yaranasr33@gmail.com', NULL, NULL, 0, '$2a$11$l/YhhhRVqM9EFU8e29Zh6eXDIScCWNXft0c4ExNMXsZGs7/NiTaLa', '2969ec36398ba0ee70268f9e7e4d825d1f4e0b7a70dd2755c867192cab85fe0c', '2018-03-24 11:00:16', '2018-02-28 12:15:58', '2018-03-10 12:30:03', NULL, NULL, 3, '2018-03-10 12:30:06', '2018-03-05 14:55:48', '196.141.255.248', '197.45.219.97', 0, NULL, 'sypzypo9HJUvt8-Q7V8Y', '2018-02-28 12:15:27', '2018-02-28 12:14:51', NULL, NULL, '2018-02-28 12:14:51', '2018-03-30 08:00:05', NULL, '2018-03-10 12:30:41', NULL, 'EG', NULL, NULL, NULL, NULL, 'yara.nasr', '+201002923876', 0, 0, 2, 8, NULL, NULL, 'ar', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6xmiejnh2hccx6dk4md3evak34vc4dfxiueqp52ijfnznewabygj2a3sbl3m5nsa', NULL, '0', 'molwf33iipugmdhwyo457xhcvedlfmp2', '2018-02-28 12:20:12', 0, NULL, NULL, NULL, 'kdbyvfll34evz35k', 0, 1, 0, 'E5A0581B135DC79330787D82D1292087', NULL, 'PS804045465', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(19, 'alaa@payers.net', 'Alaa', 'Almehmadi', 0, '$2a$11$gQgBQM6Zw31v/cel/L3hBuSWL/Y2ztLGR/Bpj6aM4pPHBKmWdGX1i', NULL, NULL, NULL, '2018-03-12 22:19:02', NULL, NULL, 19, '2018-04-01 20:37:35', '2018-03-31 23:07:17', '51.36.77.141', '51.36.77.141', 0, NULL, 'wrTBj5pKc91sxmBYgngu', '2018-03-12 22:19:25', '2018-03-12 22:19:02', NULL, NULL, '2018-03-12 22:19:02', '2018-04-01 20:37:35', NULL, '2018-04-02 06:05:02', NULL, 'EG', 'Saudi', 'Jed', '11111', NULL, 'adalm7madi', '+966566995992', 0, 0, 1, 8, NULL, 0, 'ar', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '743spanssakpsdptrpszo6bt2lcy5ffikcosqtac6v752cahe7ssph4t2obpszbz', NULL, '0', 'id3ttcr4rxajhwzl7wt3cqmn36j6ip2d', '2018-03-12 22:28:35', 0, NULL, NULL, NULL, 'ljn3d6vddwpl4aol', 0, 0, 0, '9C2A980AB057A31696F066A217FDBDA5', NULL, 'PS688017244', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Riyadh', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(20, 'engy1192@gmail.com', NULL, NULL, 0, '$2a$11$zmwyyOuYu5DQJ0ZDSlv7mutvL4QCKHE2eZ18q2npFaykTgg/A6I2C', NULL, NULL, NULL, '2018-03-14 12:39:34', NULL, NULL, 13, '2018-04-02 12:26:22', '2018-04-01 08:52:49', '156.198.234.146', '156.198.234.146', 0, NULL, 'oT6-ioAiFxvqX7GuP8F3', '2018-03-14 00:00:00', '2018-03-14 12:39:34', NULL, NULL, '2018-03-14 12:39:34', '2018-04-02 12:26:22', NULL, '2018-04-02 12:27:42', NULL, NULL, NULL, NULL, NULL, NULL, 'engytoma', '+201011219324', 0, 0, 2, 5, NULL, NULL, 'ar', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ou4uttukj22ipqjfkbmoekkwbpre3bq42iholcpoai5tozamnsqz6ex2dlmlsh5s', NULL, 'f', NULL, NULL, 0, NULL, NULL, NULL, 'sohjeapax7tovyx6', 0, 0, 0, 'FE5C717259B13B9BB8918D2670EDC13C', NULL, 'PS114243266', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(21, 'khalid.khalil@servicehigh.com', NULL, NULL, 0, '$2a$11$hhAXtrdPQhHGMFvNOwoM2emntQoL7uldQrt2juzraG48FNNyYoPBe', NULL, NULL, NULL, '2018-03-15 10:49:32', NULL, NULL, 41, '2018-04-04 09:09:39', '2018-04-04 09:08:06', '196.221.41.202', '196.221.41.202', 0, NULL, 'MEoPeUAC3M3bCf7purBf', '2018-03-15 10:55:06', '2018-03-15 10:49:32', NULL, NULL, '2018-03-15 10:49:32', '2018-04-04 09:10:05', NULL, '2018-04-04 10:09:10', NULL, NULL, NULL, NULL, NULL, NULL, 'khalidkhalil1993', '+201146700763', 0, 0, 2, 8, NULL, NULL, 'ar', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yyhpcemzsftrkfkspcwbxpgbngfj4a5wqgf3kgljl6fumnfgxayhsya6jp7m5n3v', NULL, '0', 'nx4elxpnl3ejc6ttrabeo3vdkphdjt3j', '2018-04-04 08:51:28', 0, NULL, NULL, NULL, 'agxbqzbeezgkpvga', 0, 0, 0, 'F5D2BCD5CF5C5B6F56BC419E5FED06F5', NULL, 'PS355687358', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `usergroups_banks`
--

CREATE TABLE IF NOT EXISTS `usergroups_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_group_id` int(11) DEFAULT NULL,
  `company_bank_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=187 ;

--
-- Dumping data for table `usergroups_banks`
--

INSERT INTO `usergroups_banks` (`id`, `users_group_id`, `company_bank_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2018-02-20 13:23:25', '2018-03-04 13:19:15'),
(2, 2, 1, 1, '2018-02-20 13:23:25', '2018-02-20 13:23:25'),
(3, 3, 1, 1, '2018-02-20 13:23:25', '2018-02-20 13:23:25'),
(4, 4, 1, 1, '2018-02-20 13:23:25', '2018-02-20 13:23:25'),
(5, 5, 1, 1, '2018-02-20 13:23:25', '2018-02-20 13:23:25'),
(6, 8, 1, 1, '2018-02-20 13:23:25', '2018-02-20 13:23:25'),
(7, 1, 2, 1, '2018-02-20 13:23:25', '2018-02-20 13:23:25'),
(8, 2, 2, 1, '2018-02-20 13:23:25', '2018-02-20 13:23:25'),
(9, 3, 2, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(10, 4, 2, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(11, 5, 2, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(12, 8, 2, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(13, 1, 3, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(14, 2, 3, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(15, 3, 3, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(16, 4, 3, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(17, 5, 3, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(18, 8, 3, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(19, 1, 4, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(20, 2, 4, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(21, 3, 4, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(22, 4, 4, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(23, 5, 4, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(24, 8, 4, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(25, 1, 5, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(26, 2, 5, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(27, 3, 5, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(28, 4, 5, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(29, 5, 5, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(30, 8, 5, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(31, 1, 6, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(32, 2, 6, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(33, 3, 6, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(34, 4, 6, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(35, 5, 6, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(36, 8, 6, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(37, 1, 7, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(38, 2, 7, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(39, 3, 7, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(40, 4, 7, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(41, 5, 7, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(42, 8, 7, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(43, 1, 8, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(44, 2, 8, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(45, 3, 8, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(46, 4, 8, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(47, 5, 8, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(48, 8, 8, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(49, 1, 9, 1, '2018-02-20 13:23:26', '2018-02-20 13:23:26'),
(50, 2, 9, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(51, 3, 9, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(52, 4, 9, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(53, 5, 9, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(54, 8, 9, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(55, 1, 10, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(56, 2, 10, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(57, 3, 10, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(58, 4, 10, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(59, 5, 10, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(60, 8, 10, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(61, 1, 11, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(62, 2, 11, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(63, 3, 11, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(64, 4, 11, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(65, 5, 11, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(66, 8, 11, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(67, 1, 12, 0, '2018-02-20 13:23:27', '2018-03-04 13:20:04'),
(68, 2, 12, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(69, 3, 12, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(70, 4, 12, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(71, 5, 12, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(72, 8, 12, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(73, 1, 13, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(74, 2, 13, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(75, 3, 13, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(76, 4, 13, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(77, 5, 13, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(78, 8, 13, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(79, 1, 14, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(80, 2, 14, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(81, 3, 14, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(82, 4, 14, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(83, 5, 14, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(84, 8, 14, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(85, 1, 15, 1, '2018-02-20 13:23:27', '2018-02-20 13:23:27'),
(86, 2, 15, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(87, 3, 15, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(88, 4, 15, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(89, 5, 15, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(90, 8, 15, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(91, 1, 16, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(92, 2, 16, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(93, 3, 16, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(94, 4, 16, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(95, 5, 16, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(96, 8, 16, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(97, 1, 17, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(98, 2, 17, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(99, 3, 17, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(100, 4, 17, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(101, 5, 17, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(102, 8, 17, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(103, 1, 18, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(104, 2, 18, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(105, 3, 18, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(106, 4, 18, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(107, 5, 18, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(108, 8, 18, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(109, 1, 19, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(110, 2, 19, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(111, 3, 19, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(112, 4, 19, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(113, 5, 19, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(114, 8, 19, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(115, 1, 20, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(116, 2, 20, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(117, 3, 20, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(118, 4, 20, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(119, 5, 20, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(120, 8, 20, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(121, 1, 21, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(122, 2, 21, 1, '2018-02-20 13:23:28', '2018-02-20 13:23:28'),
(123, 3, 21, 1, '2018-02-20 13:23:29', '2018-02-20 13:23:29'),
(124, 4, 21, 1, '2018-02-20 13:23:29', '2018-02-20 13:23:29'),
(125, 5, 21, 1, '2018-02-20 13:23:29', '2018-02-20 13:23:29'),
(126, 8, 21, 1, '2018-02-20 13:23:29', '2018-02-20 13:23:29'),
(127, 1, 22, 1, '2018-02-20 13:23:29', '2018-02-20 13:23:29'),
(128, 2, 22, 1, '2018-02-20 13:23:29', '2018-02-20 13:23:29'),
(129, 3, 22, 1, '2018-02-20 13:23:29', '2018-02-20 13:23:29'),
(130, 4, 22, 1, '2018-02-20 13:23:29', '2018-02-20 13:23:29'),
(131, 5, 22, 1, '2018-02-20 13:23:29', '2018-02-20 13:23:29'),
(132, 8, 22, 1, '2018-02-20 13:23:29', '2018-02-20 13:23:29'),
(133, 1, 23, 1, '2018-02-22 15:00:58', '2018-03-04 13:18:14'),
(134, 2, 23, 1, '2018-02-22 15:00:58', '2018-02-22 15:00:58'),
(135, 3, 23, 1, '2018-02-22 15:00:58', '2018-02-22 15:00:58'),
(136, 4, 23, 1, '2018-02-22 15:00:58', '2018-02-22 15:00:58'),
(137, 5, 23, 1, '2018-02-22 15:00:58', '2018-02-22 15:00:58'),
(138, 8, 23, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(139, 1, 24, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(140, 2, 24, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(141, 3, 24, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(142, 4, 24, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(143, 5, 24, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(144, 8, 24, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(145, 1, 25, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(146, 2, 25, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(147, 3, 25, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(148, 4, 25, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(149, 5, 25, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(150, 8, 25, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(151, 1, 26, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(152, 2, 26, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(153, 3, 26, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(154, 4, 26, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(155, 8, 26, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(156, 5, 26, 1, '2018-02-22 00:00:00', '2018-02-22 00:00:00'),
(157, 1, 27, 1, '2018-02-24 00:00:00', '2018-02-24 00:00:00'),
(158, 2, 27, 1, '2018-02-24 00:00:00', '2018-02-24 00:00:00'),
(159, 3, 27, 1, '2018-02-24 00:00:00', '2018-02-24 00:00:00'),
(160, 4, 27, 1, '2018-02-24 00:00:00', '2018-02-24 00:00:00'),
(161, 5, 27, 1, '2018-02-24 00:00:00', '2018-02-24 00:00:00'),
(162, 8, 27, 1, '2018-02-24 00:00:00', '2018-02-24 00:00:00'),
(163, 1, 28, NULL, '2018-03-27 13:44:40', '2018-03-27 13:44:40'),
(164, 2, 28, NULL, '2018-03-27 13:44:40', '2018-03-27 13:44:40'),
(165, 3, 28, NULL, '2018-03-27 13:44:40', '2018-03-27 13:44:40'),
(166, 4, 28, NULL, '2018-03-27 13:44:40', '2018-03-27 13:44:40'),
(167, 5, 28, NULL, '2018-03-27 13:44:40', '2018-03-27 13:44:40'),
(168, 8, 28, NULL, '2018-03-27 13:44:40', '2018-03-27 13:44:40'),
(169, 1, 29, NULL, '2018-03-27 13:45:25', '2018-03-27 13:45:25'),
(170, 2, 29, NULL, '2018-03-27 13:45:26', '2018-03-27 13:45:26'),
(171, 3, 29, NULL, '2018-03-27 13:45:26', '2018-03-27 13:45:26'),
(172, 4, 29, NULL, '2018-03-27 13:45:26', '2018-03-27 13:45:26'),
(173, 5, 29, NULL, '2018-03-27 13:45:26', '2018-03-27 13:45:26'),
(174, 8, 29, NULL, '2018-03-27 13:45:26', '2018-03-27 13:45:26'),
(175, 1, 30, NULL, '2018-04-01 09:52:46', '2018-04-01 09:52:46'),
(176, 2, 30, NULL, '2018-04-01 09:52:46', '2018-04-01 09:52:46'),
(177, 3, 30, NULL, '2018-04-01 09:52:46', '2018-04-01 09:52:46'),
(178, 4, 30, NULL, '2018-04-01 09:52:46', '2018-04-01 09:52:46'),
(179, 5, 30, NULL, '2018-04-01 09:52:46', '2018-04-01 09:52:46'),
(180, 8, 30, NULL, '2018-04-01 09:52:46', '2018-04-01 09:52:46'),
(181, 1, 31, NULL, '2018-04-04 10:00:18', '2018-04-04 10:00:18'),
(182, 2, 31, NULL, '2018-04-04 10:00:18', '2018-04-04 10:00:18'),
(183, 3, 31, NULL, '2018-04-04 10:00:18', '2018-04-04 10:00:18'),
(184, 4, 31, NULL, '2018-04-04 10:00:18', '2018-04-04 10:00:18'),
(185, 5, 31, NULL, '2018-04-04 10:00:18', '2018-04-04 10:00:18'),
(186, 8, 31, NULL, '2018-04-04 10:00:18', '2018-04-04 10:00:18');

-- --------------------------------------------------------

--
-- Table structure for table `userinboxtos`
--

CREATE TABLE IF NOT EXISTS `userinboxtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(36) NOT NULL,
  `subject` varchar(250) NOT NULL,
  `message` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reply_msg_id` int(11) DEFAULT NULL,
  `reply_or_main` varchar(200) NOT NULL,
  `read` int(11) DEFAULT '0',
  `user_id_to` varchar(36) NOT NULL,
  `del_user_id` tinyint(1) NOT NULL DEFAULT '0',
  `del_user_id_to` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `userinboxtos`
--

INSERT INTO `userinboxtos` (`id`, `user_id`, `subject`, `message`, `created`, `reply_msg_id`, `reply_or_main`, `read`, `user_id_to`, `del_user_id`, `del_user_id_to`) VALUES
(1, '10', '', '', '2018-03-03 12:38:14', NULL, 'main', 1, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_banks`
--

CREATE TABLE IF NOT EXISTS `users_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userd_id` int(11) NOT NULL,
  `bank_type` varchar(10) NOT NULL DEFAULT 'withdraw' COMMENT 'withdraw , pay',
  `currencies_withdraw_ratios_id` int(11) DEFAULT NULL COMMENT 'company_bank_id for pay ',
  `personal_name` varchar(200) DEFAULT NULL,
  `business_name` varchar(200) DEFAULT NULL,
  `nationalid` varchar(50) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `relation` varchar(200) DEFAULT NULL,
  `nickname` varchar(200) DEFAULT NULL,
  `bank_name` varchar(200) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `country_code` varchar(50) DEFAULT NULL,
  `branch` varchar(200) DEFAULT NULL,
  `branchnumber` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `from_mobile` varchar(50) DEFAULT NULL,
  `to_mobile` varchar(50) DEFAULT NULL,
  `account_name` varchar(200) DEFAULT NULL,
  `account_email` varchar(200) DEFAULT NULL,
  `account_number` varchar(100) DEFAULT NULL,
  `visa_cvv` int(11) DEFAULT NULL,
  `swiftcode` varchar(100) DEFAULT NULL,
  `valid_swift_code` varchar(11) DEFAULT NULL COMMENT 'this validation for swift code',
  `expire_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `iban` varchar(200) DEFAULT NULL,
  `bban` varchar(100) DEFAULT NULL,
  `checksum` int(11) DEFAULT NULL,
  `bank_code` int(11) DEFAULT NULL,
  `check_digit` int(11) DEFAULT NULL,
  `sepa` tinyint(1) DEFAULT NULL,
  `iban_validity` varchar(50) DEFAULT NULL,
  `iban_checksum` varchar(50) DEFAULT NULL,
  `iban_length` varchar(50) DEFAULT NULL,
  `iban_structure` varchar(50) DEFAULT NULL,
  `account_checksum` varchar(50) DEFAULT NULL,
  `currency` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users_banks`
--

INSERT INTO `users_banks` (`id`, `userd_id`, `bank_type`, `currencies_withdraw_ratios_id`, `personal_name`, `business_name`, `nationalid`, `birth_date`, `relation`, `nickname`, `bank_name`, `country`, `country_code`, `branch`, `branchnumber`, `phone`, `from_mobile`, `to_mobile`, `account_name`, `account_email`, `account_number`, `visa_cvv`, `swiftcode`, `valid_swift_code`, `expire_date`, `created_at`, `updated_at`, `status`, `city`, `address`, `postal_code`, `iban`, `bban`, `checksum`, `bank_code`, `check_digit`, `sepa`, `iban_validity`, `iban_checksum`, `iban_length`, `iban_structure`, `account_checksum`, `currency`) VALUES
(1, 3, 'withdraw', 4, 'aa', NULL, NULL, '2018-03-20', 'a', 'a', 'Western Union', 'EG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-30 14:34:02', '2018-03-30 10:34:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD'),
(2, 3, 'withdraw', 6, 'klkl', NULL, '123456789', NULL, NULL, 'ui', 'Egypt Post', 'Egypt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-31 18:38:57', '2018-04-04 09:43:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'EGP'),
(3, 3, 'pay', 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ff', NULL, NULL, NULL, 'ff', NULL, NULL, 'ff', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-01 17:02:13', '2018-04-01 13:02:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD'),
(4, 5, 'withdraw', 14, 'fssfsafsfsdf', '', NULL, NULL, 'sdfsdf', 'sdfsdfsd', 'HOUSING BANK FOR TRADE AND FINANCE, THE', 'Jordan', 'JO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '000048324100105001', NULL, NULL, NULL, NULL, '2018-04-02 19:54:09', '2018-04-02 15:54:09', NULL, NULL, NULL, NULL, 'JO55HBHO0520000048324100105001', 'HBHO0520000048324100105001', 55, NULL, NULL, 0, 'Valid', 'Valid', 'Valid', 'Valid', 'No Validation', 'USD');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `family_limit` float DEFAULT '0' COMMENT 'family limit send',
  `max_family_recieve` float DEFAULT '10000',
  `minimum_value` float NOT NULL DEFAULT '0' COMMENT 'for users transfer',
  `minimum_withdraw` float NOT NULL,
  `month_withdraws_count` int(11) NOT NULL,
  `month_max_withdraw` float NOT NULL,
  `day_withdraws_count` int(11) NOT NULL,
  `day_max_withdraw` float NOT NULL,
  `max_withdraw_each_time` float NOT NULL DEFAULT '0',
  `add_balance_ratio` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `name`, `description`, `family_limit`, `max_family_recieve`, `minimum_value`, `minimum_withdraw`, `month_withdraws_count`, `month_max_withdraw`, `day_withdraws_count`, `day_max_withdraw`, `max_withdraw_each_time`, `add_balance_ratio`) VALUES
(1, 'Diamond', 'this is VIP user', 20000, 1000, 0, 10, 600, 200000, 6, 25000, 1000, 3),
(2, 'Gold', 'this is special user', 2000, 3000, 2, 10, 500, 100000, 5, 20000, 500, 5),
(3, 'Platinum', 'this is bronze user', 1500, 3000, 2, 20, 400, 50000, 4, 15000, 400, 6),
(4, 'silver', 'this is silver user', 10, 10, 0, 30, 300, 40000, 2, 1000, 300, 7),
(5, 'Bronze', 'this is regular user', 5000, 5000, 2, 20, 200, 30000, 20, 2000, 200, 10),
(8, 'Unverified', 'Unverified User', 1000, 1000, 2, 10, 100, 20000, 1, 1000, 100, 10);

-- --------------------------------------------------------

--
-- Table structure for table `users_wallets_balances`
--

CREATE TABLE IF NOT EXISTS `users_wallets_balances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wallet_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance` float DEFAULT '0',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=108 ;

--
-- Dumping data for table `users_wallets_balances`
--

INSERT INTO `users_wallets_balances` (`id`, `user_id`, `currency`, `wallet_name`, `balance`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'USD', 'US Dollar', 13162, 0, '2018-01-23 19:01:10', '2018-04-01 13:21:29'),
(2, 2, 'USD', 'US Dollar', 7905.41, 0, '2018-01-23 19:04:54', '2018-04-03 15:13:16'),
(3, 2, 'SAR', 'Saudi Riyal', 13675, 0, '2018-01-23 19:07:28', '2018-03-31 23:52:17'),
(4, 2, 'EGP', 'Egyptian Pound', 27267.6, 0, '2018-01-23 19:07:44', '2018-03-31 23:53:01'),
(5, 1, 'SAR', 'Saudi Riyal', 0, 0, '2018-01-23 19:20:58', '2018-04-01 13:21:29'),
(6, 3, 'USD', 'US Dollar', 10007.1, 0, '2018-01-23 19:23:14', '2018-04-03 10:35:03'),
(7, 3, 'EGP', 'Egyptian Pound', 8000, 0, '2018-01-23 19:44:22', '2018-03-31 13:41:48'),
(8, 1, 'EGP', 'Egyptian Pound', 0, 0, '2018-01-23 19:58:08', '2018-04-01 13:21:09'),
(9, 3, 'SAR', 'Saudi Riyal', 10783.1, 0, '2018-01-23 20:11:23', '2018-03-31 13:41:48'),
(10, 4, 'USD', 'US Dollar', 10000, 0, '2018-01-24 08:25:54', '2018-03-15 07:12:40'),
(11, 5, 'USD', 'US Dollar', 9907, 0, '2018-01-24 09:28:54', '2018-04-02 15:54:09'),
(12, 6, 'USD', 'US Dollar', 10000, 0, '2018-01-24 09:29:00', '2018-03-25 08:01:49'),
(13, 6, 'SAR', 'Saudi Riyal', 10000, 0, '2018-01-24 10:23:34', '2018-03-17 13:53:45'),
(14, 6, 'EGP', 'Egyptian Pound', 10000, 0, '2018-01-24 10:23:44', '2018-03-14 13:45:39'),
(15, 7, 'USD', 'US Dollar', 8100, 0, '2018-01-25 11:25:35', '2018-04-04 10:24:32'),
(16, 7, 'SAR', 'Saudi Riyal', 1700, 0, '2018-01-25 11:26:55', '2018-04-04 09:46:41'),
(18, 4, 'SAR', 'Saudi Riyal', 10000, 0, '2018-01-25 11:34:08', '2018-02-19 12:17:59'),
(19, 8, 'USD', 'US Dollar', 10000, 0, '2018-01-25 16:03:12', '2018-01-25 16:35:32'),
(20, 5, 'SAR', 'Saudi Riyal', 10000, 0, '2018-01-25 17:19:18', '2018-03-08 16:37:27'),
(21, 9, 'USD', 'US Dollar', 10128, 0, '2018-01-25 19:37:59', '2018-04-03 10:34:31'),
(22, 8, 'SAR', 'Saudi Riyal', 10000, 0, '2018-01-26 06:29:41', '2018-01-26 06:29:41'),
(23, 9, 'SAR', 'Saudi Riyal', 10080.8, 0, '2018-01-26 22:24:21', '2018-04-01 11:49:39'),
(24, 10, 'USD', 'US Dollar', 10000, 0, '2018-01-27 10:19:52', '2018-03-04 12:15:42'),
(25, 4, 'EGP', 'Egyptian Pound', 10000, 0, '2018-01-27 14:03:57', '2018-03-13 10:47:17'),
(26, 5, 'EGP', 'Egyptian Pound', 10000, 0, '2018-01-27 20:27:57', '2018-03-16 16:00:05'),
(27, 11, 'USD', 'US Dollar', 11743, 0, '2018-02-01 10:32:50', '2018-04-04 10:24:32'),
(28, 12, 'USD', 'US Dollar', 10000, 0, '2018-02-06 14:06:06', '2018-02-06 14:06:06'),
(30, 15, 'USD', 'US Dollar', 10000, 0, '2018-02-20 08:54:22', '2018-03-08 09:29:06'),
(31, 15, 'SAR', 'Saudi Riyal', 10000, 0, '2018-02-20 08:58:57', '2018-02-20 08:58:57'),
(33, 17, 'USD', 'US Dollar', 10000, 0, '2018-02-28 12:02:30', '2018-02-28 12:02:30'),
(34, 18, 'USD', 'US Dollar', 10000, 0, '2018-02-28 12:14:54', '2018-02-28 12:14:54'),
(35, 19, 'USD', 'US Dollar', 10000, 0, '2018-03-12 22:19:06', '2018-03-12 22:19:06'),
(38, 20, 'USD', 'US Dollar', 10014, 0, '2018-03-14 12:39:36', '2018-04-02 16:00:04'),
(39, 20, 'SAR', 'Saudi Riyal', 10000, 0, '2018-03-14 13:02:47', '2018-03-28 11:59:47'),
(40, 20, 'EGP', 'Egyptian Pound', 10000, 0, '2018-03-14 14:46:46', '2018-03-28 12:00:09'),
(41, 21, 'USD', 'US Dollar', 10860.3, 0, '2018-03-15 00:00:00', '2018-04-01 10:08:23'),
(42, 21, 'SAR', 'Saudi Riyal', 6700, 0, '2018-03-15 00:00:00', '2018-04-01 10:08:23'),
(43, 11, 'EGP', 'Egyptian Pound', 10157.7, 0, '2018-03-25 09:46:49', '2018-04-04 10:27:54'),
(44, 21, 'EGP', 'Egyptian Pound', 10000, 0, '2018-03-25 10:45:08', '2018-03-25 10:45:08'),
(95, 7, 'EGP', 'Egyptian Pound', 4163.46, 0, '2018-04-01 13:23:19', '2018-04-04 10:27:54'),
(107, 11, 'SAR', 'Saudi Riyal', 141.9, 0, '2018-04-03 14:43:03', '2018-04-04 09:46:41');

-- --------------------------------------------------------

--
-- Table structure for table `users_wallets_balances_frozens`
--

CREATE TABLE IF NOT EXISTS `users_wallets_balances_frozens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_id_to` int(11) DEFAULT NULL,
  `purpose_id` int(11) DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wallet_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance` float DEFAULT NULL,
  `netbalance` float NOT NULL,
  `ratio_company` float DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approve` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1-approved',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1-deleted by admin, 2-deleted by user',
  `Suspension_period` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=82 ;

--
-- Dumping data for table `users_wallets_balances_frozens`
--

INSERT INTO `users_wallets_balances_frozens` (`id`, `user_id`, `user_id_to`, `purpose_id`, `currency`, `wallet_name`, `balance`, `netbalance`, `ratio_company`, `note`, `approve`, `deleted`, `Suspension_period`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 'USD', NULL, 1000, 945, NULL, '0', 1, 1, 1, '2018-03-30 12:35:12', '2018-03-31 09:39:25'),
(2, 3, 20, 1, 'USD', NULL, 20, 14, NULL, 'jjj', 1, 0, 2, '2018-03-31 07:48:40', '2018-04-02 16:00:04'),
(3, 9, 3, 1, 'USD', NULL, 200, 185, NULL, 'gg', 0, 2, 2, '2018-03-31 11:54:30', '2018-03-31 11:59:20'),
(4, 9, 3, 2, 'USD', NULL, 20, 15.2, NULL, 'kk', 1, 0, 2, '2018-03-31 11:55:08', '2018-04-02 16:00:11'),
(5, 9, 3, 3, 'USD', NULL, 30, 21.9, NULL, '25j', 1, 0, NULL, '2018-03-31 11:55:52', '2018-03-31 11:55:52'),
(6, 3, 9, 1, 'USD', NULL, 200, 180, NULL, 'hjjkk', 1, 0, 2, '2018-03-31 11:56:58', '2018-03-31 11:59:44'),
(7, 3, 9, 1, 'USD', NULL, 200, 180, NULL, '', 0, 1, 5, '2018-03-31 11:58:46', '2018-04-03 10:35:03'),
(8, 9, 3, 1, 'USD', NULL, 40, 33, NULL, '', 0, 1, 5, '2018-03-31 12:02:37', '2018-03-31 12:03:13'),
(9, 2, 1, 1, 'USD', NULL, 99.09, 91.1264, NULL, '', 0, 0, NULL, '2018-04-02 09:49:33', '2018-04-02 09:49:33'),
(10, 11, 7, 1, 'USD', NULL, 100, 96, NULL, NULL, 0, 0, NULL, '2018-04-03 10:18:01', '2018-04-03 10:18:01'),
(11, 11, 7, 2, 'USD', NULL, 20, 17, NULL, NULL, 0, 0, NULL, '2018-04-03 10:31:43', '2018-04-03 10:31:43'),
(12, 9, 3, 2, 'USD', NULL, 20, 15.2, NULL, NULL, 0, 1, NULL, '2018-04-03 10:34:02', '2018-04-03 10:34:31'),
(13, 11, 7, 1, 'USD', NULL, 20, 18.4, NULL, NULL, 0, 0, NULL, '2018-04-03 10:41:31', '2018-04-03 10:41:31'),
(14, 11, 7, 2, 'EGP', NULL, 100, 93, NULL, NULL, 0, 0, NULL, '2018-04-03 10:50:49', '2018-04-03 10:50:49'),
(15, 11, 7, 2, 'EGP', NULL, 100, 93, NULL, NULL, 0, 0, NULL, '2018-04-03 10:51:55', '2018-04-03 10:51:55'),
(16, 11, 7, 2, 'EGP', NULL, 50, 45.5, NULL, NULL, 0, 0, NULL, '2018-04-03 10:52:29', '2018-04-03 10:52:29'),
(17, 11, 7, 1, 'EGP', NULL, 50, 31.2324, NULL, NULL, 0, 0, NULL, '2018-04-03 10:55:19', '2018-04-03 10:55:19'),
(18, 11, 7, 1, 'USD', NULL, 100, 96, NULL, NULL, 0, 0, NULL, '2018-04-03 11:13:51', '2018-04-03 11:13:51'),
(19, 11, 7, 1, 'EGP', NULL, 50, 31.2324, NULL, NULL, 0, 0, NULL, '2018-04-03 12:52:28', '2018-04-03 12:52:28'),
(20, 7, 11, 1, 'SAR', NULL, 100, 95, NULL, NULL, 0, 0, NULL, '2018-04-03 12:55:46', '2018-04-03 12:55:46'),
(21, 7, 11, 3, 'SAR', NULL, 100, 93, NULL, NULL, 1, 0, NULL, '2018-04-03 12:57:25', '2018-04-03 12:57:25'),
(22, 7, 11, 1, 'SAR', NULL, 2000, 1900, NULL, NULL, 0, 0, NULL, '2018-04-03 13:06:09', '2018-04-03 13:06:09'),
(29, 11, 7, 2, 'EGP', NULL, 100, 60.4648, NULL, NULL, 0, 0, NULL, '2018-04-03 13:36:34', '2018-04-03 13:36:34'),
(30, 11, 7, 1, 'USD', NULL, 100, 96, NULL, NULL, 0, 0, NULL, '2018-04-03 13:37:58', '2018-04-03 13:37:58'),
(31, 7, 11, 2, 'SAR', NULL, 100, 81.3, NULL, NULL, 0, 0, NULL, '2018-04-03 13:40:27', '2018-04-03 13:40:27'),
(32, 7, 11, 1, 'SAR', NULL, 100, 76.625, NULL, NULL, 0, 0, NULL, '2018-04-03 13:42:40', '2018-04-03 13:42:40'),
(33, 7, 11, 1, 'EGP', NULL, 100, 8.662, NULL, NULL, 0, 0, NULL, '2018-04-03 13:48:24', '2018-04-03 13:48:24'),
(34, 7, 11, 1, 'EGP', NULL, 100, 8.662, NULL, NULL, 0, 0, NULL, '2018-04-03 13:49:50', '2018-04-03 13:49:50'),
(35, 7, 11, 1, 'EGP', NULL, 100, 79.7324, NULL, NULL, 0, 0, NULL, '2018-04-03 13:50:45', '2018-04-03 13:50:45'),
(36, 7, 11, 1, 'SAR', NULL, 100, 93.325, NULL, NULL, 0, 0, NULL, '2018-04-03 13:51:33', '2018-04-03 13:51:33'),
(37, 11, 7, 1, 'EGP', NULL, 50, 31.2324, NULL, NULL, 0, 0, NULL, '2018-04-03 13:54:48', '2018-04-03 13:54:48'),
(38, 11, 7, 2, 'USD', NULL, 20, 17, NULL, NULL, 0, 0, NULL, '2018-04-03 13:55:57', '2018-04-03 13:55:57'),
(39, 11, 7, 2, 'EGP', NULL, 100, 60.4648, NULL, NULL, 0, 0, NULL, '2018-04-03 13:56:39', '2018-04-03 13:56:39'),
(40, 11, 7, 1, 'EGP', NULL, 50, 31.2324, NULL, NULL, 0, 0, NULL, '2018-04-03 13:57:33', '2018-04-03 13:57:33'),
(41, 7, 11, 1, 'SAR', NULL, 100, 76.625, NULL, NULL, 0, 0, NULL, '2018-04-03 13:58:25', '2018-04-03 13:58:25'),
(42, 7, 11, 3, 'EGP', NULL, 100, -10.6056, NULL, NULL, 1, 0, NULL, '2018-04-03 14:00:19', '2018-04-03 14:00:19'),
(43, 11, 7, 3, 'EGP', NULL, 100, 63.4648, NULL, NULL, 1, 0, NULL, '2018-04-03 14:00:51', '2018-04-03 14:00:51'),
(44, 7, 11, 3, 'USD', NULL, 100, 87, NULL, NULL, 1, 0, NULL, '2018-04-03 14:08:29', '2018-04-03 14:08:29'),
(45, 7, 11, 1, 'SAR', NULL, 100, 76.625, NULL, NULL, 0, 0, NULL, '2018-04-03 14:41:15', '2018-04-03 14:41:15'),
(46, 7, 11, 3, 'SAR', NULL, 100, 70.95, NULL, NULL, 1, 0, NULL, '2018-04-03 14:42:23', '2018-04-03 14:42:23'),
(47, 7, 11, 3, 'SAR', NULL, 100, 70.95, NULL, NULL, 1, 0, NULL, '2018-04-03 14:43:03', '2018-04-03 14:43:03'),
(48, 1, 2, 1, 'USD', NULL, 10, 4.5, NULL, NULL, 1, 0, NULL, '2018-04-03 15:12:33', '2018-04-03 15:13:16'),
(49, 7, 11, 1, 'USD', NULL, 50, 42.5, NULL, NULL, 0, 0, NULL, '2018-04-04 08:41:43', '2018-04-04 08:41:43'),
(50, 7, 11, 3, 'USD', NULL, 100, 87, NULL, NULL, 1, 0, NULL, '2018-04-04 09:22:10', '2018-04-04 09:22:10'),
(51, 7, 11, 1, 'USD', NULL, 100, 90, NULL, NULL, 0, 0, NULL, '2018-04-04 09:23:22', '2018-04-04 09:23:22'),
(52, 7, 11, 3, 'USD', NULL, 100, 87, NULL, NULL, 1, 0, NULL, '2018-04-04 09:24:08', '2018-04-04 09:24:08'),
(53, 7, 11, 1, 'USD', NULL, 100, 90, NULL, NULL, 0, 0, NULL, '2018-04-04 09:24:22', '2018-04-04 09:24:22'),
(54, 7, 11, 3, 'EGP', NULL, 100, -10.6056, NULL, NULL, 1, 0, NULL, '2018-04-04 09:24:54', '2018-04-04 09:24:54'),
(55, 7, 11, 3, 'EGP', NULL, 100, -10.6056, NULL, NULL, 1, 0, NULL, '2018-04-04 09:26:03', '2018-04-04 09:26:03'),
(56, 7, 11, 3, 'EGP', NULL, 100, -10.6056, NULL, NULL, 1, 0, NULL, '2018-04-04 09:38:11', '2018-04-04 09:38:11'),
(57, 7, 11, 3, 'SAR', NULL, 100, 70.95, NULL, NULL, 1, 0, NULL, '2018-04-04 09:46:41', '2018-04-04 09:46:41'),
(58, 7, 11, 3, 'USD', NULL, 100, 87, NULL, NULL, 1, 0, NULL, '2018-04-04 09:49:22', '2018-04-04 09:49:22'),
(59, 7, 11, 3, 'USD', NULL, 100, 87, NULL, NULL, 1, 0, NULL, '2018-04-04 09:50:21', '2018-04-04 09:50:21'),
(60, 7, 11, 3, 'USD', NULL, 100, 87, NULL, NULL, 1, 0, NULL, '2018-04-04 09:51:46', '2018-04-04 09:51:46'),
(61, 1, 2, 1, 'USD', NULL, 10, 4.5, NULL, NULL, 0, 0, 1, '2018-04-04 09:51:48', '2018-04-04 09:52:20'),
(62, 7, 11, 3, 'USD', NULL, 100, 87, NULL, NULL, 1, 0, NULL, '2018-04-04 09:52:25', '2018-04-04 09:52:25'),
(63, 7, 11, 3, 'USD', NULL, 100, 87, NULL, NULL, 1, 0, NULL, '2018-04-04 09:56:56', '2018-04-04 09:56:56'),
(64, 7, 11, 3, 'EGP', NULL, 100, -10.6056, NULL, NULL, 1, 0, NULL, '2018-04-04 09:58:04', '2018-04-04 09:58:04'),
(65, 7, 11, 3, 'USD', NULL, 100, 87, NULL, NULL, 1, 0, NULL, '2018-04-04 09:58:44', '2018-04-04 09:58:44'),
(66, 7, 11, 3, 'EGP', NULL, 100, -10.6056, NULL, NULL, 1, 0, NULL, '2018-04-04 10:06:04', '2018-04-04 10:06:04'),
(67, 7, 11, 3, 'USD', NULL, 100, 96, NULL, NULL, 1, 0, NULL, '2018-04-04 10:09:30', '2018-04-04 10:09:30'),
(68, 7, 11, 3, 'EGP', NULL, 100, 79.7324, NULL, NULL, 1, 0, NULL, '2018-04-04 10:10:49', '2018-04-04 10:10:49'),
(69, 7, 11, 3, 'USD', NULL, 100, 96, NULL, NULL, 1, 0, NULL, '2018-04-04 10:13:28', '2018-04-04 10:13:28'),
(70, 7, 11, 3, 'EGP', NULL, 100, 77, NULL, NULL, 1, 0, NULL, '2018-04-04 10:14:03', '2018-04-04 10:14:03'),
(71, 7, 11, 3, 'USD', NULL, 100, 96, NULL, NULL, 1, 0, NULL, '2018-04-04 10:16:06', '2018-04-04 10:16:06'),
(72, 7, 11, 3, 'USD', NULL, 100, 96, NULL, NULL, 1, 0, NULL, '2018-04-04 10:17:17', '2018-04-04 10:17:17'),
(73, 7, 11, 3, 'USD', NULL, 100, 96, NULL, NULL, 1, 0, NULL, '2018-04-04 10:17:32', '2018-04-04 10:17:32'),
(74, 7, 11, 3, 'USD', NULL, 100, 96, NULL, NULL, 1, 0, NULL, '2018-04-04 10:17:47', '2018-04-04 10:17:47'),
(75, 7, 11, 3, 'USD', NULL, 100, 96, NULL, NULL, 1, 0, NULL, '2018-04-04 10:19:54', '2018-04-04 10:19:54'),
(76, 7, 11, 3, 'USD', NULL, 100, 96, NULL, NULL, 1, 0, NULL, '2018-04-04 10:20:21', '2018-04-04 10:20:21'),
(77, 7, 11, 3, 'USD', NULL, 100, 96, NULL, NULL, 1, 0, NULL, '2018-04-04 10:24:12', '2018-04-04 10:24:12'),
(78, 7, 11, 3, 'USD', NULL, 100, 96, NULL, NULL, 1, 0, NULL, '2018-04-04 10:24:32', '2018-04-04 10:24:32'),
(79, 7, 11, 3, 'EGP', NULL, 100, 77, NULL, NULL, 1, 0, NULL, '2018-04-04 10:25:22', '2018-04-04 10:25:22'),
(80, 7, 11, 3, 'EGP', NULL, 100, 77, NULL, NULL, 1, 0, NULL, '2018-04-04 10:27:54', '2018-04-04 10:27:54'),
(81, 7, 11, 1, 'USD', NULL, 100, 90, NULL, NULL, 0, 0, NULL, '2018-04-04 10:30:14', '2018-04-04 10:30:14');

-- --------------------------------------------------------

--
-- Table structure for table `users_wallets_frozens_purposes`
--

CREATE TABLE IF NOT EXISTS `users_wallets_frozens_purposes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `logo_file_name` varchar(255) DEFAULT NULL,
  `logo_content_type` varchar(255) DEFAULT NULL,
  `logo_file_size` int(11) DEFAULT NULL,
  `logo_updated_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users_wallets_frozens_purposes`
--

INSERT INTO `users_wallets_frozens_purposes` (`id`, `name`, `logo_file_name`, `logo_content_type`, `logo_file_size`, `logo_updated_at`, `created_at`, `updated_at`) VALUES
(1, 'Pay for a commodity', 'transfer.png', 'image/png', 5386, '2018-03-26 08:49:49', '2018-01-01 09:48:58', '2018-03-26 08:49:49'),
(2, 'Pay for service', 'transfer_(4).png', 'image/png', 4983, '2018-03-26 08:50:07', '2018-01-01 09:58:00', '2018-03-26 08:50:07'),
(3, 'Pay for friends or family', 'user.png', 'image/png', 10607, '2018-03-26 08:50:26', '2018-01-01 09:58:48', '2018-03-26 08:50:26');

-- --------------------------------------------------------

--
-- Table structure for table `users_wallets_frozens_ratios`
--

CREATE TABLE IF NOT EXISTS `users_wallets_frozens_ratios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_group_id` int(11) DEFAULT NULL,
  `user_wallet_purpose_ratio_id` int(11) DEFAULT NULL,
  `ratio` float NOT NULL DEFAULT '0',
  `fees` float NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `users_wallets_frozens_ratios`
--

INSERT INTO `users_wallets_frozens_ratios` (`id`, `users_group_id`, `user_wallet_purpose_ratio_id`, `ratio`, `fees`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 3, 1, '2018-01-02 11:36:46', '2018-01-31 08:26:19'),
(2, 1, 2, 5, 2, '2018-01-02 11:37:05', '2018-01-31 08:11:59'),
(3, 1, 3, 2, 2, '2018-01-02 11:37:37', '2018-01-02 11:37:37'),
(4, 5, 1, 5, 5, '2018-01-02 11:38:35', '2018-01-02 11:38:35'),
(5, 5, 2, 4, 4, '2018-01-02 11:39:09', '2018-01-02 11:39:09'),
(6, 5, 3, 3, 1, '2018-01-02 11:39:25', '2018-01-31 08:28:12'),
(7, 2, 1, 2, 2, '2018-01-03 09:47:41', '2018-01-03 09:48:08'),
(8, 2, 2, 3, 3, '2018-01-06 09:59:00', '2018-01-06 09:59:00'),
(9, 2, 3, 3, 3, '2018-01-06 09:59:25', '2018-01-06 09:59:25'),
(10, 3, 1, 1, 1, '2018-01-06 10:00:40', '2018-01-06 10:00:40'),
(11, 3, 2, 2, 2, '2018-01-06 10:00:56', '2018-01-06 10:00:56'),
(12, 3, 3, 3, 3, '2018-01-06 10:01:11', '2018-01-06 10:01:11'),
(13, 4, 1, 4, 4, '2018-01-06 11:14:41', '2018-01-06 11:14:41'),
(14, 4, 2, 5, 5, '2018-01-06 11:14:56', '2018-01-06 11:14:56'),
(15, 4, 3, 6, 6, '2018-01-06 11:15:33', '2018-01-06 11:15:33'),
(22, 8, 1, 8, 4, '2018-01-07 18:02:58', '2018-01-07 18:02:58'),
(23, 8, 2, 7, 4, '2018-01-07 18:02:58', '2018-01-07 18:02:58'),
(24, 8, 3, 5, 4, '2018-01-07 18:02:58', '2018-01-07 18:02:58');

-- --------------------------------------------------------

--
-- Table structure for table `user_lists`
--

CREATE TABLE IF NOT EXISTS `user_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `user_lists`
--

INSERT INTO `user_lists` (`id`, `user_id`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'searchpost?time=&type=approved_transfers&search=', '2018-03-30 12:40:50', '2018-03-30 12:40:50'),
(2, 1, 'searchpost?time=&type=approved_transfers&search=', '2018-03-30 13:30:19', '2018-03-30 13:30:19'),
(3, 1, 'searchpost?time=&type=onlinebanks&search=', '2018-03-30 14:33:36', '2018-03-30 14:33:36'),
(4, 1, 'searchpost?time=&type=users_wallets_ballences_frozens', '2018-03-30 14:33:41', '2018-03-30 14:33:41'),
(5, 1, 'searchpost?time=&type=users_wallets_ballences_frozens', '2018-03-30 14:33:48', '2018-03-30 14:33:48'),
(6, 1, 'searchpost?time=&type=approved_transfers&search=', '2018-03-30 14:33:53', '2018-03-30 14:33:53'),
(7, 3, 'searchpost?time=&type=balances_withdraws&search=', '2018-03-31 08:25:51', '2018-03-31 08:25:51'),
(8, 3, 'searchpost?time=&type=&search=', '2018-03-31 18:41:54', '2018-03-31 18:41:54'),
(9, 3, 'searchpost?time=&type=onlinebanks&search=', '2018-03-31 22:38:19', '2018-03-31 22:38:19'),
(10, 3, 'searchpost?time=&type=balances_withdraws&search=', '2018-04-01 07:41:52', '2018-04-01 07:41:52'),
(11, 3, 'searchpost?time=&type=refunded_delivered&search=', '2018-04-01 11:18:38', '2018-04-01 11:18:38'),
(12, 3, 'searchpost?time=&type=carts&search=', '2018-04-01 14:44:40', '2018-04-01 14:44:40'),
(13, 3, 'searchpost?time=&type=carts&search=', '2018-04-01 14:44:52', '2018-04-01 14:44:52'),
(14, 3, 'searchpost?time=&type=&search=', '2018-04-01 17:14:48', '2018-04-01 17:14:48'),
(15, 3, 'searchpost?time=&type=users_wallets_balances', '2018-04-01 17:46:22', '2018-04-01 17:46:22'),
(16, 3, 'searchpost?time=lastday&type=balances_withdraws&search=', '2018-04-01 21:16:23', '2018-04-01 21:16:23'),
(17, 3, 'searchpost?time=lastweek&type=&search=', '2018-04-01 21:16:46', '2018-04-01 21:16:46'),
(18, 3, 'searchpost?time=lastweek&type=&search=', '2018-04-01 21:22:22', '2018-04-01 21:22:22'),
(19, 3, 'searchpost?time=lastweek&type=&search=', '2018-04-01 21:22:26', '2018-04-01 21:22:26'),
(20, 3, 'searchpost?time=lastweek&type=onlinebanks&search=', '2018-04-01 21:22:52', '2018-04-01 21:22:52'),
(21, 3, 'searchpost?time=lastweek&type=onlinebanks&search=', '2018-04-01 21:39:25', '2018-04-01 21:39:25'),
(22, 3, 'searchpost?time=lastweek&type=onlinebanks&search=', '2018-04-01 21:40:08', '2018-04-01 21:40:08'),
(23, 3, 'searchpost?time=lastweek&type=onlinebanks&search=', '2018-04-01 21:40:45', '2018-04-01 21:40:45');

-- --------------------------------------------------------

--
-- Table structure for table `user_notes`
--

CREATE TABLE IF NOT EXISTS `user_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `role_id` varchar(150) DEFAULT NULL,
  `important` int(2) NOT NULL COMMENT '1-important, 0-unimportant',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `user_notes`
--

INSERT INTO `user_notes` (`id`, `user_id`, `added_by`, `note`, `role_id`, `important`, `created_at`, `updated_at`) VALUES
(1, 3, 10, '', '[""]', 1, '2018-03-31 09:41:11', '2018-03-31 09:41:11'),
(2, 3, 10, '', '[""]', 1, '2018-03-31 09:41:13', '2018-03-31 09:41:13'),
(3, 3, 10, '', '[""]', 1, '2018-03-31 10:41:57', '2018-03-31 10:41:57'),
(4, 3, 10, '', '[""]', 1, '2018-04-02 09:37:21', '2018-04-02 09:37:21'),
(5, 3, 10, '', '[""]', 1, '2018-04-02 09:37:25', '2018-04-02 09:37:25'),
(6, 21, 2, 'فببلالللتبلتلتب', '["1", "2", ""]', 0, '2018-04-03 11:20:09', '2018-04-03 11:20:09'),
(7, 1, 2, '', '[""]', 1, '2018-04-03 14:08:51', '2018-04-03 14:08:51'),
(8, 1, 2, '', '[""]', 1, '2018-04-03 14:08:59', '2018-04-03 14:08:59');

-- --------------------------------------------------------

--
-- Table structure for table `user_verifications`
--

CREATE TABLE IF NOT EXISTS `user_verifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `nationalid_type` int(11) DEFAULT NULL,
  `nationalid_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalid_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalid_issued_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalid_start_date` date DEFAULT '2018-01-01',
  `nationalid_expire_date` date DEFAULT '2018-01-01',
  `nationalid_birth_date` date DEFAULT '2018-01-01',
  `address_document_type` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_line1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_line2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_document_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_document_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_document_postal_code` int(11) DEFAULT NULL,
  `credit_card_id` int(11) DEFAULT NULL,
  `nationalid_verified` int(30) DEFAULT '0',
  `nationalid_back_verified` int(11) NOT NULL DEFAULT '0',
  `address_document_verified` int(30) DEFAULT '0',
  `credit_card_verified` int(30) DEFAULT '0',
  `selfie_verified` int(30) DEFAULT '0',
  `nationalid_refuse_messege` text COLLATE utf8_unicode_ci,
  `nationalid_back_refuse_messege` text COLLATE utf8_unicode_ci COMMENT 'changed from credit card refuse messege to national id back',
  `address_document_refuse_messege` text COLLATE utf8_unicode_ci,
  `selfie_refuse_messege` text COLLATE utf8_unicode_ci,
  `all_verified` int(30) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `nationalid_front_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalid_front_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalid_front_file_size` int(11) DEFAULT NULL,
  `nationalid_front_updated_at` datetime DEFAULT NULL,
  `nationalid_back_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalid_back_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalid_back_file_size` int(11) DEFAULT NULL,
  `nationalid_back_updated_at` datetime DEFAULT NULL,
  `address_document_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_document_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_document_file_size` int(11) DEFAULT NULL,
  `address_document_updated_at` datetime DEFAULT NULL,
  `selfie_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selfie_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selfie_file_size` int(11) DEFAULT NULL,
  `selfie_updated_at` datetime DEFAULT NULL,
  `day_withdraw_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Dumping data for table `user_verifications`
--

INSERT INTO `user_verifications` (`id`, `user_id`, `nationalid_type`, `nationalid_no`, `nationalid_country`, `nationalid_issued_by`, `nationalid_start_date`, `nationalid_expire_date`, `nationalid_birth_date`, `address_document_type`, `address_line1`, `address_line2`, `address_document_city`, `address_document_country`, `address_document_postal_code`, `credit_card_id`, `nationalid_verified`, `nationalid_back_verified`, `address_document_verified`, `credit_card_verified`, `selfie_verified`, `nationalid_refuse_messege`, `nationalid_back_refuse_messege`, `address_document_refuse_messege`, `selfie_refuse_messege`, `all_verified`, `created_at`, `updated_at`, `nationalid_front_file_name`, `nationalid_front_content_type`, `nationalid_front_file_size`, `nationalid_front_updated_at`, `nationalid_back_file_name`, `nationalid_back_content_type`, `nationalid_back_file_size`, `nationalid_back_updated_at`, `address_document_file_name`, `address_document_content_type`, `address_document_file_size`, `address_document_updated_at`, `selfie_file_name`, `selfie_content_type`, `selfie_file_size`, `selfie_updated_at`, `day_withdraw_count`) VALUES
(1, 1, 1, '2', 'EG', '', '2018-02-20', '2018-02-01', '2018-02-03', 'ماستر كارد', 'Al Jizah, El Haram, Maryotya,', 'Sama City Towers, Flat 212', 'Al Jizah', 'Egypt', 2255577, NULL, 1, 1, 1, 0, 1, '', 'eeee', '', '', 1, '2018-01-23 19:01:09', '2018-03-08 08:58:03', '322072_269280246465107_563812545_o.jpg', 'image/jpeg', 87781, '2018-01-23 21:35:43', '4.png', 'image/png', 1587623, '2018-02-04 14:43:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, 2, 0, '1007741489', 'SA', '', '2018-01-01', '2018-01-01', '2018-01-01', '', '0000', '0000', '', '', 21353, NULL, 1, 1, 1, 2, 1, '', '', '', '', 1, '2018-01-23 19:04:54', '2018-03-25 06:49:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, 3, 0, '123456789123', 'EG', '', '2013-02-05', '2018-01-01', '2018-01-01', 'ماستر كارد', 'دمياط', 'دمياط الجديدة', 'دمياط', 'مصر', 22022, NULL, 1, 1, 1, 1, 1, '', '', '', '', 1, '2018-01-23 19:23:14', '2018-03-29 08:23:30', 'Chrysanthemum.jpg', 'image/jpeg', 879394, '2018-01-25 07:58:46', 'Penguins.jpg', 'image/jpeg', 777835, '2018-02-08 11:48:00', 'Chrysanthemum.jpg', 'image/jpeg', 879394, '2018-02-08 14:14:51', '17553419_1749947761963372_7859631055446362296_n.jpg', 'image/jpeg', 29413, '2018-02-08 11:48:22', 5),
(4, 4, 0, '19935478258551', 'EG', '', '2018-01-01', '2018-01-01', '2018-02-04', '', 'دمياط , ارض اللواء', 'دمياط الدور التانى', 'دمياط', 'مصر', 1234589, NULL, 1, 1, 1, 0, 1, '', '', '', '', 1, '2018-01-24 08:25:54', '2018-03-14 12:09:40', 'blackcard-400x300.png', 'image/png', 86761, '2018-03-14 12:07:57', 'test-image.jpeg', 'image/jpeg', 60892, '2018-03-14 12:08:18', 'test-image.jpeg', 'image/jpeg', 60892, '2018-03-14 12:09:05', 'pending.png', 'image/png', 43317, '2018-03-14 12:08:45', 0),
(5, 5, 1, '254864125445', 'EG', '', '2018-01-01', '2018-01-25', '2016-01-06', '', '', '', '', '', 25252, NULL, 1, 1, 1, 0, 1, '', '', '', '', 1, '2018-01-24 09:28:54', '2018-03-29 03:59:08', '_310x310_2d554ab006f3fa3d5e6cdccaa6ba21a3ff63b74b0925fceaff260a45622bd6c0.jpg', 'image/jpeg', 17803, '2018-01-25 17:30:42', NULL, NULL, NULL, NULL, '8gdk1Fd.png', 'image/png', 123607, '2018-01-25 17:47:58', NULL, NULL, NULL, NULL, 0),
(6, 6, 1, '123456789', 'EG', 'eg', '2018-02-02', '2022-02-02', '1990-02-02', '', '5100', 'st51', 'دمياط', 'مصر', 5151, NULL, 1, 1, 1, 0, 1, '', '', 'الابلبللبيل', '', 1, '2018-01-24 09:29:00', '2018-03-10 14:40:25', 'photo_2018-01-22_15-14-45.jpg', 'image/jpeg', 29836, '2018-02-04 12:08:14', 'photo_2018-01-22_15-14-58.jpg', 'image/jpeg', 170699, '2018-02-01 11:51:49', 'photo_2018-01-22_15-15-00.jpg', 'image/jpeg', 110849, '2018-02-04 12:08:28', 'photo_2018-01-22_15-14-58.jpg', 'image/jpeg', 170699, '2018-02-21 09:48:31', 0),
(7, 7, 0, '22', 'SA', '', '2013-12-03', '2020-01-08', '1993-07-25', 'عقد تمليك عقار', 'EG,منصوره,المنصوره طلخا', 'المنصورة- طلخا', 'دمياط', 'Egypt', 123, NULL, 0, 0, 0, 0, 0, '', '', '', '', 1, '2018-01-25 11:25:35', '2018-03-10 13:17:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(8, 8, NULL, '898989', '', '', '2018-01-01', '2018-01-01', '2018-01-01', '', '', '', '', '', NULL, NULL, 2, 0, 0, 0, 0, '', '', '', '', 0, '2018-01-25 16:03:12', '2018-03-29 03:59:28', 'unnamed١.jpg', 'image/jpeg', 102129, '2018-01-26 06:31:38', NULL, NULL, NULL, NULL, 'Screen_Shot_2017-12-15_at_2.26.30_PM.png', 'image/png', 49949, '2018-01-26 06:32:13', NULL, NULL, NULL, NULL, 0),
(9, 9, NULL, NULL, NULL, NULL, '2018-01-01', '2018-01-01', '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 1, '2018-01-25 19:37:59', '2018-01-25 19:37:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(10, 10, NULL, NULL, '', '', '2018-02-03', '2018-02-03', '2018-02-03', '0000-00-00', '', '', '', '', NULL, NULL, 0, 0, 1, 0, 0, NULL, NULL, NULL, NULL, 0, '2018-01-27 10:19:52', '2018-02-03 08:12:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(11, 11, 0, '1234500', 'SA', NULL, '2018-01-01', '2018-01-01', '2018-01-01', '', '', '', '', '', NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 1, '2018-02-01 10:32:50', '2018-03-10 12:35:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(12, 12, NULL, '', '', '', '2018-01-01', '2018-01-01', '2018-01-01', '', '', '', '', '', NULL, NULL, 0, 0, 0, 0, 0, '', '', '', '', 0, '2018-02-06 14:06:05', '2018-03-31 14:51:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(15, 15, NULL, NULL, '', '', '2018-01-01', '2018-01-01', '2018-01-01', '', 'Giza', 'Sama City Towers, Flat 212', '', '', NULL, NULL, 1, 1, 1, 0, 1, '', '', '', '', 1, '2018-02-20 08:54:22', '2018-02-20 09:15:56', '18641343_10154805118544164_1083305544_o.jpg', 'image/jpeg', 419257, '2018-02-20 08:57:37', '18641736_10154805118534164_835243790_o.jpg', 'image/jpeg', 383463, '2018-02-20 08:59:44', '18641630_10154805114004164_2014349056_o.jpg', 'image/jpeg', 345000, '2018-02-20 09:01:15', '18676682_10154805118519164_1444280893_o.jpg', 'image/jpeg', 362230, '2018-02-20 09:00:12', 0),
(17, 17, NULL, NULL, NULL, NULL, '2018-01-01', '2018-01-01', '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0, '2018-02-28 12:02:30', '2018-02-28 12:02:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(18, 18, 0, '', 'EG', NULL, '2018-01-01', '2018-01-01', '2018-01-01', '', '', '', '', '', NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0, '2018-02-28 12:14:54', '2018-03-10 10:09:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(19, 19, 0, '', 'EG', '', '2018-01-01', '2018-01-01', '2018-01-01', '', 'Jed', 'Jed', '', '', 11111, NULL, 1, 1, 0, 0, 1, '', '', '١٢٣', '٥٥٥', 0, '2018-03-12 22:19:06', '2018-03-14 21:01:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(20, 20, NULL, '', '', '', '2018-01-01', '2018-01-01', '2018-01-01', '', '', '', '', '', NULL, NULL, 1, 1, 1, 0, 1, '', '', '', '', 1, '2018-03-14 12:39:36', '2018-03-14 12:46:26', 'Capturehjh.PNG', 'image/png', 21738, '2018-03-14 12:43:58', 'Capturekj.PNG', 'image/png', 3349, '2018-03-14 12:44:11', 'Capturekjjhkhk.PNG', 'image/png', 21993, '2018-03-14 12:44:38', 'Captureh.PNG', 'image/png', 14430, '2018-03-14 12:44:25', 0),
(21, 21, NULL, NULL, NULL, NULL, '2018-01-01', '2018-01-01', '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0, '2018-03-15 00:00:00', '2018-03-15 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `watchdogs`
--

CREATE TABLE IF NOT EXISTS `watchdogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0',
  `user_to` int(11) DEFAULT '0',
  `added_by` varchar(255) DEFAULT '0',
  `foreign_id` int(11) DEFAULT '0',
  `controller` varchar(100) DEFAULT NULL,
  `action_page` varchar(150) DEFAULT NULL,
  `details` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `device_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=225 ;

--
-- Dumping data for table `watchdogs`
--

INSERT INTO `watchdogs` (`id`, `user_id`, `user_to`, `added_by`, `foreign_id`, `controller`, `action_page`, `details`, `created_at`, `device_name`) VALUES
(1, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.233.185.3', '2018-03-30 14:06:31', '_desktop_Windows_41.233.185.3_Chrome_65.0.3325.181'),
(2, 21, 0, '21', 1, 'User_wallet_ballence', 'walletstransferpost', 'you have replaced/1000.0 SAR/with a value of/260.7 USD /Transaction ID/WAVXNHECGJPD5IO1B2', '2018-03-30 14:08:40', '_desktop_Windows_41.233.185.3_Chrome_65.0.3325.181'),
(3, 3, 0, '3', 1, 'users_bank', 'save_western_gram', 'you have added new bank for/Western Union', '2018-03-30 14:34:02', '_desktop_Windows_156.199.116.55_Chrome_65.0.3325.181'),
(4, 3, 0, '3', 1, 'balances_withdraws', 'save_western_gram', 'withdraw request/100.0 USD /Transaction ID/WBT84XRQ7WASJL6ZBV', '2018-03-30 14:34:02', '_desktop_Windows_156.199.116.55_Chrome_65.0.3325.181'),
(5, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.68.76.215', '2018-03-30 14:36:20', '_desktop_Mac_41.68.76.215_Chrome_65.0.3325.181'),
(6, 3, 0, '3', 2, 'balances_withdraws', 'save_western_gram', 'withdraw request/200.0 USD /Transaction ID/WBCXJUAKWE8094YVLZ', '2018-03-30 15:19:36', '_desktop_Windows_156.199.116.55_Chrome_65.0.3325.181'),
(7, 1, 2, '1', 1, 'User_wallet_ballence_frozen', 'create balance operation', 'balance was transferred with value of/1000.0 USD/From User/linkOnline/to the user/alm7madi/Transaction ID/BTZFLU02IRA9MXQKV8', '2018-03-30 16:35:13', '_desktop_Mac_41.68.76.215_Chrome_65.0.3325.181'),
(8, 1, 2, '1', 1, 'User_wallet_ballence_frozen', 'approvedbalance', 'Balance Delivered/1000.0 USD/From User/linkOnline/to the user/alm7madi', '2018-03-30 16:40:19', '_desktop_Mac_41.68.76.215_Chrome_65.0.3325.181'),
(9, 3, 0, '3', 3, 'balances_withdraws', 'save_western_gram', 'withdraw request/20.0 USD /Transaction ID/WBJIC06A84DTEX2HYW', '2018-03-30 17:17:12', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(10, 3, 0, '3', 4, 'balances_withdraws', 'save_western_gram', 'withdraw request/20.0 USD /Transaction ID/WBD2UK37OQM5XSR9L0', '2018-03-30 17:26:29', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(11, 3, 0, '3', 5, 'balances_withdraws', 'save_western_gram', 'withdraw request/20.0 USD /Transaction ID/WBP3XH10Q46AEWRVSL', '2018-03-30 17:32:35', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(12, 3, 0, '3', 6, 'balances_withdraws', 'save_western_gram', 'withdraw request/20.0 USD /Transaction ID/WBQEW0P7UZ8VYK5FJ1', '2018-03-30 17:33:44', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(13, 3, 0, '3', 6, 'balances_withdraws', 'cancel', 'balancewithdraw canceled/20.0  USD/Transaction ID/WBQEW0P7UZ8VYK5FJ1', '2018-03-30 17:34:20', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(14, 21, 0, '21', 11, 'User_wallet_ballence', 'walletstransferpost', 'you have replaced/2000.0 SAR/with a value of/521.4 USD /Transaction ID/WALIPYHVW6SDFQO8J0', '2018-03-30 17:38:28', '_desktop_Windows_41.233.185.3_Chrome_65.0.3325.181'),
(15, 3, 0, '10', 5, 'balances_withdraws', 'withdrawstatus', 'completed withdraw request/20.0 USD /Transaction ID/WBP3XH10Q46AEWRVSL', '2018-03-30 17:39:37', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(16, 3, 0, '10', 4, 'balances_withdraws', 'withdrawstatus', 'rejected withdraw request/20.0 USD /Transaction ID/WBD2UK37OQM5XSR9L0', '2018-03-30 17:40:11', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(17, 3, 0, '10', 4, 'balances_withdraws', 'withdrawstatus', 'completed withdraw request/20.0 USD /Transaction ID/WBD2UK37OQM5XSR9L0', '2018-03-30 17:40:43', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(18, 3, 0, '10', 3, 'balances_withdraws', 'withdrawstatus', 'refunded withdraw request/20.0 USD /Transaction ID/WBJIC06A84DTEX2HYW', '2018-03-30 18:04:00', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(19, 3, 0, '3', 15, 'User_wallet_ballence', 'walletstransferpost', 'you have replaced/100.0 USD/with a value of/367.5 SAR /Transaction ID/WACKV907NBA81DTL3W', '2018-03-30 18:58:59', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(20, 3, 0, '3', 7, 'balances_withdraws', 'save_western_gram', 'withdraw request/20.0 USD /Transaction ID/WBBOVHLFQM7C9X4YIG', '2018-03-30 19:28:30', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(21, 3, 0, '10', 7, 'balances_withdraws', 'withdrawstatus', 'recieved withdraw request/20.0 USD /Transaction ID/WBBOVHLFQM7C9X4YIG', '2018-03-30 19:31:39', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(22, 3, 0, '10', 7, 'balances_withdraws', 'withdrawstatus', 'workon withdraw request/20.0 USD /Transaction ID/WBBOVHLFQM7C9X4YIG', '2018-03-30 19:32:48', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(23, 3, 0, '10', 7, 'balances_withdraws', 'withdrawstatus', 'exception withdraw request/20.0 USD /Transaction ID/WBBOVHLFQM7C9X4YIG', '2018-03-30 19:34:12', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(24, 3, 0, '3', 8, 'balances_withdraws', 'save_western_gram', 'withdraw request/22.0 USD /Transaction ID/WB2PMX0CLAT3GD4OQE', '2018-03-30 20:04:02', '_desktop_Windows_156.198.154.26_Chrome_65.0.3325.181'),
(25, 19, 0, '19', 0, 'userds ', ' Sign in ', 'Sign in from ip :5.41.87.145', '2018-03-31 03:00:46', '_desktop_Mac_5.41.87.145_Safari_11.1'),
(26, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.68.76.215', '2018-03-31 04:26:17', '_desktop_Mac_41.68.76.215_Chrome_65.0.3325.181'),
(27, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.117.199', '2018-03-31 11:44:08', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(28, 3, 20, '3', 2, 'User_wallet_ballence_frozen', 'create balance operation', 'balance was transferred with value of/20.0 USD/From User/EngineeraAmiraa/to the user/engytoma/Transaction ID/BTJUN78V6QY40P3ADG', '2018-03-31 11:48:40', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(29, 3, 0, '3', 9, 'balances_withdraws', 'save_western_gram', 'withdraw request/200.0 USD /Transaction ID/WBDL492NY61KTJSIPE', '2018-03-31 11:55:03', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(30, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.117.199', '2018-03-31 11:57:56', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(31, 3, 0, '3', 10, 'balances_withdraws', 'save_western_gram', 'withdraw request/23.0 USD /Transaction ID/WBP0ARUVJGYHD2C98X', '2018-03-31 12:04:23', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(32, 3, 0, '10', 9, 'balances_withdraws', 'withdrawstatus', 'completed withdraw request/200.0 USD /Transaction ID/WBDL492NY61KTJSIPE', '2018-03-31 12:04:53', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(33, 1, 0, '1', 0, 'userds', 'failed signin', '41.68.76.215', '2018-03-31 12:23:43', '_desktop_Mac_41.68.76.215_Chrome_65.0.3325.181'),
(34, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.117.199', '2018-03-31 12:23:46', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(35, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.68.76.215', '2018-03-31 12:24:25', '_desktop_Mac_41.68.76.215_Chrome_65.0.3325.181'),
(36, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.117.199', '2018-03-31 12:24:46', '_desktop_Mac_41.68.76.215_Chrome_65.0.3325.181'),
(37, 3, 0, '3', 0, 'userds', 'failed signin', '196.221.117.199', '2018-03-31 12:25:23', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(38, 3, 0, '3', 0, 'userds', 'failed signin', '196.221.117.199', '2018-03-31 12:26:14', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(39, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.117.199', '2018-03-31 12:26:26', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(40, 3, 0, '3', 0, 'userds', 'failed signin', '196.221.117.199', '2018-03-31 12:27:41', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(41, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.117.199', '2018-03-31 12:28:34', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(42, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-03-31 13:08:28', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(43, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-03-31 13:13:46', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(44, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-03-31 13:16:20', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(45, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.68.6.205', '2018-03-31 13:27:43', '_desktop_Mac_41.68.6.205_Chrome_65.0.3325.181'),
(46, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.68.6.205', '2018-03-31 13:29:05', '_desktop_Mac_41.68.6.205_Chrome_65.0.3325.181'),
(47, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-03-31 13:35:13', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(48, 10, 0, '10', 23, 'email_tempalte', 'mass_mail', 'send mass mail', '2018-03-31 13:35:48', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(49, 10, 0, '10', 23, 'email_tempalte', 'mass_mail', 'send mass mail', '2018-03-31 13:36:00', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(50, 10, 0, '10', 23, 'email_tempalte', 'mass_mail', 'send mass mail', '2018-03-31 13:36:13', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(51, 10, 0, '10', 23, 'email_tempalte', 'mass_mail', 'send mass mail', '2018-03-31 13:36:26', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(52, 1, 2, '10', 1, 'users_wallets_ballences_frozens', 'refundedbalance', 'balance refunded/1000.0 USD/from user/linkOnline/to user/alm7madi', '2018-03-31 13:39:25', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(53, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-03-31 13:39:58', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(54, 3, 0, '10', 0, 'user_notes', 'show_user_notes', 'Admin dadded a note for user/EngineeraAmiraa', '2018-03-31 13:41:11', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(55, 3, 0, '10', 0, 'user_notes', 'show_user_notes', 'Admin dadded a note for user/EngineeraAmiraa', '2018-03-31 13:41:13', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(56, 3, 0, '3', 11, 'balances_withdraws', 'save_western_gram', 'withdraw request/30.0 USD /Transaction ID/WB82XO1MIP4YTV6UK7', '2018-03-31 13:57:12', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(57, 3, 0, '3', 11, 'balances_withdraws', 'cancel', 'balancewithdraw canceled/30.0  USD/Transaction ID/WB82XO1MIP4YTV6UK7', '2018-03-31 13:57:18', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(58, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-03-31 14:00:25', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(59, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-03-31 14:00:41', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(60, 21, 0, '21', 0, 'userds', 'failed signin', '156.198.234.146', '2018-03-31 14:00:50', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(61, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.68.6.205', '2018-03-31 14:34:28', '_desktop_Mac_41.68.6.205_Chrome_65.0.3325.181'),
(62, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.68.6.205', '2018-03-31 14:34:57', '_desktop_Mac_41.68.6.205_Chrome_65.0.3325.181'),
(63, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.68.6.205', '2018-03-31 14:37:37', '_desktop_Mac_41.68.6.205_Chrome_65.0.3325.181'),
(64, 3, 0, '10', 0, 'user_notes', 'show_user_notes', 'Admin dadded a note for user/EngineeraAmiraa', '2018-03-31 14:41:57', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(65, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-03-31 14:57:45', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(66, 11, 0, '11', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-03-31 15:11:03', '_desktop_Windows_156.198.234.146_Chrome_64.0.3282.186'),
(67, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-03-31 15:17:18', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(68, 9, 0, '9', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.117.199', '2018-03-31 15:53:38', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(69, 9, 3, '9', 3, 'User_wallet_ballence_frozen', 'create balance operation', 'balance was transferred with value of/200.0 USD/From User/Amira/to the user/EngineeraAmiraa/Transaction ID/BT7XAD5MPNW1JK0ZFY', '2018-03-31 15:54:30', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(70, 9, 3, '9', 4, 'User_wallet_ballence_frozen', 'create balance operation', 'balance was transferred with value of/20.0 USD/From User/Amira/to the user/EngineeraAmiraa/Transaction ID/BT1MORGBZJW30Q756S', '2018-03-31 15:55:09', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(71, 9, 3, '9', 5, 'User_wallet_ballence_frozen', 'create balance operation', 'Balance Delivered/30.0 USD/From User/Amira/to the user/EngineeraAmiraa/Transaction ID/BTR1YA6XNSQ8W5BPK7', '2018-03-31 15:55:52', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(72, 3, 9, '3', 6, 'User_wallet_ballence_frozen', 'create balance operation', 'balance was transferred with value of/200.0 USD/From User/EngineeraAmiraa/to the user/Amira/Transaction ID/BTM3850C7QXNWD2ORY', '2018-03-31 15:56:58', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(73, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.68.6.205', '2018-03-31 15:58:34', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(74, 3, 9, '3', 7, 'User_wallet_ballence_frozen', 'create balance operation', 'balance was transferred with value of/200.0 USD/From User/EngineeraAmiraa/to the user/Amira/Transaction ID/BTZF5QV27TXIG34BUS', '2018-03-31 15:58:46', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(75, 9, 3, '3', 3, 'User_wallet_ballence_frozen', 'deleteOperation', 'balance refunded/200.0 USD/from user/Amira/to user/EngineeraAmiraa', '2018-03-31 15:59:20', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(76, 3, 9, '3', 6, 'User_wallet_ballence_frozen', 'approvedbalance', 'Balance Delivered/200.0 USD/From User/EngineeraAmiraa/to the user/Amira', '2018-03-31 15:59:44', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(77, 9, 3, '9', 8, 'User_wallet_ballence_frozen', 'create balance operation', 'balance was transferred with value of/40.0 USD/From User/Amira/to the user/EngineeraAmiraa/Transaction ID/BT5LQUSCG8RHWIVTE9', '2018-03-31 16:02:37', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(78, 9, 3, '10', 8, 'users_wallets_ballences_frozens', 'refundedbalance', 'balance refunded/40.0 USD/from user/Amira/to user/EngineeraAmiraa', '2018-03-31 16:03:13', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(79, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-03-31 16:44:10', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(80, 3, 0, '3', 1, 'carts', 'paypalconfirm ', 'pay money for cart with value  450.0', '2018-03-31 17:20:43', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(81, 3, 0, '3', 1, 'carts', 'cartbalancetouser', 'add balance by cart number  450.0', '2018-03-31 17:21:44', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(82, 10, 0, '0', 1, 'users banks', ' update user bank', ' update user bank  with id :  1', '2018-03-31 17:26:48', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(83, 3, 0, '3', 2, 'carts', 'paypalconfirm ', 'pay money for cart with value  450.0', '2018-03-31 17:30:30', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(84, 3, 0, '3', 2, 'carts', 'cartbalancetouser', 'add balance by cart number  450.0', '2018-03-31 17:30:57', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(85, 3, 0, '3', 38, 'User_wallet_ballence', 'walletstransferpost', 'you have replaced/2000.0 EGP/with a value of/415.6 SAR /Transaction ID/WA04DZXTHCJANKSUPG', '2018-03-31 17:41:48', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(86, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-03-31 18:11:17', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(87, 3, 0, '10', 2, 'users_bank', 'users_banks_save_egyptian_mail', 'you have added new bank for/Egypt Post', '2018-03-31 18:38:57', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(88, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-03-31 19:15:28', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(89, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-03-31 19:16:32', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(90, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :197.50.59.191', '2018-03-31 21:40:54', '_desktop_Mac_197.50.59.191_Chrome_65.0.3325.181'),
(91, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :197.50.59.191', '2018-03-31 21:42:22', '_desktop_Mac_197.50.59.191_Chrome_65.0.3325.181'),
(92, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.57.127', '2018-03-31 22:04:10', '_desktop_Windows_41.44.57.127_Chrome_65.0.3325.181'),
(93, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.57.127', '2018-03-31 22:05:50', '_desktop_Mac_41.68.6.12_Chrome_65.0.3325.181'),
(94, 19, 0, '19', 0, 'userds ', ' Sign in ', 'Sign in from ip :51.36.77.141', '2018-04-01 03:07:17', '_desktop_Mac_51.36.77.141_Safari_11.1'),
(95, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :51.36.77.141', '2018-04-01 03:39:43', '_desktop_Mac_51.36.77.141_Safari_11.1'),
(96, 2, 0, '2', 39, 'User_wallet_ballence', 'walletstransferpost', 'you have replaced/1000.0 USD/with a value of/3675.0 SAR /Transaction ID/WAQZP1UB3GHVTYR7OW', '2018-04-01 03:52:17', '_desktop_Mac_51.36.77.141_Safari_11.1'),
(97, 2, 0, '2', 40, 'User_wallet_ballence', 'walletstransferpost', 'you have replaced/1000.0 USD/with a value of/17267.6 EGP /Transaction ID/WASMTZH7XVU39L2F8R', '2018-04-01 03:53:01', '_desktop_Mac_51.36.77.141_Safari_11.1'),
(98, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.117.199', '2018-04-01 11:20:39', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(99, 9, 0, '9', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.117.199', '2018-04-01 12:35:01', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(100, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-01 12:37:34', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(101, 20, 0, '20', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-01 12:52:49', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(102, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-01 12:53:51', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(103, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-01 13:06:21', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(104, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-01 13:06:37', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(105, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.117.199', '2018-04-01 13:26:13', '_desktop_Windows_196.221.117.199_Chrome_65.0.3325.181'),
(106, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.68.40.35', '2018-04-01 14:06:49', '_desktop_Mac_41.68.40.35_Chrome_65.0.3325.181'),
(107, 21, 0, '21', 41, 'User_wallet_ballence', 'walletstransferpost', 'you have replaced/300.0 SAR/with a value of/78.21 USD /Transaction ID/WAEDKBWSOQ21GM0YAC', '2018-04-01 14:08:24', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(108, 11, 0, '11', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-01 14:15:59', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(109, 9, 0, '9', 42, 'User_wallet_ballence', 'walletstransferpost', 'you have replaced/22.0 USD/with a value of/80.85 SAR /Transaction ID/WATB82VZFIMGH9CJUN', '2018-04-01 15:49:39', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(110, 3, 0, '3', 1, 'tickets', 'new / ticket', 'new ticket has been added with number/ 793190 222', '2018-04-01 15:55:52', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(111, 3, 0, '3', 12, 'balances_withdraws', 'save_western_gram', 'withdraw request/22.0 USD /Transaction ID/WBSJM5X0AN1GFOPDWH', '2018-04-01 16:05:37', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(112, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-01 16:08:04', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(113, 3, 0, '10', 12, 'balances_withdraws', 'withdrawstatus', 'exception withdraw request/22.0 USD /Transaction ID/WBSJM5X0AN1GFOPDWH', '2018-04-01 16:08:47', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(114, 3, 0, '3', 3, 'Onlinebanks', 'banktransfer_post', 'payment request/140238/addvalue/100.0', '2018-04-01 17:01:50', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(115, 3, 0, '3', 3, 'Onlinebanks', 'add_payment_data', 'payment information added/140238/addvalue/100.0', '2018-04-01 17:02:13', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(116, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :197.45.219.97', '2018-04-01 17:08:13', '_desktop_Mac_197.45.219.97_Chrome_65.0.3325.162'),
(117, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :197.45.219.97', '2018-04-01 17:09:19', '_desktop_Mac_197.45.219.97_Chrome_65.0.3325.162'),
(118, 3, 0, '10', 3, 'Onlinebanks', 'payment_exception', 'excepion payment request/ 140238/addvalue/100.0', '2018-04-01 17:15:35', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(119, 1, 0, '1', 45, 'User_wallet_ballence', 'walletstransferpost', 'you have replaced/10000.0 EGP/with a value of/555.0 USD /Transaction ID/WACINKE59OAX17LYPH', '2018-04-01 17:21:09', '_desktop_Mac_197.45.219.97_Chrome_65.0.3325.162'),
(120, 1, 0, '1', 46, 'User_wallet_ballence', 'walletstransferpost', 'you have replaced/10000.0 SAR/with a value of/2607.0 USD /Transaction ID/WAOSHCD83KI7XJ2MRY', '2018-04-01 17:21:29', '_desktop_Mac_197.45.219.97_Chrome_65.0.3325.162'),
(121, 10, 0, '10', 23, 'email_tempalte', 'mass_mail', 'send mass mail', '2018-04-01 17:35:40', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(122, 10, 0, '10', 23, 'email_tempalte', 'mass_mail', 'send mass mail', '2018-04-01 17:35:43', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(123, 3, 0, '3', 3, 'Onlinebanks', 'exception_reply', 'reply to exception/ 140238/addvalue/100.0', '2018-04-01 17:42:09', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(124, 3, 0, '10', 3, 'carts', 'offline_payment ', 'pay money for cart with value  90.0', '2018-04-01 18:05:38', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(125, 3, 0, '10', 3, 'Onlinebanks', 'complete_payment ', 'complete payment request/ 140238/addvalue/100.0', '2018-04-01 18:05:38', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(126, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-01 18:06:02', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(127, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-01 18:11:32', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(128, 7, 0, '7', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-01 18:12:38', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(129, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-01 18:17:37', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(130, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-01 18:37:38', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(131, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.74.119', '2018-04-01 21:07:56', '_desktop_Windows_156.198.74.119_Chrome_65.0.3325.181'),
(132, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.74.119', '2018-04-01 21:16:30', '_desktop_Windows_156.198.74.119_Chrome_65.0.3325.181'),
(133, 19, 0, '19', 0, 'userds ', ' Sign in ', 'Sign in from ip :51.36.77.141', '2018-04-02 00:37:35', '_desktop_Mac_51.36.77.141_Safari_11.1'),
(134, 3, 0, '3', 4, 'Onlinebanks', 'banktransfer_post', 'payment request/653801/addvalue/50.0', '2018-04-02 03:08:54', '_desktop_Mac_41.68.40.35_Chrome_65.0.3325.181'),
(135, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :102.184.73.229', '2018-04-02 11:38:04', '_desktop_Windows_102.184.73.229_Chrome_65.0.3325.181'),
(136, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :102.184.73.229', '2018-04-02 11:38:45', '_desktop_Windows_102.184.73.229_Chrome_65.0.3325.181'),
(137, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-02 12:01:39', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(138, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-02 12:36:29', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(139, 7, 0, '7', 0, 'userds', 'failed signin', '156.198.234.146', '2018-04-02 12:48:02', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(140, 7, 0, '7', 0, 'userds', 'failed signin', '156.198.234.146', '2018-04-02 12:48:20', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(141, 7, 0, '7', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-02 12:49:02', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(142, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-02 12:50:00', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(143, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-02 12:59:18', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(144, 10, 0, '0', 2, 'helpfaq', 'update help  ', 'update help with id :  2', '2018-04-02 13:35:58', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(145, 10, 0, '0', 2, 'helpfaq', 'update help  ', 'update help with id :  2', '2018-04-02 13:36:08', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(146, 3, 0, '10', 0, 'user_notes', 'show_user_notes', 'Admin dadded a note for user/EngineeraAmiraa', '2018-04-02 13:37:21', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(147, 3, 0, '10', 0, 'user_notes', 'show_user_notes', 'Admin dadded a note for user/EngineeraAmiraa', '2018-04-02 13:37:25', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(148, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :176.47.48.91', '2018-04-02 13:44:34', '_desktop_Mac_176.47.48.91_Safari_11.1'),
(149, 10, 0, '10', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.68.40.35', '2018-04-02 13:45:30', '_desktop_Mac_41.68.40.35_Chrome_65.0.3325.181'),
(150, 2, 1, '2', 9, 'User_wallet_ballence_frozen', 'create balance operation', 'balance was transferred with value of/99.09 USD/From User/alm7madi/to the user/linkOnline/Transaction ID/BTM93FYO47R2AZLU5V', '2018-04-02 13:49:33', '_desktop_Mac_176.47.48.91_Safari_11.1'),
(151, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-02 13:59:25', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(152, 3, 0, '3', 3, 'userds', 'edit_profile', ' /first name has been updated from amira to Eng /last name has been updated from Hassan to Amira by user/ EngineeraAmiraa', '2018-04-02 14:55:25', '_desktop_Windows_102.184.73.229_Chrome_65.0.3325.181'),
(153, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-02 16:10:25', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(154, 20, 0, '20', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-02 16:26:22', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(155, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-02 16:31:39', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(156, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-02 16:32:23', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(157, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.198.234.146', '2018-04-02 16:33:10', '_desktop_Windows_156.198.234.146_Chrome_65.0.3325.181'),
(158, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-02 17:03:42', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(159, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-02 17:07:00', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(160, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-02 17:15:28', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(161, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-02 17:18:02', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(162, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-02 17:18:58', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(163, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-02 17:19:00', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(164, 5, 0, '5', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-02 17:20:25', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(165, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-02 17:23:07', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(166, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-02 17:27:37', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(167, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-02 18:56:56', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(168, 5, 0, '5', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-02 18:57:27', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(169, 5, 0, '5', 4, 'users_bank', 'save', 'you have added new bank for/HOUSING BANK FOR TRADE AND FINANCE, THE', '2018-04-02 19:54:09', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(170, 5, 0, '5', 13, 'balances_withdraws', 'save', 'withdraw request/93.0  USD/Transaction ID/WBH28T0VXPSZNO9MJG', '2018-04-02 19:54:09', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(171, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.201.21.29', '2018-04-02 23:47:19', '_desktop_Windows_156.201.21.29_Chrome_65.0.3325.181'),
(172, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.68.40.35', '2018-04-03 03:57:15', '_desktop_Mac_41.68.40.35_Chrome_65.0.3325.181'),
(173, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.68.40.35', '2018-04-03 04:01:04', '_desktop_Mac_41.68.40.35_Chrome_65.0.3325.181'),
(174, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.69.222.118', '2018-04-03 11:31:02', '_desktop_Windows_41.69.222.118_Chrome_65.0.3325.181'),
(175, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-03 11:44:24', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(176, 7, 0, '7', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-03 11:45:32', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(177, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.69.222.118', '2018-04-03 11:45:48', '_desktop_Windows_41.69.222.118_Chrome_65.0.3325.181'),
(178, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.68.40.35', '2018-04-03 11:46:10', '_desktop_Mac_41.68.40.35_Chrome_65.0.3325.181'),
(179, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-03 12:25:51', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(180, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-03 12:26:52', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(181, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-03 12:36:24', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(182, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :95.185.146.197', '2018-04-03 12:40:43', '_desktop_Mac_95.185.146.197_Safari_11.1'),
(183, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-03 12:44:38', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(184, 3, 0, '2', 4, 'Onlinebanks', 'payment_exception', 'excepion payment request/ 653801/addvalue/50.0', '2018-04-03 13:17:40', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(185, 3, 0, '3', 5, 'Onlinebanks', 'banktransfer_post', 'payment request/736459/addvalue/500.0', '2018-04-03 13:28:41', '_desktop_Windows_41.69.222.118_Chrome_65.0.3325.181'),
(186, 3, 0, '2', 3, 'cart', 'disable cart', 'disable cart with id :  3', '2018-04-03 13:48:40', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(187, 3, 0, '2', 3, 'cart', 'refund cart', 'refund cart with id :  3', '2018-04-03 13:48:58', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(188, 9, 3, '1', 12, 'users_wallets_ballences_frozens', 'refundedbalance', 'balance refunded/20.0 USD/from user/Amira/to user/EngineeraAmiraa', '2018-04-03 14:34:32', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(189, 3, 9, '1', 7, 'users_wallets_ballences_frozens', 'refundedbalance', 'balance refunded/200.0 USD/from user/EngineeraAmiraa/to user/Amira', '2018-04-03 14:35:03', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(190, 21, 0, '2', 0, 'user_notes', 'show_user_notes', 'Admin dadded a note for user/khalidkhalil1993', '2018-04-03 15:20:09', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(191, 3, 0, '3', 1, 'credit_cards', 'create_credit_card', 'Credit Card has been added by/EngineeraAmiraa /with status/ Pending /with number/ 123', '2018-04-03 15:55:00', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(192, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-03 17:11:38', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(193, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-03 17:11:40', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(194, 0, 0, 'A2', 5, 'cart', 'cart_payforcart ', 'pay money for cart of all users with value  81.0', '2018-04-03 17:43:26', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(195, 1, 0, '2', 0, 'user_notes', 'show_user_notes', 'Admin dadded a note for user/linkOnline', '2018-04-03 18:08:51', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(196, 1, 0, '2', 0, 'user_notes', 'show_user_notes', 'Admin dadded a note for user/linkOnline', '2018-04-03 18:08:59', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(197, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-03 18:30:41', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(198, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :197.45.219.97', '2018-04-03 18:57:46', '_desktop_Mac_197.45.219.97_Chrome_65.0.3325.162'),
(199, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :197.45.219.97', '2018-04-03 18:59:27', '_desktop_Mac_197.45.219.97_Chrome_65.0.3325.162'),
(200, 1, 2, '1', 48, 'users_wallets_ballences_frozens', 'approvedbalance', 'Balance Transfer has been confirmed with value/10.0 USD/from user/linkOnline/to user/alm7madi', '2018-04-03 19:13:16', '_desktop_Mac_197.45.219.97_Chrome_65.0.3325.162'),
(201, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.199.219.93', '2018-04-03 22:41:57', '_desktop_Windows_156.199.219.93_Chrome_65.0.3325.181'),
(202, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :156.199.219.93', '2018-04-03 22:50:22', '_desktop_Windows_156.199.219.93_Chrome_65.0.3325.181'),
(203, 3, 0, '3', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.41.202', '2018-04-04 11:51:17', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(204, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.41.202', '2018-04-04 11:51:37', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(205, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-04 12:39:33', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(206, 1, 0, '1', 0, 'userds ', ' Sign in ', 'Sign in from ip :41.44.46.196', '2018-04-04 12:40:23', '_desktop_Windows_41.44.46.196_Chrome_65.0.3325.181'),
(207, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.41.202', '2018-04-04 12:56:37', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(208, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.41.202', '2018-04-04 13:08:06', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(209, 21, 0, '21', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.41.202', '2018-04-04 13:09:39', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(210, 6, 0, '6', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.41.202', '2018-04-04 13:09:49', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(211, 21, 0, '21', NULL, 'credit_cards', 'create_credit_card', 'Credit Card can''t be added by/khalidkhalil1993', '2018-04-04 13:23:59', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(212, 21, 0, '21', 2, 'credit_cards', 'create_credit_card', 'Credit Card has been added by/khalidkhalil1993 /with status/ Pending /with number/ 378734493671000', '2018-04-04 13:24:18', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(213, 21, 0, '21', 3, 'credit_cards', 'create_credit_card', 'Credit Card has been added by/khalidkhalil1993 /with status/ Pending /with number/ 6011111111111117', '2018-04-04 13:26:26', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(214, 21, 0, '21', NULL, 'credit_cards', 'create_credit_card', 'Credit Card can''t be added by/khalidkhalil1993', '2018-04-04 13:27:06', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(215, 21, 0, '21', 4, 'credit_cards', 'create_credit_card', 'Credit Card has been added by/khalidkhalil1993 /with status/ Pending /with number/ 6011111111111117', '2018-04-04 13:27:13', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(216, 21, 0, '21', NULL, 'credit_cards', 'create_credit_card', 'Credit Card can''t be added by/khalidkhalil1993', '2018-04-04 13:28:55', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(217, 6, 0, '6', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.41.202', '2018-04-04 13:30:01', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(218, 6, 0, '6', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.41.202', '2018-04-04 13:30:31', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(219, 21, 0, '21', NULL, 'credit_cards', 'create_credit_card', 'Credit Card can''t be added by/khalidkhalil1993', '2018-04-04 13:31:11', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(220, 21, 0, '21', NULL, 'credit_cards', 'create_credit_card', 'Credit Card can''t be added by/khalidkhalil1993', '2018-04-04 13:33:44', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(221, 3, 0, '3', 2, 'users_bank', 'edit/ egyptian_post', 'Bank data has been modified/Egypt Post', '2018-04-04 13:43:42', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(222, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.41.202', '2018-04-04 13:52:17', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(223, 2, 0, '2', 0, 'userds ', ' Sign in ', 'Sign in from ip :196.221.41.202', '2018-04-04 14:05:32', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181'),
(224, 21, 0, '21', 5, 'credit_cards', 'create_credit_card', 'Credit Card has been added by/khalidkhalil1993 /with status/ Pending /with number/ 5610591081018250', '2018-04-04 14:09:09', '_desktop_Windows_196.221.41.202_Chrome_65.0.3325.181');

-- --------------------------------------------------------

--
-- Table structure for table `withdraws_banks`
--

CREATE TABLE IF NOT EXISTS `withdraws_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `withdraw_id` int(11) NOT NULL,
  `currencies_withdraw_ratios_id` int(11) DEFAULT NULL,
  `personal_name` varchar(255) DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `nationalid` varchar(255) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `branch` varchar(255) DEFAULT NULL,
  `branchnumber` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `account_email` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `visa_cvv` int(11) DEFAULT NULL,
  `swiftcode` varchar(255) DEFAULT NULL,
  `valid_swift_code` varchar(255) DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `iban` varchar(255) DEFAULT NULL,
  `bban` varchar(255) DEFAULT NULL,
  `checksum` int(11) DEFAULT NULL,
  `bank_code` int(11) DEFAULT NULL,
  `check_digit` int(11) DEFAULT NULL,
  `sepa` tinyint(1) DEFAULT NULL,
  `iban_validity` varchar(255) DEFAULT NULL,
  `iban_checksum` varchar(255) DEFAULT NULL,
  `iban_length` varchar(255) DEFAULT NULL,
  `iban_structure` varchar(255) DEFAULT NULL,
  `account_checksum` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `withdraws_banks`
--

INSERT INTO `withdraws_banks` (`id`, `user_id`, `bank_id`, `withdraw_id`, `currencies_withdraw_ratios_id`, `personal_name`, `business_name`, `nationalid`, `birth_date`, `relation`, `nickname`, `bank_name`, `country`, `country_code`, `branch`, `branchnumber`, `phone`, `account_name`, `account_email`, `account_number`, `visa_cvv`, `swiftcode`, `valid_swift_code`, `expire_date`, `status`, `city`, `address`, `postal_code`, `iban`, `bban`, `checksum`, `bank_code`, `check_digit`, `sepa`, `iban_validity`, `iban_checksum`, `iban_length`, `iban_structure`, `account_checksum`, `currency`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 1, 4, 'aa', NULL, NULL, '2018-03-20', 'a', 'a', 'Western Union', 'EG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', '2018-03-30 10:34:02', '2018-03-30 10:34:02'),
(2, 3, 1, 2, 4, 'aa', NULL, NULL, '2018-03-20', 'a', 'a', 'Western Union', 'EG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', '2018-03-30 11:19:36', '2018-03-30 11:19:36'),
(3, 3, 1, 3, 4, 'aa', NULL, NULL, '2018-03-20', 'a', 'a', 'Western Union', 'EG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', '2018-03-30 13:17:12', '2018-03-30 13:17:12'),
(4, 3, 1, 4, 4, 'aa', NULL, NULL, '2018-03-20', 'a', 'a', 'Western Union', 'EG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', '2018-03-30 13:26:29', '2018-03-30 13:26:29'),
(5, 3, 1, 5, 4, 'aa', NULL, NULL, '2018-03-20', 'a', 'a', 'Western Union', 'EG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', '2018-03-30 13:32:35', '2018-03-30 13:32:35'),
(6, 3, 1, 6, 4, 'aa', NULL, NULL, '2018-03-20', 'a', 'a', 'Western Union', 'EG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', '2018-03-30 13:33:44', '2018-03-30 13:33:44'),
(7, 3, 1, 7, 4, 'aa', NULL, NULL, '2018-03-20', 'a', 'a', 'Western Union', 'EG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', '2018-03-30 15:28:30', '2018-03-30 15:28:30'),
(8, 3, 1, 8, 4, 'aa', NULL, NULL, '2018-03-20', 'a', 'a', 'Western Union', 'EG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', '2018-03-30 16:04:02', '2018-03-30 16:04:02'),
(9, 3, 1, 9, 4, 'aa', NULL, NULL, '2018-03-20', 'a', 'a', 'Western Union', 'EG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', '2018-03-31 07:55:03', '2018-03-31 07:55:03'),
(10, 3, 1, 10, 4, 'aa', NULL, NULL, '2018-03-20', 'a', 'a', 'Western Union', 'EG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', '2018-03-31 08:04:23', '2018-03-31 08:04:23'),
(11, 3, 1, 11, 4, 'aa', NULL, NULL, '2018-03-20', 'a', 'a', 'Western Union', 'EG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', '2018-03-31 09:57:12', '2018-03-31 09:57:12'),
(12, 3, 1, 12, 4, 'aa', NULL, NULL, '2018-03-20', 'a', 'a', 'Western Union', 'EG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', '2018-04-01 12:05:37', '2018-04-01 12:05:37'),
(13, 5, 4, 13, 14, 'fssfsafsfsdf', '', NULL, NULL, 'sdfsdf', 'sdfsdfsd', 'HOUSING BANK FOR TRADE AND FINANCE, THE', 'Jordan', 'JO', NULL, NULL, NULL, NULL, NULL, '000048324100105001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'JO55HBHO0520000048324100105001', 'HBHO0520000048324100105001', 55, NULL, NULL, 0, 'Valid', 'Valid', 'Valid', 'Valid', 'No Validation', 'USD', '2018-04-02 15:54:09', '2018-04-02 15:54:09');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
