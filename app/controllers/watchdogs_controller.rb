class WatchdogsController < ApplicationController
  	before_filter :authenticate_userd!

	# GET /whatchdogs
	def index

		begin
		@watchdogs = Watchdog.where(:user_id => current_userd.id).all.paginate(page: params[:page], per_page: 3).order('created_at DESC')

		rescue => e
		    @code = SecureRandom.hex

			@log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
			redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
		end

	end

	# show watchdog of user
	def show

		begin
	    @watchdog = Watchdog.where(:user_id => current_userd.id).first

	    rescue => e
		    @code = SecureRandom.hex

			@log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
			redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
		end

	end
  
  	def search

	  	begin
	  	rescue => e
			@code = SecureRandom.hex

			@log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
			redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
		end
  	end

  	# get results of search watchdogs
  	def searchpost

  		begin
		if !params[:watchdogserarch]
			params[:watchdogserarch]= params 
		end

		if params[:watchdogserarch][:contrtyp].include?('all') && !params[:watchdogserarch][:user_id].blank?
			if params[:watchdogserarch][:end] == '' 
				@watchdogs = Watchdog.where("created_at >= :start_date AND user_id = :userid ",
								{:start_date => params[:watchdogserarch][:start],:userid => params[:watchdogserarch][:user_id]}).all

			elsif params[:watchdogserarch][:start] == ''
				@watchdogs = Watchdog.where("created_at <= :end_date AND user_id = :userid",
								{:end_date => params[:watchdogserarch][:end],:userid => params[:watchdogserarch][:user_id]}).all
				
			elsif params[:watchdogserarch][:start] == '' && params[:watchdogserarch][:end] == ''
				@watchdogs = Watchdog.where("user_id = :userid", {:userid => params[:watchdogserarch][:user_id]}).all
					
			else
				@watchdogs = Watchdog.where("created_at >= :start_date AND created_at <= :end_date AND user_id = :userid",
								{:start_date => params[:watchdogserarch][:start],:end_date => params[:watchdogserarch][:end],:userid => params[:watchdogserarch][:user_id]}).all
			end
			
	    elsif params[:watchdogserarch][:contrtyp].include?('all') 
			if params[:watchdogserarch][:end] == ''
				@watchdogs = Watchdog.where("created_at >= :start_date ",
								{:start_date => params[:watchdogserarch][:start]}).all
				
			elsif params[:watchdogserarch][:start] == ''
				@watchdogs = Watchdog.where("created_at <= :end_date",
								{:end_date => params[:watchdogserarch][:end]}).all
					
			elsif params[:watchdogserarch][:end] == '' && params[:watchdogserarch][:start] == ''
				@watchdogs = Watchdog.all
					
			else
				@watchdogs = Watchdog.where("created_at >= :start_date AND created_at <= :end_date ",
								{:start_date => params[:watchdogserarch][:start],:end_date => params[:watchdogserarch][:end]}).all
			end
		
		elsif  !params[:watchdogserarch][:user_id].blank?
		    if params[:watchdogserarch][:end] == ''
				@watchdogs = Watchdog.where("created_at >= :start_date AND controller in (:type) AND user_id = :userid",
								{:start_date => params[:watchdogserarch][:start],:type => params[:watchdogserarch][:contrtyp],:userid => params[:watchdogserarch][:user_id]}).all
				
			elsif params[:watchdogserarch][:start] == ''
				@watchdogs = Watchdog.where("created_at <= :end_date  AND controller in (:type) AND user_id = :userid",
								{:end_date => params[:watchdogserarch][:end],:type => params[:watchdogserarch][:contrtyp],:userid => params[:watchdogserarch][:user_id]}).all
					
			elsif params[:watchdogserarch][:end] == '' && params[:watchdogserarch][:start] == ''
				@watchdogs = Watchdog.where("controller in (:type) AND user_id = :userid",
								{:type => params[:watchdogserarch][:contrtyp],:userid => params[:watchdogserarch][:user_id]}).all
					
			else
				@watchdogs = Watchdog.where("created_at >= :start_date AND created_at <= :end_date  AND controller in (:type) AND user_id = :userid",
								{:start_date => params[:watchdogserarch][:start],:end_date => params[:watchdogserarch][:end],:type => params[:watchdogserarch][:contrtyp],:userid => params[:watchdogserarch][:user_id]}).all
			end
		
		else
		    if params[:watchdogserarch][:end] == ''
				@watchdogs = Watchdog.where("created_at >= :start_date AND controller in (:type)",
								{:start_date => params[:watchdogserarch][:start],:type => params[:watchdogserarch][:contrtyp]}).all
				
			elsif params[:watchdogserarch][:start] == ''
				@watchdogs = Watchdog.where("created_at <= :end_date  AND controller in (:type)",
								{:end_date => params[:watchdogserarch][:end],:type => params[:watchdogserarch][:contrtyp]}).all
					
			elsif params[:watchdogserarch][:end] == '' && params[:watchdogserarch][:start] == ''
				@watchdogs = Watchdog.where("controller in (:type)",
								{:type => params[:watchdogserarch][:contrtyp]}).all
					
			else
				@watchdogs = Watchdog.where("created_at >= :start_date AND created_at <= :end_date  AND controller in (:type)",
								{:start_date => params[:watchdogserarch][:start],:end_date => params[:watchdogserarch][:end],:type => params[:watchdogserarch][:contrtyp]}).all
			end
	   	end

	   	rescue => e
		    @code = SecureRandom.hex

			@log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
			redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
		end

 	end
  
  	def set_watchdog

  		begin
      	@watchdog = Watchdog.find(params[:id])

      	rescue => e
		    @code = SecureRandom.hex

			@log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
			redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
		end

    end

    # DELETE /users/1
  	def destroy

  		begin
    	@watchdog.destroy

    	respond_to do |format|
      		format.html { redirect_to watchdogs_url }
      		format.json { head :no_content }
    	end

    	rescue => e
		    @code = SecureRandom.hex

			@log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
			redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
		end

  	end

  
end

