class CurrenciesWithdrawTypesController < ApplicationController
  before_action :set_currencies_withdraw_type, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_userd! 
  
  # GET /currencies_withdraw_types
  def index

    begin
    if current_userd.role_id == 1
      @currencies_withdraw_types = CurrenciesWithdrawType.all
    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def currencies 

    begin
    @wallets=UsersWalletsBalance.where(user_id: current_userd.id,status: 1)

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
  
  end

  def show
  end

  # GET /currencies_withdraw_types/new
  def new

    begin
    if current_userd.role_id == 1
      @currencies_withdraw_type = CurrenciesWithdrawType.new
    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # GET /currencies_withdraw_types/edit
  def edit
  end

  # POST /currencies_withdraw_types/create
  def create

    begin
    if current_userd.role_id == 1
    @currencies_withdraw_type = CurrenciesWithdrawType.new(currencies_withdraw_type_params)

      respond_to do |format|

        if @currencies_withdraw_type.save
          format.html { redirect_to @currencies_withdraw_type, notice: 'Currencies withdraw type was successfully created.' }
          format.json { render :show, status: :created, location: @currencies_withdraw_type }
        else
          format.html { render :new }
          format.json { render json: @currencies_withdraw_type.errors, status: :unprocessable_entity }
        end
        
      end

    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # Update /currencies_withdraw_types/
  def update

    begin
    if current_userd.role_id == 1

      respond_to do |format|

        if @currencies_withdraw_type.update(currencies_withdraw_type_params)
          format.html { redirect_to @currencies_withdraw_type, notice: 'Currencies withdraw type was successfully updated.' }
          format.json { render :show, status: :ok, location: @currencies_withdraw_type }
        else
          format.html { render :edit }
          format.json { render json: @currencies_withdraw_type.errors, status: :unprocessable_entity }
        end

      end

    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # DELETE /currencies_withdraw_types
  def destroy

    begin
    if current_userd.role_id == 1
      @currencies_withdraw_type.destroy

      respond_to do |format|
      format.html { redirect_to currencies_withdraw_types_url, notice: 'Currencies withdraw type was successfully destroyed.' }
      format.json { head :no_content }
      end

    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_currencies_withdraw_type
      @currencies_withdraw_type = CurrenciesWithdrawType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def currencies_withdraw_type_params
      params.require(:currencies_withdraw_type).permit(:name,:currency_code,:logo,:country_code)
    end

end
