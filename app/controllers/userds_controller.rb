class UserdsController < ApplicationController
  before_filter :authenticate_userd!,:except => [:forget_password, :reset,:payers_welcome,:uniquetel]
  before_action :set_user, only: [:show, :edit, :update, :destroy, :usercarts ,:account_settings] 
  before_filter  :handle_two_factor_authentication,:except => [:resend_tfa] 
  respond_to :js, :json, :html
  

   def test_limits
    @data = Userd.find_by_id(current_userd.id)
   end

  def user_sms_check

    begin
    render layout: false
    @phone = current_userd.tel

    if session[:sms_sent] == 0   
      uri = URI.parse("https://api.infobip.com/2fa/1/pin")
      request = Net::HTTP::Post.new(uri)
      request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
      request["content-type"] = 'application/json'
      request["accept"] = 'application/json'
      request.body = JSON.dump({    
      "applicationId":"451C42313BC234ABF92206EEA0796C1B",
      "messageId":"9A9EDA35B0552EF4E942BF272ADA1955",
      "from":"Payers",
      "to": @phone,
      })
      req_options = {
        use_ssl: uri.scheme == "https",
      }

      response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      @response = http.request(request)
      @res= @response.body
      @applications_list= ActiveSupport::JSON.decode(@res)
      current_userd.update(:pin_id => @applications_list["pinId"] )
      session[:sms_sent] = 1
      end

    end

    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def payers_welcome
  end

  def backup_status

    begin
    @newcode=[*('0'..'9')].to_a.shuffle[0,6].join
    @code= ActiveSupport::JSON.decode(@newcode)
    current_userd.update(:backup_code =>  @newcode)
    render :json => { 'response': @code }

    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def change_locale
  
    begin
    current_userd.update(:locale =>params[:locale])
    redirect_to(request.env['HTTP_REFERER'])

    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def user_sms_check_post
  
    begin
    @code= params[:sms_verify][:code].to_i
    @pin_id = current_userd.pin_id
    @backup_code = current_userd.backup_code.to_i
    uri = URI.parse("https://api.infobip.com/2fa/1/pin/#{@pin_id}/verify")
     request = Net::HTTP::Post.new(uri)
     request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
       request["content-type"] = 'application/json'
       request["accept"] = 'application/json'
     request.body = JSON.dump({
      "pin": @code
    })     
     req_options = {
       use_ssl: uri.scheme == "https",
     }
     
    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      @response = http.request(request)
      @res= @response.body
      @applications_list= ActiveSupport::JSON.decode(@res)      
    end
  
    if @applications_list["verified"] ==  true 
    flash[:notice] = t('Successfully logged in with mobile messages')
    session[:sms_active] = 0 
    elsif    @code ==  @backup_code
    @newcode=[*('0'..'9')].to_a.shuffle[0,6].join
    current_userd.update(:backup_code => @newcode , :active_otp_secret => 0,:sms_active => 0,:gauth_enabled => '0' )
    Emailcode.backupcode(current_userd,@newcode).deliver
    flash[:notice] = t('Successfully logged in with Backup Code')
    session[:sms_active] = 0   
    else 
      flash[:alert] = 'الكود خطأ'              
    end

    redirect_to(root_path)

    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
    
  end

  def usercarts

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Carts'), usercarts_path , :title => "Back to the Index"         
    @usercart = Cart.where("user_id = ? and status = ?" , current_userd.id , 1).all.paginate(page: params[:active_page], per_page: 10).order('created_at DESC')
    @chargedcarts = Cart.where("user_id = ? and status = ?" , current_userd.id , 0).all.paginate(page: params[:charged_page], per_page: 10).order('updated_at DESC')
    
    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end  

  def resend_tfa

    begin
    @i = ROTP::TOTP.new(current_userd.otp_secret_key)
    @code =@i.now
    @path =request.env['HTTP_REFERER']
    @email=current_userd.email

    if current_userd.sms_active == 0
      Emailcode.sendcode(current_userd,@code).deliver
    end
    
    if current_userd.sms_active == 1      
      @phone = current_userd.tel
      url = URI("https://api.infobip.com/sms/1/text/single")
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE 
      request = Net::HTTP::Post.new(url)
      request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
      request["content-type"] = 'application/json'
      request["accept"] = 'application/json'     
      request.body = "{\"from\":\"Payers\",\"to\":\"#{@phone}\",   \"text\":\" كود التفعيل #{@code}\"}"     
      response = http.request(request)
      puts response.read_body    
    end
      
    redirect_to(@path,notice:t("Code has been sent successfully"))

    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def account_settings

    begin
    add_breadcrumb t('home'), :root_path 
    @dollar_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'USD').first 
    @pound_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'EGP').first 
    @riyal_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'SAR').first 
    @users_banks = Userd.find(current_userd.id).users_bank 
    @usertickets = Userd.find(current_userd.id).ticket.where(:ticket_id =>0 ).paginate(page: params[:usertickets_page], per_page: 20).order('created_at DESC') 
    @balancenotapproved = UsersWalletsBalancesFrozen.where("user_id =? and approve =?" ,current_userd.id  ,"0").paginate(page: params[:wait_page], per_page: 3).order('created_at DESC') 
    @wallet_conversions = Transaction.where("user_id = ? and controller = ?",current_userd.id , "User_wallet_ballence").paginate(page: params[:num_page], per_page: 3).order('created_at DESC')
    @watch_dogss = Watchdog.where(["user_id =? and controller = ?  ",current_userd.id  , 'userds' ]).paginate(page: params[:watch_dogss], per_page: 3).order('created_at DESC') 
    


    @payment = "Onlinebanks"
    @carts = "carts"
    @wallet = 'User_wallet_ballence'
    @frozen = 'User_wallet_ballence_frozen '
    @withdraw = 'balances_withdraws'

    @watch_dogs = Notification.where([
      "addeduser_id = ? and controller = ? or
      addeduser_id = ? and controller = ? or
      user_id = ? and controller = ? or
      user_id = ? and controller LIKE ? or
      user_id = ? and controller LIKE ? or
      user_id = ? and controller LIKE ? or
      user_id = ? and controller LIKE ? or
      user_id = ? and controller LIKE ? " ,

      current_userd.id ,'User_wallet_ballence_frozen',
      current_userd.id ,'update_suspension_period',  
      current_userd.id ,'update_suspension_period',                        
      current_userd.id ,"%#{@carts}%",
      current_userd.id ,"%#{@payment}%",
      current_userd.id ,"%#{@wallet}%",
      current_userd.id ,"%#{@frozen}%",
      current_userd.id ,"%#{@withdraw}%"
      ]).order('created_at DESC').limit(10) 


    # @payment = "Onlinebanks"
    # @watch_dogs = Notification.where([
    #   "addeduser_id = ? and controller = ? or
    #   user_id = ? and controller in (?) or
    #   user_id = ? and controller LIKE ? " ,
      
    #   current_userd.id ,'User_wallet_ballence_frozen',                         
    #   current_userd.id ,['Ticket','carts','User_wallet_ballence','User_wallet_ballence_frozen',
    #     'onlinebank','buycart','chargecart','balances_withdraws','User_wallet_ballence/walletstransferpost',
    #     'carts/refund_cart',''],
    #   current_userd.id ,"%#{@payment}%"]).order('created_at DESC').limit(10) 



    @usertickets1 = Ticket.where("userd_id = ?",current_userd.id).count
    @usertickets2 = Ticket.where("userd_id = ? and closed =?",current_userd.id,"0").count
    @userbanks2 = UsersBank.where("userd_id = ? and bank_type = ? ",current_userd.id , "withdraw").count

    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end


  
  def changeprofile 

#     @obj = ActiveSupport::JSON.decode(params[:userd][:slim].to_s)
#        @image1 = @obj["output"]["image"]
#     @name = @obj["output"]["name"]
#     @type = @obj["output"]["type"]
# @image = Base64.encode64(@image1)
# # @upload = AttachmentsUploader.new

# #     @image = params[:userd][:avatar]
# #     @name = params[:userd][:avatar].original_filename
# #     @type = params[:userd][:avatar].content_type



#     # UPDATE `admin_attachments` SET `content` = 

  


#    @att =  Attachment.where(:id => current_userd.avatar).first
   
#    sql = "UPDATE `attachments` SET `content` = #{@image} where id = #{@att.id}"
#    @records_array = ActiveRecord::Base.connection.execute(sql)
   
#   #  @att.content = @upload.store!(@image)

#    @att.file_name = @name
#    @att.file_ext = @type
   
#    @att.save!


  #   @att.update(:content => params[:userd][:avatar],:user_id => current_userd.id ,:file_ext => @type,:file_name => @name)

  #  @ticket = Ticket.create(:foreign_id => '11',:userd_id => current_userd.id,:ticket_number => rand(100000..999999),:body => 'note',:status => "customer reply",:predefined_reply_id => 123,:ticket_id => 12,:attachment => @image )

  # @att.update(:content => @image,:user_id => current_userd.id ,:file_ext => @image,:file_name => @name)





  #        begin
  #   if current_userd.avatar != nil  
  #   @att =  Attachment.where(:id => current_userd.avatar).first
  #   @att.update(:content => params[:userd][:avatar],:user_id => current_userd.id ,:file_ext => @type,:file_name => @name)
  #   else  

  #     @att =  Attachment.create(:uuid => SecureRandom.uuid , :content => params[:userd][:avatar],:user_id => current_userd.id,:file_ext => @type,:file_name => @name)
  #     current_userd.update(:avatar => @att.id)
  
  # end
     
  #    redirect_to(request.env['HTTP_REFERER'],notice:t("Profile picture has been changed successfully"))

  #   rescue =>e
  #     @code = SecureRandom.hex
  #     @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
  #     redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
  #   end


  #   # *****************
      begin
    if current_userd.avatar != nil  
    @att =  Attachment.where(:id => current_userd.avatar).first
    @att.update(:content => params[:userd][:avatar],:user_id => current_userd.id ,:file_ext => params[:userd][:avatar].content_type,:file_name => params[:userd][:avatar].original_filename)
    else  

      @att =  Attachment.create(:uuid => SecureRandom.uuid , :content => params[:userd][:avatar],:user_id => current_userd.id,:file_ext => params[:userd][:avatar].content_type,:file_name => params[:userd][:avatar].original_filename )
      current_userd.update(:avatar => @att.id)
  
  end
     
     redirect_to(request.env['HTTP_REFERER'],notice:t("Profile picture has been changed successfully"))

    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def forgot

    # begin
    # if params[:userd][:email].blank?
    #   return render json: {error: 'Email not present'}
    # end

    # @user = Userd.where("email = :email AND (tel = :tel OR user_verifications.nationalid_no = :nationalid) ",
    # {:email => params[:userd][:email] , :tel => params[:userd][:tel] , :nationalid => params[:userd][:nationalid]}).joins("INNER JOIN user_verifications ON user_verifications.user_id = userds.id").first

    # if @user.present?
    #   @m = @user.send_reset_password_instructions    
    #   redirect_to(new_userd_session_path,notice:t("send_instructions"))
    # else
    #   redirect_to(request.env['HTTP_REFERER'],notice:t("Account not found"))
    # end

    # rescue =>e
    #   @code = SecureRandom.hex
    #   ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s)
    # end

  end


  def user_profile

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('user profile'), user_profile_path , :title => "Back to the Index"
    @watch_dogs = Watchdog.where("user_id = ? and action_page =? or user_id = ? and action_page = ?" , current_userd.id,' Sign in ', current_userd.id,'failed signin').all.order('created_at DESC').limit(10)
    @userdata = Userd.where("id = ?",current_userd.id).first
    @userdata2 = UserVerification.where("user_id = ?",current_userd.id).first
    @usertickets = Ticket.where("userd_id = ?",current_userd.id).count
    @usertickets2 = Ticket.where("userd_id = ? and closed =?",current_userd.id,"0").count
    @userbanks2 = UsersBank.where("userd_id = ? and bank_type = ? ",current_userd.id , "withdraw").count
    @dollar_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'USD').first 
    @pound_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'EGP').first 
    @riyal_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'SAR').first      
    @failed_signin = Watchdog.where("user_id =? and action_page =?" ,current_userd.id, 'failed signin').last
      
    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def log_operations
  
    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('log operations'), log_operations_path , :title => "Back to the Index"
    @watch_dogs = Watchdog.where("user_id = ? and action_page =? or user_id = ? and controller = ?" , current_userd.id,' Sign in ', current_userd.id,' Sign out ').all.order('created_at DESC')     
   
    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def failed_operations

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('failed operations'), failed_operations_path , :title => "Back to the Index"
    @fail_signin = Watchdog.where("user_id =? and action_page =?" ,current_userd.id, 'failed signin').all.order('created_at DESC')
      
    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end
  

  def userstatistics

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('statistics'), userstatistics_path , :title => "Back to the Index"
    @failed_signin = Watchdog.where("user_id = ? and action_page =?",current_userd.id,"failed signin").count
    @bank_counts = UsersBank.where("userd_id = ?",current_userd.id).count
    @tickets_count = Ticket.where("userd_id = ?",current_userd.id).count
    @convert_count = UsersWalletsBalancesFrozen.where("user_id =?",current_userd.id).count
    @frozen_conversions_count = UsersWalletsBalancesFrozen.where("user_id_to =? and approve =?",current_userd.id  ,"1").count
    @withdraw_count = BalancesWithdraw.where("user_id = ?",current_userd.id).all
    @wallet_count = UsersWalletsBalance.where("user_id = ?",current_userd.id).all
    @wallets_number = UsersWalletsBalance.where("user_id = ?  and is_deleted = ?",current_userd.id,'0').all   
    @wallet_conversion_count = Transaction.where("user_id = ? and controller = ?",current_userd.id , "User_wallet_ballence").count
    @notification_count = Notification.where("user_id = ? ",current_userd.id).count
    @cart = Cart.select("value").where("user_id = ? ",current_userd.id).all
    @cartt =     Cart.where("user_id = ? ",current_userd.id).order('created_at DESC').pluck(:value)
    @walletchart =     UsersWalletsBalance.where("user_id = ?  and  is_deleted = ?",current_userd.id,'0').pluck(:balance)  
    @convert_chart = UsersWalletsBalancesFrozen.where("user_id =?",current_userd.id).order('created_at DESC').pluck(:balance)   
    @withdraw_chart = BalancesWithdraw.where("user_id = ?",current_userd.id).order('created_at DESC').pluck(:thesum)
    
    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
  
  end

  def uniquetel


    # begin
      @usertel = params[:tel]
      @isunique = Userd.where("tel = ?", @usertel).first
      @applicationId = "451C42313BC234ABF92206EEA0796C1B"
      @messegeid = "9A9EDA35B0552EF4E942BF272ADA1955"
     
 #########################  no Sms Verification ############################################     
if @isunique == nil


  @message = true


  else 
    @message = false


  end


  render :json => { 'message': @message,'isunique': @isunique }

  ##########################################################


  ###########################  sms verification  ###############################
# if @isunique == nil
#       @phone = @usertel
#       @tel = @phone[0,4]
  
#       @ur = ''
#       if @tel == '+966'|| @tel == '+970'
#         @ur = URI.parse("https://api.infobip.com/2fa/1/pin?ncNeeded=false")
     
#       else
#         @ur = URI.parse("https://api.infobip.com/2fa/1/pin")
#       end

#       if params[:type] == "voice"

#         @applicationId = "F7C7FBD29F434BD039D6C701402AA215"
#       @messegeid = "E7176A93BC4DF0EC5AC8B3D530AFBAD0"
      
#       @ur = URI.parse("https://api.infobip.com/2fa/1/pin/voice")
#     end
      
      
  
#       uri = @ur
#       request = Net::HTTP::Post.new(uri)
#       request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
#       request["content-type"] = 'application/json'
#       request["accept"] = 'application/json'
#       request.body = JSON.dump({
#                                 "applicationId": @applicationId,
#                                 "messageId": @messegeid,
#                                 "from":"Payers",
#                                 "to": @phone
#                               })
  
#       req_options =  {
#                         use_ssl: uri.scheme == "https",
#                       }
       
#       response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
#       @response = http.request(request)
#       @res= @response.body
#       @applications_list= ActiveSupport::JSON.decode(@res)
       
  
#         @message = true
#           render :json => { 'response': @applications_list ,'message': @message }
#       end

#     else
#       @message = false
#       render :json => { 'message': @message,'isunique': @isunique }

#     end


    

    # rescue =>e
    #   @code = SecureRandom.hex
    #   ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s)
    # end

  end
    
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = Userd.find(current_userd.id)
    end
 
end

