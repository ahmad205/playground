class SettingsController < ApplicationController
  before_action :set_setting, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_userd!,:isthisadmin




   	   
  # GET /whatchdogs
  # GET /whatchdogs.json
  def index
    @settings = Setting.all

        #@settings = Setting.where("parent_id = ?",nil).all

  end

def show
  end

  def set_setting
      @setting = Setting.find(params[:id])
    end
    
    
    # GET /users/new
  def new
              @setting = Setting.new

  end


  # GET /setting/1/edit
  def edit
  	    
  end

  # POST /setting
  # POST /setting.json
  def create
    @setting = Setting.new(setting_params)

    respond_to do |format|
      if @setting.save

      	Watchdog.create(:user_id => current_userd.id,:controller => 'settings', :action_page =>"new / setting",:foreign_id=>@setting.id )
        format.html { redirect_to action: "index", notice: 'User was successfully created.' }
        format.json { render action: 'index', status: :created, location: @setting }
      else
        format.html { render action: 'new' }
        format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /setting/1
  # PATCH/PUT /setting/1.json
  def update
    respond_to do |format|
      if @setting.update(setting_params)
        Watchdog.create(:user_id => current_userd.id,:controller => 'settings', :action_page =>"update / setting",:foreign_id=>@setting.id )
        format.html { redirect_to action: "index", notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end


    # DELETE /setting/1
  # DELETE /setting/1.json
  def destroy
    @setting.destroy
    respond_to do |format|
      format.html { redirect_to settings_url }
      format.json { head :no_content }
    end
  end

  
  
  private
    def setting_params
      params.require(:setting).permit(:name, :key, :value, :description,:parent_id)
    end
   
   
end

