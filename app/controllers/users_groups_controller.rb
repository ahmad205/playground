class UsersGroupsController < ApplicationController
  before_action :set_users_group, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_userd! 
 

  def index
    begin
    @users_groups = UsersGroup.all

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end
 
  def show
    begin
    @user = Userd.where("group_id = ?",params[:id]).all
    
    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def new

    begin
    @users_group = UsersGroup.new

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end
  
  def edit

    begin
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def create

    begin
    @users_group = UsersGroup.new(users_group_params)

    respond_to do |format|

      if @users_group.save
	      Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'users_groups', :action_page =>" add users group ",:foreign_id=>@users_group.id ,:details=> " add users group with id :  "+ @users_group.id.to_s )
        format.html { redirect_to @users_group, notice: 'Users group was successfully created.' }
        format.json { render :show, status: :created, location: @users_group }
      
      else
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'users_groups', :action_page =>" add users group ",:foreign_id=>@users_group.id ,:details=> " fails to add users group with id :  "+ @users_group.id.to_s )
		    format.html { render :new }
        format.json { render json: @users_group.errors, status: :unprocessable_entity }
      end

    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def update

    begin
    respond_to do |format|
      if @users_group.update(users_group_params)
	  	  Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'users_groups', :action_page =>" update users group ",:foreign_id=>@users_group.id ,:details=> " update users group with id :  "+ @users_group.id.to_s )
        format.html { redirect_to @users_group, notice: 'Users group was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_group }
      
      else
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'users_groups', :action_page =>" update users group ",:foreign_id=>@users_group.id ,:details=> " fails to update users group with id :  "+ @users_group.id.to_s )
		    format.html { render :edit }
        format.json { render json: @users_group.errors, status: :unprocessable_entity }
      end

    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def destroy

    begin
  	Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'users_groups', :action_page =>" destroy users group ",:foreign_id=>@users_group.id ,:details=> " destroy users group with id :  "+ @users_group.id.to_s )
    @users_group.destroy

    respond_to do |format|
      format.html { redirect_to users_groups_url, notice: 'Users group was successfully destroyed.' }
      format.json { head :no_content }
    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_group
      @users_group = UsersGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_group_params
      params.require(:users_group).permit(:name, :description,:fees ,:order_ratio, :transfer_ratio, :add_balance_ratio, :withdraw_sum)
    end
    
end
