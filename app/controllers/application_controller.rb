class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery
  require 'device_detector'
  require "redis"
  # require 'telegram/bot'
before_filter :set_current_user
  around_filter :userd_time_zone, if: :current_userd

  protect_from_forgery with: :exception
  
  before_action :authenticate_userd!,:check_protection_settings

 before_filter :sms_verify, :except => [:resendsms,:user_sms_check,:user_sms_check_post,:resendsms,:destroy]
   before_filter :verify_data,:except => [:destroy_userd_session_path,:new_userd_session_path]

  before_action  :configure_permitted_parameters, if: :devise_controller?
  

  before_filter :set_flash_notice
  #before_action :verify_data ,:except => [:user_profile]
  
  # before_filter  :handle_two_factor_authentication,:except => [:resend_tfa]
  # skip_before_filter :handle_two_factor_authentication, :only => [:backup_email]

  
    helper_method :current_user ,:userprivilage,:user_signed_in
       before_filter :set_locale  ,:require_login ,:unotes ,:sitesetting ,:authenticate_userd!

layout :setlayout

# rescue_from ActionController::RoutingError, :with => :render_404


# private
#   def render_404(exception = nil)
#     if exception
#         logger.info "Rendering 404: #{exception.message}"
#     end
      
#     render :file => "#{Rails.root}/public/404.html", :status => 404, :layout => false
#   end




#protected

#   def configure_permitted_parameters
#  #   devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:name,:username,:tel,:avatar,:nationalidphoto,:role_id,:group_id,:loginips,:nationalid,:vodafonecash,:country,:currency,:attachments) }
# end
def set_flash_notice
 flash_notice = flash[:notice]
end

before_filter :set_current_user

def set_current_user
  Userd.current = current_userd
end

private




def userd_time_zone(&block)
  Time.use_zone(current_userd.time_zone, &block)
end

def check_protection_settings 
    if userd_signed_in?

      if current_userd.gauth_enabled == '1'

        current_userd.update(:active_otp_secret => 0 ,:sms_active => 0) 

      end

    end

end


def verify_data 


  




    if userd_signed_in?
        if current_userd.hard_lock == 1
        #redirect_to destroy_userd_session_path,notice:"Your account has locked by Admin, contact with our support"
        sign_out :userd if userd_signed_in?
        redirect_to new_userd_session_path,notice:"Your account has locked by Admin, contact with our support"
        #sign_out current_userd, :bypass => true
      end
    end



end


def backup_email

  # if userd_signed_in?

  # # @userd = Userd.where(:id => current_userd.id).first 
  # # @token=params[:token]
  # # @totp = ROTP::TOTP.new(@userd.gauth_secret, issuer: "Payerz")

  # end


end
def sms_verify


   if session[:sms_active] == 1
  flash[:notice] = 'يجب تاكيد الدخول عن طريق رسائل الموبايل'

redirect_to user_sms_check_path 


   end


end



def resource_params
  #params.require(:userd).permit( :email,:invitation_token, :username)
end


protected

  def configure_permitted_parameters
    #devise_parameter_sanitizer.permit(:sign_in, keys: [:otp_attempt])
    #devise_parameter_sanitizer.permit(:sign_in) { |u| u.permit(:otp_attempt) }

    #devise_parameter_sanitizer.permit(:accept_invitation, keys: [:username])
    
  end


# protected
def after_sign_in_path_for(resource)
#   super(resource)
if (request.url == "https://dashboard.payers.net/userds/sign_up" || request.url == "https://dashboard.payers.net/userds/password/new" ||
  request.url == "https://dashboard.payers.net/userds/confirmation/new" || request.url == "https://dashboard.payers.net/userds/unlock/new")
  root_path
end
#   $ee=request.env['HTTP_USER_AGENT']
  

#   @client = DeviceDetector.new($ee)
  
#   session[:sms_sent] = 0
#   session[:balancewithdraw] = 1
  
#   session[:balancewithdraw_smssent] = 0
  
#   session[:sms_active] = current_userd.sms_active

  
# @full =@client.device_name.to_s + '_' + @client.device_type + '_' + @client.os_name.to_s + '_' + @client.os_full_version.to_s + '_' + @client.name.to_s + '_' + @client.full_version.to_s
#  @userd = Userd.where(:id => resource.id).first 
#   #Watchdog.create(:user_id => @userd.id,:controller => 'userds ', :action_page =>" Sign in " ,:details=> "Sign in from ip :"+ request.remote_ip,:device_name=> $ee+")")
#   Watchdog.create(:user_id => @userd.id,:added_by => @userd.id,:controller => 'userds ', :action_page =>" Sign in " ,:details=> "Sign in from ip :"+ request.remote_ip,:device_name=> @full )
#                #  redirect_to :'account_settings' and return
#                account_settings_path

end


  
private

def current_user
  #@current_user ||= Userd.find(session[:user_id]) if session[:user_id]
  

end


require "ipaddr"


def set_locale
  # if params[:locale] is nil then I18n.default_locale will be used
    # I18n.locale = params[:locale]

    if userd_signed_in?

      I18n.locale = current_userd.locale.to_s
    else
      I18n.locale = 'ar'
    end
 # I18n.locale = 'ar'

end

def redirect_if_unverified
    redirect_to new_verify_url, notice: "Please verify your phone number"
end

def reset_pin!
  self.update_column(:pin, rand(1000..9999))
end

def unverify!
  self.update_column(:verified, false)
end

def send_pin!
  reset_pin!
  unverify!
  SendPinJob.perform_later(self)
end

def setlayout
 
  # if current_userd  && current_userd.role_id  == 2
    if current_userd  

         "frontend"
else
     "site"
     #redirect_to new_userd_session_path, :notice => 'if you want to add a notice'
   
   end
  # if current_userd 
  #   if current_userd.role_id  == 1 
  #      "admin"
  #   else  
  #       "frontend"
  #   end
  # else
  #       "site"
  # end
end

# user notifications

def unotes
 
  if current_user 
              @uinbox =    Userinboxto.where(user_id_to:  current_userd.id , read: 0)
              @unote =     Notification.where(user_id:  current_userd.id , read: 0)

 
  end
end

 def sitesetting

  #@activation = 1
  
 
  ####################################



 if current_userd && current_userd.email != nil
      @tickets_number = Ticket.ticket_number(current_userd.email)
      else
        @tickets_number = 0
      end
  ##############################
      @user_agent=request
  
               Watchdog.setagent(@user_agent)


               @stitle =    Setting.where(key: 'ttle').first
               @transfer_ratio =    Setting.where(key:'transfer_ratio').first
               @sitename =    Setting.where(key:'sitename').first
               @facebook =    Setting.where(key: 'facebook').first
               @twitter =    Setting.where(key: 'twitter').first
               @youtube =    Setting.where(key: 'youtube').first
               @copyrights =    Setting.where(key: 'copyrights').first
               @metadecription =    Setting.where(key: 'metadecription').first
               @semail =    Setting.where(key: 'email').first
               @Terms =    Setting.where(key: 'Terms').first
               @phone =    Setting.where(key: 'phone').first
               @address =    Setting.where(key: 'adress').first
               @siteclose =    Setting.where(key: 'siteclose').first
               @msgclose =    Setting.where(key: 'msgclose').first
               @useractivate =    Setting.where(key:'useractivate').first
              
              @usdtoegp =    Setting.where(key:'USD TO EGP').first
             @egptousd =    Setting.where(key:'EGP TO USD').first
              @egptors =    Setting.where(key:'EGP TO SAR').first
                          
              @rstoegp =    Setting.where(key:'SAR TO EGP').first
              @usdtors =    Setting.where(key:'USD TO SAR').first
              @rstousd =    Setting.where(key:'SAR TO USD').first

              @uni_usdtoegp =    Setting.where(key:'Universal USD TO EGP').first
             @uni_egptousd =    Setting.where(key:'Universal EGP TO USD').first
              @uni_egptors =    Setting.where(key:'Universal EGP TO SAR').first
                          
              @uni_rstoegp =    Setting.where(key:'Universal SAR TO EGP').first
              @uni_usdtors =    Setting.where(key:'Universal USD TO SAR').first
              @uni_rstousd =    Setting.where(key:'Universal SAR TO USD').first

              @cart_usdtoegp =    Setting.where(key:'Cart USD TO EGP').first
              @cart_usdtors =    Setting.where(key:'Cart USD TO SAR').first


              
              @admin_susp = Setting.where(key:'frozen balance').first
              @susp_interval = Setting.where(key:'Suspension_period').first

               @withdraw_date = Setting.where(key:'withdraw_date').first

              @helpcomment =    Setting.where(key:'helpcomments').first
              if (current_userd)
                
                
                #@notifications = Notification.where("user_id = ? and read = ?",current_userd.id,'0').limit(10).order('created_at DESC')
                @notifications1 = Notification.where("user_id = ? OR addeduser_id = ?",current_userd.id,current_userd.id)

               
                @notifications = @notifications1.where(read: 0).order('created_at DESC') 
                # execpt some user actions from notifications
                # @notifications = @notifications.where.not("controller in (?)",['Onlinebanks/add_payment_data','Onlinebanks/exception_reply'])

             @dollar_wallet = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id,'USD').first
              @egp_wallet = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id,'EGP').first
              @sar_wallet = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id,'SAR').first

            @dollar_frozen = UsersWalletsBalancesFrozen.where("user_id_to = ? and approve = ? and deleted = ? and currency = ?",current_userd.id,0,0,"USD").all
            @total_frozen_dollar = @dollar_frozen.sum(:netbalance)
            @riyal_frozen = UsersWalletsBalancesFrozen.where("user_id_to = ? and approve = ? and deleted = ? and currency = ?",current_userd.id,0,0,"SAR").all
            @total_frozen_riyal = @riyal_frozen.sum(:netbalance)
            @egp_frozen = UsersWalletsBalancesFrozen.where("user_id_to = ? and approve = ? and deleted = ? and currency = ?",current_userd.id,0,0,"EGP").all
            @total_frozen_egp = @egp_frozen.sum(:netbalance)


              @user_verifications = UserVerification.where('user_id = ?' , current_userd.id).first
              

              @balancewithdraw_usd = CurrenciesWithdrawRatio.where("currency_id = ? and status =?",1,1).all 
              @balancewithdraw_egp = CurrenciesWithdrawRatio.where("currency_id = ? and status =?",3,1).all 
              @balancewithdraw_rys = CurrenciesWithdrawRatio.where("currency_id = ? and status =?",6,1).all 

              @usd_b = @balancewithdraw_usd.where("id = ?" ,14).first 
              @western_b = @balancewithdraw_usd.where("id = ?" ,4).first 
              @egyptian_mail_b = @balancewithdraw_usd.where("id = ?" ,4).first 
              
              
              @rys_b = @balancewithdraw_rys.where("id = ?" ,15).first 
               @egp_b = @balancewithdraw_egp.where("id = ?",13).first 
               

              # @expenses_usd= ((@usd_b.ratio_bank+@usd_b.ratio_company+@usd_b.additional_expenses)/100)
              # @expenses_western= ((@western_b.ratio_bank+@western_b.ratio_company+@western_b.additional_expenses)/100)
              # @expenses_egyptian_mail= ((@egyptian_mail_b.ratio_bank+@egyptian_mail_b.ratio_company+@egyptian_mail_b.additional_expenses)/100)
              
              # @expenses_egp= ((@egp_b.ratio_bank+@egp_b.ratio_company+@egp_b.additional_expenses)/100) 
              #  @expenses_rys= ((@rys_b.ratio_bank+@rys_b.ratio_company+@rys_b.additional_expenses)/100) 

              
              end
              

 
 
 end

 

def require_login
    # unless current_user
    #   redirect_to log_in_path
    # end
  end

 def userprivilage 
  #    @current_user = User.find(session[:user_id]) ? session[:user_id] : false

   # @userprivilage ||=  RolePrivilege.find(:first, :conditions=>["role_id = ?", current_user.role_id])
    @userprivilage = RolePrivilege.find_by_role_id(current_userd.role_id)


  end



def authenticate_user
#   if current_user.nil?
#     flash[:error] = 'You must be signed in to view that page.'
#     redirect_to :'log_in'
# else if @current_user.role_id !=  1  
# =begin
#      case params[:controller] 
#       when 'users'    
#           if  userprivilage.useradd != 1 and (params[:action] == 'new' && params[:action] != 'index')
#            flash[:error] = 'not allowed '
#            redirect_to :'account_settings'
#           end
#           if  params[:action] == 'destroy' 
#            flash[:error] = 'not allowed '
#            redirect_to :'account_settings'
#           end
          
#       when 'user_servers'    
#          if  params[:action] != 'index' && params[:action] != 'show' && params[:action] != 'srvget' && userprivilage.serveradd != 1
#             flash[:error] = 'not allowed '
#             redirect_to :'account_settings'
#           end
#       when 'roleprivilages'    
#             flash[:error] = 'not allowed '
#             redirect_to :'account_settings'
#       else
#          # @userprivilage=  RolePrivileg.find(:first, :conditions=>["roleships_id = ?", current_user.role_id])

#          #   @current_user_privilage = Roleship.find(current_user.role_id)  

#      end
# =end
   
#     end     
#   end
end


def isthisadmin
    
      if current_userd.role_id != 1 
              # flash[:error] = 'not allowed '
               redirect_to :root
       end
 end
 
  #protect_from_forgery with: :exception
end
