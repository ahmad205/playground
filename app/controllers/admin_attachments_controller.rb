class AdminAttachmentsController < ApplicationController

    def admin_attach
  
        @attachment = AdminAttachment.where(:id => params[:id].to_i).first
        if   @attachment != nil 
          @data_url = @attachment.content.url
        @png  = Base64.decode64(@data_url['data:image/png;base64,'.length .. -1])
     
         if params[:type] == "img"
         send_data @png
         end
     
         if params[:type] == "link"
          render :layout => false
         end
     
       else
     
         redirect_to(root_path,:notice => t('not allowed'))
         end
    end
  
end
