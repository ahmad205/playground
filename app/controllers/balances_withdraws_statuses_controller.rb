class BalancesWithdrawsStatusesController < ApplicationController
  before_action :set_balances_withdraws_status, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_userd! 
  
  # GET /balances_withdraws_statuses
  # GET /balances_withdraws_statuses.json
  def index
    @balances_withdraws_statuses = BalancesWithdrawsStatus.all
  end

  # GET /balances_withdraws_statuses/1
  # GET /balances_withdraws_statuses/1.json
  def show
  end

  # def withdraw_image
   
  #    @balanceopd = BalancesWithdrawsStatus.where(:id => params[:id].to_i).first
  #    if  current_userd &&    @balanceopd.user_id == current_userd.id
  #     send_file BalancesWithdrawsStatus.where(:id => params[:id].to_i,:user_id => current_userd.id ).first.attachments.path
  #    else 
  #     redirect_to(balances_withdraws_statuses_url,:notice => 'صورة غير مسموح لك الدخول عليها')
  #     end
  # end

  # GET /balances_withdraws_statuses/new
  def new
    @balances_withdraws_status = BalancesWithdrawsStatus.new
  end

  # GET /balances_withdraws_statuses/1/edit
  def edit
  end

  # POST /balances_withdraws_statuses
  # POST /balances_withdraws_statuses.json
  def create
    @balances_withdraws_status = BalancesWithdrawsStatus.new(balances_withdraws_status_params)

    respond_to do |format|
      if @balances_withdraws_status.save
        format.html { redirect_to @balances_withdraws_status, notice: 'Balances withdraws status was successfully created.' }
        format.json { render :show, status: :created, location: @balances_withdraws_status }
      else
        format.html { render :new }
        format.json { render json: @balances_withdraws_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /balances_withdraws_statuses/1
  # PATCH/PUT /balances_withdraws_statuses/1.json
  # def update_hint
    
  #   if params[:balances_withdraw][:balances_withdraws_status][:user_hint] != nil
  #   @id = params[:balances_withdraw][:balances_withdraws_status][:balances_withdraws_id].to_i
  #   @balanceopd = BalancesWithdrawsStatus.where("balances_withdraws_id = ?", @id).first
  #   @dd = params[:balances_withdraw][:balances_withdraws_status][:user_hint].to_s
  #   @balanceopd.update(:user_hint => @dd )
  #   redirect_to(request.env['HTTP_REFERER'],notice:"Message Sent")
   
  #   end
  # end


  def  withdraw_status_image


    @status = BalancesWithdrawsStatus.where(:id => params[:id].to_i).first
    if  current_userd &&  @status != nil   &&  @status.user_id == current_userd.id
      @data_url = @status.attachments.url
    @png  = Base64.decode64(@data_url['data:image/png;base64,'.length .. -1])
 
      send_data @png
    else
 
     redirect_to(root_path,:notice => t('A picture you are not allowed to access'))
     end


   
  end
 


  def update
     
    respond_to do |format|

      if @balances_withdraws_status.update(balances_withdraws_status_params)
        format.html { redirect_to @balances_withdraws_status, notice: 'Balances withdraws status was successfully updated.' }
        format.json { render :show, status: :ok, location: @balances_withdraws_status }
      else
        format.html { render :edit }
        format.json { render json: @balances_withdraws_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /balances_withdraws_statuses/1
  # DELETE /balances_withdraws_statuses/1.json
  def destroy
    @balances_withdraws_status.destroy
    respond_to do |format|
      format.html { redirect_to balances_withdraws_statuses_url, notice: 'Balances withdraws status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_balances_withdraws_status
      @balances_withdraws_status = BalancesWithdrawsStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def balances_withdraws_status_params
      params.require(:balances_withdraws_status).permit(:balances_withdraw_id,:user_hint ,:status, :user_id, :user_bank_id, :order_created_hint ,:order_delivered_hint,:order_workon_hint,:order_exception_hint,:order_done_hint,:attachments,:exception_message)
    end
end
