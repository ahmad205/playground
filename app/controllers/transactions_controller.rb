class TransactionsController < ApplicationController
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]


  def index

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Balance Operations'), transactions_path , :title => "Back to the Index"
    @transactions = Transaction.where("user_id = ? or user_id_to = ? ",current_userd.id,current_userd.id).all.order('created_at DESC')

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def show
  end

  # GET /transactions/new
  def new
    @transaction = Transaction.new
  end

  def search

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Balance Operations'), searchpost_path , :title => "Back to the Index"
    @last_hour = Time.now - 1.hours
    @last_day = Time.now - 1.days
    @last_week = Time.now - 7.days
    @last_month = Time.now - 30.days
    @last_year = Time.now - 365.days
    @searchs =  params[:search]
    @controllersearch =  params[:type]

    if !params[:transsearch]
      params[:transsearch]= params   
    end

    if params[:transsearch][:time] == 'lastyear'
  	@searchtime = @last_year
    elsif params[:transsearch][:time] == 'lastmonth'
  	@searchtime = @last_month
  	elsif params[:transsearch][:time] == 'lastweek'
  	@searchtime = @last_week
  	elsif params[:transsearch][:time] == 'lastday'
  	@searchtime = @last_day
  	elsif params[:transsearch][:time] == 'lasthour'
  	@searchtime = @last_hour
    end

    if params[:type] == "refunded_requests" or params[:type] == "refunded_delivered" 
    @controllersearch = "users_wallets_ballences_frozens/deleteOperation"
    elsif params[:type] == "approved_transfers"
    @controllersearch = "users_wallets_ballences_frozens/approvedbalance"
    elsif params[:type] == "notapproved_transfers"
    @controllersearch = "users_wallets_ballences_frozens/create"
    end
 
    if params[:search] != ""
      @ticketss = Transaction.where("transaction_id = :transid AND user_id = :id or transaction_id = :transid AND user_id_to = :id",{  :transid => params[:search] ,:id => current_userd.id} ).first
    
    elsif params[:transsearch][:time] != "" && params[:transsearch][:type] != ""
       
      if params[:transsearch][:type].include?('users_wallets_ballences_frozens/create')
      	@froz_bal = Transaction.where("created_at >= :searchtime AND user_id_to = :id AND (controller LIKE :search)",{  :searchtime => @searchtime ,:id => current_userd.id,:search => "%#{@controllersearch}%"}).all.order('created_at DESC')
      elsif params[:transsearch][:type].include?('users_wallets_ballences_frozens/approvedbalance')
        @deliv_bal = Transaction.where("created_at >= :searchtime AND user_id_to = :id AND (controller LIKE :search)",{  :searchtime => @searchtime ,:id => current_userd.id,:search => "%#{@controllersearch}%"}).all.order('created_at DESC')
      elsif params[:transsearch][:type].include?('refunded_delivered')
        @refun_bal = Transaction.where("created_at >= :searchtime AND user_id_to = :id AND (controller LIKE :search)",{  :searchtime => @searchtime ,:id => current_userd.id,:search => "%#{@controllersearch}%"}).all.order('created_at DESC')
      else
        @tickets = Transaction.where("created_at >= :searchtime AND user_id = :id AND (controller LIKE :search)",{  :searchtime => @searchtime ,:id => current_userd.id,:search => "%#{@controllersearch}%"}).all.order('created_at DESC')
      end
    
    elsif params[:transsearch][:time] == "" && params[:transsearch][:type] != ""

      if params[:transsearch][:type].include?('users_wallets_ballences_frozens/create')
      	@froz_bal = Transaction.where("user_id_to = :id AND (controller LIKE :search)",{ :id => current_userd.id ,:search => "%#{@controllersearch}%"}).all.order('created_at DESC') 
      elsif params[:transsearch][:type].include?('users_wallets_ballences_frozens/approvedbalance')
        @deliv_bal = Transaction.where("user_id_to = :id AND (controller LIKE :search)",{ :id => current_userd.id ,:search => "%#{@controllersearch}%"}).all.order('created_at DESC') 
      elsif params[:transsearch][:type].include?('refunded_delivered')
        @refun_bal = Transaction.where("user_id_to = :id AND (controller LIKE :search)",{ :id => current_userd.id ,:search => "%#{@controllersearch}%"}).all.order('created_at DESC') 
      else
        @tickets = Transaction.where("user_id = :id AND (controller LIKE :search)",{ :id => current_userd.id ,:search => "%#{@controllersearch}%"}).all.order('created_at DESC') 
      end
    elsif params[:transsearch][:time] != "" && params[:transsearch][:type] == ""
     
        @tickets = Transaction.where("created_at >= :searchtime AND user_id_to = :id or created_at >= :searchtime AND user_id = :id",{  :searchtime => @searchtime ,:id => current_userd.id}).all.order('created_at DESC')
  
    end  
    
    @link1 =request.original_url.to_s
    @link =@link1.split("/")[3]
    UserList.create(:user_id =>  current_userd.id , :description => @link )

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
    
  end

  def searchpost
  end
  
  def edit
  end

  def create

    begin
    @transaction = Transaction.new(transaction_params)
    @transaction.transaction_id = rand(10_000..99_999)

    respond_to do |format|

      if @transaction.save
        format.html { redirect_to @transaction, notice: 'Transaction was successfully created.' }
        format.json { render :show, status: :created, location: @transaction }
      else
        format.html { render :new }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end

    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def update

    begin
    respond_to do |format|

      if @transaction.update(transaction_params)
        format.html { redirect_to @transaction, notice: 'Transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @transaction }
      else
        format.html { render :edit }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end

    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
  
  end

  def user_wallets_transactions
    
    begin
    @transactions = Transaction.where("user_id = ? and controller = ? ",current_userd.id,'users_wallets_balances/walletstransferpost').all.paginate(page: params[:page], per_page: 5).order('created_at DESC') 
    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def destroy

    begin
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to transaction_url, notice: 'Transaction was successfully destroyed.' }
      format.json { head :no_content }
    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_params
      params.require(:transaction).permit(:process_id, :controller, :description,:company_bank_data,:user_bank_data,:ticket_opend)
    end


end
