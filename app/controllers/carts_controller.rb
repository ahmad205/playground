class CartsController < ApplicationController
  before_action :set_cart, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user, :isthisadmin , :except => [:buycart,:payment_method_ratios ,:show,:pcart ,:cartbalance,:cartbalancetouser ,:payment_method ,:cart_preview,:addbalance_show,:edit_balance]

  #show balance after charge
  def addbalance_show

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Add balance operation'), addbalance_show_path
    @tran_id = params[:id]
    @balance = Transaction.where("id = ? and controller =? ",@tran_id,"carts/cartbalancetouser" ).first
    
    if current_userd.id != @balance.user_id
      redirect_to account_settings_path, notice: ' Not Allowed'
    end

    rescue => e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    
    end

  end
  
# show balance after admin add or subtract balance
  def edit_balance

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('edit balance'), edit_balance_path
    @tran_id = params[:id]
    @balance = Transaction.where("id = ? and controller =? ",@tran_id,"users_wallets_balances/admin_wallet_edit_post" ).first
    @get_status = @balance.description.to_s.split('/')
    if (@get_status[0] == "Admin added a balance with a value")
      @status = "add"
    elsif (@get_status[0] == "Admin subtracted a balance with a value")
      @status = "subtract"
    end
    if current_userd.id != @balance.user_id
      redirect_to account_settings_path, notice: ' Not Allowed'
    end

    rescue => e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    
    end

  end

  # show card details in a popup
  def pcart

    begin
    @watch_dogs = Notification.where(["user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ?" , current_userd.id , 'Ticket', current_userd.id ,'User_wallet_ballence' , current_userd.id ,'User_wallet_ballence_frozen', current_userd.id ,'onlinebank' ]).paginate(page: params[:watch_dogs], per_page: 4).order('created_at DESC') 
    @pc = params[:id].to_s
    @cart= Cart.find(@pc) 
 
    if current_userd.id != @cart.user_id
      redirect_to account_settings_path, notice: 'you can not view this cart.'
    end
   
    @pre= AESCrypt.decrypt(@cart.prefix, 'ServHight')  
    @cartnn=    AESCrypt.decrypt(@cart.cart_number, 'ServHight') 
    @username = Userd.where("id =?", @cart.user_id).first

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end
   
  
  # show card details
  def show
 
    begin
    if @cart.user_id == current_userd.id or current_userd.role_id == 1
      @pre= AESCrypt.decrypt(@cart.prefix, 'ServHight')  
      @cartnn=    AESCrypt.decrypt(@cart.cart_number, 'ServHight') 
    end

    if current_userd.id != @cart.user_id
      redirect_to account_settings_path, notice: 'you can not view this cart.'
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # get card balance
  def cartbalance

    begin
    @ubalance= UsersWalletsBalance.where("user_id = ? and currency= ?",current_userd.id,'USD').first
    @dollar_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'USD').first 
    @pound_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'EGP').first 
    @riyal_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'SAR').first 
    @users_banks = Userd.find(current_userd.id).users_bank 
    @usertickets = Userd.find(current_userd.id).ticket.where(:ticket_id =>0 ).paginate(page: params[:usertickets_page], per_page: 20).order('created_at DESC') 
    @balancenotapproved = UsersWalletsBalancesFrozen.where("user_id =? and approve =?" ,current_userd.id  ,"0").paginate(page: params[:wait_page], per_page: 3).order('created_at DESC') 
    @wallet_conversions = Transaction.where("user_id = ? and controller = ?",current_userd.id , "User_wallet_ballence").paginate(page: params[:num_page], per_page: 3).order('created_at DESC')
    @watch_dogss = Watchdog.where(["user_id =? and controller = ?  ",current_userd.id  , 'userds' ]).paginate(page: params[:watch_dogss], per_page: 3).order('created_at DESC') 
    @payment = "Onlinebanks"
    @watch_dogs = Notification.where(["addeduser_id = ? and controller = ? or user_id = ? and controller in (?) or user_id = ? and controller LIKE ? " ,current_userd.id ,'User_wallet_ballence_frozen', current_userd.id ,['Ticket','User_wallet_ballence','User_wallet_ballence_frozen','onlinebank','buycart','chargecart','balances_withdraws','User_wallet_ballence/walletstransferpost'],current_userd.id ,"%#{@payment}%"]).order('created_at DESC').limit(10) 
    @usertickets1 = Ticket.where("userd_id = ?",current_userd.id).count
    @usertickets2 = Ticket.where("userd_id = ? and closed =?",current_userd.id,"0").count
    @userbanks2 = UsersBank.where("userd_id = ? ",current_userd.id).count
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end
  
  # post card balance
  def cartbalancetouser

    begin
    add_breadcrumb t('home'), :root_path
    
    loop do
        @trans_id = "AB"+[*('A'..'Z'),*('0'..'9')].to_a.shuffle[0,16].join
        break @trans_id unless Transaction.where(transaction_id: @trans_id).exists?
    end
   
    @prefix=AESCrypt.encrypt(params[:cartbalance][:prefix].to_i, 'ServHight')
    @cartnum=AESCrypt.encrypt(params[:cartbalance][:cartnum].to_i, 'ServHight')

    @findcart = Cart.where("prefix = ? and cart_number =? and   status=? AND expire_date >= ?", @prefix.to_s,@cartnum.to_s,  1,Date.today).first
    @ubalance= UsersWalletsBalance.where("user_id = ? and currency= ?",current_userd.id,'USD').first
    @dollar_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'USD').first 
    @pound_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'EGP').first 
    @riyal_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'SAR').first 
    @users_banks = Userd.find(current_userd.id).users_bank 
    @usertickets = Userd.find(current_userd.id).ticket.where(:ticket_id =>0 ).paginate(page: params[:usertickets_page], per_page: 20).order('created_at DESC') 
    @balancenotapproved = UsersWalletsBalancesFrozen.where("user_id =? and approve =?" ,current_userd.id  ,"0").paginate(page: params[:wait_page], per_page: 3).order('created_at DESC') 
    @wallet_conversions = Transaction.where("user_id = ? and controller = ?",current_userd.id , "User_wallet_ballence").paginate(page: params[:num_page], per_page: 3).order('created_at DESC')
    @watch_dogss = Watchdog.where(["user_id =? and controller = ?  ",current_userd.id  , 'userds' ]).paginate(page: params[:watch_dogss], per_page: 3).order('created_at DESC') 
    @watch_dogs = Notification.where(["user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ?" , current_userd.id , 'Ticket', current_userd.id ,'User_wallet_ballence'  , current_userd.id ,'User_wallet_ballence_frozen', current_userd.id ,'onlinebank', current_userd.id ,'buycart', current_userd.id ,'chargecart', current_userd.id ,'balances_withdraws']).order('created_at DESC') 
    @usertickets1 = Ticket.where("userd_id = ?",current_userd.id).count
    @usertickets2 = Ticket.where("userd_id = ? and closed =?",current_userd.id,"0").count
    @userbanks2 = UsersBank.where("userd_id = ? ",current_userd.id).count
          
    if @findcart  != nil 

      if @findcart.user_id == nil || @findcart.user_id == current_userd.id       
        @findcart.user_id  = current_userd.id
   
        if @ubalance == nil  
          UsersWalletsBalance.create(:user_id => current_userd.id , :currency => 'USD' , :wallet_name => 'Dollar' , :balance => @findcart.value ,:status => '1' )
          @findcart.status = 0
  			  @findcart.save
          @before_charge = "0"
          @after_charge = @findcart.value
        
        else
   				@before_charge = @ubalance.balance.to_f
          @ubalance.balance = @ubalance.balance.to_f + @findcart.value.to_f
          @ubalance.save
          @findcart.status = 0
          @findcart.save
          @after_charge = @ubalance.balance
      
        end
      
        @controllersearch= 'carts'
        @paymethod = Transaction.where("foreign_id = :id AND (controller LIKE :search)",{ :id => @findcart.id ,:search => "%#{@controllersearch}%"}).first

        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'carts', :action_page =>"cartbalancetouser",:foreign_id=>@findcart.id ,:details=> "add balance by cart number  "+ @findcart.value.to_s  )
        Transaction.create(:user_id => current_userd.id,:foreign_id => @findcart.id,:w_from_name => "USD",:w_to_name =>@paymethod.w_to_name,:value_from => @findcart.value,:beforecharge => @before_charge.to_s,:w_to_balance_after => @after_charge, :controller =>"carts/cartbalancetouser",:transaction_id=>@trans_id,:description=> "you have added balance with a value of/"+ @findcart.value.to_s + " USD"    )
        
        @tran_id = Transaction.where("transaction_id = ?", @trans_id).first
        Notification.create(:user_id =>  current_userd.id,:trans_id => @tran_id.id ,:controller => 'carts/cartbalancetouser',:foreign_id => @findcart.id.to_s,:stats => 'you have added balance with a value of/' + @findcart.value.to_s +  " USD " + '/Your balance has become/' + @ubalance.balance.to_s +  " USD " )
        Chargebalance.charge(current_userd,@findcart, @ubalance.balance).deliver
        @success_msg = t('you have added balance with a value of') + " " + @findcart.value.to_s +  " USD " + t('Your balance has become') + " " + @ubalance.balance.to_s+  " USD "
         flash[:alert] =  @success_msg
         @type = true

      else
        flash[:error] = t('Card not Valid')
        @fail_messege = t('Card not Valid')
        @type = false
      end
    
    end

    # redirect_to root_path , cart: @type 
    redirect_to controller: 'userds', action: 'account_settings', card_charged: @type




    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end 

  # buy new card
  def buycart

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('New Balance Card'), buycart_path

    @cards = Card.where(:display => 1 ).all.order(value: :asc).limit(5).pluck(:value)

    @cart = Cart.new

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # ratios of payment methods 
  def payment_method_ratios

    begin
    @id = params[:id].to_i
    @value = params[:value].to_f
    @bank = CompanyBank.where(:id => @id ).first
    @result = @value * (@bank.ratio / 100) + @bank.fees
    render :json => { 'value': @result ,'bankdata': @bank }

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
  
  end

  # get payment methods
  def payment_method

    begin
    @userv = UserVerification.where("user_id = ?",current_userd.id).first
    @all_company_banks = CompanyBank.all

    if @userv.all_verified == 1 
      @online_payment_methods= @all_company_banks.joins("INNER JOIN usergroups_banks ON usergroups_banks.company_bank_id = company_banks.id AND usergroups_banks.status = 1 AND company_banks.status = 1 AND company_banks.bank_category = 'online' AND usergroups_banks.users_group_id = #{current_userd.users_group.id}")
      @offline_payment_methods= @all_company_banks.joins("INNER JOIN usergroups_banks ON usergroups_banks.company_bank_id = company_banks.id AND usergroups_banks.status = 1 AND company_banks.status = 1 AND company_banks.bank_category = 'offline' AND usergroups_banks.users_group_id = #{current_userd.users_group.id}")
      @crypto_payment_methods= @all_company_banks.joins("INNER JOIN usergroups_banks ON usergroups_banks.company_bank_id = company_banks.id AND usergroups_banks.status = 1  AND company_banks.status = 1  AND company_banks.bank_category = 'crypto' AND usergroups_banks.users_group_id = #{current_userd.users_group.id}")
      
      @offline_pound_methods= @all_company_banks.joins("INNER JOIN usergroups_banks ON usergroups_banks.company_bank_id = company_banks.id AND usergroups_banks.status = 1 AND company_banks.status = 1 AND company_banks.bank_category = 'offline' AND (company_banks.bank_subcategory = 'localeg/bank' OR company_banks.bank_subcategory = 'localeg/post' OR company_banks.bank_subcategory = 'localeg/vodafonecash') AND usergroups_banks.users_group_id = #{current_userd.users_group.id}")
      @offline_riyal_methods= @all_company_banks.joins("INNER JOIN usergroups_banks ON usergroups_banks.company_bank_id = company_banks.id AND usergroups_banks.status = 1 AND company_banks.status = 1 AND company_banks.bank_category = 'offline' AND company_banks.bank_subcategory = 'localsar' AND usergroups_banks.users_group_id = #{current_userd.users_group.id}")
      @offline_dolar_methods= @all_company_banks.joins("INNER JOIN usergroups_banks ON usergroups_banks.company_bank_id = company_banks.id AND usergroups_banks.status = 1 AND company_banks.status = 1 AND company_banks.bank_category = 'offline' AND (company_banks.bank_subcategory = 'interusd/swift' OR company_banks.bank_subcategory = 'interusd/western' OR company_banks.bank_subcategory = 'interusd/money') AND usergroups_banks.users_group_id = #{current_userd.users_group.id}")

    else 
      @online_payment_methods= @all_company_banks.joins("INNER JOIN usergroups_banks ON usergroups_banks.company_bank_id = company_banks.id AND usergroups_banks.status = 1 AND company_banks.status = 1 AND company_banks.status_v = 1 AND company_banks.bank_category = 'online' AND usergroups_banks.users_group_id = #{current_userd.users_group.id}")
      @offline_payment_methods= @all_company_banks.joins("INNER JOIN usergroups_banks ON usergroups_banks.company_bank_id = company_banks.id AND usergroups_banks.status = 1 AND company_banks.status = 1 AND company_banks.status_v = 1 AND company_banks.bank_category = 'offline' AND usergroups_banks.users_group_id = #{current_userd.users_group.id}")
      @crypto_payment_methods= @all_company_banks.joins("INNER JOIN usergroups_banks ON usergroups_banks.company_bank_id = company_banks.id AND usergroups_banks.status = 1  AND company_banks.status = 1 AND company_banks.status_v = 1  AND company_banks.bank_category = 'crypto' AND usergroups_banks.users_group_id = #{current_userd.users_group.id}")

      @offline_pound_methods= @all_company_banks.joins("INNER JOIN usergroups_banks ON usergroups_banks.company_bank_id = company_banks.id AND usergroups_banks.status = 1 AND company_banks.status = 1 AND company_banks.status_v = 1 AND company_banks.bank_category = 'offline' AND (company_banks.bank_subcategory = 'localeg/bank' OR company_banks.bank_subcategory = 'localeg/post' OR company_banks.bank_subcategory = 'localeg/vodafonecash') AND usergroups_banks.users_group_id = #{current_userd.users_group.id}")
      @offline_riyal_methods= @all_company_banks.joins("INNER JOIN usergroups_banks ON usergroups_banks.company_bank_id = company_banks.id AND usergroups_banks.status = 1 AND company_banks.status = 1 AND company_banks.status_v = 1 AND company_banks.bank_category = 'offline' AND company_banks.bank_subcategory = 'localsar' AND usergroups_banks.users_group_id = #{current_userd.users_group.id}")
      @offline_dolar_methods= @all_company_banks.joins("INNER JOIN usergroups_banks ON usergroups_banks.company_bank_id = company_banks.id AND usergroups_banks.status = 1  AND company_banks.status = 1 AND company_banks.status_v = 1  AND company_banks.bank_category = 'offline' AND (company_banks.bank_subcategory = 'interusd/swift' OR company_banks.bank_subcategory = 'interusd/western' OR company_banks.bank_subcategory = 'interusd/money') AND usergroups_banks.users_group_id = #{current_userd.users_group.id}")
  
    end

    rescue => e
      @code = SecureRandom.hex
      ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s)
    end

  end

  # preview of card details
  def cart_preview

    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Show Cart') 
     begin
    @pc = params[:id].to_s
    @cart= Cart.find(@pc)
     @trans = Transaction.where([ "user_id = ? and controller LIKE ? and foreign_id = ?" ,current_userd.id ,"%carts%",@cart.id]).first

    if current_userd.id != @cart.user_id
      redirect_to account_settings_path, notice: 'you can not view this cart.'
    end
   
    @pre= AESCrypt.decrypt(@cart.prefix, 'ServHight')  
    @cartnn=    AESCrypt.decrypt(@cart.cart_number, 'ServHight') 
    @username = Userd.where("id =?", @cart.user_id).first 
  
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end
 
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart
      @cart = Cart.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cart_params
      params.require(:cart).permit(:user_id, :prefix, :cart_number, :value, :status, :expire_date)
    end
end
