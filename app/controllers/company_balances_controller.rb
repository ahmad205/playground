class CompanyBalancesController < ApplicationController
  before_action :set_company_balance, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_userd!, :isthisadmin 

  # GET /company_balances
  def index
    
    begin    
    @company_balances = CompanyBalance.all.paginate(page: params[:wait_page], per_page: 3).order('created_at DESC')
    
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # SHOW /company_balances
  def show

    begin
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
  
  end

  # GET /company_balances/new
  def new

    begin
    @company_balance = CompanyBalance.new
    rescue => e   
      @code = SecureRandom.hex

      
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # GET /company_balances/1/edit
  def edit

    begin
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # POST /company_balances
  def create

    begin
    @company_balance = CompanyBalance.new(company_balance_params)

    respond_to do |format|

      if @company_balance.save
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'company_balance', :action_page =>"add company balance ",:foreign_id=>@company_balance.id ,:details=> "add company balance with id :  "+ @company_balance.id.to_s )
        format.html { redirect_to @company_balance, notice: 'Company balance was successfully created.' }
        format.json { render :show, status: :created, location: @company_balance }
      
      else
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'company_balance', :action_page =>"add company balance ",:foreign_id=>@company_balance.id ,:details=> "fails to add company balance with id :  "+ @company_balance.id.to_s )
        format.html { render :new }
        format.json { render json: @company_balance.errors, status: :unprocessable_entity }
      
      end

    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # PATCH/PUT /company_balances/1
  def update

    begin
    respond_to do |format|

      if @company_balance.update(company_balance_params)
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'company_balance', :action_page =>"update company balance ",:foreign_id=>@company_balance.id ,:details=> "update company balance with id :  "+ @company_balance.id.to_s )
        format.html { redirect_to @company_balance, notice: 'Company balance was successfully updated.' }
        format.json { render :show, status: :ok, location: @company_balance }
      
      else
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'company_balance', :action_page =>"update company balance ",:foreign_id=>@company_balance.id ,:details=> "fails to update company balance with id :  "+ @company_balance.id.to_s )
        format.html { render :edit }
        format.json { render json: @company_balance.errors, status: :unprocessable_entity }
      
      end

    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # DELETE /company_balances/1
  def destroy

    begin
    Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'company_balance', :action_page =>"destroy company balance ",:foreign_id=>@company_balance.id ,:details=> "destroy company balance with id :  "+ @company_balance.id.to_s )
    @company_balance.destroy

    respond_to do |format|
      format.html { redirect_to company_balances_url, notice: 'Company balance was successfully destroyed.' }
      format.json { head :no_content }
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_balance
      @company_balance = CompanyBalance.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_balance_params
      params.require(:company_balance).permit(:user_id, :operation_id, :trans_id, :balance, :status, :hint, :balances_operation_id, :balances_withdraw_id)
    end
end
