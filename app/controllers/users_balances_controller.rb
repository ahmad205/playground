class UsersBalancesController < ApplicationController
  before_action :set_users_balance, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_userd!
     respond_to :js, :json, :html


  # GET /UsersBalances
  # GET /UsersBalances.json
  def index
      if  current_userd.role_id ==  1 
       @users_balances = UsersBalance.all.paginate(page: params[:page], per_page: 3).order('created_at DESC')
 
    else
       @users_balances = UsersBalance.where("user_id = ? ",current_userd.id).all.paginate(page: params[:page], per_page: 3).order('created_at DESC')
 
    end    
    
    
  end

  def convert 
    
    @userid=params[:id].to_i

    @users_balance = UsersBalance.where("user_id = ? ",@userid).first
 
    # if userprivilage.balanceadd == 1 or current_userd.role_id == 1
    # @users_balance = UsersBalance.new 
    # else 
    #    redirect_to(balances_operations_url, notice: 'not allowed to add balance')  
    # end 

  end

    def calculate
        #@value = params[:value]

            #format.json  { render json: {} , status: 200 }
     respond_to do |format|
    format.json { head :ok }
end
  
      end

      
  def convertpost
    @userid=params[:user_id].to_i
      #@users_balancee = UsersBalance.find(params[:user_id])

  @users_balancee= UsersBalance.where("user_id = ?",current_userd.id).first
  @value=params[:users_balance][:value].to_f

 if params[:users_balance][:type_conversion_from].to_s=='usd' && params[:users_balance][:type_conversion_to].to_s == 'egp' && @users_balancee.balance.to_f >= params[:users_balance][:value].to_f

 $ubalanceDollar=(@users_balancee.balance.to_f)-(params[:users_balance][:value].to_f)
 $incresedvalue=params[:users_balance][:value].to_f*(@usdtoegp.value.to_f)
 $ubalanceEg=(@users_balancee.balanceEgp.to_f)+(params[:users_balance][:value].to_f*(@usdtoegp.value.to_f))
 UsersBalance.where('user_id = ?', current_userd.id).update_all(:balance => $ubalanceDollar,:balanceEgp => $ubalanceEg)
 redirect_to(request.env['HTTP_REFERER'],notice:"تم تحويل #{@value} دولار امريكي ل #{$incresedvalue} جنية مصري")
end

if params[:users_balance][:type_conversion_from].to_s=='egp' && params[:users_balance][:type_conversion_to].to_s == 'usd' && @users_balancee.balanceEgp.to_f >= params[:users_balance][:value].to_f
 $ubalanceEg=(@users_balancee.balanceEgp.to_f)-(params[:users_balance][:value].to_f)
$incresedvalue=params[:users_balance][:value].to_f/(@egptousd.value.to_f)
  $ubalanceDollar=(@users_balancee.balance.to_f)+(params[:users_balance][:value].to_f/(@egptousd.value.to_f))

 UsersBalance.where('user_id = ?', current_userd.id).update_all(:balance => $ubalanceDollar,:balanceEgp => $ubalanceEg)
  redirect_to(request.env['HTTP_REFERER'],notice:"تم تحويل #{@value} جنية مصري ل #{$incresedvalue} دولار امريكي")

end

if params[:users_balance][:type_conversion_from].to_s=='egp' && params[:users_balance][:type_conversion_to].to_s == 'rys' && @users_balancee.balanceEgp.to_f >= params[:users_balance][:value].to_f
 $ubalanceEg=(@users_balancee.balanceEgp.to_f)-(params[:users_balance][:value].to_f)
$incresedvalue=params[:users_balance][:value].to_f/(@egptors.value.to_f)
  $ubalanceRys=(@users_balancee.balanceRys.to_f)+(params[:users_balance][:value].to_f/(@egptors.value.to_f))
 UsersBalance.where('user_id = ?', current_userd.id).update_all(:balanceEgp => $ubalanceEg,:balanceRys => $ubalanceRys)
  redirect_to(request.env['HTTP_REFERER'],notice:"تم تحويل #{@value} جنية مصري ل #{$incresedvalue} ريال سعودي")

end


if params[:users_balance][:type_conversion_from].to_s=='rys' && params[:users_balance][:type_conversion_to].to_s == 'egp' && @users_balancee.balanceRys.to_f >= params[:users_balance][:value].to_f
 $ubalanceRys=(@users_balancee.balanceRys.to_f)-(params[:users_balance][:value].to_f)
 $incresedvalue=params[:users_balance][:value].to_f*(@rstoegp.value.to_f)

 $ubalanceEg=(@users_balancee.balanceEgp.to_f)+(params[:users_balance][:value].to_f*@rstoegp.value.to_f)
 UsersBalance.where('user_id = ?', current_userd.id).update_all(:balanceRys => $ubalanceRys,:balanceEgp => $ubalanceEg)
  redirect_to(request.env['HTTP_REFERER'],notice:"تم تحويل #{@value} ريال سعودي ل #{$incresedvalue} جنية مصري ")

end


if params[:users_balance][:type_conversion_from].to_s=='usd' && params[:users_balance][:type_conversion_to].to_s == 'rys' && @users_balancee.balance.to_f >= params[:users_balance][:value].to_f
 $ubalanceDollar=(@users_balancee.balance.to_f)-(params[:users_balance][:value].to_f)
 $incresedvalue=params[:users_balance][:value].to_f*(@usdtors.value.to_f)
 $ubalanceRys=(@users_balancee.balanceRys.to_f)+(params[:users_balance][:value].to_f*(@usdtors.value.to_f))
 UsersBalance.where('user_id = ?', current_userd.id).update_all(:balance => $ubalanceDollar,:balanceRys => $ubalanceRys)
  redirect_to(request.env['HTTP_REFERER'],notice:"تم تحويل #{@value} دولار امريكي ل #{$incresedvalue} ريال سعودي ")

end


if params[:users_balance][:type_conversion_from].to_s=='rys' && params[:users_balance][:type_conversion_to].to_s == 'usd' && @users_balancee.balanceRys.to_f >= params[:users_balance][:value].to_f
 $ubalanceRys=(@users_balancee.balanceRys.to_f)-(params[:users_balance][:value].to_f)
 $incresedvalue=params[:users_balance][:value].to_i/(@rstousd.value.to_f)
 $ubalanceDollar=(@users_balancee.balance.to_f)+(params[:users_balance][:value].to_i/(@rstousd.value.to_f))  
 UsersBalance.where('user_id = ?', current_userd.id).update_all(:balance => $ubalanceDollar,:balanceRys => $ubalanceRys)
  redirect_to(request.env['HTTP_REFERER'],notice:"تم تحويل #{@value} ريال سعودي ل #{$incresedvalue} دولار امريكي ")

end

end








    # @retv=  UsersBalance.convert(@users_balance.user_id,@users_balance.type_conversion_from,@users_balance.type_conversion_to,@users_balance.value)
     #@retv=  UsersBalance.convert(params[:user_id].to_i,params[:user_id].to_i,params[:type_conversion_from].to_s,params[:type_conversion_to].to_s,params[:value].to_i)
        #  end
       # format.html { redirect_to @users_balance, notice: 'UsersBalance was successfully created.' }

    

  

  # GET /UsersBalances/1
  # GET /UsersBalances/1.json
  def show
  end

  # GET /UsersBalances/new
  def new
  if userprivilage.balanceadd == 1 or current_userd.role_id == 1
    @users_balance = UsersBalance.new 
    else 
       redirect_to(balances_operations_url, notice: 'not allowed to add balance')  
    end 
  end

  # GET /UsersBalances/1/edit
  def edit
  end

  # POST /UsersBalances
  # POST /UsersBalances.json
  def create
    @users_balance = UsersBalance.new(users_balance_params)

    respond_to do |format|
      if @users_balance.save
	    Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'user balance', :action_page =>" add user balance",:foreign_id=>@users_balance.id ,:details=> " add user balance  with id :  "+ @users_balance.id.to_s )

        format.html { redirect_to @users_balance, notice: 'UsersBalance was successfully created.' }
        format.json { render :show, status: :created, location: @users_balance }
      else
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'user balance', :action_page =>" add user balance",:foreign_id=>@users_balance.id ,:details=> " fails to add user balance  with id :  "+ @users_balance.id.to_s )
		format.html { render :new }
        format.json { render json: @users_balance.errors, status: :unprocessable_entity }
      end
    end
  end


  # PATCH/PUT /UsersBalances/1
  # PATCH/PUT /UsersBalances/1.json
  def update
    respond_to do |format|
      if @users_balance.update(users_balance_params)
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'user balance', :action_page =>" update user balance",:foreign_id=>@users_balance.id ,:details=> " update user balance  with id :  "+ @users_balance.id.to_s )
		format.html { redirect_to @users_balance, notice: 'UsersBalance was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_balance }
      else
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'user balance', :action_page =>" update user balance",:foreign_id=>@users_balance.id ,:details=> " fails to update user balance  with id :  "+ @users_balance.id.to_s )
		format.html { render :edit }
        format.json { render json: @users_balance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /UsersBalances/1
  # DELETE /UsersBalances/1.json
  def destroy
    Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'user balance', :action_page =>" destroy user balance",:foreign_id=>@users_balance.id ,:details=> " destroy user balance  with id :  "+ @users_balance.id.to_s )

    @users_balance.destroy
    respond_to do |format|
      format.html { redirect_to users_balances_url, notice: 'UsersBalance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_balance
      @users_balance = UsersBalance.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_balance_params
    ##  params[:UsersBalance]
            params.require(:users_balance).permit(:name,   :user_id ,   :balance ,:balanceEgp,:balanceRys ,:hint, :created_at, :modified_at )

    end
end
