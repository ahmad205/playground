class InvitationsController < Devise::InvitationsController
  
    #before_filter :update_sanitized_params, only: :update
  

    def new
      self.resource = resource_class.new
      render :new,:layout => false
    end

    
    # PUT /resource/invitation
    def update
      respond_to do |format|
        format.js do
          invitation_token = Devise.token_generator.digest(resource_class, :invitation_token, update_resource_params[:invitation_token])
          self.resource = resource_class.where(invitation_token: invitation_token).first
          resource.skip_password = true
          resource.update_attributes update_resource_params.except(:invitation_token)
        end

       
          
        format.html do

         

          super

          @account_number='PS'+rand(100000000..999999999).to_s
          #render :layout => false
          
            @userd = Userd.where(:username => params[:userd][:username]).first 
           # @userd = Userd.find_by_invitation_token(params[:userd][:invitation_token]).first
            
            # @userd.update(:name => params[:userd][:name],:account_number =>@account_number,:country=> params[:userd][:country],:tel=> params[:userd][:tel],:avatar=> params[:userd][:avatar],:nationalidphoto=> params[:userd][:nationalidphoto],:role_id=> 2,:group_id=> 5,:nationalid=> params[:userd][:nationalid],:vodafonecash=> params[:userd][:vodafonecash],:country=> params[:userd][:country],:currency=> params[:userd][:currency],:attachments=> params[:userd][:attachments])
            @userd.update(:is_company => params[:userd][:is_company],:role_id=> 2,:group_id=> 5,:account_number =>@account_number)
            #  #UserMailer.welcome_email(@userd).deliver
             Watchdog.create(:user_id => @userd.id,:controller => 'users ', :action_page =>" add user ",:foreign_id=>@userd.id ,:details=> "add user with id :  "+ @userd.id.to_s )
             UserVerification.create(:user_id =>@userd.id)
            #UsersBalance.create(:name => @user.name,:user_id => @user.id, :hint =>"NEW",:balance=>0,:balanceRys=>0,:balanceEgp=>0)
            Notification.create(:user_id => @userd.id,:controller => 'userd',:stats => 'تم إنشاء حساب عضو جديد باسم  ' +@userd.email )
            UsersWalletsBalance.create(:user_id => @userd.id , :currency => "USD" , :wallet_name => "US Dollar" , :balance => '0' ,:status => '1' )
            @userd.send(:assign_auth_secret)
            @userd.save!
            
        end
      end
    end
  
  
    protected
  
    def update_resource_params
     # devise_parameter_sanitizer.permit(:accept_invitation, keys: [:username, :password, :password_confirmation, :invitation_token])

     # params.require(:userd).permit(:accept_invitation, keys: [:username, :password, :password_confirmation, :invitation_token])

      params.require(:userd).permit(:username, :password, :password_confirmation, :invitation_token)
      
      
      
    end
  end
  