class CurrenciesWithdrawRatiosController < ApplicationController
  before_action :set_currencies_withdraw_ratio, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_userd! 
  
  # GET /currencies_withdraw_ratios
  def index

    # begin
    # if current_userd.role_id == 1
    # @currencies_withdraw_ratios = CurrenciesWithdrawRatio.all
    # end

    # rescue =>e
    #   @code = SecureRandom.hex
    #   ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s)
    # end

  end

  def types
  
    # begin
    # @currencies_withdraw_ratios = CurrenciesWithdrawRatio.where('currency_id = ?' , params[:id]).all
    # rescue =>e
    #   @code = SecureRandom.hex
    #   ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s)
    # end

  end

  # Show /currencies_withdraw_ratios
  def show
  end

  # GET /currencies_withdraw_ratios/new
  def new

    # begin
    # if current_userd.role_id == 1
    #   @currencies_withdraw_ratio = CurrenciesWithdrawRatio.new
    # end

    # rescue =>e
    #   @code = SecureRandom.hex
    #   ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s)
    # end

  end

  # GET /currencies_withdraw_ratios/edit
  def edit
  end

  # POST /currencies_withdraw_ratios
  def create

    # begin
    # if current_userd.role_id == 1
    #   @currencies_withdraw_ratio = CurrenciesWithdrawRatio.new(currencies_withdraw_ratio_params)

    #   respond_to do |format|

    #     if @currencies_withdraw_ratio.save
    #       format.html { redirect_to @currencies_withdraw_ratio, notice: 'Currencies withdraw ratio was successfully created.' }
    #       format.json { render :show, status: :created, location: @currencies_withdraw_ratio }
    #     else
    #       format.html { render :new }
    #       format.json { render json: @currencies_withdraw_ratio.errors, status: :unprocessable_entity }
    #     end

    #   end

    # end

    # rescue =>e
    #   @code = SecureRandom.hex
    #   ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s)
    # end

  end

  # Update /currencies_withdraw_ratios
  def update

    # if current_userd.role_id == 1
    #   respond_to do |format|
    #     if @currencies_withdraw_ratio.update(currencies_withdraw_ratio_params)
    #       format.html { redirect_to @currencies_withdraw_ratio, notice: 'Currencies withdraw ratio was successfully updated.' }
    #       format.json { render :show, status: :ok, location: @currencies_withdraw_ratio }
    #     else
    #       format.html { render :edit }
    #       format.json { render json: @currencies_withdraw_ratio.errors, status: :unprocessable_entity }
    #     end

    #   end

    # end

    # rescue =>e
    #   @code = SecureRandom.hex
    #   ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s)
    # end

  end

  # DELETE /currencies_withdraw_ratios
  def destroy

    # begin
    # if current_userd.role_id == 1
    #   @currencies_withdraw_ratio.destroy
    #   respond_to do |format|
    #     format.html { redirect_to currencies_withdraw_ratios_url, notice: 'Currencies withdraw ratio was successfully destroyed.' }
    #     format.json { head :no_content }
    #   end

    # end

    # rescue =>e
    #   @code = SecureRandom.hex
    #   ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s)
    # end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_currencies_withdraw_ratio
      @currencies_withdraw_ratio = CurrenciesWithdrawRatio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def currencies_withdraw_ratio_params
      params.require(:currencies_withdraw_ratio).permit(:name, :currency_id, :hint, :ratio_bank, :ratio_company, :additional_expenses, :status, :ima)
    end

end
