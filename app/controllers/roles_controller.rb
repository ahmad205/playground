class RolesController < ApplicationController
  before_action :set_role, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_userd!,:isthisadmin

  # GET /roles
  # GET /roles.json
  def index
    @roles = Role.all
  end
  
  # GET /roles/1
  # GET /roles/1.json
  def show
         @users = Role.find(params[:id]).user

  end

  # GET /roles/new
  def new
    @role = Role.new
  end

  # GET /roles/1/edit
  def edit
  end

  # POST /roles
  # POST /roles.json
  def create
    @role = Role.new(role_params)

    respond_to do |format|
      if @role.save
	    Watchdog.create(:user_id => current_userd.id,:controller => 'role', :action_page =>" add role",:foreign_id=>@role.id ,:details=> " add role  with id :  "+ @role.id.to_s )

        format.html { redirect_to @role, notice: 'Role was successfully created.' }
        format.json { render :show, status: :created, location: @role }
      else
         Watchdog.create(:user_id => current_userd.id,:controller => 'role', :action_page =>" add role",:foreign_id=>@role.id ,:details=> "fails to  add role  with id :  "+ @role.id.to_s )
		format.html { render :new }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /roles/1
  # PATCH/PUT /roles/1.json
  def update
    respond_to do |format|
      if @role.update(role_params)
        Watchdog.create(:user_id => current_userd.id,:controller => 'role', :action_page =>" update role",:foreign_id=>@role.id ,:details=> " update role  with id :  "+ @role.id.to_s )
		format.html { redirect_to @role, notice: 'Role was successfully updated.' }
        format.json { render :show, status: :ok, location: @role }
      else
        Watchdog.create(:user_id => current_userd.id,:controller => 'role', :action_page =>" update role",:foreign_id=>@role.id ,:details=> "fails to  update role  with id :  "+ @role.id.to_s )
		format.html { render :edit }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /roles/1
  # DELETE /roles/1.json
  def destroy
     Watchdog.create(:user_id => current_userd.id,:controller => 'role', :action_page =>" destroy role",:foreign_id=>@role.id ,:details=> " destroy role  with id :  "+ @role.id.to_s )
	@role.destroy
    respond_to do |format|
      format.html { redirect_to roles_url, notice: 'Role was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_role
      @role = Role.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def role_params
      params.require(:role).permit(:user_id)
    end
end
