class CompanyBanksController < ApplicationController
  before_action :set_company_bank, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_userd! ,:isthisadmin
  
  # GET /company_banks
  def index

    begin
    @company_banks = CompanyBank.all

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # GET /company_banks/1
  def show

    begin
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # GET /company_banks/new
  def new

    begin
    @company_bank = CompanyBank.new

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # GET /company_banks/1/edit
  def edit

    begin
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # POST /company_banks
  def create

    begin
    @company_bank = CompanyBank.new(company_bank_params)

    respond_to do |format|
      if @company_bank.save
        format.html { redirect_to @company_bank, notice: 'Company bank was successfully created.' }
        format.json { render :show, status: :created, location: @company_bank }
      
      else
        format.html { render :new }
        format.json { render json: @company_bank.errors, status: :unprocessable_entity }
      end
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # PATCH/PUT /company_banks/1
  def update

    begin
    respond_to do |format|
      if @company_bank.update(company_bank_params)
        format.html { redirect_to @company_bank, notice: 'Company bank was successfully updated.' }
        format.json { render :show, status: :ok, location: @company_bank }
      
      else
        format.html { render :edit }
        format.json { render json: @company_bank.errors, status: :unprocessable_entity }
      end
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # DELETE /company_banks/1
  def destroy

    begin
    @company_bank.destroy
    respond_to do |format|
      format.html { redirect_to company_banks_url, notice: 'Company bank was successfully destroyed.' }
      format.json { head :no_content }
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_bank
      @company_bank = CompanyBank.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_bank_params
      params.require(:company_bank).permit(:bank_key,:bank_name,:encryptkey,:currency,:country,:city,:branch,:branch_code,:phone,:name,:nationalid,:account_name,:account_email,:account_number,:visa_cvv,:swiftcode,:ibancode, :expire_date,:fees,:ratio, :status,:status_v,:logo,:bank_category,:bank_subcategory)
    end
end
