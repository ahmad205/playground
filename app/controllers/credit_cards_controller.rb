class CreditCardsController < ApplicationController
  before_action :set_credit_card, only: [:show, :edit, :update, :destroy]
  before_filter :set_come_soon,:except =>[:come_soon]



  def set_come_soon
       redirect_to come_soon_path

  end

  def come_soon

    render layout: false


  end
  # GET /credit_cards
  def index

    begin
    #appear only approved or pending cards
    @credit_cards = CreditCard.where(:status => [ 2,1 ],:user_id => current_userd.id ).all.order('created_at DESC')
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # GET /credit_cards/1
  def show

    begin
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  #get type of card
  def getcardtype

    begin
    @number = params[:number].to_i
    @detector = CreditCardValidations::Detector.new(@number)
    @brand =   @detector.brand
    
    if  @detector.brand == nil
      @brand =  t("invalid")
    end
    render :json => { 'type': @brand }

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # GET /credit_cards/new
  def new

    begin
    @credit_card = CreditCard.new

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # GET /credit_cards/1/edit
  def edit

    begin
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # POST /credit_cards
  def create

    begin
    @credit_card = CreditCard.new(credit_card_params)
    @credit_card.user_id = current_userd.id

    respond_to do |format|
      if @credit_card.save

        if @credit_card.status == 2
          @status = "Pending"

        elsif @credit_card.status == 1
          @status = "Approved"

        elsif @credit_card.status == 0
          @status = "Not Approved"
        end

        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'credit_cards', :action_page =>"create_credit_card",:foreign_id=>@credit_card.id ,:details=> "Credit Card has been added by/"+ current_userd.username + " /with status/ " + @status + " /with number/ " + @credit_card.card_no )
        format.html { redirect_to credit_cards_path, notice: t('Credit card was successfully created') }
        format.json { render :show, status: :created, location: @credit_card }
      
      else
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'credit_cards', :action_page =>"create_credit_card",:foreign_id=>@credit_card.id ,:details=> "Credit Card can't be added by/"+ current_userd.username )
        format.html { render :new }
        format.json { render json: @credit_card.errors, status: :unprocessable_entity }
      
      end
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def credit_card_image

    @credit_card = CreditCard.where(:id => params[:id].to_i).first
    if  current_userd &&    @credit_card.userd_id == current_userd.id

     send_file CreditCard.where(:id => params[:id].to_i,:userd_id => current_userd.id ).first.photo.path

    else 

     redirect_to(credit_cards_path,:notice => t('A picture you are not allowed to access'))
     end



  end

  # PATCH/PUT /credit_cards/1
  def update

    begin
    respond_to do |format|
      @credit_card.user_id = current_userd.id

      if @credit_card.update(credit_card_params)

        if @credit_card.status == 2
          @status = "Pending"

        elsif @credit_card.status == 1
          @status = "Approved"

        elsif @credit_card.status == 0
          @status = "Not Approved"
        end

        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'credit_cards', :action_page =>"update_credit_card",:foreign_id=>@credit_card.id ,:details=> "Credit Card has been updated by/"+ current_userd.username + " /with status/ " + @status + " /with number/ " + @credit_card.card_no )
        format.html { redirect_to @credit_card, notice: t('Credit card was successfully updated') }
        format.json { render :show, status: :ok, location: @credit_card }
      
      else
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'credit_cards', :action_page =>"update_credit_card",:foreign_id=>@credit_card.id ,:details=> "Credit Card can't be updated by/"+ current_userd.username )
        format.html { render :edit }
        format.json { render json: @credit_card.errors, status: :unprocessable_entity }
      
      end
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # DELETE /credit_cards/1
  def destroy

    begin
    @credit_card.destroy

    respond_to do |format|
      Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'credit_cards', :action_page =>"destroy_credit_card",:foreign_id=>@credit_card.id ,:details=> "Credit Card was successfully destroyed by/"+ current_userd.username )
      format.html { redirect_to credit_cards_url, notice: t('Credit card was successfully destroyed') }
      format.json { head :no_content }
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_credit_card
      @credit_card = CreditCard.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def credit_card_params
      params.require(:credit_card).permit(:user_id, :name, :card_no, :card_type,:photo,:selfie, :expire_date)
    end
end
