class WithdrawsBanksController < ApplicationController
  before_action :set_withdraws_bank, only: [:show, :edit, :update, :destroy]

  # GET /withdraws_banks
  # GET /withdraws_banks.json
  def index
    @withdraws_banks = WithdrawsBank.all
  end

  # GET /withdraws_banks/1
  # GET /withdraws_banks/1.json
  def show
  end

  # GET /withdraws_banks/new
  def new
    @withdraws_bank = WithdrawsBank.new
  end

  # GET /withdraws_banks/1/edit
  def edit
  end

  # POST /withdraws_banks
  # POST /withdraws_banks.json
  def create
    @withdraws_bank = WithdrawsBank.new(withdraws_bank_params)

    respond_to do |format|
      if @withdraws_bank.save
        format.html { redirect_to @withdraws_bank, notice: 'Withdraws bank was successfully created.' }
        format.json { render :show, status: :created, location: @withdraws_bank }
      else
        format.html { render :new }
        format.json { render json: @withdraws_bank.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /withdraws_banks/1
  # PATCH/PUT /withdraws_banks/1.json
  def update
    respond_to do |format|
      if @withdraws_bank.update(withdraws_bank_params)
        format.html { redirect_to @withdraws_bank, notice: 'Withdraws bank was successfully updated.' }
        format.json { render :show, status: :ok, location: @withdraws_bank }
      else
        format.html { render :edit }
        format.json { render json: @withdraws_bank.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /withdraws_banks/1
  # DELETE /withdraws_banks/1.json
  def destroy
    @withdraws_bank.destroy
    respond_to do |format|
      format.html { redirect_to withdraws_banks_url, notice: 'Withdraws bank was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_withdraws_bank
      @withdraws_bank = WithdrawsBank.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def withdraws_bank_params
      params.require(:withdraws_bank).permit(:user_id,:bank_id,:withdraw_id, :currencies_withdraw_ratios_id, :personal_name, :business_name, :nationalid, :birth_date, :relation, :nickname, :bank_name, :country, :country_code, :branch, :branchnumber, :phone, :account_name, :account_email, :account_number, :visa_cvv, :swiftcode, :valid_swift_code, :expire_date, :status, :city, :address, :postal_code, :iban, :bban, :checksum, :bank_code, :check_digit, :sepa, :iban_validity, :iban_checksum, :iban_length, :iban_structure, :account_checksum, :currency)
    end
end
