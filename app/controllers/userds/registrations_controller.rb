
# require 'authy'
require 'json'

class Userds::RegistrationsController < Devise::RegistrationsController
  
    #  before_action :configure_sign_up_params, only: [:create]
  #  before_action :configure_account_update_params, only: [:update]
  before_filter :authenticate_userd!
 
#  prepend_before_action :check_captcha, only: [:create] # Change this to be any actions you want to protect.


  private
def check_captcha
  unless verify_recaptcha
    self.resource = resource_class.new sign_up_params
    respond_with_navigational(resource) { redirect_to :back }
  end 
end 



  # private
  #   def check_captcha
  #     unless verify_recaptcha
  #       self.resource = resource_class.new sign_up_params
  #       resource.validate # Look for any other validation errors besides Recaptcha
  #       set_minimum_password_length
  #        respond_with resource
  #       #  payers_welcome_path
  #       #respond_with resource

  #     end 
  #   end



def after_update_path_for(resource)
      
       @userd = UserVerification.where(:user_id => resource.id).joins("INNER JOIN userds ON userds.id = user_verifications.user_id").first 

       
       if params[:user_verification][:address_line1] || params[:user_verification][:address_line2]
         @userd.update(:address_line1 => params[:user_verification][:address_line1],:address_line2 => params[:user_verification][:address_line2])
         #user_path(resource)
         if @all_status != ""

      Watchdog.create(:user_id => @userd.userd.id,:added_by => @userd.userd.id,:controller => 'userds', :action_page =>"edit_profile",:foreign_id=>@userd.userd.id ,:details=> @all_status + " by user/ " + @userd.userd.username)

      end
         user_profile_path
       end
             # redirect_to(root_path,notice:"تم ارسال الكود بنجاح") and return

       
    end
  def sign_up_params
    params.require(:userd).permit(:username,:tel,:email, :password, :password_confirmation)
  end

  def account_update_params
    @user = Userd.joins("INNER JOIN user_verifications ON user_verifications.user_id = userds.id").find(current_userd.id)
     # @userd = UserVerification.where(:user_id => resource.id).first

      @post_code = @user.postal_code.to_s
      @firstname = @user.f_name
      @lastname = @user.l_name
      @nationality = @user.nationality
      @country = @user.country
      @city = @user.city
      @address1 = @user.user_verification.address_line1
      @address2 = @user.user_verification.address_line2
      # @mail = @user.email
      @phone = @user.tel
      @timezone = @user.time_zone




      # @user.skip_reconfirmation!
      # @user.update_column(:email, params[:userd][:email])


        
        if params[:userd][:postal_code] != nil and @post_code != nil and @post_code != params[:userd][:postal_code]
            @postal_code = " /postal code has been updated from " + @post_code + " to " + params[:userd][:postal_code]
            else
             @postal_code = "" 
        end
        if params[:userd][:f_name] != nil and @firstname != nil and @firstname != params[:userd][:f_name]
            @f_name = " /first name has been updated from " + @firstname + " to " + params[:userd][:f_name].to_s
            else
              @f_name =""
        end
        if params[:userd][:l_name] != nil and @lastname != nil and @lastname != params[:userd][:l_name]
            @l_name = " /last name has been updated from " + @lastname + " to " + params[:userd][:l_name].to_s
            else
              @l_name =""
        end
        if params[:userd][:email] != nil and @mail != nil and @mail != params[:userd][:email]
            @email = " /user email has been updated from " + @mail + " to " + params[:userd][:email].to_s
            else
              @email =""
        end
        if params[:userd][:nationality] != nil and @nationality != nil and @nationality != params[:userd][:nationality]
            @national = " /user nationality has been updated from " + @nationality + " to " + params[:userd][:nationality].to_s
            else
              @national = ""
        end
        if params[:userd][:country] != nil and @country != nil and @country != params[:userd][:country]
            @country2 = " /user country has been updated from " + @country + " to " + params[:userd][:country].to_s
            else
              @country2 =""
        end
        if params[:userd][:city] != nil and @city != nil and @city != params[:userd][:city]
            @city2 = " /user city has been updated from " + @city + " to " + params[:userd][:city].to_s
            else
              @city2 =""
        end
        if params[:userd][:tel] != nil and @phone != nil and @phone != params[:userd][:tel]
            @tel = " /user telephone has been updated from " + @phone + " to " + params[:userd][:tel].to_s
            else
              @tel =""
        end

        if params[:userd][:time_zone] != nil and @timezone != nil and @timezone != params[:userd][:time_zone]
            @time_zone = " /user time zone has been updated from " + @timezone + " to " + params[:userd][:time_zone].to_s
            else
              @time_zone =""
        end
       
        if params[:user_verification][:address_line1] != nil and @address1 != nil and @address1 != params[:user_verification][:address_line1]
            @address_line1 = " /user address line1 has been updated from " + @address1 + " to " + params[:user_verification][:address_line1].to_s
            else
              @address_line1 = ""
        end
        if params[:user_verification][:address_line2] != nil and @address2 != nil and @address2 != params[:user_verification][:address_line2]
            @address_line2 = " /user address line2 has been updated from " + @address2 + " to " + params[:user_verification][:address_line2].to_s
            else
              @address_line2 = ""
        end

               @all_status = @postal_code + @f_name + @l_name + @tel + @email + @national + @country2 + @city2 + @time_zone + @address_line1 + @address_line2


      
    params.require(:userd).permit(:f_name,:l_name,:time_zone,:address_line1,:nationality,:currency,:tel,:nationalid,:vodafonecash,:country,:city,:postal_code, :email, :password, :password_confirmation, :current_password ,:avatar)
    #params.require(:user_verification).permit(:address_line1,:address_line2)
  end

 
  # DELETE /resource
  def destroy  
    resource.soft_delete
    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)  
    set_flash_message :notice, :destroyed if is_flashing_format?  
    yield resource if block_given?  
    respond_with_navigational(resource){ redirect_to after_sign_out_path_for(resource_name) }  
  end  
  

  #The path used after sign up for inactive accounts.
  def after_inactive_sign_up_path_for(resource)
    super(resource)

    @account_number='PS'+rand(100000000..999999999).to_s
  @userd = Userd.where(:id => resource.id).first 
  @userd.update(:username => params[:userd][:username],:is_company => params[:userd][:is_company],:role_id=> 2,:group_id=> 8,:tel => params[:userd][:tel],:pin_id => params[:userd][:pin_id],:pin_voice_id => params[:userd][:pin_voice_id],:account_number =>@account_number)  
  
  payers_welcome_path
  end
end
