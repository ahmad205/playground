# class Devise::CheckgaController < Devise::SessionsController

  class Userds::CheckgaController < Devise::SessionsController

  prepend_before_filter :devise_resource, :only => [:show]
  prepend_before_filter :require_no_authentication, :only => [ :show, :update ]

  include Devise::Controllers::Helpers

  def show
    
    @tmpid = params[:id]
    if @tmpid.nil?
      redirect_to :root
    else
      render :show
    end
  end

  def update
    resource = resource_class.find_by_gauth_tmp(params[resource_name]['tmpid'])

      if not resource.nil?

        if params[resource_name]['gauth_token'].to_i == resource.backup_code.to_i
          @user = Userd.find_by_id(resource.id)
          @newcode=[*('0'..'9')].to_a.shuffle[0,6].join
          @user.update(:backup_code => @newcode , :active_otp_secret => 0,:sms_active => 0,:gauth_enabled => '0' )
           Emailcode.backupcode(@user.email,@newcode).deliver

        end
        if resource.validate_token(params[resource_name]['gauth_token'].to_i) || params[resource_name]['gauth_token'].to_i == resource.backup_code.to_i
          set_flash_message(:notice, :signed_in) if is_navigational_format?

          sign_in(resource_name,resource)
          warden.manager._run_callbacks(:after_set_user, resource, warden, {:event => :authentication})
          # respond_with resource, :location => after_sign_in_path_for(resource)
          respond_with resource, :location => root_path

        if not resource.class.ga_remembertime.nil? 
            cookies.signed[:gauth] = {
              :value => resource.email << "," << Time.now.to_i.to_s,
              :secure => !(Rails.env.test? || Rails.env.development?),
              :expires => (resource.class.ga_remembertime + 1.days).from_now
            }

        end
      else
          set_flash_message(:error, :error)
          redirect_to :root
        end

      else
        set_flash_message(:error, :error)
        redirect_to :root
    end
  end




  private

  def devise_resource
    self.resource = resource_class.new
  end


end