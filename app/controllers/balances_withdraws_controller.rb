class BalancesWithdrawsController < ApplicationController
  before_action :set_balances_withdraw, only: [:show, :edit, :update, :destroy]
  before_action :verify_sms, only: [:save_western_gram,:save_egyptian_mail,:save]
  before_action :verify_user, only: [:main,:main_post]

  before_filter :authenticate_userd!
  #skip_before_action :verify_authenticity_token
  #before_filter :authenticate_token,:only => [:save]


  #verify if user is allowed to get to balance withdraw
  def verify_user
      begin 
      @userv = UserVerification.where("user_id = ?",current_userd.id).first
      if @userv.all_verified != 1 
        redirect_to(all_verifications_path,notice:t("please verify the account"))        
      end
      rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
          end 
  end

  # GET /balances_withdraws
  # GET /balances_withdraws.json



  def check_limit
      begin
      @user = Userd.find_by_id(current_userd)
      @value = params[:value]
      @currency = params[:currency]
      @withdraw = params[:withdraw]
      @messege = BalancesWithdraw.checklimit(@withdraw,@value,@currency,@user.id)
      render :json => { 'response': @messege }
    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id

    end 
  end

  def index
      @balances_withdraws = BalancesWithdraw.where("user_id = ? " ,current_userd.id).all.paginate(page: params[:wait_page], per_page: 3).order('created_at DESC')
  end

  def get_expenses
      @expenses = BalancesWithdrawsRatio.getexpenses(params[:currencies_withdraw_ratios_id].to_i,params[:thesum].to_f,current_userd)
      render :json => { 'expenses': @expenses }
  end

  def get_previous 
    @previous = UsersBank.where(id: params[:previous_user_bank].to_i).first    
    render :json => { 'previous': @previous }
  end

  def main
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Balances Withdraws'),balances_withdraws_main_path

    @group = UsersGroup.find_by_id(current_userd.group_id)
   if current_userd.d_withdraw_count.to_i >= @group.day_withdraws_count.to_i
     redirect_to account_settings_path, notice: t('You have exceeded the allowed withdraw count today')and return
   end
    if current_userd.m_withdraw_count.to_i >= @group.month_withdraws_count.to_i
    	 redirect_to account_settings_path, notice: t('You have exceeded the allowed withdraw count this month')and return
   end
    @balances_withdraw = BalancesWithdraw.new
    @country=params[:country]
    @isibann=0
    @arr = ['AL','DZ','AD','AO','AT','AZ','BH','BE','BJ','BA','BR','VG','BG','BF','BI','CM','CV','CR','CI','HR','CY','CZ','DK','FO','GL','DO','EE','FI','AX','FR','GF','PF','TF','GP','MQ','YT','NC','RE','BL','MF','PM','WF','GE','DE','GI','GR','GT','HU','IS','AA','IR','IE','IT','JO','KZ','XK','KW','LV','LB','LI','LT','LU','MK','MG','ML','MT','MR','MU','MD','MC','ME','MZ','NL','NO','PK','PS','PL','PT','QA','RO','LC','SM','ST','SA','SN','RS','SC','SK','SI','ES','SE','CH','TL','TN','TR','UA','AE','GB'].to_json

    session[:balancewithdraw] = 1
  end


  def authenticate_token
        @kh=request.original_url.to_s
        @url = URI.parse(@kh)


        if (params[:token]==nil)
          redirect_to ddevise_path(url: @url.to_s)

        end
        if (params[:token]!=nil)

          @userd = Userd.where(:id => current_userd.id).first
          @token=params[:token]
          @totp = ROTP::TOTP.new(@userd.gauth_secret, issuer: "Payerz")
          @pro=   @totp.verify_with_drift(@token.to_i, 1000, Time.now - 30) # => true
          flash[:success] = 'Sueccessfully True Token'
          if (!@pro)
            redirect_to (ddevise_path)

        end
      end
    end


  # GET /balances_withdraws/1
  # GET /balances_withdraws/1.json
  def show

    begin
      add_breadcrumb t('home'), :root_path
      add_breadcrumb t('Balances Withdraws'), balances_withdraw_path , :title => "Back to the Index"
  @balanceopd = BalancesWithdrawsStatus.where("balances_withdraws_id = ?",params[:id]).all.order('created_at DESC')

  @balancancel = BalancesWithdrawsStatus.where("balances_withdraws_id = ? AND status = ?",params[:id],'canceled').first

      if @balances_withdraw.user_id != current_userd.id 

        redirect_to(root_path,notice:t("you can not view this data."))


      end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end


def egyptian_mail
    @currencies_withdraw_ratios_id=params[:id]
    @balances_withdraw = BalancesWithdraw.new
    @egyptian_mail = CurrenciesWithdrawRatio.where("id = ?",6).first
    @userbanks=UsersBank.where(userd_id: current_userd.id,bank_type: "withdraw",currencies_withdraw_ratios_id: 6)
end

def western_gram
    @currencies_withdraw_ratios_id=params[:id]
    @userbanks=UsersBank.where(userd_id: current_userd.id,bank_type: "withdraw",currencies_withdraw_ratios_id: params[:id].to_i)
    @balances_withdraw = BalancesWithdraw.new
end
 
  # GET /balances_withdraws/new
def new
    begin
        @currencies_withdraw_ratios_id=params[:id]
        if params[:id] == "6"
      redirect_to :action=>'egyptian_mail'
      end
      if (params[:id] == "12" || params[:id] == "4")
      redirect_to :action=>'western_gram',:id=>params[:id]
      end

        if params[:country] == "EG"
          @userbanks=UsersBank.where(userd_id: current_userd.id,bank_type: "withdraw",country_code: "EG",currencies_withdraw_ratios_id: @currencies_withdraw_ratios_id.to_i)
        end

        if params[:country] == "SA"
          @userbanks=UsersBank.where(userd_id: current_userd.id,bank_type: "withdraw",country_code: "SA",currencies_withdraw_ratios_id: @currencies_withdraw_ratios_id.to_i)
        end

        if params[:country] == "US"
          @userbanks=UsersBank.where(userd_id: current_userd.id,bank_type: "withdraw",currencies_withdraw_ratios_id: @currencies_withdraw_ratios_id.to_i)
        end



    @balances_withdraw = BalancesWithdraw.new
    @country=params[:country]
    @isibann=0
    @arr = ['AL','DZ','AD','AO','AT','AZ','BH','BE','BJ','BA','BR','VG','BG','BF','BI','CM','CV','CR','CI','HR','CY','CZ','DK','FO','GL','DO','EE','FI','AX','FR','GF','PF','TF','GP','MQ','YT','NC','RE','BL','MF','PM','WF','GE','DE','GI','GR','GT','HU','IS','AA','IR','IE','IT','JO','KZ','XK','KW','LV','LB','LI','LT','LU','MK','MG','ML','MT','MR','MU','MD','MC','ME','MZ','NL','NO','PK','PS','PL','PT','QA','RO','LC','SM','ST','SA','SN','RS','SC','SK','SI','ES','SE','CH','TL','TN','TR','UA','AE','GB'].to_json
    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end 
end


#this function get data from two banks 1- western union /2-money gram
 def save_western_gram

     begin  

          @oldbalance=@dollar_wallet.balance.to_f
          @transaction_id = Transaction.gettransaction   

          if  @dollar_wallet.balance.to_f < params[:balances_withdraw][:thesum].to_f
            redirect_to balances_withdraws_main_path, notice: t('your balance is less than the value')and return
          else
            #do the withdraw process in the model and return the data of it
          @country = params['users_country']['country']
          @balances_withdraw = BalancesWithdraw.balancewithdraw(params,current_userd.id,@transaction_id,'USD',@country)
          session[:balancewithdraw] = 1

           end
     redirect_to(balances_withdraw_path(:id=>@balances_withdraw.id),notice: t("withdraw request")+ " #{@balances_withdraw.thesum.to_s}"+ ' USD '+  t('Transaction ID') + " #{@transaction_id.to_s} ") and return


    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end 


end




#this function get data from one bank egyptian_mail
def save_egyptian_mail

  begin  

  
    @transaction_id = Transaction.gettransaction
    @wallet=UsersWalletsBalance.where(user_id: current_userd.id,currency: "EGP").first
    @currencies_withdraw_ratios = CurrenciesWithdrawRatio.where("id = ?",6).first
    @oldbalance=@wallet.balance.to_f
    if  @wallet.balance.to_f < params[:balances_withdraw][:thesum].to_f
      redirect_to balances_withdraws_main_path, notice: t('your balance is less than the value')and return
    else

      @balances_withdraw = BalancesWithdraw.balancewithdraw(params,current_userd.id,@transaction_id,'EGP','Egypt')
      session[:balancewithdraw] = 1

    end

    redirect_to(balances_withdraw_path(:id=>@balances_withdraw.id),notice: t("withdraw request")+ " #{@balances_withdraw.thesum.to_s} " + ' EGP '+  t('Transaction ID') + " #{@transaction_id.to_s} ") and return

  rescue =>e
    @code = SecureRandom.hex

    @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
    redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
  end 
end




#this function responsible for withdraw from these categories 
# 1- Saudia Arabia (ibann)
# 2- Egyptian Pound (it has it's own data)
# 3- Dollar (including Both Ibann Countries & swift Countries)

def save

  begin 

    @transaction_id = Transaction.gettransaction         
    if (params[:balances_withdraw][:country]== 'EG')   #Egyption_Pound
      @cur = 'EGP'
      @wallet= @egp_wallet

    elsif (params[:balances_withdraw][:country]== 'SA')   #Egyption_Pound

      @cur = 'SAR'
      @wallet= @sar_wallet


    elsif (params[:balances_withdraw][:country]== 'US')   #Egyption_Pound
      @cur = 'USD'
      @wallet= @dollar_wallet


    end


    if  @wallet.balance.to_f < params[:balances_withdraw][:thesum].to_f
    redirect_to balances_withdraws_main_path, notice: t('your balance is less than the value')and return
    else

    @balances_withdraw = BalancesWithdraw.savebalancewithdraw(params,current_userd.id,@transaction_id,@cur)
    end


               
       redirect_to(balances_withdraw_path(:id=>@balances_withdraw.id),notice: t("withdraw request")+ " #{@balances_withdraw.thesum.to_s} " + @wallet.currency + t("Transaction ID") + " #{@transaction_id.to_s} ") and return
                                                          

      rescue =>e

        @code = SecureRandom.hex
        @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
        redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id

      end

end

  # GET /balances_withdraws/1/edit
  def edit
    begin
     @balanceopd = BalancesWithdrawsStatus.where("balances_withdraws_id = ?",params[:id]).first
if @balanceopd.status ==0

      redirect_to(request.env['HTTP_REFERER'],notice:t("can't edit because the request canceled"))

  end

    if  current_userd.role_id !=  1
		flash[:error] = 'not allowed to update balance withdraw '
               redirect_to @balances_withdraw
  end
  

rescue =>e
  @code = SecureRandom.hex

  @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
  redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
end 

  end

def update_hint
    
    if params[:user_hint] != nil
    @id = params[:balances_withdraws_id].to_i
    @balanceopd = BalancesWithdrawsStatus.where("balances_withdraws_id = ? and status =?", @id,'exception').last
    @dd = params[:user_hint].to_s
    @balanceopd.update(:user_hint => @dd )
    redirect_to(request.env['HTTP_REFERER'],notice:t("Your message has been sent"))
   
    end
end

#when there is an exception status the user is allowed to post a reply for one time from here
def exceptionmessage
    begin
      @balanceopdd = BalancesWithdrawsStatus.find_by(id: params[:id],status: 'exception')
     @balanceopdd.update(hint: params[:exception_message])
      redirect_to(request.env['HTTP_REFERER'],notice:"Message Sent")


    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end 

end
    

  # POST /balances_withdraws
  # POST /balances_withdraws.json
  def create
   
  end

  # PATCH/PUT /balances_withdraws/1
  # PATCH/PUT /balances_withdraws/1.json
  def update
   
  end

  # DELETE /balances_withdraws/1
  # DELETE /balances_withdraws/1.json
  def destroy
   
  end


#the user is allowed to cancel the withdraw he recently done if the status of the withdraw still in creation status
def canceloperation


   begin
      @balances_withdraw = BalancesWithdraw.where( :id =>  params[:id]).first  


  if  @balances_withdraw.status.to_s == "created" && @balances_withdraw.user_id == current_userd.id
    @balances_withdraw = BalancesWithdraw.cancelwithdraw(params,current_userd.id)
    flash[:notice] = t("request was canceled")
  else
    flash[:notice] = t("error")

  end 
  redirect_to(request.env['HTTP_REFERER'])
  
  rescue =>e
    @code = SecureRandom.hex
    @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
    redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
  end 
end




  #confirm access to balance with draw with sms
  def verify_sms
    begin
      session[:balancewithdraw] == 0
      # if session[:balancewithdraw] == 1
      #    flash[:notice] = t('you must confirm access by phone messages')
      #    redirect_to root_path
      # end
    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end 
  end

  #send you sms (test version)
  def balance_with_draw_sms_check
     begin
      render layout: false
        @phone = current_userd.tel
        session[:balancewithdraw_smssent] = 1
      #  if   session[:balancewithdraw_smssent] == 0

      #    uri = URI.parse("https://api.infobip.com/2fa/1/pin")
      #  request = Net::HTTP::Post.new(uri)
      #  request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
      #    request["content-type"] = 'application/json'
      #    request["accept"] = 'application/json'

      #  request.body = JSON.dump({

      #   "applicationId":"451C42313BC234ABF92206EEA0796C1B",
      #   "messageId":"9A9EDA35B0552EF4E942BF272ADA1955",
      #   "from":"Payers",
      #  "to": @phone,

      #   })

      #  req_options = {
      #    use_ssl: uri.scheme == "https",
      #  }

      #  response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      #   @response = http.request(request)
      #    @res= @response.body
      #    @applications_list= ActiveSupport::JSON.decode(@res)
      #    current_userd.update(:pin_id => @applications_list["pinId"] )
      #    session[:balancewithdraw_smssent] = 1
      #   end
    # end
    rescue =>e
    @code = SecureRandom.hex

    @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
    redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
  end 
 end

  #verify your sms (test version)

  def balance_with_draw_sms_check_post

      #render layout: false

    @code= params[:sms_verify][:code].to_i
    @pin_id = current_userd.pin_id
        uri = URI.parse("https://api.infobip.com/2fa/1/pin/#{@pin_id}/verify")
         request = Net::HTTP::Post.new(uri)
         request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
           request["content-type"] = 'application/json'
           request["accept"] = 'application/json'

         request.body = JSON.dump({
          "pin": @code
        })

         req_options = {
           use_ssl: uri.scheme == "https",
         }

         response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|

           @response = http.request(request)
           @res= @response.body
           @applications_list= ActiveSupport::JSON.decode(@res)

         end

         if @applications_list["verified"] ==  true

                flash[:notice] = t('you must confirm access by phone messages')
                session[:balancewithdraw] = 0


           else

                   flash[:alert] = 'الكود خطأ'


          end
          redirect_to(balances_withdraws_main_path)
     end

    
##-----------##
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_balances_withdraw
      @balances_withdraw = BalancesWithdraw.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def balances_withdraw_params
      params.require(:balances_withdraw).permit(:thesum, :user_id, :netsum, :withdraw_sum  , :status, :hint, :expect_date, :user_bank_id, :users_bank_id,:order_number)
    
  end
end