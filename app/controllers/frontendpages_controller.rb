class FrontendpagesController < ApplicationController
  before_action :set_frontendpage, only: [:show, :edit, :update, :destroy]
   before_filter :authenticate_userd!, :except => [:show ,:contact ,:about]


  # GET /frontendpages
  # GET /frontendpages.json
  def index
    @frontendpages = Frontendpage.all
  end

def routing
  render :layout => false

end

  def error_page 

    render :layout => false

  end



  # GET /frontendpages/1
  # GET /frontendpages/1.json
  def show
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Help'), frontendpage_path
    @frontendpage = Frontendpage.find(params[:id])
  end
  def contact
    render :layout => false

  end 
   def about
  end
  # GET /frontendpages/new
  def new
    @frontendpage = Frontendpage.new
  end

  # GET /frontendpages/1/edit
  def edit
  end

  # POST /frontendpages
  # POST /frontendpages.json
  def create
    @frontendpage = Frontendpage.new(frontendpage_params)

    respond_to do |format|
      if @frontendpage.save
        format.html { redirect_to @frontendpage, notice: 'Frontendpage was successfully created.' }
        format.json { render :show, status: :created, location: @frontendpage }
      else
        format.html { render :new }
        format.json { render json: @frontendpage.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /frontendpages/1
  # PATCH/PUT /frontendpages/1.json
  def update
    respond_to do |format|
      if @frontendpage.update(frontendpage_params)
        format.html { redirect_to @frontendpage, notice: 'Frontendpage was successfully updated.' }
        format.json { render :show, status: :ok, location: @frontendpage }
      else
        format.html { render :edit }
        format.json { render json: @frontendpage.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /frontendpages/1
  # DELETE /frontendpages/1.json
  def destroy
    @frontendpage.destroy
    respond_to do |format|
      format.html { redirect_to frontendpages_url, notice: 'Frontendpage was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_frontendpage
      @frontendpage = Frontendpage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def frontendpage_params
          params.require(:frontendpage).permit(:pagename,   :title ,   :pagecontent ,:pagename_en,   :title_en ,   :pagecontent_en   )

    end
end
