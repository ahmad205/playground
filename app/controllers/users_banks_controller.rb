class UsersBanksController < ApplicationController
  before_action :set_users_bank, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_userd!
  
   respond_to :json

  # GET /users_banks
  # GET /users_banks.json
  def index

      add_breadcrumb t('home'), :root_path
      add_breadcrumb t('Bank Accounts'),users_banks_path
   
      @users_banks = UsersBank.where(:bank_type => 'withdraw',:userd_id => current_userd.id).all.order('created_at DESC')
     
    end

def searchpost

  add_breadcrumb t('home'), :root_path
  add_breadcrumb t('Bank Accounts'),searchpost_path


   @userbank = UsersBank.all

   if params[:currency] == "USD"
    @userbank = UsersBank.where("currencies_withdraw_ratios_id = :currency AND userd_id = :id AND (nickname LIKE :search)",
      {:currency => ('4' or '5' or '12' or '14'),:id => current_userd.id , :search => "%#{@search}%"})
elsif params[:currency] == "SAR"
    @userbank = UsersBank.where("currencies_withdraw_ratios_id = :currency AND userd_id = :id AND (nickname LIKE :search)",
      {:currency => 15,:id => current_userd.id , :search => "%#{@search}%"})
elsif params[:currency] == "EGP"
    @userbank = UsersBank.where("currencies_withdraw_ratios_id = :currency AND userd_id = :id AND (nickname LIKE :search)",
      {:currency => ('6' or '13') ,:id => current_userd.id , :search => "%#{@search}%"})
end 
   @userbank = @userbank.where(["nickname LIKE ?  OR username LIKE ? ","%#{params[:searchtext]}%","%#{params[:searchtext]}%",params[:searchtext]]).joins("INNER JOIN userds ON userds.id = users_banks.userd_id").distinct.all if  params[:searchtext].present?


end

  def main_bank
    @userbank = UsersBank.new
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Add New Account'),users_banks_main_path
  end

 

  def for_balanceop

    @usersbanksll = UsersBank.where(:userd_id => params[:id]) 
    render :json => @usersbanksll.to_json

  end
     

  def show
    if @users_bank.userd_id != current_userd.id
       redirect_to account_settings_path, notice: t('you can not view this data')
     end
  end

  # GET /users_banks/new
  def new

    @currencies_withdraw_ratios_id=params[:id]
    if params[:id] == "6"
   redirect_to :action=>'egyptian_mail'
   end
  if (params[:id] == "12" || params[:id] == "4")
   redirect_to :action=>'western_gram',:id=>params[:id]
   end

    

    if params[:country] == "EG"
@userbanks=UsersBank.where(userd_id: current_userd.id,country_code: "EG",currencies_withdraw_ratios_id: @currencies_withdraw_ratios_id.to_i)
@cu = CurrenciesWithdrawRatio.where("id = ?",13).first 

end
 if params[:country] == "SA"
@userbanks=UsersBank.where(userd_id: current_userd.id,country_code: "SA",currencies_withdraw_ratios_id: @currencies_withdraw_ratios_id.to_i)
@cu = CurrenciesWithdrawRatio.where("id = ?",15).first 

end
 if params[:country] == "US"
@userbanks=UsersBank.where(userd_id: current_userd.id,currencies_withdraw_ratios_id: @currencies_withdraw_ratios_id.to_i)
@cu = CurrenciesWithdrawRatio.where("id = ?",14).first 

end      
        @users_bank = UsersBank.new
       @country=params[:country]
       # @users_bank = UsersBank.new
@isibann=0
@arr = ['AL','DZ','AD','AO','AT','AZ','BH','BE','BJ','BA','BR','VG','BG','BF','BI','CM','CV','CR','CI','HR','CY','CZ','DK','FO','GL','DO','EE','FI','AX','FR','GF','PF','TF','GP','MQ','YT','NC','RE','BL','MF','PM','WF','GE','DE','GI','GR','GT','HU','IS','AA','IR','IE','IT','JO','KZ','XK','KW','LV','LB','LI','LT','LU','MK','MG','ML','MT','MR','MU','MD','MC','ME','MZ','NL','NO','PK','PS','PL','PT','QA','RO','LC','SM','ST','SA','SN','RS','SC','SK','SI','ES','SE','CH','TL','TN','TR','UA','AE','GB'].to_json

  end

  def egyptian_mail
    @currencies_withdraw_ratios_id=params[:id]

    @userbank = UsersBank.new
    @egyptian_mail = CurrenciesWithdrawRatio.where("id = ?",6).first 
          @userbanks=UsersBank.where(userd_id: current_userd.id,currencies_withdraw_ratios_id: 6)

  end
def western_gram
    @currencies_withdraw_ratios_id=params[:id]

  @userbanks=UsersBank.where(userd_id: current_userd.id,currencies_withdraw_ratios_id: 4)

   @userbank = UsersBank.new
  @choosen = CurrenciesWithdrawRatio.where("id = ?",params[:id]).first 

  end


  def save_egyptian_mail
    begin
      @UsersBank = UsersBank.newuserbank(params,current_userd.id,'EGP')
      flash[:notice] = t('you have added new bank for') + @UsersBank.bank_name.to_s
      redirect_to(users_banks_path)
      rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end 
  end

 

  def save_western_gram

     begin
      @UsersBank = UsersBank.newuserbank(params,current_userd.id,'USD')
      flash[:notice] = t('you have added new bank for') + @UsersBank.bank_name.to_s
      redirect_to(users_banks_path)
      rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end 

end
 
  def edit_bank
  add_breadcrumb t('home'), :root_path
  add_breadcrumb t('Edit Bank Account'),edit_bank_path
  @userbank_id=params[:id]
  # @userbanks=UsersBank.where(userd_id: current_userd.id,currencies_withdraw_ratios_id: 4)
  
    @userbank = UsersBank.where("id = ? and userd_id = ?" ,@userbank_id, current_userd.id).first
    @currencies_withdraw_ratios_id = @userbank.currencies_withdraw_ratios_id
    @choosen = CurrenciesWithdrawRatio.where("id = ?",@currencies_withdraw_ratios_id).first 
  end


       def edit_egyptian_mail_bank
        @currencies_withdraw_ratios_id=params[:id]
        @userbanks=UsersBank.where(userd_id: current_userd.id,currencies_withdraw_ratios_id: 6)
        @userbank = UsersBank.new
        @choosen = CurrenciesWithdrawRatio.where("id = ?",params[:id]).first 
       end

       def edit_egyptian_mail
        add_breadcrumb t('home'), :root_path
        add_breadcrumb t('Edit Bank Account'),edit_egyptian_mail_path
          @userbank_id=params[:id]               
          @userbank = UsersBank.where("id = ? and userd_id = ?" ,@userbank_id, current_userd.id).first
          if @userbank.userd_id != current_userd.id
             redirect_to(root_path,notice: t('Not allowed to view this data'))
          end
          @currencies_withdraw_ratios_id = @userbank.currencies_withdraw_ratios_id
          

       end

       def post_edit_egyptianmail

        @test = params[:users_bank][:id].to_i
        @usersbank = UsersBank.where("id = ? and userd_id = ? " ,@test, current_userd.id).first
        @usersbank.userd_id = current_userd.id
                 @usersbank.personal_name = params[:users_bank][:personal_name]
                 @usersbank.business_name = params[:users_bank][:business_name]
                 @usersbank.branch = params[:users_bank][:branch]
                 @usersbank.account_number = params[:users_bank][:account_number]
                 @usersbank.nickname = params[:users_bank][:nickname]
                 @usersbank.nationalid = params[:users_bank][:nationalid]
                 @usersbank.country = "Egypt"
                
                 @usersbank.save
                 Notification.create(:user_id => current_userd.id,:controller => 'users_bank',:foreign_id => @usersbank.id.to_s ,:stats => 'Bank data has been modified/' + @usersbank.bank_name.to_s  )
                 Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'users_bank', :action_page =>"edit/ egyptian_post",:foreign_id=>@usersbank.id ,:details=> 'Bank data has been modified/' + @usersbank.bank_name.to_s)
                 redirect_to(request.env['HTTP_REFERER'],notice: t('Bank data has been modified') + " #{@usersbank.bank_name.to_s} ")
                 

       end


       # edit previousely stored bank
   def edit_western_gram
       add_breadcrumb t('home'), :root_path
        add_breadcrumb t('Edit Bank Account'),edit_western_gram_path
      @userbank_id=params[:id]
# @userbanks=UsersBank.where(userd_id: current_userd.id,currencies_withdraw_ratios_id: 4)

  @userbank = UsersBank.where("id = ? and userd_id = ?" ,@userbank_id, current_userd.id).first
  @currencies_withdraw_ratios_id = @userbank.currencies_withdraw_ratios_id
  @choosen = CurrenciesWithdrawRatio.where("id = ?",@currencies_withdraw_ratios_id).first 
  
   end
   
   def post_edit_western
    @test = params[:users_bank][:id].to_i
    @usersbank = UsersBank.where("id = ? and userd_id = ? " ,@test, current_userd.id).first
    @usersbank.userd_id = current_userd.id
             @usersbank.personal_name = params[:users_bank][:personal_name]
             @usersbank.nickname = params[:users_bank][:nickname]
             @usersbank.relation = params[:users_bank][:relation]
             @usersbank.birth_date = params[:users_bank][:birth_date]
             @usersbank.country = "US"
           if  params[:users_bank][:currencies_withdraw_ratios_id] == 4
             @usersbank.bank_name = 'Western Union'
           end
            if  params[:users_bank][:currencies_withdraw_ratios_id] == 12
              @usersbank.bank_name = 'Money Gram'
             end
             # @UsersBank.currencies_withdraw_ratios_id=@currencies_withdraw_ratios.id
             @usersbank.save
             Notification.create(:user_id => current_userd.id,:controller => 'users_bank',:foreign_id => @usersbank.id.to_s ,:stats => 'Bank data has been modified/' + @usersbank.bank_name.to_s )
             Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'users_bank', :action_page =>"edit/ edit_western",:foreign_id=>@usersbank.id ,:details=> 'Bank data has been modified/' + @usersbank.bank_name.to_s)
             redirect_to(request.env['HTTP_REFERER'],notice: t('Bank data has been modified') + " #{@usersbank.bank_name.to_s} ")
             
     
        end


  def edit_bank_conversion

    @currencies_withdraw_ratios_id=params[:id]
    if params[:id] == "6"
   redirect_to :action=>'egyptian_mail'
   end
  if (params[:id] == "12" || params[:id] == "4")
   redirect_to :action=>'western_gram',:id=>params[:id]
   end



    if params[:country] == "EG"
@userbanks=UsersBank.where(userd_id: current_userd.id,country_code: "EG",currencies_withdraw_ratios_id: @currencies_withdraw_ratios_id.to_i)
@cu = CurrenciesWithdrawRatio.where("id = ?",13).first

end
 if params[:country] == "SA"
@userbanks=UsersBank.where(userd_id: current_userd.id,country_code: "SA",currencies_withdraw_ratios_id: @currencies_withdraw_ratios_id.to_i)
@cu = CurrenciesWithdrawRatio.where("id = ?",15).first

end
 if params[:country] == "US"
@userbanks=UsersBank.where(userd_id: current_userd.id,currencies_withdraw_ratios_id: @currencies_withdraw_ratios_id.to_i)
@cu = CurrenciesWithdrawRatio.where("id = ?",14).first

end



@userbank = UsersBank.new
@country=params[:country]
       # @users_bank = UsersBank.new
@isibann=0
@arr = ['AL','DZ','AD','AO','AT','AZ','BH','BE','BJ','BA','BR','VG','BG','BF','BI','CM','CV','CR','CI','HR','CY','CZ','DK','FO','GL','DO','EE','FI','AX','FR','GF','PF','TF','GP','MQ','YT','NC','RE','BL','MF','PM','WF','GE','DE','GI','GR','GT','HU','IS','AA','IR','IE','IT','JO','KZ','XK','KW','LV','LB','LI','LT','LU','MK','MG','ML','MT','MR','MU','MD','MC','ME','MZ','NL','NO','PK','PS','PL','PT','QA','RO','LC','SM','ST','SA','SN','RS','SC','SK','SI','ES','SE','CH','TL','TN','TR','UA','AE','GB'].to_json
  end


     
   
     def save
   

      begin
        @UsersBank = UsersBank.newuserbank(params,current_userd.id,'USD')
        flash[:notice] = t('you have added new bank for') + @UsersBank.bank_name.to_s
        redirect_to(users_banks_path)
        rescue =>e
        @code = SecureRandom.hex

        @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
        redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
      end 
 
   end
  # GET /users_banks/1/edit
  def edit
  end

  # POST /users_banks
  # POST /users_banks.json
  def create
    @users_bank = UsersBank.new(users_bank_params)

    respond_to do |format|
      if @users_bank.save
                Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'users banks', :action_page =>" add user bank",:foreign_id=>@users_bank.id ,:details=> " add user bank  with id :  "+ @users_bank.id.to_s )
		format.html { redirect_to @users_bank, notice: 'UsersBank was successfully created.' }
        format.json { render :show, status: :created, location: @users_bank }
      else
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'users banks', :action_page =>" add user bank",:foreign_id=>@users_bank.id ,:details=> "fails to  add user bank  with id :  "+ @users_bank.id.to_s )
		format.html { render :new }
        format.json { render json: @users_bank.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users_banks/1
  # PATCH/PUT /users_banks/1.json
  def update
    respond_to do |format|
      if @users_bank.update(users_bank_params)
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'users banks', :action_page =>" update user bank",:foreign_id=>@users_bank.id ,:details=> " update user bank  with id :  "+ @users_bank.id.to_s )
		format.html { redirect_to @users_bank, notice: 'UsersBank was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_bank }
      else
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'users banks', :action_page =>" update user bank",:foreign_id=>@users_bank.id ,:details=> "fails to  update user bank  with id :  "+ @users_bank.id.to_s )
		format.html { render :edit }
        format.json { render json: @users_bank.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users_banks/1
  # DELETE /users_banks/1.json
  def destroy
    
    
    @notcompleted_balancewithdraw=BalancesWithdrawsStatus.where("user_bank_id = ? and status != ?" ,@users_bank.id, 5)
    @count_notcompleted = @notcompleted_balancewithdraw.count
    if (@count_notcompleted>0)

      #flash[:success] = 'Cant Delete'
      redirect_to(request.env['HTTP_REFERER'],notice: t('you can not delete this bank account'))
      
    else 
      @users_bank.destroy
      Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'users banks', :action_page =>" destroy user bank",:foreign_id=>@users_bank.id ,:details=> "you have deleted this bank account/" + ' '  + @users_bank.bank_name.to_s )
       Notification.create(:user_id => current_userd.id,:foreign_id => @users_bank.id,:controller => 'users_bank',:stats => "you have deleted this bank account/" + ' ' + @users_bank.bank_name.to_s)
       respond_to do |format|
      format.html { redirect_to users_banks_url, notice: t('you have deleted this bank account') }
      format.json { head :no_content }
    end
    end      
   
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_bank
      @users_bank = UsersBank.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_bank_params
      params.require(:users_bank).permit(:userd_id,:bank_id,:relation,:nickname,:birth_date,:personal_name,:business_name,:nationalid,:country,:currency,:bank_name,:currencies_withdraw_ratios_id,:branch,:account_name,:account_email,:account_number,:visa_cvv,:swiftcode,:expire_date,:created_at,:status)
    end
end
