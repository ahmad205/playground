class UsersWalletsBalancesFrozensController < ApplicationController
  before_action :set_users_wallets_balances_frozen, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_userd! , except: [:mailstatus, :namestatus ]


  def namestatus

    @usersname = Userd.where(:username => params[:username]).first  
      if (@usersname)
        @msg='True'
      else
        @msg='False'
      end
    render :json => { 'bool': @msg }
      
  end

  def mailstatus

    @usersmail2 = Userd.where(:email => params[:email]).first

    if (@usersmail2)
      @message='True'
    else
      @message='False'
    end
    render :json => { 'bool': @message }

  end


  def walletfrozen

    begin
    @usersnamell = Userd.where("account_number = ? or email = ? or username = ?", params[:id],params[:id],params[:id]).first

    if (@usersnamell)
       @usertogroup = UsersGroup.find_by_id(@usersnamell.group_id)
       @user_verification_to = UserVerification.where("user_id = ?",@usersnamell.id).first
       render :json => { 'usersnamell': @usersnamell, 'usertogroup': @usertogroup ,'user_verification_to': @user_verification_to }
    else               
       render :json => { 'usersnamell': @usersnamell }
    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def show

    begin
    add_breadcrumb t('home'), :root_path

    if @users_wallets_balances_frozen.user_id == current_userd.id and @users_wallets_balances_frozen.approve == 0 and @users_wallets_balances_frozen.deleted == 0
       add_breadcrumb t('Confirm Balance Transfer'), users_wallets_balances_frozen_path
    else
       add_breadcrumb t('Balance Transfer Data'), users_wallets_balances_frozen_path
    end

    unless (@users_wallets_balances_frozen.user_id == current_userd.id) || ( current_userd.id == @users_wallets_balances_frozen.user_id_to )
       redirect_to account_settings_path, notice: t('you can not view this data')
    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end


  def check_expenses

    #begin
    @usergroup_limit = UsersGroup.find_by_id(current_userd.group_id)
    @riyal_family_limit = @usergroup_limit.family_limit.to_f * @usdtors.value.to_f 
    @egp_family_limit = @usergroup_limit.family_limit.to_f * @usdtoegp.value.to_f
    @riyal_minimum_limit = @usergroup_limit.minimum_value.to_f * @usdtors.value.to_f
    @egp_minimum_limit = @usergroup_limit.minimum_value.to_f * @usdtoegp.value.to_f 
    @current_limit = Userd.find_by_id(current_userd.id) 
    @riyal_current_limit = @current_limit.current_family_limit.to_f * @usdtors.value.to_f
    @egp_current_limit = @current_limit.current_family_limit.to_f * @usdtoegp.value.to_f 
    @userto = params[:userto]
    @userid = current_userd.id
    @purpose_id = params[:purpose_id]
    @compared_val = params[:compared_val].to_f
    @currency = params[:currency]
    @message = UsersWalletsFrozensRatio.checkexpenses(@compared_val,@currency,@dollar_wallet,@egp_wallet,@sar_wallet,@purpose_id.to_i,@userid.to_i,@userto.to_i)
    @res= '@response.body'
    #@message = 'voila'

    render :json => { 'response': @message }

    # rescue =>e
    #   @code = SecureRandom.hex

    #   @log =  ErrorsLog.create(:userd_id => current_userd.id.to_i,:error_msg => e.message.to_s,:error_code => @code.to_s) 
    #   redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    # end
       
  end

 
  def userwallet_frozen_get

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t("Balance Transfer User"), new_users_wallets_balances_frozen_path 
   
    @usergroup_limit = UsersGroup.find_by_id(current_userd.group_id)
    @riyal_family_limit = @usergroup_limit.family_limit.to_f * @usdtors.value.to_f 
    @egp_family_limit = @usergroup_limit.family_limit.to_f * @usdtoegp.value.to_f
    @riyal_minimum_limit = @usergroup_limit.minimum_value.to_f * @usdtors.value.to_f
    @egp_minimum_limit = @usergroup_limit.minimum_value.to_f * @usdtoegp.value.to_f 
    @current_limit = Userd.find_by_id(current_userd.id) 
    @riyal_current_limit = @current_limit.current_family_limit.to_f * @usdtors.value.to_f
    @egp_current_limit = @current_limit.current_family_limit.to_f * @usdtoegp.value.to_f 
    @current_user_verification = UserVerification.where("user_id = ?",current_userd.id).first
    @remaining = current_userd.users_group.family_limit.to_f - current_userd.current_family_limit.to_f 
    @remaining_riyal = @remaining * @usdtors.value.to_f 
    @remaining_egp = @remaining * @usdtoegp.value.to_f 
    @users_wallets_balances_frozen = UsersWalletsBalancesFrozen.new
    @frozenbalance =  Setting.where(key:'frozen balance').first 
    @transfer_purpose = UsersWalletsFrozensPurpose.all
    
    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def savetransfer
    
    begin
        @users_wallets_balances_frozen = UsersWalletsBalancesFrozen.new
        @users_wallets_balances_frozen.user_id = params[:users_wallets_balances_frozen][:user_id]
        @users_wallets_balances_frozen.user_id_to = params[:users_wallets_balances_frozen][:user_id_to]
        @users_wallets_balances_frozen.currency = params[:users_wallets_balances_frozen][:currency]
        @users_wallets_balances_frozen.balance = params[:users_wallets_balances_frozen][:balance]
        @users_wallets_balances_frozen.purpose_id = params[:users_wallets_balances_frozen][:purpose_id]
        @users_wallets_balances_frozen.note = params[:users_wallets_balances_frozen][:note]
        @usdtoegp =  @usdtoegp.value
        @usdtors = @usdtors.value
        @expenses = UsersWalletsFrozensRatio.getexpense(@users_wallets_balances_frozen.purpose_id.to_i,@users_wallets_balances_frozen.balance.to_f,@users_wallets_balances_frozen.user_id_to,@users_wallets_balances_frozen.currency,@usdtors,@usdtoegp)
        
        @check_limit = UsersWalletsFrozensRatio.checkexpenses(@users_wallets_balances_frozen.balance,@users_wallets_balances_frozen.currency,@dollar_wallet,@egp_wallet,@sar_wallet,@users_wallets_balances_frozen.purpose_id.to_i,@users_wallets_balances_frozen.user_id,@users_wallets_balances_frozen.user_id_to.to_i)
        if @check_limit == ''
        
        loop do
        @trans_id = "BT"+[*('A'..'Z'),*('0'..'9')].to_a.shuffle[0,16].join
        break @trans_id unless Transaction.where(transaction_id: @trans_id).exists?
        end 
    
        @frozenbalance =  Setting.where(key:'frozen balance').first 
        @Suspension_period = Setting.where(key:'Suspension_period').first 
        @sender = Userd.find_by_id(@users_wallets_balances_frozen.user_id.to_i)
        @userto = Userd.find_by_id(@users_wallets_balances_frozen.user_id_to.to_i)
        @current_user_verification = UserVerification.where("user_id = ?",@users_wallets_balances_frozen.user_id.to_i).first
        @userto_verification = UserVerification.where("user_id = ?",@users_wallets_balances_frozen.user_id_to.to_i).first
        @userbalance = UsersWalletsBalance.where(["user_id = ? and currency = ?",current_userd.id , @users_wallets_balances_frozen.currency]).lock('LOCK IN SHARE MODE').first
        @usertobalance = UsersWalletsBalance.where(["user_id = ? and currency = ?",@users_wallets_balances_frozen.user_id_to , @users_wallets_balances_frozen.currency]).lock('LOCK IN SHARE MODE').first
        
        if (!@usertobalance) or @usertobalance.is_deleted == 1     
        @balance_to_before = 0        
        else
        @balance_to_before = @usertobalance.balance
        end
        @balance_from_before = @userbalance.balance
        @usergroup = UsersGroup.where("id = ?",@userto.group_id).first
        @users_wallets_balances_frozen.netbalance = @users_wallets_balances_frozen.balance.to_f - @expenses.to_f
        @userbalance.balance = @userbalance.balance.to_f - @users_wallets_balances_frozen.balance.to_f

        if  @Suspension_period.value.to_i == 1    
        @users_wallets_balances_frozen.Suspension_period = params[:users_wallets_balances_frozen][:Suspension_period]
        end
        
        if  @Suspension_period.value.to_i == 2
        @users_wallets_balances_frozen.Suspension_period = @frozenbalance.value
        end
                    
        #if the transfer purpose is family
        if (@users_wallets_balances_frozen.purpose_id == 3 )
            @current_limit = Userd.find_by_id(current_userd.id)
            @current_userto_limit = Userd.find_by_id(@users_wallets_balances_frozen.user_id_to.to_i)

            if @users_wallets_balances_frozen.currency == "USD"
            @new_limit = @current_limit.current_family_limit.to_f + @users_wallets_balances_frozen.balance.to_f
            @new_userto_limit = @current_userto_limit.current_max_family_recieve.to_f + @users_wallets_balances_frozen.balance.to_f


            elsif @users_wallets_balances_frozen.currency == "SAR"
                @new_sar_limit = @users_wallets_balances_frozen.balance.to_f * @rstousd.value.to_f
                @new_limit = @current_limit.current_family_limit.to_f + @new_sar_limit.to_f
                @new_userto_limit = @current_userto_limit.current_max_family_recieve.to_f + @new_sar_limit.to_f

            elsif @users_wallets_balances_frozen.currency == "EGP"
                @new_egp_limit = @users_wallets_balances_frozen.balance.to_f * @egptousd.value.to_f  
                @new_limit = @current_limit.current_family_limit.to_f + @new_egp_limit.to_f
                @new_userto_limit = @current_userto_limit.current_max_family_recieve.to_f + @new_egp_limit.to_f

            end
                    
            @current_limit.update(:current_family_limit => @new_limit)
            @current_userto_limit.update(:current_max_family_recieve => @new_userto_limit)

        end


        ActiveRecord::Base.transaction do
        if (!@usertobalance)
            @usertobalance = UsersWalletsBalance.create(:user_id => @users_wallets_balances_frozen.user_id_to , :currency => @users_wallets_balances_frozen.currency , :wallet_name => @userbalance.wallet_name , :balance => 0)
            Notification.create(:user_id => @users_wallets_balances_frozen.user_id_to,:added_by => current_userd.id,:foreign_id => @usertobalance.id, :controller => 'User_wallet_ballence',:stats => 'you have created/'+ @usertobalance.wallet_name)
            Watchdog.create(:user_id => @users_wallets_balances_frozen.user_id_to,:added_by => current_userd.id,:controller => 'User_wallet_ballence', :action_page =>"create new wallet",:foreign_id=> @usertobalance.id,:details => 'you have created/' + @usertobalance.wallet_name)
            Wallet.newwallet(@userto, @usertobalance.wallet_name).deliver
            elsif @usertobalance.is_deleted == 1      
            @usertobalance.update(:is_deleted => 0 ) 
            Notification.create(:user_id => @users_wallets_balances_frozen.user_id_to,:added_by => current_userd.id,:foreign_id => @usertobalance.id, :controller => 'User_wallet_ballence',:stats => 'you have created/'+ @usertobalance.wallet_name)
            Watchdog.create(:user_id => @users_wallets_balances_frozen.user_id_to,:added_by => current_userd.id,:controller => 'User_wallet_ballence', :action_page =>"create new wallet",:foreign_id=> @usertobalance.id,:details => 'you have created/' + @usertobalance.wallet_name)
            Wallet.newwallet(@userto, @usertobalance.wallet_name).deliver   
            end

            if @Suspension_period.value.to_i == 2 and @frozenbalance.value.to_i==0
                @users_wallets_balances_frozen.approve =1
                @usertobalance.balance +=@users_wallets_balances_frozen.netbalance.to_f
                                
            elsif @users_wallets_balances_frozen.purpose_id == 3
                @users_wallets_balances_frozen.approve =1
                @usertobalance.balance +=@users_wallets_balances_frozen.netbalance.to_f
            end

            @usertobalance.save       
            @users_wallets_balances_frozen.save and @userbalance.save

            @company_ratio =  @users_wallets_balances_frozen.balance.to_f - @users_wallets_balances_frozen.netbalance.to_f   
            @company_bal = CompanyBalance.create(:user_id => @users_wallets_balances_frozen.user_id_to,:foreign_id => @users_wallets_balances_frozen.id,:operation_id => '3',:balance => @company_ratio,:currency =>  @users_wallets_balances_frozen.currency ,:status => '1')
            end                            
            
            @usertobalance = UsersWalletsBalance.where(["user_id = ? and currency = ?",@users_wallets_balances_frozen.user_id_to , @users_wallets_balances_frozen.currency]).first
                
            if @users_wallets_balances_frozen.purpose_id == 3
                @tran_id = Transaction.create(:transaction_id => @trans_id ,:foreign_id => @users_wallets_balances_frozen.id,:user_id => current_userd.id,:user_id_to => @users_wallets_balances_frozen.user_id_to,:value_from => @users_wallets_balances_frozen.balance,:w_from_name => @users_wallets_balances_frozen.currency,
                    :w_from_balance_before => @balance_from_before,:w_from_balance_after => @userbalance.balance ,:w_to_name => @users_wallets_balances_frozen.currency,:value_to => @users_wallets_balances_frozen.netbalance,
                    :w_to_balance_before => @balance_to_before,:w_to_balance_after => @usertobalance.balance, :controller => 'users_wallets_ballences_frozens/approvedbalance',:description => 'Balance Delivered/' +(@users_wallets_balances_frozen.balance.to_s) + ' '+@users_wallets_balances_frozen.currency +  '/From User/'  + @sender.username +  '/to the user/' + @userto.username + '/Transaction ID/' + @trans_id.to_s)

                Notification.create(:user_id => current_userd.id,:trans_id => @tran_id.id,:added_by => current_userd.id, :addeduser_id => @users_wallets_balances_frozen.user_id_to, :foreign_id => @users_wallets_balances_frozen.id, :controller => 'User_wallet_ballence_frozen',:stats => 'Balance Delivered/' +(@users_wallets_balances_frozen.balance.to_s) + ' '+@users_wallets_balances_frozen.currency +  '/From User/'  + @sender.username +  '/to the user/' + @userto.username + '/Transaction ID/' + @trans_id.to_s)
                @company_ratio =  @users_wallets_balances_frozen.balance.to_f - @users_wallets_balances_frozen.netbalance.to_f
                Watchdog.create(:user_id => current_userd.id,:user_to => @users_wallets_balances_frozen.user_id_to,:added_by => current_userd.id,:controller => 'User_wallet_ballence_frozen', :action_page =>"create balance operation",:foreign_id=> @users_wallets_balances_frozen.id,:details => 'Balance Delivered/' +(@users_wallets_balances_frozen.balance.to_s) + ' '+@users_wallets_balances_frozen.currency +  '/From User/'  + @sender.username +  '/to the user/' + @userto.username + '/Transaction ID/' + @trans_id.to_s)
                
                Usertouser.confirmtransfer(current_userd,@userto, @users_wallets_balances_frozen,@trans_id,"approve").deliver
                @sendername = Userd.where("id = ?",@users_wallets_balances_frozen.user_id).first
                Usertouser.confirmtransfertransferto(@sendername,@userto, @users_wallets_balances_frozen,@trans_id,"approve").deliver
                redirect_to account_settings_path, notice: t('Balance Delivered') + " " + " #{@users_wallets_balances_frozen.balance}  #{@users_wallets_balances_frozen.currency}" +t('to the user') +" "+ "#{@userto.username}"+" "+ t('Transaction ID') + ":" +  "#{@trans_id} "                                                                                                                                                                                                                                         
            else
                @tran_id = Transaction.create(:transaction_id => @trans_id ,:foreign_id => @users_wallets_balances_frozen.id,:user_id => current_userd.id,:user_id_to => @users_wallets_balances_frozen.user_id_to,:value_from => @users_wallets_balances_frozen.balance,:w_from_name => @users_wallets_balances_frozen.currency,
                    :w_from_balance_before => @balance_from_before,:w_from_balance_after => @userbalance.balance ,:w_to_name => @users_wallets_balances_frozen.currency,:value_to => @users_wallets_balances_frozen.netbalance,
                    :w_to_balance_before => @balance_to_before,:w_to_balance_after => @usertobalance.balance, :controller => 'users_wallets_ballences_frozens/create',:description => 'balance was transferred with value of/' +(@users_wallets_balances_frozen.balance.to_s) + ' '+@users_wallets_balances_frozen.currency +  '/From User/'  + @sender.username +  '/to the user/' + @userto.username + '/Transaction ID/' + @trans_id.to_s)

                Notification.create(:user_id => current_userd.id,:trans_id => @tran_id.id,:added_by => current_userd.id, :addeduser_id => @users_wallets_balances_frozen.user_id_to, :foreign_id => @users_wallets_balances_frozen.id, :controller => 'User_wallet_ballence_frozen',:stats => 'balance was transferred with value of/' +(@users_wallets_balances_frozen.balance.to_s) + ' '+@users_wallets_balances_frozen.currency +  '/From User/'  + @sender.username +  '/to the user/' + @userto.username )
                @company_ratio =  @users_wallets_balances_frozen.balance.to_f - @users_wallets_balances_frozen.netbalance.to_f
                
                Watchdog.create(:user_id => current_userd.id,:user_to => @users_wallets_balances_frozen.user_id_to,:added_by => current_userd.id,:controller => 'User_wallet_ballence_frozen', :action_page =>"create balance operation",:foreign_id=> @users_wallets_balances_frozen.id,:details => 'balance was transferred with value of/' +(@users_wallets_balances_frozen.balance.to_s) + ' '+@users_wallets_balances_frozen.currency +  '/From User/'  + @sender.username +  '/to the user/' + @userto.username + '/Transaction ID/' + @trans_id.to_s)        
                Usertouser.transfer(current_userd,@userto, @users_wallets_balances_frozen,@trans_id).deliver
                @sendername = Userd.where("id = ?",@users_wallets_balances_frozen.user_id).first
                Usertouser.transferto(@sendername,@userto, @users_wallets_balances_frozen,@trans_id).deliver
                redirect_to account_settings_path ,notice: t('balance was transferred with value of') + " #{@users_wallets_balances_frozen.balance.to_s}"+ " #{@users_wallets_balances_frozen.currency} " +  t('to the user') +" #{@userto.username} " + t('Transaction ID') + " #{@trans_id.to_s} " 
            end                    
            @company_bal.update(:trans_id => @tran_id.id)

            elsif @check_limit != ''
                flash[:notice] = @check_limit
                redirect_to userwallet_frozen_path 
            end

        rescue =>e
            @code = SecureRandom.hex

            @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
            redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
          end
                  
    end

  def approvedbalance
     
    begin
    loop do
      @trans_id = "BT"+[*('A'..'Z'),*('0'..'9')].to_a.shuffle[0,16].join
      break @trans_id unless Transaction.where(transaction_id: @trans_id).exists?
    end 
    
    @balanceopd1 = UsersWalletsBalancesFrozen.where("id = ?",params[:id]).lock('LOCK IN SHARE MODE').first       
    if @balanceopd1.approve == 0
        @user_balance_before_confirm = UsersWalletsBalance.where("user_id = ? and currency = ? ",@balanceopd1.user_id, @balanceopd1.currency).lock('LOCK IN SHARE MODE').first
        @usertobalance1 = UsersWalletsBalance.where(["user_id = ? and currency = ?" , @balanceopd1.user_id_to , @balanceopd1.currency]).lock('LOCK IN SHARE MODE').first
        @sender = Userd.where("id = ?",@balanceopd1.user_id).lock('LOCK IN SHARE MODE').first
        @userto = Userd.where("id = ?",@balanceopd1.user_id_to).lock('LOCK IN SHARE MODE').first
        ActiveRecord::Base.transaction do

          if params[:id] and @balanceopd1.deleted == 0  and  current_userd.id == @balanceopd1.user_id  and  @balanceopd1.approve == 0

                @balancetobefore = @usertobalance1.balance
                @balanceopd1.approve = 1
                @usertobalance1.balance = @usertobalance1.balance.to_f + @balanceopd1.netbalance.to_f     
                @usertobalance1.is_deleted = 0
                @balanceopd1.save
                @usertobalance1.save
                @user_balance_after_confirm = UsersWalletsBalance.where("user_id = ? and currency = ? ",@balanceopd1.user_id, @balanceopd1.currency).first
                @tran_id = Transaction.create(:transaction_id => @trans_id ,:foreign_id => @balanceopd1.id,:user_id =>  @balanceopd1.user_id,:user_id_to =>  @balanceopd1.user_id_to,:value_from =>  @balanceopd1.balance,:w_from_name =>  @balanceopd1.currency,
                :w_from_balance_before => @user_balance_before_confirm.balance,:w_from_balance_after => @user_balance_after_confirm.balance ,:w_to_name =>  @balanceopd1.currency,:value_to =>  @balanceopd1.netbalance,
                :w_to_balance_before => @balancetobefore,:w_to_balance_after => @usertobalance1.balance, :controller => 'users_wallets_ballences_frozens/approvedbalance',:description => 'Balance Delivered/'+(@balanceopd1.balance.to_s) + ' '+@balanceopd1.currency +  '/From User/' + @sender.username +  '/to the user/' + @userto.username + '/Transaction ID/' + @trans_id.to_s)
                Notification.create(:user_id => @balanceopd1.user_id,:added_by => current_userd.id ,:addeduser_id => @balanceopd1.user_id_to,:trans_id => @tran_id.id, :foreign_id =>  @balanceopd1.id, :controller => 'User_wallet_ballence_frozen',:stats => 'Balance Delivered/'+(@balanceopd1.balance.to_s) + ' '+@balanceopd1.currency +  '/From User/' + @sender.username +  '/to the user/' + @userto.username)
                Watchdog.create(:user_id => @balanceopd1.user_id,:user_to => @balanceopd1.user_id_to,:added_by => current_userd.id,:controller => 'User_wallet_ballence_frozen', :action_page =>"approvedbalance",:foreign_id=> @balanceopd1.id,:details => 'Balance Delivered/'+(@balanceopd1.balance.to_s) + ' '+@balanceopd1.currency +  '/From User/' + @sender.username +  '/to the user/' + @userto.username)
                Usertouser.confirmtransfer(current_userd,@userto, @balanceopd1,@tran_id.transaction_id,"approve").deliver
                Usertouser.confirmtransfertransferto(@current_userd,@userto, @balanceopd1,@tran_id.transaction_id,"approve").deliver
                redirect_to (request.env['HTTP_REFERER']), notice: t('Balance Delivered') +" #{@balanceopd1.balance.to_s} " + " #{@balanceopd1.currency} " + t('From User')+ " #{@sender.username} " + t('to the user') +" #{@userto.username} " 
        else 
          redirect_to (request.env['HTTP_REFERER']), notice: ' Not Allowed'
        end
        
        end
    else 
      redirect_to (request.env['HTTP_REFERER']), notice: t("Balance transfer proccess is successfully completed")

    end
    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def refund_request

    begin
    loop do
      @trans_id = "BT"+[*('A'..'Z'),*('0'..'9')].to_a.shuffle[0,16].join
      break @trans_id unless Transaction.where(transaction_id: @trans_id).exists?
    end

      @balanceopd1 = UsersWalletsBalancesFrozen.where(["id = ? ", params[:id].to_i]).lock('LOCK IN SHARE MODE').first
      @user_from_wallet = UsersWalletsBalance.where(["user_id = ? and currency = ? ",@balanceopd1.user_id.to_i, @balanceopd1.currency]).lock('LOCK IN SHARE MODE').first
      @user_to_wallet = UsersWalletsBalance.where(["user_id = ? and currency = ?" , @balanceopd1.user_id_to.to_i , @balanceopd1.currency]).lock('LOCK IN SHARE MODE').first
      
    UsersWalletsBalance.transaction do
      @sender = Userd.find_by_id(@balanceopd1.user_id)
      @userto = Userd.where("id = ?",@balanceopd1.user_id_to).first
      @tranfer_date = @balanceopd1.created_at

      if @balanceopd1.purpose_id == 1 
      @refund_period = Time.now - 30.to_i.days 
      elsif @balanceopd1.purpose_id == 2 
      @refund_period = Time.now - 45.to_i.days 
      elsif @balanceopd1.purpose_id == 3 
      @refund_period = Time.now - 15.to_i.days 
      end 

      if params[:deleted]  and current_userd.id == @balanceopd1.user_id_to  and  @balanceopd1.deleted == 0
      
        if (@balanceopd1.approve == 1 and @user_to_wallet.balance < @balanceopd1.netbalance)
            redirect_to (request.env['HTTP_REFERER']), notice: t("رصيدك لا يكفى")and return
        end

        if @tranfer_date >= @refund_period
            @user_from_before_refund = @user_from_wallet.balance
            @user_to_before_refund = @user_to_wallet.balance
            @user_balance = @user_from_before_refund.to_f + @balanceopd1.balance.to_f
            @balanceopd1.update(:deleted => "2")

            if @user_from_wallet.is_deleted == 1
              Wallet.newwallet(@sender, @user_from_wallet.wallet_name).deliver
              Notification.create(:user_id => @balanceopd1.user_id,:added_by => current_userd.id,:foreign_id => @user_from_wallet.id, :controller => 'User_wallet_ballence',:stats => 'you have created/'+ @user_from_wallet.wallet_name)
              Watchdog.create(:user_id => @balanceopd1.user_id,:added_by => current_userd.id,:controller => 'User_wallet_ballence', :action_page =>"create new wallet",:foreign_id=> @user_from_wallet.id,:details => 'you have created/' + @user_from_wallet.wallet_name)
            end

            @user_from_wallet.update(:balance => @user_balance ,:is_deleted => "0" )
            if @balanceopd1.approve == 1
                if @user_to_wallet.is_deleted == 1
                  Wallet.newwallet(@userto, @user_to_wallet.wallet_name).deliver
                  Notification.create(:user_id => @balanceopd1.user_id,:added_by => current_userd.id,:foreign_id => @user_to_wallet.id, :controller => 'User_wallet_ballence',:stats => 'you have created/'+ @user_to_wallet.wallet_name)
                  Watchdog.create(:user_id => @balanceopd1.user_id,:added_by => current_userd.id,:controller => 'User_wallet_ballence', :action_page =>"create new wallet",:foreign_id=> @user_to_wallet.id,:details => 'you have created/' + @user_to_wallet.wallet_name)
                end
                @userto_balance = @user_to_before_refund.to_f - @balanceopd1.netbalance.to_f
                @user_to_wallet.update(:balance => @userto_balance ,:is_deleted => "0" )
            end

            # @sender_verifications = UserVerification.where('user_id = ?' , @balanceopd1.user_id).first
            # @reciver_verifications = UserVerification.where('user_id = ?' , @balanceopd1.user_id_to).first

            if (@balanceopd1.purpose_id == 3)	
              @return_sender_limit = @sender.current_family_limit.to_f - @balanceopd1.balance.to_f 
              @sender.update(:current_family_limit => @return_sender_limit)
              @return_userto_limit = @userto.current_max_family_recieve.to_f - @balanceopd1.balance.to_f 
              @userto.update(:current_max_family_recieve => @return_userto_limit)
            end
            
            @user_from_after_refund = UsersWalletsBalance.where("user_id = ? and currency = ? ",@balanceopd1.user_id, @balanceopd1.currency).first
            @user_to_after_refund = UsersWalletsBalance.where("user_id = ? and currency = ? ",@balanceopd1.user_id_to, @balanceopd1.currency).first
            Transaction.create(:transaction_id => @trans_id ,:foreign_id => @balanceopd1.id,:user_id =>  @balanceopd1.user_id,:user_id_to =>  @balanceopd1.user_id_to,:value_from =>  @balanceopd1.balance,:w_from_name =>  @balanceopd1.currency,
            :w_from_balance_before => @user_from_before_refund,:w_from_balance_after => @user_from_after_refund.balance ,:w_to_name =>  @balanceopd1.currency,:value_to =>  @balanceopd1.netbalance,
            :w_to_balance_before => @user_to_before_refund,:w_to_balance_after => @user_to_after_refund.balance, :controller => 'users_wallets_ballences_frozens/deleteOperation',:description => "balance refunded/" +(@balanceopd1.balance.to_s) + ' '+@balanceopd1.currency +  "/from user/" + @sender.username +  "/to user/" + @userto.username + "/Transactio ID/" + @trans_id.to_s)
            @tran_id = Transaction.where("transaction_id = ?", @trans_id).first
            @company_ratio =  @balanceopd1.balance.to_f - @balanceopd1.netbalance.to_f
            Notification.create(:user_id => @balanceopd1.user_id, :addeduser_id => @balanceopd1.user_id_to,:added_by => current_userd.id,:trans_id => @tran_id.id, :foreign_id =>  @balanceopd1.id, :controller => 'User_wallet_ballence_frozen',:stats => "balance refunded/" +(@balanceopd1.balance.to_s) + ' '+@balanceopd1.currency +  "/from user/" + @sender.username +  "/to user/" + @userto.username)
            Watchdog.create(:user_id => @balanceopd1.user_id,:user_to => @balanceopd1.user_id_to,:added_by => current_userd.id,:controller => 'User_wallet_ballence_frozen', :action_page =>"deleteOperation",:foreign_id=> @balanceopd1.id,:details => "balance refunded/" +(@balanceopd1.balance.to_s) + ' '+@balanceopd1.currency +  "/from user/" + @sender.username +  "/to user/" + @userto.username)
            CompanyBalance.create(:user_id => @balanceopd1.user_id_to, :foreign_id =>  @balanceopd1.id,:operation_id => '3',:trans_id => @tran_id.id,:balance => @company_ratio,:currency =>  @balanceopd1.currency ,:status => '0')
            Usertouser.cancel_transfer_mail(@sender,@userto, @balanceopd1,@tran_id.transaction_id,"refund").deliver
            Usertouser.cancel_transfer_to_mail(@sender,@userto,@balanceopd1,@tran_id.transaction_id,"refund").deliver
        end
            
      end

      redirect_to (request.env['HTTP_REFERER']), notice: t("The amount has been deducted from your balance")
    
    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

 
  def download

    begin
    @posts = UsersWalletsBalancesFrozen.order(:id)

    respond_to do |format|
      format.html # don't forget if you pass html
      format.csv {send_data@posts.to_csv}
      format.xls {send_data@posts.to_csv(col_sep: "\t")}
    end
    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_users_wallets_balances_frozen
    @users_wallets_balances_frozen = UsersWalletsBalancesFrozen.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def users_wallets_balances_frozen_params
    params.require(:users_wallets_balances_frozen).permit(:user_id,:gauth_token, :user_id_to,:purpose_id, :currency, :wallet_name, :balance,:netbalance, :ratio_company, :status)
  end

  
end