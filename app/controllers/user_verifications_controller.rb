class UserVerificationsController < ApplicationController
  before_action :set_user_verification, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_userd!, except: [:send2fasms ,:verifysms,:resendsms,:mailconfirm,:unlockconfirm,:sendvoicesms]

  skip_before_filter :authenticate_userd!, :only => [:telegram_webhook]
  skip_before_action :authenticate_userd!, :only => [:telegram_webhook]


  # GET /user_verifications/1/edit
  def edituserprofile

    begin
    @user_verifications = UserVerification.where('user_id = ?' , current_userd.id).first
    @user_verifications.update(:address_line1 => params[:user_verifications][:address_line1],:address_line2 => params[:user_verifications][:address_line2])
  
    rescue => e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    
    end

  end

  def mailconfirm

    begin
    @usersmail = Userd.where(:email => params[:email]).first

    if (@usersmail.confirmed_at != nil)
      @message='True'
    else
      @message='False'
    end
    render :json => { 'bool': @message }

    rescue => e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def unlockconfirm

    begin
    @userslock = Userd.where(:email => params[:email]).first

    if (@userslock.locked_at != nil)
      @message='True'
    else
      @message='False'
    end
    render :json => { 'message': @message }

    rescue => e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # activate national_id of user
  def nationalid_activate

    begin
    @user_verifications = UserVerification.where('user_id = ?' , current_userd.id).first

    rescue => e
      @code = SecureRandom.hex


      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id    end

  end
   
  #activate visa of user
  def visa_activate

    begin
    @user_verifications = UserVerification.where('user_id = ?' , current_userd.id).first

    rescue => e
      @code = SecureRandom.hex


      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id    end

  end


  def pre_verification

    begin
    @user_verifications = UserVerification.where('user_id = ?' , current_userd.id).first
    
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # activate user_account according to steps 
  def all_verifications

    add_breadcrumb t('home'), :root_path
    add_breadcrumb t("User Verification"), all_verifications_path 

    begin
    @user_verifications = UserVerification.where('user_id = ?' , current_userd.id).first
    @dollar_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'USD').first 
    @pound_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'EGP').first 
    @riyal_balance = UsersWalletsBalance.where("user_id = ? and currency = ?",current_userd.id, 'SAR').first 
    
    @users_banks = Userd.find(current_userd.id).users_bank 
    @usertickets = Userd.find(current_userd.id).ticket.where(:ticket_id =>0 ).paginate(page: params[:usertickets_page], per_page: 20).order('created_at DESC') 
    
    @balancenotapproved = UsersWalletsBalancesFrozen.where("user_id =? and approve =?" ,current_userd.id  ,"0").paginate(page: params[:wait_page], per_page: 3).order('created_at DESC') 
    @wallet_conversions = Transaction.where("user_id = ? and controller = ?",current_userd.id , "User_wallet_ballence").paginate(page: params[:num_page], per_page: 3).order('created_at DESC')
    @watch_dogss = Watchdog.where(["user_id =? and controller = ?  ",current_userd.id  , 'userds' ]).paginate(page: params[:watch_dogss], per_page: 3).order('created_at DESC') 
    @watch_dogs = Notification.where(["addeduser_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ?" ,current_userd.id ,'User_wallet_ballence_frozen', current_userd.id , 'Ticket', current_userd.id ,'User_wallet_ballence'  , current_userd.id ,'User_wallet_ballence_frozen', current_userd.id ,'onlinebank', current_userd.id ,'buycart', current_userd.id ,'chargecart', current_userd.id ,'balances_withdraws']).order('created_at DESC').limit(10) 
    
    @usertickets1 = Ticket.where("userd_id = ?",current_userd.id).count
    @usertickets2 = Ticket.where("userd_id = ? and closed =?",current_userd.id,"0").count
    
    @userbanks2 = UsersBank.where("userd_id = ? ",current_userd.id).count

    # render :layout => false

    rescue => e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id

    end
    
  end

  # activate address of user
  def address_activate

    begin
    @user_verifications = UserVerification.where('user_id = ?' , current_userd.id).first

    rescue => e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id

    end

  end

  # activate selfie of user
  def selfie_activate

    begin
    @user_verifications = UserVerification.where('user_id = ?' , current_userd.id).first

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # get inactivated accounts
  def admin_panel

    begin 
    @user_verifications = UserVerification.where("all_verified = ? ", 0)

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end  
    
  end


  def telegram_webhook
  
    begin
    # Coinpayment.test3(params.to_s).deliver

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end


  def changetel 
    
    begin
    @pin_id = current_userd.pin_id
    @pcode=params[:userd][:verification_code].to_s
    uri = URI.parse("https://api.infobip.com/2fa/1/pin/#{@pin_id}/verify")

    request = Net::HTTP::Post.new(uri)
    request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
    request["content-type"] = 'application/json'
    request["accept"] = 'application/json'
    request.body = JSON.dump({
                              "pin": @pcode
                            })

    req_options = {
                    use_ssl: uri.scheme == "https",
                  }

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      @response = http.request(request)
      @res= @response.body
      @applications_list= ActiveSupport::JSON.decode(@res)
  
    end

    if @applications_list["verified"] ==  true
      flash[:notice] = t("Code service has been activated via SMS")
      current_userd.update(:tel => params[:userd][:tel],:sms_active => 1,:active_otp_secret => 0)

    else 
      flash[:alert] = t("Error Code")
    end

    redirect_to(user_profile_path)

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def veriffysms 

    begin
    @code = params[:code]

    @pin_id = params[:pinId]
    
  
    uri = URI.parse("https://api.infobip.com/2fa/1/pin/#{@pin_id}/verify")
    request = Net::HTTP::Post.new(uri)
    request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
    request["content-type"] = 'application/json'
    request["accept"] = 'application/json'
    request.body = JSON.dump({
                              "pin": @code 
                            })
     
    req_options = {
                    use_ssl: uri.scheme == "https",
                  }
     
    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      @response = http.request(request)
      @res= @response.body
      @applications_list= ActiveSupport::JSON.decode(@res)

      if (@applications_list['verified'] == true)
        session[:balancewithdraw] = 0
      end

      render :json => { 'response': @applications_list ,'message': @message }
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

     
  def verifysms 

    begin
    @code = params[:code]

    if current_userd != nil 
      @pin_id=current_userd.pin_id
    else 
      @pin_id = params[:pinId]
    end
  
    uri = URI.parse("https://api.infobip.com/2fa/1/pin/#{@pin_id}/verify")
    request = Net::HTTP::Post.new(uri)
    request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
    request["content-type"] = 'application/json'
    request["accept"] = 'application/json'
    request.body = JSON.dump({
                              "pin": @code 
                            })
     
    req_options = {
                    use_ssl: uri.scheme == "https",
                  }
     
    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      @response = http.request(request)
      @res= @response.body
      @applications_list= ActiveSupport::JSON.decode(@res)

      if (@applications_list['verified'] == true)
        session[:balancewithdraw] = 0
      end

      render :json => { 'response': @applications_list ,'message': @message }
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end



  def verify_image
    
    

    @verification = UserVerification.where(:user_id => params[:id].to_i).first

    if  current_userd &&  @verification != nil   &&  @verification.user_id == current_userd.id

      if params[:type] == 'selfie'
      @data_url = @verification.selfie.url
      end

      if params[:type] == 'address'
        @data_url = @verification.address_document.url
      end


        if params[:type] == 'front'
          @data_url = @verification.nationalid_front.url
        end


          if params[:type] == 'back'
            @data_url = @verification.nationalid_back.url
          end

      

    @png  = Base64.decode64(@data_url['data:image/png;base64,'.length .. -1])
    render layout: false

      # send_data @png
    else 


     redirect_to(root_path,:notice => t('A picture you are not allowed to access'))
     end

 end


  def update

     begin
   
    #  @user_verification.nationalid_country = params[:user_verification][:nationalid_country]

  
  if  params[:user_verification][:nationalid_no].present? &&  @user_verification.nationalid_verified == 0

      @user_verification.nationalid_verified = 2

    ### nationalid_front

    if @user_verification.nationalid_front != nil  && @user_verification.nationalid_front != 0 
        @att =  Attachment.where(:id => @user_verification.nationalid_front).first
        @att.update(:content => params[:user_verification][:nationalid_front],:user_id => current_userd.id,:file_ext => params[:user_verification][:nationalid_front].content_type,:file_name => params[:user_verification][:nationalid_front].original_filename)
    else  
    
        @att =  Attachment.create(:uuid => SecureRandom.uuid , :content => params[:user_verification][:nationalid_front] ,:user_id => current_userd.id ,:file_ext => params[:user_verification][:nationalid_front].content_type,:file_name => params[:user_verification][:nationalid_front].original_filename)
        @user_verification.nationalid_front = @att.id
    end
    ### end nationalid_front

    ### nationalid_back

     if @user_verification.nationalid_back != nil  && @user_verification.nationalid_back != 0 
        @att =  Attachment.where(:id => @user_verification.nationalid_back).first
        @att.update(:content => params[:user_verification][:nationalid_back],:user_id => current_userd.id,:file_ext => params[:user_verification][:nationalid_back].content_type,:file_name => params[:user_verification][:nationalid_back].original_filename)
    else  
    
        @att =  Attachment.create(:uuid => SecureRandom.uuid , :content => params[:user_verification][:nationalid_back] ,:user_id => current_userd.id,:file_ext => params[:user_verification][:nationalid_back].content_type,:file_name => params[:user_verification][:nationalid_back].original_filename )
        @user_verification.nationalid_back = @att.id
    end
    ### end nationalid_back

   end



   if  params[:user_verification][:address_document_type].present? && @user_verification.address_document_verified == 0
    @user_verification.address_document_verified = 2


  if @user_verification.address_document != nil  && @user_verification.address_document != 0 
      @att =  Attachment.where(:id => @user_verification.address_document).first
      @att.update(:content => params[:user_verification][:address_document],:user_id => current_userd.id,:file_ext => params[:user_verification][:address_document].content_type,:file_name => params[:user_verification][:address_document].original_filename)
  else  
  
      @att =  Attachment.create(:uuid => SecureRandom.uuid , :user_id => current_userd.id,:content => params[:user_verification][:address_document],:file_ext => params[:user_verification][:address_document].content_type,:file_name => params[:user_verification][:address_document].original_filename )
      @user_verification.address_document = @att.id
  end

end


   if  params[:user_verification][:selfie].present? && @user_verification.selfie_verified == 0
    @user_verification.selfie_verified = 2


  if @user_verification.selfie != nil  && @user_verification.selfie != 0 
      @att =  Attachment.where(:id => @user_verification.selfie).first
      @att.update(:content => params[:user_verification][:selfie],:user_id => current_userd.id,:file_ext => params[:user_verification][:selfie].content_type,:file_name => params[:user_verification][:selfie].original_filename)
  else  
  
      @att =  Attachment.create(:uuid => SecureRandom.uuid , :user_id => current_userd.id,:content => params[:user_verification][:selfie] ,:file_ext => params[:user_verification][:selfie].content_type,:file_name => params[:user_verification][:selfie].original_filename)
      @user_verification.selfie = @att.id
  end

end

  #  @user_verification.update(user_verification_params)



    respond_to do |format|
      if @user_verification.update(user_verification_params)
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'user_verifications', :action_page =>"upload_verification_data",:details => "user uploaded his verification data")
        format.html {         redirect_to all_verifications_path , notice: t('User verification was successfully updated') }
        format.json { render :index, status: :ok, location: @user_verification }
      
      else
        format.html { render :edit }
        format.json { render json: @user_verification.errors, status: :unprocessable_entity }
      end
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end
  
  def send2fasms


    begin

      @applicationId = "451C42313BC234ABF92206EEA0796C1B"
      @messegeid = "9A9EDA35B0552EF4E942BF272ADA1955"
    if current_userd != nil 
      @phone= current_userd.tel
      @tel = current_userd.tel[0,4]

    else 
      @phone = params[:phone]
      @tel = @phone[0,4]
    end

    @ur = ''
    if @tel == '+966' ||  @tel == '+970' || @tel == "+963"
      @ur = URI.parse("https://api.infobip.com/2fa/1/pin?ncNeeded=false")
   
    else
      @ur = URI.parse("https://api.infobip.com/2fa/1/pin")
    end

    uri = @ur
    request = Net::HTTP::Post.new(uri)
    request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
    request["content-type"] = 'application/json'
    request["accept"] = 'application/json'
    request.body = JSON.dump({
                              "applicationId": @applicationId,
                              "messageId": @messegeid,
                              "from":"Payers",
                              "to": @phone
                            })

    req_options =  {
                      use_ssl: uri.scheme == "https",
                    }
     
    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      @response = http.request(request)
      @res= @response.body
      @applications_list= ActiveSupport::JSON.decode(@res)
      if current_userd != nil 
        current_userd.update(:pin_id => @applications_list["pinId"] )
      end

      @message = 'true'
        render :json => { 'response': @applications_list ,'message': @message }
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end


  def resendsms

    begin
    if current_userd != nil 
      @pin = current_userd.pin_id
    else 
      @pin = params[:pinId]
    end
      
    uri = URI.parse("https://api.infobip.com/2fa/1/pin/#{@pin}/resend")
    request = Net::HTTP::Post.new(uri)
    request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
    request["content-type"] = 'application/json'
    request["accept"] = 'application/json'
      
    req_options = {
                    use_ssl: uri.scheme == "https",
                  }
      
    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      @response = http.request(request)
      @res= @response.body
      @applications_list= ActiveSupport::JSON.decode(@res)
          
      @message = 'true'
        render :json => { 'response': @applications_list ,'message': @message }
    end
    
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def resendsmsa2

    begin
    
      @pin = params[:pinId]
    
      
    uri = URI.parse("https://api.infobip.com/2fa/1/pin/#{@pin}/resend")
    request = Net::HTTP::Post.new(uri)
    request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
    request["content-type"] = 'application/json'
    request["accept"] = 'application/json'
      
    req_options = {
                    use_ssl: uri.scheme == "https",
                  }
      
    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      @response = http.request(request)
      @res= @response.body
      @applications_list= ActiveSupport::JSON.decode(@res)
          
      @message = 'true'
        render :json => { 'response': @applications_list ,'message': @message }
    end
    
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end


  def sendsms_post

    

    ###################################################
    # begin
    # @phone = params[:sendsms][:phonereal].to_s
    # uri = URI.parse("https://api.infobip.com/2fa/1/pin/voice")
    # request = Net::HTTP::Post.new(uri)
    # request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
    # request["content-type"] = 'application/json'
    # request["accept"] = 'application/json'
    
    # request.body = JSON.dump({
    #                           "applicationId":"F7C7FBD29F434BD039D6C701402AA215",
    #                           "messageId":"E7176A93BC4DF0EC5AC8B3D530AFBAD0",
    #                           "from":"Payers",
    #                           "to": @phone,
    #                         })
    
    # req_options = {
    #                 use_ssl: uri.scheme == "https",
    #               }
    # response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
    #   @response = http.request(request)
    #   @res= @response.body
    # end

    # # flash[:notice] = t("A voice message has been sent to the number") + @phone + t("Successfully")
    
    # flash[:notice] = @response.body

    # rescue => e
    #   @code = SecureRandom.hex

    #   @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
    #   redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    # end 
    ###################################################

  end


  def sendvoicesms

    begin
      if current_userd != nil 
        @pin = current_userd.pin_id
      else 
        @pin = params[:pinId]
      end
        
      uri = URI.parse("https://api.infobip.com/2fa/1/pin/#{@pin}/resend/voice")
      request = Net::HTTP::Post.new(uri)
      request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
      request["content-type"] = 'application/json'
      request["accept"] = 'application/json'
      request.body = JSON.dump({
        "applicationId":"451C42313BC234ABF92206EEA0796C1B",
        "messageId":"7EDE108637D2D661ED7C6B0486BBB03F",
      })
        
      req_options = {
                      use_ssl: uri.scheme == "https",
                    }
        
      response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        @response = http.request(request)
        @res= @response.body
        @applications_list= ActiveSupport::JSON.decode(@res)
            
        @message = 'true'
          render :json => { 'response': @applications_list ,'message': @message }
      end


      
      rescue => e
        @code = SecureRandom.hex
  
        @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
        redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
      end
  


  end

  def sendvoicesms_post


  end
  def sendsms



    #  uri = URI.parse("https://api.infobip.com/2fa/2/applications/")
    #  request = Net::HTTP::Post.new(uri)
    #  request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
    #    request["content-type"] = 'application/json'
    #    request["accept"] = 'application/json'
     
    #  request.body = JSON.dump({
    #   "name":"Payers Voice",
    #   "configuration": {
    #       "pinAttempts": 10,
    #       "allowMultiplePinVerifications": false,
    #       "pinTimeToLive": "15m",
    #       "verifyPinLimit": "1/3s",
    #       "sendPinPerApplicationLimit": "10000/1d",
    #       "sendPinPerPhoneNumberLimit": "3/1d"
    #   },
    #   "enabled": true
    # })
     
    #  req_options = {
    #    use_ssl: uri.scheme == "https",
    #  }
     
    #  response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
  
    #    @response = http.request(request)
    #    @res= @response.body
    #  end





    #  uri = URI.parse("https://api.infobip.com/2fa/1/applications/F7C7FBD29F434BD039D6C701402AA215/messages")
    #  request = Net::HTTP::Post.new(uri)
    #  request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
    #    request["content-type"] = 'application/json'
    #    request["accept"] = 'application/json'
     
    #  request.body = JSON.dump({
    #   "pinType":"NUMERIC",
    #   "pinPlaceholder":"<pin>",
    #   "messageText":"Your pin is <pin>,,,,,Your pin is <pin>",
    #   "pinLength":4,
    #   "senderId":"Payers",
    #   "language": "en",
    #   "repeatDTMF": "1#",
    #   "speechRate": 1
    # })
     
    #  req_options = {
    #    use_ssl: uri.scheme == "https",
    #  }
     
    #  response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
  
    #    @response = http.request(request)
    #    @res= @response.body
    #  end



  #  uri = URI.parse("https://api.infobip.com/2fa/1/applications/E5993EB421C7AB1145DC9DE8FE948F6D/messages")
  # request = Net::HTTP::Get.new(uri)
  # request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
  #   request["content-type"] = 'application/json'
  #   request["accept"] = 'application/json'
  
  
  
  # req_options = {
  #   use_ssl: uri.scheme == "https",
  # }
  
  # response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
  
  #   @response = http.request(request)
  #   @res= @response.body
  # end
  

    # # render :layout => true


    # begin

    #   #  @a = Userd.where(:id => current_userd.idd).first
    # rescue => e
    #  @code = SecureRandom.hex
    #  @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
    #   redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    # end

  end

  
  def sms_activate_post
    
    begin 
    @code = params[:userd][:verification_code]
    if current_userd != nil 
      @pin_id=current_userd.pin_id
    else 
      @pin_id = params[:pinId]
    end
      
    uri = URI.parse("https://api.infobip.com/2fa/1/pin/#{@pin_id}/verify")
    request = Net::HTTP::Post.new(uri)
    request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
    request["content-type"] = 'application/json'
    request["accept"] = 'application/json'
    request.body = JSON.dump({
                              "pin": @code 
                            })

    req_options = {
                    use_ssl: uri.scheme == "https",
                  }
    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      @response = http.request(request)
      @res= @response.body
      @applications_list= ActiveSupport::JSON.decode(@res)

      if (@applications_list['verified'] == true)
    
        if current_userd.sms_active == 0 && current_userd.pin_id != nil
          current_userd.update(:sms_active => 1,:active_otp_secret => 0 ,:gauth_enabled => '0')
          flash[:notice] = t("Mobile code activate successfully")
        end

      else 
        flash[:notice] = t("Error Code")
      end
    end

    redirect_to(user_profile_path)

    rescue => e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end


  def sms_activate

    begin
    if current_userd.sms_active == 0 && current_userd.pin_id != nil
      current_userd.update(:sms_active => 1,:active_otp_secret => 0 ,:gauth_enabled => '0')
      flash[:notice] = t("Mobile code activate successfully")
    end
      redirect_to(request.env['HTTP_REFERER'])

    rescue => e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end
    

  def sms_deactivate
      
    begin
    if current_userd.sms_active == 1 
      current_userd.update(:sms_active => 0)
      flash[:notice] = t("Mobile code inactivate successfully")
    end
           
    redirect_to(request.env['HTTP_REFERER'])
      
    rescue => e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end


  def active_otp_secret
    
    begin
    if params[:id]
      @userid=params[:id].to_i
      @user = Userd.find_by_id(@userid)
      
      if  @user.active_otp_secret == 1 
        @user.update(active_otp_secret: 0 )
        flash[:alert] = t("Dual authentication has been disabled by email")
        
      else @user.active_otp_secret = 0
        @user.update(active_otp_secret: 1, sms_active: 0 ,:gauth_enabled => '0')
        flash[:notice] = t("Dual authentication has been activated by email")
        @user.save
      end
    end
    redirect_to(request.env['HTTP_REFERER'])

    rescue => e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
  
  end

  def email_activate_post

    @code = params[:userd][:verification_codee].to_i
    @response = ROTP::TOTP.new(current_userd.otp_secret_key)
    @v=   @response.verify_with_drift(@code.to_i, 1000, Time.now - 30) # => true

   if  @v == true

   current_userd.update(:sms_active => 0,:active_otp_secret => 1 ,:gauth_enabled => '0')
   flash[:notice] = t("Auth code via email has been activated")
   
   else

    flash[:notice] = t("Invalid code")


        end
        redirect_to(request.env['HTTP_REFERER'])




  end


  def previous_attachments

    begin
    @userid=params[:id].to_i
    @type=params[:type].to_s
    @user_verifications = UserVerification.find_by_id(@userid)
    @fronturl=@user_verifications.nationalid_front.url
    
    @used="system/user_verifications/"+@type+"/000/000/00"+@userid.to_s+"/original/"
    @images = Dir.glob("public/system/user_verifications/"+@type+"/000/000/00"+@userid.to_s+"/original/*")

    rescue => e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end 


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_verification
    
       
        @user_verification = UserVerification.where('user_id = ?' , current_userd.id).first
      
                        
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_verification_params
      params.require(:user_verification).permit(:user_id, :nationalid_type,:nationalid_back_verified, :nationalid_no, :nationalid_country, :nationalid_issued_by, :nationalid_start_date, :nationalid_expire_date, :nationalid_birth_date, :address_document_type, :address_line1, :address_line2, :address_document_city, :address_document_country, :address_document_postal_code, :credit_card_type, :credit_card_name, :credit_card_last4nos, :credit_card_expire_date, :nationalid_verified, :address_document_verified, :credit_card_verified, :selfie_verified)
    end

end