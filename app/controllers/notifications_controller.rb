class NotificationsController < ApplicationController
  before_action :set_notification, only: [:show, :edit, :update, :destroy]

  # GET /notifications
  def index

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Notifications Log'),notifications_path

    @notificationsetting =  Setting.where(key:'notification').first                                                                                                    
    @notificationss = Notification.where("notifications.user_id = ? or notifications.addeduser_id = ?" , current_userd.id , current_userd.id).all.order('created_at DESC')
    @aff = Notification.find_by_user_id( current_userd.id)
 
    if @aff != nil
      if  @aff.read != 1 
          @aff.read = 1 
          @aff.save
      end
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # update all notifications to be read
  def allread 

    begin
    @current_notis = Notification.where("user_id = ? " ,current_userd.id ).all
    
    @current_notis.each do |current_noti| 
      current_noti.update(:read => 1)
    end 
    redirect_to(request.env['HTTP_REFERER'])

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end


  def coinmoney
    
    begin
    rescue => e
      @code = SecureRandom.hex

@log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  #show notification  
  def show
    
    begin
    @notification.update(:read => 1)

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
    
  end

  # GET /notifications/new
  def new

    begin
    @notification = Notification.new

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
  end

  #create new notification
  def create

    begin
    @notification = Notification.new(notification_params)

    respond_to do |format|
      if @notification.save
        format.html { redirect_to @notification, notice: 'Notification was successfully created.' }
        format.json { render :show, status: :created, location: @notification }
      
      else
        format.html { render :new }
        format.json { render json: @notification.errors, status: :unprocessable_entity }
      
      end
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # PATCH/PUT /notifications/1
  def update

    begin
    respond_to do |format|
      if @notification.update(notification_params)
        format.html { redirect_to @notification, notice: 'Notification was successfully updated.' }
        format.json { render :show, status: :ok, location: @notification }
      
      else
        format.html { render :edit }
        format.json { render json: @notification.errors, status: :unprocessable_entity }

      end
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def search

    begin
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # get results of search
  def searchpost

    begin
    @last_hour = Time.now - 1.hours
    @last_day = Time.now - 1.days
    @last_week = Time.now - 7.days
    @last_month = Time.now - 30.days
    @last_year = Time.now - 365.days
    @notificationsetting =  Setting.where(key:'notification').first

    if !params[:notificationsearch]
      params[:notificationsearch]= params 
    end

    @search =  params[:search]
    @controllersearch =  params[:searchtime]

    if @controllersearch == "last hour"
      @searchdate = @last_hour

    elsif @controllersearch == "last day"
      @searchdate = @last_day

    elsif @controllersearch == "last week"
      @searchdate = @last_week

    elsif @controllersearch == "last month"
      @searchdate = @last_month

    elsif @controllersearch == "last year"
      @searchdate = @last_year
    end
 
    if params[:notificationsearch][:search] != "" and params[:notificationsearch][:searchtime] != ""
        @notifications2 = Notification.where("notifications.created_at >= :start_date AND notifications.user_id = :id AND (stats LIKE :search or notetype like :search)",
          {:start_date => @searchdate,:id => current_userd.id , :search => "%#{@search}%"}).all.order('created_at DESC')
    
    elsif params[:notificationsearch][:search] != "" and params[:notificationsearch][:searchtime] == ""
      @notifications2 = Notification.where("notifications.user_id = :id AND (stats LIKE :search or notetype like :search)",
          {:id => current_userd.id , :search => "%#{@search}%"}).all.order('created_at DESC')
    
    elsif params[:notificationsearch][:search] == "" and params[:notificationsearch][:searchtime] != ""
      @notifications2 = Notification.where("notifications.user_id = :id AND notifications.created_at >= :start_date ",
          {:id => current_userd.id , :start_date => @searchdate}).all.order('created_at DESC')
    
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end


  # DELETE /notifications/1
  def destroy

    begin
    @notification.destroy

    respond_to do |format|
      format.html { redirect_to notifications_url, notice: 'Notification was successfully destroyed.' }
      format.json { head :no_content }
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notification
      @notification = Notification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def notification_params
      params.require(:notification).permit(:user_id,  :addeduser_id,  :foreign_id,  :controller,   :stats  , :notetype )
    end
end
