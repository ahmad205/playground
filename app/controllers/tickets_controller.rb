class TicketsController < ApplicationController
  before_action :authenticate_userd!,:set_ticket, only: [:show, :edit, :update, :destroy ]
  before_filter :authenticate_userd!
  require "freshdesk"

  # GET /tickets
  # GET /tickets.json

  def freshdesk_post

     begin
    @title = params[:freshdesk][:title].to_s
    @description = params[:freshdesk][:description].to_s
    @mail = current_userd.email.to_s
    @phone = current_userd.tel.to_s
    @trans_id = params[:freshdesk][:trans_id]
    @transactions = Transaction.where("transaction_id = ?", @trans_id.to_s).first
    if @transactions == nil
      @id = @trans_id.split('#')
       @log = Watchdog.find(@id[0].to_s)
      @ip = @log.device_name.split("_")
      @trans_id = @ip[3].to_s
      @title = "#" + @id[0].to_s + " " + t("An unauthorized login process")
      @log.ticket_opend = 1
      @log.save
    end
    require 'uri'
    require 'net/http'

    url = URI("https://payers.freshdesk.com/api/v2/tickets")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(url)
    request["authorization"] = 'Basic YW1pckBwYXllcnMubmV0OmtSanVvOUFXNkxKejRxa0ZlOEZ1'
    request["content-type"] = 'application/json'
    request["cache-control"] = 'no-cache'
    request["postman-token"] = '529e9cdb-223f-fd64-bd69-4913d50a727b'
    request.body = "{\n        \"cc_emails\": [\"#{@mail}\"],
    \n        \"group_id\": null,
    \n        \"priority\": 2,
    \n        \"email\": \"#{@mail}\",
    \n        \"source\": 2,
    \n        \"status\": 2,
    \n        \"subject\": \"#{@title}\",
    \n        \"type\": \"القضايا والمنازعات\",
    \n        \"description\": \"<div dir=\\\"rtl\\\">\\n<div dir=\\\"ltr\\\">#{@description}</div>\\n</div>\",
    \n        \"custom_fields\": {\n            \"cf_phone\": \"#{@phone}\",
    \n            \"cf_reportq\": \"#{@trans_id}\"\n        }\n       \n    }\n    \n    "

    response = http.request(request)
    @ress = response.read_body
    @updatess = ActiveSupport::JSON.decode(@ress)
    puts  @updatess

    if @transactions != nil
    @transactions.ticket_opend = 1
    @transactions.save
    end
    redirect_to('https://dashboard.payers.net/',notice:t("new ticket has been added"))

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end
  
  
  def freshdesk

    begin
    require 'uri'
    require 'net/http'
    @email = current_userd.email

    url = URI("https://payers.freshdesk.com/api/v2/tickets?email=#{@email}")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Get.new(url)
    request["authorization"] = 'Basic YW1pckBwYXllcnMubmV0OmtSanVvOUFXNkxKejRxa0ZlOEZ1'
    request["content-type"] = 'application/json'
    request["cache-control"] = 'no-cache'
    request["postman-token"] = '5430a7c2-eaa2-f800-5cf6-5bccecc45447'

      @response = http.request(request)
      @res  = @response.body
      @updates= ActiveSupport::JSON.decode(@res)
      # session[:ticket_number] = @updates
    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def help_center

    utctime =  Time.now.getutc.to_i.to_s
    @user_id = current_userd.id.to_i

    digest  = OpenSSL::Digest::Digest.new('MD5')
    @gen_hash_from_params_hash = OpenSSL::HMAC.hexdigest(digest,'5b6c8f870785a7114044f46b7a384354',current_userd.username+'5b6c8f870785a7114044f46b7a384354'+current_userd.email+utctime) 
  
    redirect_url = 'https://support.payers.net'+"/login/sso?name="+current_userd.username+"&email="+current_userd.email+"&timestamp="+utctime+"&phone="+current_userd.tel+"&hash="+@gen_hash_from_params_hash.to_s
    redirect_to redirect_url

  end

  def ticket_image
    
    # if current_userd
    # send_file Ticket.where(:user_id => current_userd.id ,:id => 3).first.attachment.path
    # end

    # @user = Userd.where(:id => current_userd.id).first
     @ticket = Ticket.where(:id => params[:id].to_i).first
     if  current_userd &&    @ticket.userd_id == current_userd.id

      send_file Ticket.where(:id => params[:id].to_i,:userd_id => current_userd.id ).first.attachment.path

     else 

      redirect_to(tickets_url,:notice => t('A picture you are not allowed to access'))
      end

  end
 
  def adminticket
  	  
      #  @tickets = Ticket.all
    
      	 #    @ticketspeinding = Ticket.where(:status => 'pending' )
		  if  current_userd.role_id ==  1  
      	     @ticketspeinding = Ticket.where(:status => 'pending' ).paginate(:page => params[:tickets_page], :per_page => 10).order('created_at DESC')


             @ticketsonact = Ticket.where(:status => 'inprogress' ).paginate(:page => params[:ticketsact_page], :per_page => 10).order('created_at DESC')
             ##@ticketsolved = Ticket.where(:status => 'solved' || :closed => 1 )
             @ticketsolved = Ticket.where("status = ? OR closed = ?",'solved', 1).paginate(:page => params[:ticketssolv_page], :per_page => 2).order('created_at DESC')
		 else
			 respond_to do |format|
				format.html { redirect_to tickets_url , notice: 'Not Allowed You To See This Page.' }
				format.json { head :no_content }
			 end
		 end

  end
  
  def search
  end
  
  def searchpost

    @last_hour = Time.now - 1.hours
    @last_day = Time.now - 1.days
    @last_week = Time.now - 7.days
    @last_month = Time.now - 30.days
    @last_year = Time.now - 365.days
  
   if !params[:ticketsearch]
      params[:ticketsearch]= params 
  end
  
  @search =  params[:search]
	if current_userd.role_id ==  1 
		if params[:ticketsearch][:statustyp].include?('all') 
      if params[:ticketsearch][:searchtime].include?('last year')
        @tickets = Ticket.where("created_at >= :searchtime AND (title LIKE :search or body like :search)",{  :searchtime => @last_year ,:search => "%#{@search}%"}) 
		  elsif params[:ticketsearch][:searchtime].include?('last month')
        @tickets = Ticket.where("created_at >= :searchtime AND (title LIKE :search or body like :search)",{  :searchtime => @last_month ,:search => "%#{@search}%"})
      elsif params[:ticketsearch][:searchtime].include?('last week')
        @tickets = Ticket.where("created_at >= :searchtime AND (title LIKE :search or body like :search)",{  :searchtime => @last_week ,:search => "%#{@search}%"})
      elsif params[:ticketsearch][:searchtime].include?('last day')
        @tickets = Ticket.where("created_at >= :searchtime AND (title LIKE :search or body like :search)",{  :searchtime => @last_day ,:search => "%#{@search}%"})
      elsif params[:ticketsearch][:searchtime].include?('last hour')
        @tickets = Ticket.where("created_at >= :searchtime AND (title LIKE :search or body like :search)",{  :searchtime => @last_hour ,:search => "%#{@search}%"})
      # if params[:ticketsearch][:end] == ''
			# 	@tickets = Ticket.where("created_at >= :start_date AND (title LIKE :search or body like :search)",
			# 		{:start_date => params[:ticketsearch][:start], :search => "%#{@search}%"}).all

			# elsif params[:ticketsearch][:start] == ''
			# 	@tickets = Ticket.where("created_at <= :end_date AND (title LIKE :search or body like :search)",
			# 		{:end_date => params[:ticketsearch][:end], :search => "%#{@search}%"}).all
				
			# elsif params[:ticketsearch][:start] == '' && params[:ticketsearch][:end] == ''
			# 	@tickets = Ticket.where("title LIKE :search or body like :search)",{:search => "%#{@search}%"}).all
					
			# else
			# 	@tickets = Ticket.where("created_at >= :start_date AND created_at <= :end_date AND (title LIKE :search or body like :search)",
			# 		{:start_date => params[:ticketsearch][:start],:end_date => params[:ticketsearch][:end], :search => "%#{@search}%"}).all
			      end
		 else 
      if params[:ticketsearch][:searchtime].include?('last year')
        @tickets = Ticket.where("created_at >= :searchtime AND status in (:type) AND (title LIKE :search or body like :search)",{  :searchtime => @last_year ,:type => params[:ticketsearch][:statustyp],:search => "%#{@search}%"}) 
			elsif params[:ticketsearch][:searchtime].include?('last month')
        @tickets = Ticket.where("created_at >= :searchtime AND status in (:type) AND (title LIKE :search or body like :search)",{  :searchtime => @last_month ,:type => params[:ticketsearch][:statustyp],:search => "%#{@search}%"})
      elsif params[:ticketsearch][:searchtime].include?('last week')
        @tickets = Ticket.where("created_at >= :searchtime AND status in (:type) AND (title LIKE :search or body like :search)",{  :searchtime => @last_week ,:type => params[:ticketsearch][:statustyp],:search => "%#{@search}%"})
      elsif params[:ticketsearch][:searchtime].include?('last day')
        @tickets = Ticket.where("created_at >= :searchtime AND status in (:type) AND (title LIKE :search or body like :search)",{  :searchtime => @last_day ,:type => params[:ticketsearch][:statustyp],:search => "%#{@search}%"})
      elsif params[:ticketsearch][:searchtime].include?('last hour')
        @tickets = Ticket.where("created_at >= :searchtime AND status in (:type) AND (title LIKE :search or body like :search)",{  :searchtime => @last_hour ,:type => params[:ticketsearch][:statustyp],:search => "%#{@search}%"})
      # if params[:ticketsearch][:end] == ''
			# 	 @tickets = Ticket.where("created_at >= :start_date AND status in (:type) AND (title LIKE :search or body like :search)",
			# 		{:start_date => params[:ticketsearch][:start],:type => params[:ticketsearch][:statustyp], :search => "%#{@search}%"}).all
				
			# elsif params[:ticketsearch][:start] == ''
			# 	 @tickets = Ticket.where("created_at <= :end_date  AND status in (:type) AND (title LIKE :search or body like :search)",
			# 		{:end_date => params[:ticketsearch][:end],:type => params[:ticketsearch][:statustyp], :search => "%#{@search}%"}).all
					
			# elsif params[:ticketsearch][:end] == '' && params[:ticketsearch][:start] == ''
			# 	 @tickets = Ticket.where("status in (:type) AND (title LIKE :search or body like :search)",
			# 		{:start_date => params[:ticketsearch][:start],:end_date => params[:ticketsearch][:end],:type => params[:ticketsearch][:statustyp], :search => "%#{@search}%"}).all
					
			# else
			# 	 @tickets = Ticket.where("created_at >= :start_date AND created_at <= :end_date  AND status in (:type) AND (title LIKE :search or body like :search)",
			# 		{:start_date => params[:ticketsearch][:start],:end_date => params[:ticketsearch][:end],:type => params[:ticketsearch][:statustyp], :search => "%#{@search}%"}).all
					
		 end
	  end
	else
		if !params[:statustyp].include?('pending')  && !params[:statustyp].include?('inprogress') && !params[:statustyp].include?('hold') && !params[:statustyp].include?('solved')
			  

        if params[:ticketsearch][:searchtime].include?('last year')
        @tickets = Ticket.where("created_at >= :searchtime AND userd_id = :id AND (title LIKE :search or body like :search)",{  :searchtime => @last_year ,:id => current_userd.id ,:search => "%#{@search}%"}) 
      elsif params[:ticketsearch][:searchtime].include?('last month')
        @tickets = Ticket.where("created_at >= :searchtime AND userd_id = :id AND (title LIKE :search or body like :search)",{  :searchtime => @last_month ,:id => current_userd.id,:search => "%#{@search}%"})
      elsif params[:ticketsearch][:searchtime].include?('last week')
        @tickets = Ticket.where("created_at >= :searchtime AND userd_id = :id AND (title LIKE :search or body like :search)",{  :searchtime => @last_week ,:id => current_userd.id,:search => "%#{@search}%"})
      elsif params[:ticketsearch][:searchtime].include?('last day')
        @tickets = Ticket.where("created_at >= :searchtime AND userd_id = :id AND (title LIKE :search or body like :search)",{  :searchtime => @last_day ,:id => current_userd.id,:search => "%#{@search}%"})
      elsif params[:ticketsearch][:searchtime].include?('last hour')
        @tickets = Ticket.where("created_at >= :searchtime AND userd_id = :id AND (title LIKE :search or body like :search)",{  :searchtime => @last_hour ,:id => current_userd.id,:search => "%#{@search}%"})
    #     if params[:end] == ''
        	
    #       # @tickets = Ticket.where("created_at >= :start_date AND user_id = :id AND title in (:search)",
    #       #   {:start_date => params[:start],:id => current_userd.id ,:search => params[:ticketsearch][:search] }).all

    #       @tickets = Ticket.where("created_at >= :start_date AND userd_id = :id AND (title LIKE :search or body like :search)",
    #         {:start_date => params[:start],:id => current_userd.id , :search => "%#{@search}%"}).all
                  

				# elsif params[:start] == ''
				# 	@tickets = Ticket.where("created_at <= :end_date AND userd_id = :id AND (title LIKE :search or body like :search)",
				# 		{:end_date => params[:end],:id => current_userd.id ,:search => "%#{@search}%"}).all
					
				# elsif params[:start] == '' && params[:end] == ''
				# 	@tickets = Ticket.where("userd_id = :id AND (title LIKE :search or body like :search)",{:id => current_userd.id ,:search => "%"+@search.to_s+"%"}).all

				# else
				# 	@tickets = Ticket.where("created_at >= :start_date AND created_at <= :end_date AND userd_id = :id AND (title LIKE :search or body like :search)",
				# 		{:start_date => params[:start],:end_date => params[:end],:id => current_userd.id ,:search => "%#{@search}%"}).all
				end
			 else 

        if params[:ticketsearch][:searchtime].include?('last year')
        @tickets = Ticket.where("created_at >= :searchtime AND status in (:type) AND userd_id = :id AND (title LIKE :search or body like :search)",{  :searchtime => @last_year ,:type => params[:ticketsearch][:statustyp] ,:id => current_userd.id,:search => "%#{@search}%"}) 
      elsif params[:ticketsearch][:searchtime].include?('last month')
        @tickets = Ticket.where("created_at >= :searchtime AND status in (:type) AND userd_id = :id AND (title LIKE :search or body like :search)",{  :searchtime => @last_month ,:type => params[:ticketsearch][:statustyp],:id => current_userd.id,:search => "%#{@search}%"})
      elsif params[:ticketsearch][:searchtime].include?('last week')
        @tickets = Ticket.where("created_at >= :searchtime AND status in (:type) AND userd_id = :id AND (title LIKE :search or body like :search)",{  :searchtime => @last_week ,:type => params[:ticketsearch][:statustyp],:id => current_userd.id,:search => "%#{@search}%"})
      elsif params[:ticketsearch][:searchtime].include?('last day')
        @tickets = Ticket.where("created_at >= :searchtime AND status in (:type) AND userd_id = :id AND (title LIKE :search or body like :search)",{  :searchtime => @last_day ,:type => params[:ticketsearch][:statustyp],:id => current_userd.id,:search => "%#{@search}%"})
      elsif params[:ticketsearch][:searchtime].include?('last hour')
        @tickets = Ticket.where("created_at >= :searchtime AND status in (:type) AND userd_id = :id AND (title LIKE :search or body like :search)",{  :searchtime => @last_hour ,:type => params[:ticketsearch][:statustyp],:id => current_userd.id,:search => "%#{@search}%"})
				# if params[:end] == ''
				# 	 @tickets = Ticket.where("created_at >= :start_date AND status in (:type) AND userd_id = :id AND (title LIKE :search or body like :search)",
				# 		{:start_date => params[:start],:type => params[:statustyp],:id => current_userd.id,:search => "%#{@search}%" }).all
					
				# elsif params[:start] == ''
				# 	 @tickets = Ticket.where("created_at <= :end_date  AND status in (:type) AND userd_id = :id AND (title LIKE :search or body like :search)",
				# 		{:end_date => params[:end],:type => params[:statustyp],:id => current_userd.id, :search => "%#{@search}%"}).all
						
				# elsif params[:end] == '' && params[:start] == ''
				# 	 @tickets = Ticket.where("status in (:type)AND userd_id = :id AND (title LIKE :search or body like :search)",{:type => params[:statustyp],:id => current_userd.id,:search => "%#{@search}%"}).all
						
				# else
				# 	 @tickets = Ticket.where("created_at >= :start_date AND created_at <= :end_date  AND status in (:type) AND userd_id = :id AND (title LIKE :search or body like :search)",
				# 		{:start_date => params[:start],:end_date => params[:end],:type => params[:statustyp],:id => current_userd.id, :search => "%#{@search}%"}).all
						
			 end
		  end
	end
 end
  
  
   
  def indexcloseticket
  	  
    if  current_userd.role_id ==  1  
     	               @tickets = Ticket.where(:closed => 1)

    else
      	               @tickets = Ticket.where(:closed => 1 ,:userd_id => current_userd.id )

    end    

  end
  
  
  def index
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('ticket support'),tickets_path

      
    if request.post?
      flash[:error] = 'post يا معلم'
      
    end
    if  current_userd.role_id ==  1 
      #  @tickets = Ticket.all
      @tickets = Ticket.where(" ticket_id = ? And closed  =?", 0 ,0).order('created_at DESC')

     #@tickets = Ticket.where(" user_id = ? "  ,current_userd.id).paginate(page: params[:page], per_page: 10).order('created_at DESC')
      
 
    else
          # @tickets = Ticket.find(:all, :conditions => [ "user_id >= ?", current_userd.id ])

          @tickets = Ticket.where("userd_id = ?  And ticket_id = ?", current_userd.id,0).all.order('created_at DESC')

          #@tickets = Ticket.where("userd_id = ? And ticket_id = ? And closed  =? And  department_id IN (?)", current_userd.id, 0 ,0,userprivilage.department_id ).all.paginate(page: params[:page], per_page: 10).order('created_at DESC')

    end    

  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
  # alooweddep = userprivilage.department_id
   if current_userd.id == @ticket.userd_id

  #  	if alooweddep.count(@ticket.department_id.to_s)>0 or current_userd.role_id ==  1 
             
              @ticketreplys = Ticket.where('ticket_id' => params['id']).all.order('created_at DESC')
  	          @ticketlog =  Watchdog.where('foreign_id' => params['id'] ,'controller' =>'tickets' ).all.paginate(page: params[:log_page], per_page: 10).order('created_at DESC')

   	  else 
        redirect_to(tickets_path, notice: 'not allowed to view this ticket') 
 
             # redirect_to(listticket_path, notice: 'not allowed to view this ticket')  
  	  end
  	         
  end

  # GET /tickets/new
  def new
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('add ticket'),new_ticket_path

    @ticket = Ticket.new
    if  userprivilage.ticketadd != true and current_userd.role_id !=  1  
       flash[:error] = 'not allowed to add new ticket'
               redirect_to :'tickets'

        end
  end

  # GET /tickets/1/edit
  def edit
      @ticket = Ticket.find(params[:id])

   if  (current_userd.id != @ticket.userd_id or userprivilage.ticketedit != true )and current_userd.role_id !=  1  
       flash[:error] = 'not allowed to add new ticket'
               redirect_to :'tickets'

        end
        
  end

  

  # POST /tickets
  # POST /tickets.json
  def create
  	  
     @ticket = Ticket.new(ticket_params)


     
     @ticket.ticket_number =  rand(100000..999999)
     @ticket.userd_id=current_userd.id
     
    respond_to do |format|
      
      if @ticket.save
            if @ticket.status !='customer reply'
              Notification.create(:user_id => current_userd.id, :foreign_id => @ticket.id, :controller => 'Ticket',:stats => 'new ticket has been added with number/' + ' ' + @ticket.ticket_number.to_s)
              Tickett.ticket(current_userd,@ticket.ticket_number).deliver
              Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'tickets', :action_page => "new / ticket",:foreign_id => @ticket.id ,:details => 'new ticket has been added with number/' + ' ' + @ticket.ticket_number.to_s + ' ' + @ticket.title )
            
             $mailmess= " your ticket created " 
             # TicketMailer.email_action(@baluser.email,$mailmess ).deliver

            else
              @ticket_id = params[:ticket][:id].to_i
              @existed_ticket = Ticket.where("id = ?", @ticket_id).first
              @status = "customer reply".to_s
              @existed_ticket.update(:status => "customer reply")
              @t_num = Ticket.where("id = ?",@ticket.ticket_id).first
              Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'tickets', :action_page =>"reply / ticket",:foreign_id => @ticket.id ,:details=> 'new reply to ticket with number/' + ' ' + @t_num.ticket_number.to_s )


              
              if @ticket.userd_id != current_userd.id
              Notification.create(:user_id => current_userd.id, :foreign_id => @ticket.ticket_id, :controller => 'Ticket',:stats => 'new reply to ticket with number/' + ' ' + @t_num.ticket_number.to_s)
              Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'tickets', :action_page =>"reply / ticket",:foreign_id => @ticket.ticket_id ,:details=> 'new reply to ticket with number/' + ' ' + @t_num.ticket_number.to_s )
              Tickett.ticket_reply(current_userd,@t_num.ticket_number).deliver
              #Watchdog.create(:user_id => current_userd.id,:controller => 'tickets', :action_page =>"replay / ticket" ,:foreign_id=> params[:ticket][:ticket_id] ,:details=> 'new reply to ticket with number/' + ' ' + @t_num.ticket_number.to_s )
              end
              Ticket.where(:id => @ticket.ticket_id ).limit(1).update_all(:closed =>  0  )
            
             $mailmess= "check your ticket "+  @ticket.title.to_s + "  Reply" 
             # TicketMailer.email_action(@baluser.email,$mailmess ).deliver

            end
         
         if params[:ticket][:ticket_id] 
           format.html {  redirect_to(request.env['HTTP_REFERER']) }
         else   
           format.html { redirect_to @ticket, notice: t('new ticket was successfully opened') }
           format.json { render action: 'show', status: :created, location: @ticket }
         end
 
      else
        format.html { render action: 'new' }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
    

  end

  def replay
       @ticket = Ticket.new 

  	  if params['id']
        ## @tickets = Ticket.find(:all, :conditions => [ "id = ?", params['id'] ])
         @ticketd = Ticket.where('id' => params['id']).all
         @ticketreplys = Ticket.where('ticket_id' => params['id']).all


  end
  end
   
  
  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
  	  
    respond_to do |format|
      if @ticket.update(ticket_params)
    
    Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'tickets', :action_page =>"edit / ticket",:foreign_id=>@ticket.id ,:details=> "edit ticket :  "+ @ticket.title )

        format.html { render action: 'edit' }
        format.json { head :no_content }
            format.html {  redirect_to(request.env['HTTP_REFERER']) }

      else
        format.html { render action: 'edit' }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
   @ticketd = Ticket.find(params[:id])
   

    if  (current_userd.id == @ticketd.userd_id and userprivilage.ticketdelete == true ) or current_userd.role_id ==  1  
         Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'tickets', :action_page =>"delete / ticket",:foreign_id=>@ticket.id ,:details=> "delete ticket :  "+ @ticket.title )

       @ticket.destroy
      respond_to do |format|
         format.html { redirect_to tickets_url }
         format.json { head :no_content }
      end
  

   else
         Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'tickets', :action_page =>"delete / ticket",:foreign_id=>@ticket.id ,:details=> "tried delete ticket :  "+ @ticket.title + " but he did not has privilage")
         flash[:error] = 'not allowed to   delete this ticket'
         redirect_to :'tickets'
   end
  end
  


  def closeticket

    @ticketd = Ticket.find(params[:id])

  if  (current_userd.id == @ticketd.userd_id and userprivilage.ticketclose == true ) or current_userd.role_id ==  1

    if params[:id]
          @tickeid=params[:id].to_i
          
          @aff = Ticket.find_by_id(@tickeid)
          @aff.closed = 1
          @aff.save
    end
     respond_to do |format|
            Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'tickets', :action_page =>"close / ticket",:foreign_id=>@ticketd.id ,:details=> "close ticket :  "+ @ticketd.title )
            Notification.create(:user_id => current_userd.id, :foreign_id => @ticketd.id.to_s, :controller => 'ticket',:stats => 'Ticket has been closed with number/' + ' ' +@ticketd.ticket_number.to_s)
     #   mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'
     # message_params = {:from    => 'Payerz@payerz.com',
     #                   :to      => current_userd.email,
     #                   :subject => 'إغلاق  تذكرة',
     #                   :text    => '  تم إغلاق  التذكرة رقم : '+ @ticketd.ticket_number.to_s}
                      
     # mg_client.send_message 'sandboxd398e72047544c559f4ba47aebd39f7a.mailgun.org', message_params
      Tickett.ticket_close(current_userd,@ticketd.ticket_number).deliver
       format.html { redirect_to request.env['HTTP_REFERER'] , notice: t('Ticket was successfully closed') }
       format.json { head :no_content }
    end
    else 
        Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'tickets', :action_page =>"close / ticket",:foreign_id=>@ticketd.id ,:details=> "tried to close ticket :  "+ @ticketd.title + " but he did not has privilage" )

     respond_to do |format|
       format.html { redirect_to request.env['HTTP_REFERER'] , notice: 'you do not have permission to close ticket.' }
       format.json { head :no_content }
    end
   end

      #    sql = "update tickets set closed=0 where id="+@tickeid.
      #    ActiveRecord::Base.connection.execute(sql)

   #  @ticket.update(:closed =>  1 )
     
   
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
    	if not params[:id]
    		     redirect_to tickets_url 
	         else
        
      @ticket = Ticket.find(params[:id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_params
      params.require(:ticket).permit(:userd_id, :foreign_id,:reply_attachment,   :title ,   :body , :approved , :hints, :department_id,:predefined_reply_id,:setuserticket,:priority,:attachment, :ticket_id,:status, :closed)
    end
end
