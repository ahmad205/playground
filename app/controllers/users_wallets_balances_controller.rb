class UsersWalletsBalancesController < ApplicationController

  before_action :set_users_wallets_balance, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_userd!
  before_filter  :handle_two_factor_authentication,:except => [:sendsms]
  respond_to :js, :json, :html

  # get dollar wallet for user  
  def dollar_wallet

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Dollar Wallet'), dollar_wallet_path , :title => "Back to the Index"

    # condition dollar wallet not empty and exist
    if @dollar_wallet != nil  and @dollar_wallet.is_deleted == 0 
      @users_wallets_balances = UsersWalletsBalance.where("user_id = ? and  currency = ? and is_deleted = ?" ,current_userd.id,"USD",0).first
      @frozen_balances = UsersWalletsBalancesFrozen.where("user_id_to = ? and currency = ? and approve = ? and deleted = ?" ,current_userd.id,"USD",0,0).all.order('id Desc')
      @wallet_operations = Transaction.where(["user_id = ? and w_from_name = ? or user_id_to = ? and w_from_name = ? or user_id = ? and w_to_name = ?",current_userd.id,"USD",current_userd.id,"USD",current_userd.id,"USD"]).all.order('id Desc')
    
    else
      redirect_to(account_settings_path,alert: t('not found wallet'))
      
    end

    rescue => e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # get egyptian pound wallet for user
  def egp_wallet

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Pound Wallet'), egp_wallet_path , :title => "Back to the Index"
    
    # condition egyptian pound wallet not empty and exist
    if @egp_wallet != nil  and @egp_wallet.is_deleted == 0 
      @users_wallets_balances = UsersWalletsBalance.where("user_id = ? and  currency = ? and is_deleted = ?" ,current_userd.id,"EGP",0).first
      @frozen_balances = UsersWalletsBalancesFrozen.where("user_id_to = ? and currency = ? and approve = ? and deleted = ?" ,current_userd.id,"EGP",0,0).all.order('id Desc')
      @wallet_operations = Transaction.where(["user_id = ? and w_from_name = ? or user_id_to = ? and w_from_name = ? or user_id = ? and w_to_name = ?",current_userd.id,"EGP",current_userd.id,"EGP",current_userd.id,"EGP"]).all.order('id Desc')
   
    else
      redirect_to(account_settings_path,alert:t('not found wallet'))
      
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end 

  # get riyal wallet for user
  def sar_wallet

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Riyal Wallet'), sar_wallet_path , :title => "Back to the Index"

    # condition riyal wallet not empty and exist
    if @sar_wallet != nil  and @sar_wallet.is_deleted == 0 
      @users_wallets_balances = UsersWalletsBalance.where("user_id = ? and  currency = ? and is_deleted = ?" ,current_userd.id,"SAR",0).first
      @frozen_balances = UsersWalletsBalancesFrozen.where("user_id_to = ? and currency = ? and approve = ? and deleted = ?" ,current_userd.id,"SAR",0,0).all.order('id Desc')
      @wallet_operations = Transaction.where(["user_id = ? and w_from_name = ? or user_id_to = ? and w_from_name = ? or user_id = ? and w_to_name = ?",current_userd.id,"SAR",current_userd.id,"SAR",current_userd.id,"SAR"]).all.order('id Desc')
   
    else
      redirect_to(account_settings_path,alert:t('not found wallet'))

    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
  
  end

  def operations_details

    @USD_wallet_operations = Transaction.where(["user_id = ? and w_from_name = ? or user_id_to = ? and w_from_name = ? or user_id = ? and w_to_name = ?",current_userd.id,"USD",current_userd.id,"USD",current_userd.id,"USD"]).all.order('id Desc')
    @EGP_wallet_operations = Transaction.where(["user_id = ? and w_from_name = ? or user_id_to = ? and w_from_name = ? or user_id = ? and w_to_name = ?",current_userd.id,"EGP",current_userd.id,"EGP",current_userd.id,"EGP"]).all.order('id Desc')
    @SAR_wallet_operations = Transaction.where(["user_id = ? and w_from_name = ? or user_id_to = ? and w_from_name = ? or user_id = ? and w_to_name = ?",current_userd.id,"SAR",current_userd.id,"SAR",current_userd.id,"SAR"]).all.order('id Desc')


  end

  # get reports on balances
  def balancereports

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Wallet Conversion Reports'), balancereports_path
    
    @wallet_report = Transaction.where("user_id =? and controller = ?",current_userd.id,"users_wallets_balances/walletstransferpost").all.order('id Desc')
    @confirmed_transfer1 = UsersWalletsBalancesFrozen.where("user_id = ? or user_id_to = ?",current_userd.id,current_userd.id).all.order('id Desc')
    
    @controller_names1 = "users_wallets_ballences_frozens"

    @confirmed_transfer = UsersWalletsBalancesFrozen.joins("INNER JOIN transactions ON transactions.foreign_id = users_wallets_balances_frozens.id").select("*")
                          .where("transactions.controller LIKE ? AND users_wallets_balances_frozens.user_id = ? or users_wallets_balances_frozens.user_id_to = ?","%#{@controller_names1}%",current_userd.id,current_userd.id) 
    @charged_carts = Cart.where("user_id = ? and status = ?",current_userd.id,0).all.order('id Desc')
    
    @search = "balances_withdraws"

    @withdraw = BalancesWithdraw.where("user_id = ? ",current_userd.id).all.order('created_at DESC')
    @ticketsud = Ticket.joins("INNER JOIN userds ON userds.id = tickets.userd_id").select("*")
    
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # get results of search on wallets' balances transfer operations
  def search_wallet_transfer

    begin
    @wallet_searchs =  params[:search].to_s
    @searchdate =  params[:searchtime]
    @wallet_currency =  params[:operationtyp]

    @reports = UsersWalletsBalance.searchwallettransfer( @wallet_searchs, @wallet_currency, @searchdate )

    @message = 'true'
    render :json => { 'reports': @reports  }

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # get results of search on cart charge operations
  def search_cart_charge

    begin
    @cart_searchs =  params[:search]
    @searchdate =  params[:searchtime]

    @rr = UsersWalletsBalance.searchcartcharge( @cart_searchs, @searchdate )

    @message = 'true'
    render :json => { 'reports': @rr }

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # get results of search on balance withdraw operations
  def search_balance_withdraw

    begin
    @searchdate =  params[:searchtime]
    @withdraw_searchs =  params[:search]     
    @withdraw_type = params[:operationtyp]

    @result = UsersWalletsBalance.searchbalancewithdraw( @withdraw_type, @withdraw_searchs, @searchdate )
        
    render :json => { 'reports': @result }

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # get results of search on balance transfer operations
  def search_balance_transfer

    begin
    @controller_names1 = "users_wallets_ballences_frozens"
    @balance_searchs =  params[:search]
    @searchdate =  params[:searchtime]
    @balance_type = params[:operationtyp]

    @rr = UsersWalletsBalance.searchbalancetransfer( @balance_searchs, @searchdate, @balance_type )

    @message = 'true'
    render :json => { 'reports': @rr   }

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
    
  end

  # get new user wallet balance
  def new

    begin
    add_breadcrumb t('home'), :root_path
    @users_wallets_balance = UsersWalletsBalance.new
    @users_wallets_deleted = UsersWalletsBalance.where("user_id = ? and is_deleted = ?" ,current_userd.id,1).all
    @watch_dogs = Notification.where(["addeduser_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ? or user_id = ? and controller = ?" ,current_userd.id ,'User_wallet_ballence_frozen', current_userd.id , 'Ticket', current_userd.id ,'User_wallet_ballence'  , current_userd.id ,'User_wallet_ballence_frozen', current_userd.id ,'onlinebank', current_userd.id ,'buycart', current_userd.id ,'chargecart', current_userd.id ,'balances_withdraws']).order('created_at DESC') 
    
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # show wallet transfer operation
  def showtransfer

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Review Balance Transfer'), showtransfer_path
    
    @tran_id = params[:id]
    @transfer = Transaction.where("id = ? and controller =? ",@tran_id,"users_wallets_balances/walletstransferpost" ).first
    
    if current_userd.id != @transfer.user_id
      redirect_to account_settings_path, notice: ' Not Allowed'
    end

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
  
  end

  # create user wallet balance
  def create

    begin
    @wallet_currency = params[:users_wallets_balance][:currency]
    @users_wallets_balance = UsersWalletsBalance.new(users_wallets_balance_params)
    @isrepeated=UsersWalletsBalance.where(user_id: current_userd.id,currency: @users_wallets_balance.currency).first

    @wallet_name = UsersWalletsBalance.walletname( @wallet_currency )
    @message = UsersWalletsBalance.createwallet( @wallet_currency, @users_wallets_balance, @isrepeated, @wallet_name )
    
    if @users_wallets_balance.currency == "USD"
      redirect_to dollar_wallet_path
            
    elsif @users_wallets_balance.currency == "EGP"
      redirect_to egp_wallet_path
            
    elsif @users_wallets_balance.currency == "SAR"
      redirect_to sar_wallet_path
    end

    flash[:notice] = @message
    
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  # delete user wallet balance
  def deletewallet

    begin
    @users_wallets_balance = UsersWalletsBalance.new
    @walletbalance = UsersWalletsBalance.where("id = ?",params[:id]).first 
    @balancefrozen = UsersWalletsBalancesFrozen.where("user_id_to = ? and approve = ? and deleted = ? and currency = ?",current_userd.id,0,0,@walletbalance.currency).first
    
    @message = UsersWalletsBalance.deleteewallet( @users_wallets_balance, @walletbalance, @balancefrozen )

    # condition user who delete the wallet is the same who have it
    if (current_userd.id == @walletbalance.user_id )
      
      # condition there is balance in the wallet or there is frozen balance in the wallet
      if (@walletbalance.balance != 0 or @balancefrozen != nil )
        redirect_to(request.env['HTTP_REFERER'])
      
      else
        redirect_to account_settings_path
      end

    end
    flash[:notice] = @message

    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end 

  # get transfer balances between wallets
  def get_transfer

    begin
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Wallet Transfer Balance'), wallet_transfer_path

    @users_wallets_balance = UsersWalletsBalance.new
    @username = Userd.where("id = ?", current_userd.id).first   
  
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end 

  # post transfer balances between wallets
  def walletstransferpost

    begin
    @userid=current_userd.id.to_i
    @user = Userd.find_by_id(@userid)
    @value=params[:users_wallets_balance][:value].to_f
    @from = params[:users_wallets_balance][:type_conversion_from].to_s
    @to = params[:users_wallets_balance][:type_conversion_to].to_s

    @message = UsersWalletsBalance.wallettransfer( @userid, @from.to_s, @to.to_s, @value.to_f)
    
    redirect_to :controller => 'userds', :action => 'account_settings'
    flash[:notice] = @message
    
    rescue => e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
  
  end

  private

    def set_users_wallets_balance

      @users_wallets_balance = UsersWalletsBalance.find(params[:id])
    
    end

    def users_wallets_balance_params
      
      params.require(:users_wallets_balance).permit(:user_id, :currency, :wallet_name, :balance, :status)
    
    end
 
  end