class SessionsController < ApplicationController
  layout false

	  skip_before_filter :authenticate_user! ,:require_login
	  


  def new
end

def create
 
 
   $ee=request.env['HTTP_USER_AGENT']
   user = Userd.authenticate(params[:email], params[:password])
  if user
    session[:user_id] = user.id
    
   
   
    if request.env['HTTP_USER_AGENT'] =~ /[^\(]*[^\)]Chrome\//
    	$ee.sub!("Mozilla","Google Chrome")
    end
    if request.env['HTTP_USER_AGENT'] =~ /[^\(]*[^\)]*[^\t]Firefox\//
      $ee.sub!("Mozilla","FireFox")
  end
    if request.env['HTTP_USER_AGENT'] =~ /[^\(]*[^\)]*[^\t]Opera\//
      $ee.sub!("Mozilla","Opera")
  end
   if request.env['HTTP_USER_AGENT'] =~ /[^\(]*[^\)]*[^\t]Trident\//
      $ee.sub!("Mozilla","Internet Explorer")
  end
   if request.env['HTTP_USER_AGENT'] =~ /[^\(]*[^\)]*[^\t]Edge\//
      $ee.sub!("Mozilla","Microsoft Edge")
  end
  
  
  if $ee =~ /Windows NT 6.1/
       $ee.sub!("Windows NT 6.1","Windows 7")
   end
   if $ee =~ /Windows NT 6.0/
       $ee.sub!("Windows NT 6.0","Windows Vista")
   end
   if $ee =~ /Windows NT 6.2/
       $ee.sub!("Windows NT 6.2","Windows 8")
   end
   if $ee =~ /Windows NT 6.3/
       $ee.sub!("Windows NT 6.3","Windows 8.1")
   end
   if $ee =~ /Windows NT 6.1/
       $ee.sub!("Windows NT 6.1","Windows 7")
   end
   if $ee =~ /Windows NT 10/
       $ee.sub!("Windows NT 10.0","Windows 10")
   end
   
   $ee.sub!("/5.0","")
   $ee=$ee.split(';').first

    Watchdog.create(:user_id => session[:user_id],:controller => 'sessions', :action_page =>"log in",:details=> "login= from ip "+ request.remote_ip,:device_name=> $ee+")")
    
    
    redirect_to root_url, :notice => "Logged in!"
  else
    flash.now.alert = "Invalid email or password"
    render "new"
  end
  
 
end

def destroy
    Watchdog.create(:user_id => session[:user_id],:controller => 'sessions', :action_page =>"log OUT",:details=> "logout= from ip "+ request.remote_ip,:device_name=> $ee)
 
  session[:user_id] = nil

  redirect_to root_url, :notice => "Logged out!"
end

end
