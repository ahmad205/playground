class Userds::TwoFactorsController < ApplicationController
  before_filter :authenticate_userd!
    

  def new
  end
  
  # If user has already enabled the two-factor auth, we generate a temp. otp_secret and show the 'new' template.
  # Otherwise we show the 'show' template which will allow the user to disable the two-factor auth
  def show

    begin
    unless current_userd.otp_required_for_login?
      current_userd.unconfirmed_otp_secret = Userd.generate_otp_secret
      current_userd.save!
      @qr = RQRCode::QRCode.new(two_factor_otp_url).to_img.resize(240, 240).to_data_url
      render 'new'
    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end
  
  # Activate_two_factor will return a boolean. When false is returned we presume the model has some errors.
  def create

    begin
    permitted_params = params.require(:userd).permit :password, :otp_attempt
    if current_userd.activate_two_factor permitted_params
      redirect_to root_path, notice: "You have enabled Two Factor Auth"
    else
      render 'new'
    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
    
  end
  
  # If the provided password is correct, two-factor is disabled
  def destroy

    begin
    permitted_params = params.require(:userd).permit :password
    if current_userd.deactivate_two_factor permitted_params
      redirect_to root_path, notice: "You have disabled Two Factor Auth"
    else
      render 'show'
    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end
  
  private
  
    # The url needed to generate the QRCode so it can be acquired by
    # Google Authenticator
    def two_factor_otp_url
      "otpauth://totp/%{app_id}?secret=%{secret}&issuer=%{app}" % {
        :secret => current_userd.unconfirmed_otp_secret,
        :app    => "your-app",
        :app_id => "YourApp"
      }
    end

end
