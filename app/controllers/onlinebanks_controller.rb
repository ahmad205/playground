class OnlinebanksController < ApplicationController
  before_action :set_onlinebank, only: [:show, :edit, :update, :destroy]
  
  
skip_before_filter :authenticate_userd!, :only => [:call_coinmoney,:paypal_ipn,:success,:user_reply ]
skip_before_action :authenticate_userd!, :only => [:call_coinmoney,:paypal_ipn,:success,:perfectmoneypost,:perfectmoneyerror,:user_reply]


  def index

    begin
    @onlinebanks = Onlinebank.where(:user_id => current_userd.id).all
    
    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    
    end

  end

  def online_image

    begin
    if params[:id]
      @onlinebank = Onlinebank.where(:id => params[:id].to_i).first

      if current_userd && @onlinebank.user_id == current_userd.id
        send_file Onlinebank.where(:id => params[:id].to_i,:user_id => current_userd.id ).first.avatar.path
      else 
        redirect_to(onlinebanks_url,:notice => t('You are not allowed to open this picture'))
      end

    elsif params[:ticketid]
      @ticket = Ticket.where(:id => params[:ticketid].to_i).first

      if  current_userd &&    @ticket.userd_id == current_userd.id
        send_file Ticket.where(:id => params[:ticketid].to_i,:userd_id => current_userd.id ).first.attachment.path
      else 
        redirect_to(tickets_url,:notice => t('You are not allowed to open this picture'))
      end

    end
    
    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def online_ticket_image

    begin
    @ticket = Ticket.where(:id => params[:ticketid].to_i).first

    if current_userd && @ticket.userd_id == current_userd.id
      send_file Ticket.where(:id => params[:ticketid].to_i,:userd_id => current_userd.id ).first.attachment.path
    else 
      redirect_to(tickets_url,:notice => t('You are not allowed to open this picture'))
    end

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

def user_reply

  Onlinebank.user_reply(params,current_userd.id,@cart_usdtoegp,@cart_usdtors)
  redirect_to(request.env['HTTP_REFERER'],notice:"تم الرد على الاستثناء")
end


  def uploadefile

   # begin
    @pay_req = Onlinebank.where("id = ?",params[:uploadefile][:id]).first
    @pay_tran = Transaction.where(foreign_id: params[:uploadefile][:id],controller: "Onlinebanks/banktransfer_post").first
    @order_details = @pay_tran.company_bank_data.to_s.split('/')
    @Payment_bank = UsersBank.new
        @Payment_bank.userd_id = @pay_req.user_id
    	  @Payment_bank.bank_type = "pay"
    	  @Payment_bank.phone = params[:uploadefile][:phone]
        @Payment_bank.currency = @pay_req.currency
        @Payment_bank.currencies_withdraw_ratios_id = @pay_req.company_bank_id
    	if @order_details[0] == "localeg_vodafonecash"
    	  @Payment_bank.from_mobile = params[:uploadefile][:from_mobile]
          @Payment_bank.to_mobile = params[:uploadefile][:to_mobile]
        end
        if @order_details[0] == "localeg_vodafonecash" or @order_details[0] == "interusd_western" or @order_details[0] == "interusd_money"
        	@pay_tran.update(:w_to_name => params[:uploadefile][:operation_id].to_s)
          #@Payment_bank.operation_id = params[:uploadefile][:operation_id].to_s
        end
        if @order_details[0] == "localeg_bank" or @order_details[0] == "localeg_post" or @order_details[0] == "localsar" or @order_details[0] == "interusd_swift" or @order_details[0] == "interusd_western" or @order_details[0] == "interusd_money"
        	@Payment_bank.account_name = params[:uploadefile][:name]
        end
        if @order_details[0] == "localeg_bank" or @order_details[0] == "localeg_post"  
        	@Payment_bank.nationalid = params[:uploadefile][:nationalid].to_s
        end
        if @order_details[0] == "localeg_bank" or @order_details[0] == "localsar"  
        	@Payment_bank.account_number = params[:uploadefile][:account_number].to_s
        end
        if @order_details[0] == "localsar"  
        	@Payment_bank.bank_name = params[:uploadefile][:bank_name].to_s
        end
        if @order_details[0] == "interusd_swift"  or @order_details[0] == "interusd_western" or @order_details[0] == "interusd_money" 
        	@Payment_bank.country = params[:uploadefile][:country].to_s
        end
   
    @Payment_bank.save
    @pay_tran.update(:w_to_balance_after => params[:uploadefile][:value])
        
    if @pay_req.attachments != nil  && params[:uploadefile][:attachments] != nil
      @att =  Attachment.where(:id => @pay_req.attachments).first
      @att.update(:content => params[:uploadefile][:attachments],:user_id => current_userd.id ,:file_ext => params[:uploadefile][:attachments].content_type,:file_name => params[:uploadefile][:attachments].original_filename)
      @attach =   @att.id

    elsif   @pay_req.attachments == nil  && params[:uploadefile][:attachments] != nil
        @att =  Attachment.create(:uuid => SecureRandom.uuid , :content => params[:uploadefile][:attachments],:user_id => current_userd.id,:file_ext => params[:uploadefile][:attachments].content_type,:file_name => params[:uploadefile][:attachments].original_filename )
        @attach =   @att.id

      end
       
    @pay_req.update(:attachments => @attach,:status => "Processing",:user_bank_id =>  @Payment_bank.id)





    Notification.create(:user_id => current_userd.id,:added_by => current_userd.id,:trans_id => @pay_tran.id,:foreign_id => params[:uploadefile][:id], :controller => 'Onlinebanks/add_payment_data',:stats => 'payment information added/' +  @pay_tran.transaction_id.to_s + "/addvalue/" + @pay_req.netsum.to_s )
    Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'Onlinebanks', :action_page =>"add_payment_data",:foreign_id=> params[:uploadefile][:id] ,:details=> 'payment information added/' +  @pay_tran.transaction_id.to_s + "/addvalue/" + @pay_req.netsum.to_s)
    Payment.add_payment_data(current_userd,@pay_req).deliver
   redirect_to request.env['HTTP_REFERER'] , notice: t('payment information received')

    # rescue =>e
    #   @code = SecureRandom.hex

    #   @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
    #   redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    # end
    
  end

  def offline_avatar
    @onlinebank = Onlinebank.where(:id => params[:id].to_i).first
   if  current_userd &&  @onlinebank != nil   &&  @onlinebank.user_id == current_userd.id
     @data_url = @onlinebank.attachments.url
   @png  = Base64.decode64(@data_url['data:image/png;base64,'.length .. -1])

   if params[:type] == "img"
    send_data @png
   end

   if params[:type] == "link"
    render :layout => false
  end

  else

    redirect_to(root_path,:notice => t('A picture you are not allowed to access'))
    end


  end

  def exception_ticket
  #   @ticket = Ticket.where(:id => params[:id].to_i).first
  #  if  current_userd &&  @ticket != nil   &&  @ticket.userd_id == current_userd.id
  #    @data_url = @ticket.reply_attachment.url
  #  @png  = Base64.decode64(@data_url['data:image/png;base64,'.length .. -1])

  #  if params[:type] == "img"
  #   send_data @png
  #  end

  #  if params[:type] == "link"
  #   render :layout => false
  # end

  # else

  #   redirect_to(root_path,:notice => 'صورة غير مسموح لك الدخول عليها')
  #   end
  end

  def cancel_payment

    begin
    @onlinebank = Onlinebank.where(:id => params[:id]).first
    @tran_id = Transaction.where(["foreign_id = ? and controller in (?) " , @onlinebank.id,['Onlinebanks/banktransfer_post']]).first
    @onlinebank.update(:status => "Canceled")
    Watchdog.create(:user_id => @onlinebank.user_id,:added_by => current_userd.id,:controller => 'Onlinebanks', :action_page =>"cancel_payment ",:foreign_id=>@onlinebank.id ,:details=> "cancel payment request/ " + @onlinebank.transaction_id.to_s + "/addvalue/" + @onlinebank.netsum.to_s  )
    Notification.create(:user_id => @onlinebank.user_id,:added_by => current_userd.id,:trans_id => @tran_id.id,:controller => 'Onlinebanks/cancel_payment',:foreign_id => @onlinebank.id.to_s,:stats => 'cancel payment request/ ' + @onlinebank.transaction_id.to_s + "/addvalue/" + @onlinebank.netsum.to_s)   
    Payment.cancel_payment_request(current_userd,@onlinebank).deliver
    redirect_to(request.env['HTTP_REFERER'],notice:t("Order has been cancelled"))

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def paypal
    @id = params[:id]
  end

  # paypal post function and return function with parameters
  def paypalpost

    begin

    if params[:paypalnow]
      #calculate paypal ratio of transfer
      @bank = CompanyBank.where("bank_key = 'paypal'").first
      @ratio = params[:paypalnow][:value].to_f * (@bank.ratio / 100) + @bank.fees
      @result = params[:paypalnow][:value].to_f + @ratio
      redirect_to  paypalurl_path(:val=>@result,:cardvalue => params[:paypalnow][:value].to_f )
    end
    
    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
    
  end

  def coinmoney

    begin
    @test=Coinpayments.rates(accepted: 1).delete_if { |_k, v| v["accepted"] == 0 }.keys
    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def coinmoney_post

    #begin
    @value = params[:coinmoney][:value].to_f
    @currency = params[:coinmoney][:currency]  
    @company_bank = CompanyBank.where(:bank_key => @currency.to_s).first
    @result = @value * (@company_bank.ratio / 100) + @company_bank.fees
    @final_value = @result +@value
    @transaction = Coinpayments.create_transaction(@final_value, 'USD', @currency)
    # Coinpayment.test3(@transaction.to_s).deliver

    @trans =@transaction
    @address = @transaction.address
    Onlinebank.create( :company_bank_id =>  @company_bank.id,:bank_name => @company_bank.bank_name, :user_id => current_userd.id , :status => status,:value=> @final_value,:netsum=> @value ,:refund => @trans.amount.to_s ,:currency => @currency, :transaction_id => @trans.txn_id,:address => @trans.address ,:status_url =>@trans.status_url , :purchased_at => Time.now )  
    redirect_to @trans.status_url

    # rescue =>e
    #   @code = SecureRandom.hex
    #   ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s)
    # end

  end


  def paypal_ipn

     Coinpayment.test3(params.to_s).deliver

    # @tt = "out"
    @transaction = params[:txn_id].to_s
    @userid = params[:custom].to_i
    status = params[:payment_status].to_s
    @user = Userd.where(:id => @userid ).first
    @paypal_transaction = Onlinebank.where(:tansaction => @transaction , :bank_name => 'paypal').first

    if @paypal_transaction == nil && status == "Completed"
      @timenow = Time.now.to_i
      # @uidc = params[:uid]
      # @palgross = params[:mc_gross].to_f 

      

      
  # if status == "Completed"


   ###calculate paypal ratio of transfer
   @bank= CompanyBank.where("bank_key = 'paypal'").first
   @ratio = params[:mc_gross].to_f  - @bank.fees
   @r2 = @ratio * (@bank.ratio / 100)
   @result =   params[:mc_gross].to_f - @r2
    
   @original_value = (((params[:mc_gross].to_f - @bank.fees) * 100 )/(@bank.ratio + 100))
   @tx_found = Onlinebank.create( :user_id => @userid ,:company_bank_id =>  @bank.id,:currency => 'USD',:bank_name => 'paypal', :status => status,:value=> params[:mc_gross],:netsum => @original_value.to_f, :tansaction => params[:txn_id], :transaction_id => @trans_id, :purchased_at => Time.now )
   @cartn =  Cart.generatecart1(@original_value.to_f,@tx_found.id, @userid )
   @cartdet = Cart.where("id =?",@cartn).first
   @ubalance= UsersWalletsBalance.where("user_id = ? and currency= ?",@userid,'USD').first
   @company_ratio = @original_value.to_f - @cartdet.value.to_f
  
   Watchdog.create(:user_id => @userid,:added_by => @userid,:controller => 'carts', :action_page =>"paypalconfirm ",:foreign_id=>@cartn ,:details=> "pay money for cart with value  "+ @cartdet.value.to_s  )    
   @tran_id = Transaction.create(:user_id => @userid,:foreign_id => @cartdet.id ,:w_from_name => 'USD' ,:value_from => @cartdet.value ,:w_to_name => "PayPal", :controller =>"carts/paypalconfirm",:transaction_id=>@trans_id,:description=> "A credit card has been purchased/" + @cartdet.value.to_s + " USD"  )
   Notification.create(:user_id =>  @userid,:trans_id => @tran_id.id,:controller => 'carts/paypalconfirm',:foreign_id => @cartn.to_s,:stats => 'A credit card has been purchased/' + @cartdet.value.to_s + ' ' + 'USD')
   CompanyBalance.create(:user_id => @userid,:foreign_id => @cartdet.id ,:operation_id => '1',:trans_id => @tran_id.id,:balance => @company_ratio,:currency =>  'USD' ,:status => '1')
   Chargebalance.payforcart(@user,@cartdet, @ubalance).deliver

  end
end

  def call_coinmoney

    
    @trans_id = Transaction.gettransaction   


    @tx = params[:txn_id].to_s
    @status_text = params[:status_text].to_s  #reply from api
    @tx_found= Onlinebank.where.not(address: nil).where(transaction_id: @tx).first  #this date will be compared with our data

    if @status_text == 'Complete'  && @tx_found.status != 'Complete'
        @tx_found.status = @status_text
        @tx_found.save
        @cartn =  Cart.generatecart1(@tx_found.value,@tx_found.id, @tx_found.user_id )
        @cartdet = Cart.where("id =?",@cartn).first
        @ubalance= UsersWalletsBalance.where("user_id = ? and currency= ?",@tx_found.user_id,'USD').first
        Watchdog.create(:user_id => @tx_found.user_id,:added_by => @tx_found.user_id,:controller => 'carts', :action_page =>"call_coinmoney",:foreign_id=>@cartn ,:details=> "pay money for cart with value  "+ @cartdet.value.to_s  )
        @tran_id = Transaction.create(:user_id => @tx_found.user_id ,:foreign_id => @cartdet.id ,:value_from => @cartdet.value ,:w_to_name => "CoinPayment", :controller =>"carts/call_coinmoney",:transaction_id=>@trans_id,:description=> 'A credit card has been purchased/' + @cartdet.value.to_s + " USD"  )
        Notification.create(:user_id =>  @tx_found.user_id,:trans_id => @tran_id.id,:controller => 'carts/call_coinmoney',:foreign_id => @cartn.to_s,:stats => 'A credit card has been purchased/' + @cartdet.value.to_s + 'USD')   
        Chargebalance.payforcart(current_userd,@cartdet, @ubalance).deliver
    else
        @tx_found.status = @status_text
        @tx_found.save
    end

end
# def call_coinmoney
#   @trans_id = Transaction.gettransaction   

#   # @trans_id = @timenow.to_s +  SecureRandom.urlsafe_base64(n=2).to_s
#   @tx = params[:txn_id].to_s
#   @status_text = params[:status_text].to_s #ده االرد من ال api
#   @tx_found= Onlinebank.where.not(address: nil).where(transaction_id: @tx).first #دي الداتا الي هيقارنها الي موجوده عندي
#   if  @status_text == 'Complete'  && @tx_found.status != 'Complete'

#     @tx_found.status = @status_text
#     @tx_found.save
#     @cartn =  Cart.generatecart1(@tx_found.value,@tx_found.id, @tx_found.user_id )
#     @cartdet = Cart.where("id =?",@cartn).first
#     @ubalance= UsersWalletsBalance.where("user_id = ? and currency= ?",@tx_found.user_id,'USD').first
#     Watchdog.create(:user_id => @tx_found.user_id,:added_by => @tx_found.user_id,:controller => 'carts', :action_page =>"call_coinmoney",:foreign_id=>@cartn ,:details=> "pay money for cart with value  "+ @cartdet.value.to_s  )
#     Transaction.create(:user_id => @tx_found.user_id ,:foreign_id => @cartdet.id ,:w_from_name => 'USD' ,:value_from => @cartdet.value ,:w_to_name => "CoinPayment", :controller =>"carts/call_coinmoney",:transaction_id=>@trans_id,:description=> 'A credit card has been purchased/' + @cartdet.value.to_s + " USD"  )
#     @tran_id = Transaction.where("transaction_id = ?", @trans_id).first
#     Notification.create(:user_id =>  @tx_found.user_id,:trans_id => @tran_id.id,:controller => 'carts/call_coinmoney',:foreign_id => @cartn.to_s,:stats => 'A credit card has been purchased/' + @cartdet.value.to_s + 'USD')   
#     Chargebalance.payforcart(current_userd,@cartdet, @ubalance).deliver
#   #  flash[:alert] = 'تم شراء بطاقة بقيمة '+@cartdet.value.to_s + ' USD بنجاح'
#   # redirect_to  cart_preview_path(:id=>@cartn)

#   end

# end
 
  def blockcypher
  end
 
 
  def paypalurl

    begin
    @paypalcompany= CompanyBank.where("bank_key = 'paypal'").first
    values = {
        business:  @paypalcompany.account_email,
        cmd: "_xclick",
        currency_code:"USD",
        # return: "#{Rails.application.secrets.app_host}#{paypalret_path}",
        return: "#{Rails.application.secrets.app_host}#{paypalret_path}",
        #return: "https://dashboard.payers.net#{paypalret_path}",
        # return: "<%# format_link(request) %>#{paypalret_path}",
        #invoice: params[:cardvalue],
        amount: params[:val],
        item_name: params[:cardvalue],   
        custom:  current_userd.id, 
        uid: current_userd.id,
        # notify_url: "#{Rails.application.secrets.app_host}paypalret_path"

        notify_url: "#{Rails.application.secrets.app_host}#{paypal_ipn_path}"


    }
   redirect_to  "#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query
 
  rescue =>e
    @code = SecureRandom.hex

    @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
    redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
  end

end

  protect_from_forgery except: [:hook]

  def hook

    begin
    params.permit! # Permit all Paypal input params
    status = params[:payment_status]

    if status == "Completed" 
      Onlinebank.create(:user_id =>  current_userd.id , :status => status,:value=> params[:mc_gross] , :transaction_id => params[:txn_id], :purchased_at => Time.now )       
      @cartn =  Cart.generatecart(params[:mc_gross] , params[:custom] )      
      redirect_to :controller => 'carts', :action => 'show' , id:@cartn  
    end

    render nothing: true
    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end  
  
  def banktransfer_post

    begin

    loop do
      @trans_id = [*('0'..'9')].to_a.shuffle[0,6].join
      break @trans_id unless Transaction.where(transaction_id: @trans_id).exists?
    end 

    @value = params[:banktransfer_post][:value].to_f
    @id_egp = params[:banktransfer_post][:id_egp]
    @id_sar = params[:banktransfer_post][:id_sar]
    @id_usd = params[:banktransfer_post][:id_usd]


    if (@id_egp != '')
      @bank_details = CompanyBank.find_by_id(@id_egp)
    elsif (@id_sar != '')
      @bank_details = CompanyBank.find_by_id(@id_sar)
    elsif (@id_usd != '')
      @bank_details = CompanyBank.find_by_id(@id_usd)
    end

    if @bank_details.currency == "EGP"
      @paid_value = params[:banktransfer_post][:value].to_f * @cart_usdtoegp.value.to_f
    elsif @bank_details.currency == "SAR"
        @paid_value = params[:banktransfer_post][:value].to_f * @cart_usdtors.value.to_f
    elsif @bank_details.currency == "USD"
        @paid_value = params[:banktransfer_post][:value].to_f 
    end

    @payment_req = Onlinebank.new
    @payment_req.user_id = current_userd.id
    @payment_req.company_bank_id = @bank_details.id
    @payment_req.status = 'Pending'
    @payment_req.value = params[:banktransfer_post][:value].to_f
    @payment_req.netsum = params[:banktransfer_post][:netsum].to_f
    @payment_req.transaction_id = @trans_id
    @payment_req.bank_name = @bank_details.bank_name
    @payment_req.currency = @bank_details.currency
    
    if @payment_req.save
     
    Transaction.create(:foreign_id => @payment_req.id, :transaction_id => @trans_id ,:user_id => current_userd.id ,:value_from => params[:banktransfer_post][:value].to_f,:w_from_name => @bank_details.currency,:value_to => params[:banktransfer_post][:netsum].to_f ,:w_to_balance_before => @paid_value.to_f ,:controller => 'Onlinebanks/banktransfer_post',:description => 'payment request/' + @trans_id.to_s + '/addvalue/'+ @payment_req.netsum.to_s + " USD" )
    @tran_id = Transaction.where("transaction_id = ?", @trans_id).first
    Notification.create(:user_id => current_userd.id,:added_by => current_userd.id,:trans_id => @tran_id.id,:foreign_id => @payment_req.id, :controller => 'Onlinebanks/banktransfer_post',:stats => 'payment request/' + @trans_id.to_s + '/addvalue/'+ @payment_req.netsum.to_s )
    Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'Onlinebanks', :action_page =>"banktransfer_post",:foreign_id=>@payment_req.id ,:details=> 'payment request/' + @trans_id.to_s + '/addvalue/'+ @payment_req.netsum.to_s + " USD" )
    if (@bank_details.bank_subcategory == "interusd/swift")
    @tran_id.update(:company_bank_data => ("interusd_swift" + "/bank name/" + @bank_details.bank_name + "/currency/" + @bank_details.currency + "/Fees/" + (@bank_details.fees.to_s) + "/ratio/" + (@bank_details.ratio.to_s) + "/country/" + @bank_details.country + "/Branch Name/" + @bank_details.branch + "/branch code/" + @bank_details.branch_code  + "/account name/" + @bank_details.account_name + "/account number/" + @bank_details.account_number + "/swift code/" + @bank_details.swiftcode  ))
    elsif @bank_details.bank_subcategory == "interusd/western" 
     @tran_id.update(:company_bank_data => ('interusd_western' + '/bank name/' + @bank_details.bank_name + '/currency/' + @bank_details.currency + "/Fees/" + (@bank_details.fees.to_s) + "/ratio/" + (@bank_details.ratio.to_s) + '/account name/' + @bank_details.name + '/country/' + @bank_details.country + '/city/' + @bank_details.city + '/phone/' + @bank_details.phone ) )   
    elsif @bank_details.bank_subcategory == "interusd/money"
     @tran_id.update(:company_bank_data => ('interusd_money' + '/bank name/' + @bank_details.bank_name + '/currency/' + @bank_details.currency + "/Fees/" + (@bank_details.fees.to_s) + "/ratio/" + (@bank_details.ratio.to_s) + '/account name/' + @bank_details.name + '/country/' + @bank_details.country + '/city/' + @bank_details.city + '/phone/' + @bank_details.phone ) )   
    elsif @bank_details.bank_subcategory == "localsar"
     @tran_id.update(:company_bank_data => ('localsar' + '/bank name/' + @bank_details.bank_name + '/currency/' + @bank_details.currency + "/Fees/" + (@bank_details.fees.to_s) + "/ratio/" + (@bank_details.ratio.to_s) + '/account name/' + @bank_details.name + '/iban/' + @bank_details.ibancode  ))
    elsif @bank_details.bank_subcategory == "localeg/vodafonecash"
     @tran_id.update(:company_bank_data => ('localeg_vodafonecash' + '/bank name/' + @bank_details.bank_name + '/currency/' + @bank_details.currency + "/Fees/" + (@bank_details.fees.to_s) + "/ratio/" + (@bank_details.ratio.to_s) + '/phone/' + @bank_details.phone  ))        
    elsif @bank_details.bank_subcategory == "localeg/bank"
     @tran_id.update(:company_bank_data => ('localeg_bank' + '/bank name/' + @bank_details.bank_name + '/currency/' + @bank_details.currency + "/Fees/" + (@bank_details.fees.to_s) + "/ratio/" + (@bank_details.ratio.to_s) + '/account name/' + @bank_details.account_name + '/account number/' + @bank_details.account_number + '/Branch Name/' + @bank_details.branch + '/branch code/' + @bank_details.branch_code  ) )   
    elsif @bank_details.bank_subcategory == "localeg/post"
     @tran_id.update(:company_bank_data => ('localeg_post' + '/bank name/' + @bank_details.bank_name + '/currency/' + @bank_details.currency + "/Fees/" + (@bank_details.fees.to_s) + "/ratio/" + (@bank_details.ratio.to_s) + '/account name/' + @bank_details.account_name + '/account number/' + @bank_details.account_number + '/Branch Name/' + @bank_details.branch + '/name/' + @bank_details.name + '/national id/' + @bank_details.nationalid  ))
    end
    
    flash[:notice]= t('Order successfully created')
    Payment.new_payment(current_userd,@payment_req).deliver
    end
    
    
    redirect_to :controller => 'onlinebanks', :action => 'show' , id:@payment_req.id

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end  

  def paypalconfirm

   Coinpayment.test3(params.to_s).deliver

    begin
    @timenow = Time.now.to_i
    status = params[:st].to_s
    @uidc = params[:uid]
    @palgross = params[:mc_gross].to_f

    # **********************************************
    # @transaction = params[:tx].to_s
    # @userid = params[:custom].to_i
    # status = params[:payment_status].to_s
    # @user = Userd.where(:id => @userid ).first
    # @paypal_transaction = Onlinebank.where(:tansaction => @transaction , :bank_name => 'paypal').first

    # **********************************************
    @trans_id = Transaction.gettransaction   

    if status == "Completed"
      #calculate paypal ratio of transfer
      @bank= CompanyBank.where("bank_key = 'paypal'").first
      @ratio = params[:amt].to_f  - @bank.fees
      @r2 = @ratio * (@bank.ratio / 100)
      @result =   params[:amt].to_f - @r2
      @original_value = (((params[:amt].to_f - @bank.fees) * 100 )/(@bank.ratio + 100))
      @tx_found = Onlinebank.create( :user_id => current_userd.id ,:company_bank_id =>  @bank.id,:currency => 'USD',:bank_name => 'paypal', :status => status,:value=> params[:amt],:netsum => @original_value.to_f, :transaction_id => @trans_id, :tansaction => params[:tx].to_s, :purchased_at => Time.now )
      @cartn =  Cart.generatecart1(@original_value.to_f,@tx_found.id, current_userd.id )
      @cartdet = Cart.where("id =?",@cartn).first
      @ubalance= UsersWalletsBalance.where("user_id = ? and currency= ?",current_userd.id,'USD').first
      @company_ratio = @original_value.to_f - @cartdet.value.to_f
      Watchdog.create(:user_id => current_userd.id,:added_by => current_userd.id,:controller => 'carts', :action_page =>"paypalconfirm ",:foreign_id=>@cartn ,:details=> "pay money for cart with value  "+ @cartdet.value.to_s  )
      @tran_id = Transaction.create(:user_id => current_userd.id,:foreign_id => @cartdet.id ,:w_from_name => 'USD',:value_from => @cartdet.value ,:w_to_name => "PayPal", :controller =>"carts/paypalconfirm",:transaction_id=>@trans_id,:description=> "A credit card has been purchased/" + @cartdet.value.to_s + " USD"  )
      Notification.create(:user_id =>  current_userd.id,:trans_id => @tran_id.id,:controller => 'carts/paypalconfirm',:foreign_id => @cartn.to_s,:stats => 'A credit card has been purchased/' + @cartdet.value.to_s + ' ' + 'USD')
      CompanyBalance.create(:user_id => current_userd.id,:foreign_id => @cartdet.id,:operation_id => '1',:trans_id => @tran_id.id,:balance => @company_ratio,:currency =>  'USD' ,:status => '1')
      Chargebalance.payforcart(current_userd,@cartdet, @ubalance).deliver
      flash[:alert] = t('A credit card has been purchased') + ' '+ @cartdet.value.to_s + ' USD'       
      redirect_to  cart_preview_path(:id=>@cartn)    
    end
   rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end
   
      
  end

  def mastervisa  
  end

  def mastervisapost

    @MERCHANT_ID = "0000992"
    @INTEGRATION_URL = "https://gateway.cardstream.com/direct/"
    request_data = {
        :amount => params[:visapay][:amount].to_i,
        :cardNumber => params[:visapay][:cardNumber],
        :cardExpiryMonth => params[:visapay][:cardExpiryMonth].to_i,
        :cardExpiryYear => params[:visapay][:cardExpiryYear].to_i,
        :cardCVV => params[:visapay][:cardCVV].to_i,
        :merchantID => @MERCHANT_ID, 
        :action => "SALE", 
        :type => 1,
        :currencyCode => 826,
        :orderRef => "Test purchase",
        :countryCode => 826,
        :returnInternalData => "Y",
        :threeDSRequired => "N"
    }	
    uri = URI.parse(@INTEGRATION_URL)
    http = Net::HTTP.new(uri.host, 443)
    http.use_ssl = true
    req = Net::HTTP::Post.new(uri.request_uri)
    req.set_form_data(request_data)
    response = http.request(req)
    response_hash = Rack::Utils.parse_nested_query response.body

    if response_hash['responseCode'] != "0"
    # errors.add(:id, "Payment failed #{response_hash['responseMessage']}")
    else
      self.attributes = strip_cc_response_hash(response_hash)
    end
	
	
	
end


  def cashu
    @id = params[:id]
  end

  def cashupost

  end 

  def cashuurl

    

  end

  def cashunotification

 
  end

  def cashusorry
  end

  def perfectmoney

    begin
    # Coinpayment.test3(params.to_s).deliver
    @id = params[:id]     
    @pmcompany= CompanyBank.where("bank_key = 'perfectmoney'").first

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def perfectmoney_toapi


    @INTEGRATION_URL = "https://perfectmoney.is/api/step1.asp/"

    
    # begin
      @perfectcompany= CompanyBank.where("bank_key = 'perfectmoney'").first
      @user_netsum_json = current_userd.id.to_s + params[:PAYMENT_ID].to_s
      @payment_url =  request.protocol.to_s + request.host.to_s + "/perfectmoney"
      @nopayment_url =  request.protocol.to_s + request.host.to_s + "/perfectmoneyerror"

      @values = {
          PAYEE_ACCOUNT:  @perfectcompany.account_name,
          PAYEE_NAME: "payers",
          STATUS_URL: @payment_url,
          PAYMENT_URL: @payment_url,
          PAYMENT_URL: @payment_url,
          PAYMENT_ID: current_userd.id,
          PAYMENT_AMOUNT: params[:PAYMENT_AMOUNT],
          NOPAYMENT_URL:  @nopayment_url, 
          PAYMENT_UNITS: "USD",
          SUGGESTED_MEMO: "test if work"
    
      }

    # uri = URI.parse(@INTEGRATION_URL)
    # http = Net::HTTP.new(uri.host, 443)
    # http.use_ssl = true
    # req = Net::HTTP::Post.new(uri.request_uri)
    # req.set_form_data(values)
    # response = http.request(req)
    # response_hash = Rack::Utils.parse_nested_query response.body

      @res = "https://perfectmoney.is/api/step1.asp?" + @values.to_query
   
  # redirect_to  "https://perfectmoney.is/api/step1.asp?" + @values.to_query
   

  #  redirect_to  "https://perfectmoney.is/acct/balance.asp?" + values.to_query

   
    # rescue =>e
    #   @code = SecureRandom.hex
  
    #   @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
    #   redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    # end



  end

  def perfectmoneypost

    begin

      # Coinpayment.test3(params.to_s).deliver 

    @trans_id = Transaction.gettransaction   

    #recieve the real value of cart  first then the current userid  and split them with ;
    @realcart_userid = params[:PAYMENT_ID].to_s.split("p")

    
    if params[:PAYEE_ACCOUNT] != nil && params[:PAYMENT_BATCH_NUM] != nil
        ##calculate paypal ratio of transfer
        @bank= CompanyBank.where("bank_key = 'perfectmoney'").first
        @ratio = params[:PAYMENT_AMOUNT].to_f  - @bank.fees.to_f
        @r2 = @ratio * (@bank.ratio / 100)
        @result =   params[:PAYMENT_AMOUNT].to_f - @r2
        @userid = @realcart_userid[1].to_i
        @real_cart_value = @realcart_userid[0].to_i
       
   ActiveRecord::Base.transaction do
        if @userid != 0 
        @user = Userd.where("id =?",@userid).first
        @tx_found = Onlinebank.create( :user_id =>  @userid ,:company_bank_id =>  @bank.id,:currency => 'USD',:bank_name => 'perfectmoney', :status => 'Completed',:value=> params[:PAYMENT_AMOUNT],:netsum => @real_cart_value.to_f, :transaction_id => @trans_id, :purchased_at => Time.now )
        @cartn =  Cart.generatecart1(@real_cart_value.to_f,@tx_found.id, @userid )
        @cartdet = Cart.where("id =?",@cartn).first
        @ubalance= UsersWalletsBalance.where("user_id = ? and currency= ?",@userid,'USD').first
        Watchdog.create(:user_id =>  @userid,:added_by => @userid,:controller => 'carts', :action_page =>"perfectmoneypost ",:foreign_id=>@cartn ,:details=> "pay money for cart with value  "+ @cartdet.value.to_s  )
        @tran_id = Transaction.create(:user_id =>  @userid,:foreign_id => @cartdet.id,:w_from_name => 'USD' ,:value_from => @cartdet.value ,:w_to_name => "perfectmoney", :controller =>"carts/perfectmoneypost",:transaction_id=>@trans_id,:description=> 'A credit card has been purchased/' + @cartdet.value.to_s + ' ' + 'USD')
        Notification.create(:user_id =>   @userid,:trans_id => @tran_id.id,:controller => 'carts/perfectmoneypost',:foreign_id => @cartn.to_s,:stats => 'A credit card has been purchased/' + @cartdet.value.to_s + ' ' + 'USD')  
        Chargebalance.payforcart( @user,@cartdet, @ubalance).deliver  
        flash[:alert] = t('A credit card has been purchased') + ' ' + @cartdet.value.to_s + ' USD' 
        redirect_to  cart_preview_path(:id=>@cartn)
        # else
        #   flash[:notice] = t("invalid")
        #     redirect_to root_path 
      
      end
      end
      end
    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => @userid,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def perfectmoneyerror

    flash[:alert] = t('Payment operation has been cancelled')
    redirect_to root_path , :alert => t('Payment operation has been cancelled')

  end 

  def webmoney

    begin
    @id = params[:id]
    rescue =>e
      @code = SecureRandom.hex
      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    
    end

  end

  def success

      begin
      # Coinpayment.test3(params.to_s).deliver
      rescue =>e
        @code = SecureRandom.hex

        @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
        redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
      end

  end

  def fail
  end
  
  def webmoneypost
  end

  def webmoneydone
  end

  def webmoneyfail
  end


    
  def onecard

  end

  def onecardpost
  end

  def show

    begin
    if current_userd.id != @onlinebank.user_id
      redirect_to account_settings_path, notice: 'you can not view this data.'
  else
    add_breadcrumb t('home'), :root_path
    add_breadcrumb t('Add Balance Request'), onlinebank_path , :title => "Back to the Index"
  	@bank_data = Transaction.where(["foreign_id = ? and controller in (?) " , @onlinebank.id,['Onlinebanks/banktransfer_post']]).first
  	 @name = "Onlinebanks"
    @request_updaes = Notification.where("foreign_id = :id and controller like :name" ,{:id => @onlinebank.id,:name => "%#{@name}%"}).all.order('created_at DESC')
  	@payment_exception = Ticket.where(foreign_id: @onlinebank.id ,ticket_id: 0).all.order('created_at DESC')
    @payment_replies = Ticket.where(foreign_id: @onlinebank.id).all.order('created_at ASC')
    end

  rescue =>e
    @code = SecureRandom.hex

    @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
    redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
  end

  end

  def new

    begin
    @onlinebank = Onlinebank.new

    rescue =>e
      @code = SecureRandom.hex

      @log =  ErrorsLog.create(:userd_id => current_userd.id,:error_msg => e.message.to_s,:error_code => @code.to_s) 
      redirect_to controller: 'frontendpages', action: 'error_page', id: @log.id
    end

  end

  def edit
  end

  def create
    # @onlinebank = Onlinebank.new(onlinebank_params)

    # respond_to do |format|
    #   if @onlinebank.save
    #     format.html { redirect_to @onlinebank, notice: 'Onlinebank was successfully created.' }
    #     format.json { render :show, status: :created, location: @onlinebank }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @onlinebank.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  def update
    # respond_to do |format|
    #   if @onlinebank.update(onlinebank_params)
    #     format.html { redirect_to @onlinebank, notice: 'Onlinebank was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @onlinebank }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @onlinebank.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  def destroy
    # @onlinebank.destroy
    # respond_to do |format|
    #   format.html { redirect_to onlinebanks_url, notice: 'Onlinebank was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_onlinebank
      @onlinebank = Onlinebank.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def onlinebank_params
      params.require(:onlinebank).permit(:user_id, :service_id,:attachments, :transaction,:national_id,:account_name,:account_number,:branch,:name ,:currency, :value,:paid_value, :refund,  :service_id ,:user_bank_id ,:company_bank_id,:bank_name,:transaction_id,:cardNumber, :cardCVV, :cardExpiryMonth, :cardExpiryYear,:responseMessage, :responseCode ,:purchased_at, :status , :approved , :created_at,:avatar,:bank_category,:bank_subcategory)
    end

end
