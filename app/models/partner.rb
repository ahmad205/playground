class Partner < ActiveRecord::Base
	# belongs_to :userd ,:foreign_key => "user_id"

	 attr_accessible  :name, :ratio, :usd_balance, :sar_balance, :egp_balance
	#    validates :user_id, presence: true, uniqueness: {message: "already exist"},:allow_nil => true, :allow_blank => true


		 def percent_of(n)
			self.to_f / n.to_f * 100.0
		end


		 def self.getbalance(ratio)

			@usdbalance = CompanyBalance.where(:currency => "USD" , :status => true).sum(:balance)
			@usdbalance2 = CompanyBalance.where(:currency => "USD" , :status => false).sum(:balance)
			@sarbalance = CompanyBalance.where(:currency => "SAR", :status => true).sum(:balance)
			@sarbalance2 = CompanyBalance.where(:currency => "SAR", :status => false).sum(:balance)
			@egpbalance = CompanyBalance.where(:currency => "EGP", :status => true).sum(:balance)
			@egpbalance2 = CompanyBalance.where(:currency => "EGP", :status => false).sum(:balance)
			
			@netcompany_balancessumusd = (@usdbalance - @usdbalance2)
			@netcompany_balancessumrys = (@sarbalance - @sarbalance2)
			@netcompany_balancessumegp = (@egpbalance - @egpbalance2)

			@ratio = ratio.to_f/100

		
			@finalusd = 	@netcompany_balancessumusd.to_f * @ratio
			@finalsar = 	@netcompany_balancessumrys.to_f *  @ratio
			@finalegp = 	@netcompany_balancessumegp.to_f * @ratio



				return [@finalusd,@finalsar,@finalegp]



		 end
end
