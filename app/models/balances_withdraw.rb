class BalancesWithdraw < ActiveRecord::Base
  belongs_to :users_bank
  has_one :balances_withdraws_status
 attr_accessor :account_type,:selectedcountry,:type,:type2,:account_numbersw,:gauth_token,:accountnumbereg ,:previous_user_bank,:countrysw,:countryy,:citysw,:addresssw,:birth_date,:banksw,:bankegname,:branchsw,:postalcodesw,:countrycodesw,:swiftcode,:countryib,:countrycodeib,:account_numberib,:bank_codeib,:iban,:bban,:check_digit,:checksum,:currencyib,:currencysw,:sepa,:bankib,:personal_name,:business_name,:account_type,:relation,:nickname,:iban_validity,:iban_checksum ,:iban_length ,:iban_structure, :account_checksum,:brancheg,:branchnumber,:nationalid,:currencies_withdraw_ratios_id
 
 
 validates :thesum,:netsum, numericality: { greater_than: 0}
 validates :hint	, :length => { :maximum => 500 }

   
 


def self.checklimit(withdraw,value,currency,user_id)

        @current_user = Userd.find_by_id(user_id)
        @usergroup_limit= UsersGroup.find_by_id(@current_user.group_id.to_i)
        @egptousd =    Setting.where(key:'EGP TO USD').first
        @rstousd =    Setting.where(key:'SAR TO USD').first

        @messege = ''
        @currency = currency
        @withdraw_value = value.to_f
        @egptousd = @egptousd.value.to_f
        @rstousd = @rstousd.value.to_f
        @withdraw_type = withdraw.to_i

        @wallet = UsersWalletsBalance.where(user_id: @current_user.id.to_i , currency: @currency).first
        @ratio = BalancesWithdrawsRatio.where(users_group_id: @usergroup_limit.id,currencies_withdraw_ratio_id: @withdraw_type).first
        @expenses = (@withdraw_value * @ratio.ratio.to_f/100) + @ratio.fees.to_f

        if @currency == "USD"
            @check_value = @withdraw_value
            @balance = @wallet.balance
        elsif @currency == "EGP"
            @check_value = (@withdraw_value * @egptousd)
            @balance = (@wallet.balance * @egptousd)
        elsif @currency == "SAR"
            @check_value = (@withdraw_value * @rstousd)
            @balance = (@wallet.balance * @rstousd)
        end

        if ((@expenses) >=  @withdraw_value )
            @messege  = @messege +  ' - '+  I18n.t("Adminstrative expenses greater than the value")
        end

        if ((@check_value.to_f) >  @balance )
            @messege  = @messege +  ' - '+  I18n.t("You have no enough balance")
        end

        if ((@current_user.d_withdraw_value.to_f + @check_value.to_f) > @usergroup_limit.day_max_withdraw )
            @messege  = @messege +  ' - '+  I18n.t("You have exceeded the amount allowed today")
        end

        if ((@current_user.m_withdraw_value.to_f + @check_value.to_f) > @usergroup_limit.month_max_withdraw )
            @messege  = @messege +  ' - '+ I18n.t("You have exceeded the amount allowed this month")
        end

        if ((@check_value.to_f) > @usergroup_limit.max_withdraw_each_time )
            @messege  = @messege +  ' - '+ I18n.t("You have exceeded the amount allowed per every withdraw")
        end

        if ((@check_value.to_f) < @usergroup_limit.minimum_withdraw )
            @messege  = @messege +  ' - '+ I18n.t("The amount is less than the minimum allowed")
        end

        return   @messege

end



#this function responsible for withdraw from these categories 
# 1- Saudia Arabia (ibann)
# 2- Egyptian Pound (it has it's own data)
# 3- Dollar (including Both Ibann Countries & swift Countries)
def self.savebalancewithdraw(paramss,user_id,transaction_id,currency)

    ActiveRecord::Base.transaction do

        #the user do the withdraw process
        @user = Userd.where(:id => user_id).lock('LOCK IN SHARE MODE').first
        
        @key_rate =  currency.to_s + ' '+'TO USD'
        #the rate from the coming currency to USD
        @therate =    Setting.where(key: @key_rate).first
        if @therate != nil 
            @rate_value = @therate.value.to_f
        else
            @rate_value = 1 
    
        end


        #the current wallet to withdraw from according to currency
        @wallet = UsersWalletsBalance.where("user_id = ? and currency = ?",@user.id,currency).lock('LOCK IN SHARE MODE').first
        
        #the selected bank to withdraw on it
        @currencies_withdraw_ratios = CurrenciesWithdrawRatio.where("id = ?",paramss[:balances_withdraw][:currencies_withdraw_ratios_id].to_i).first                  
        
        @UsersBank=UsersBank.new 
        @balances_withdraw=BalancesWithdraw.new
        
        # a setting for the expected date the amount increased set from this setting
        @withdraw_date = Setting.where(key:'withdraw_date').first
        @transaction_id = transaction_id
              

        
          
        ###start_with draw the balance from you wallet
        @balances_withdraw.wallet_id=@wallet.id
        @oldbalance=@wallet.balance.to_f
        @newbalance=(@wallet.balance.to_f)-(paramss[:balances_withdraw][:thesum].to_f)
        @wallet.update(balance: @newbalance.to_f )
        ####end_with draw the balance from you wallet


        #if a previous user bank was selected
        if (paramss[:balances_withdraw][:previous_user_bank] != '')
            @balances_withdraw.transaction_id = @transaction_id
            @balances_withdraw.user_id = @user.id
            @balances_withdraw.user_bank_id = paramss[:balances_withdraw][:previous_user_bank]
            @balances_withdraw.thesum = paramss[:balances_withdraw][:thesum]
            
            #adminstrative expenses to be paid by the user
            @expenses = BalancesWithdrawsRatio.getexpenses(paramss[:balances_withdraw][:currencies_withdraw_ratios_id].to_i,paramss[:balances_withdraw][:thesum].to_f,@user)
            
            #the net sum which is actually withdrawed by the user after the adminstrative expenses
            @balances_withdraw.netsum = (paramss[:balances_withdraw][:thesum].to_f) - (@expenses.to_f)
            
            #include the date now increasing the setting of days to be expected to end the balance withdraw
            @balances_withdraw.expect_date = Time.now + @withdraw_date.value.to_i.days
            
            @balances_withdraw.save
            @UsersBank = UsersBank.find_by_id(paramss[:balances_withdraw][:previous_user_bank])
            
            #save the data of the bank the withdraw happened on it so that if the user bank is edited the actual data of withdraw bank still found
            #WithdrawsBank.create(:user_id => @UsersBank.userd_id,:bank_id => @UsersBank.id,:withdraw_id=> @balances_withdraw.id,:currencies_withdraw_ratios_id => @UsersBank.currencies_withdraw_ratios_id,:personal_name => @UsersBank.personal_name,:business_name => @UsersBank.business_name ,:nationalid => @UsersBank.nationalid ,:birth_date => @UsersBank.birth_date ,:relation => @UsersBank.relation ,:nickname => @UsersBank.nickname ,:bank_name => @UsersBank.bank_name,:country => @UsersBank.country ,:country_code => @UsersBank.country_code,:branch => @UsersBank.branch,:branchnumber => @UsersBank.branchnumber,:phone => @UsersBank.phone,:account_name => @UsersBank.account_name,:account_email => @UsersBank.account_email,:account_number => @UsersBank.account_number,:visa_cvv => @UsersBank.visa_cvv,:swiftcode => @UsersBank.swiftcode,:valid_swift_code => @UsersBank.valid_swift_code,:expire_date => @UsersBank.expire_date,:status => @UsersBank.status ,:city => @UsersBank.city,:address => @UsersBank.address ,:postal_code => @UsersBank.postal_code,:iban => @UsersBank.iban,:bban => @UsersBank.bban,:checksum => @UsersBank.checksum,:bank_code => @UsersBank.bank_code,:check_digit => @UsersBank.check_digit,:sepa => @UsersBank.sepa,:iban_validity => @UsersBank.iban_validity,:iban_checksum => @UsersBank.iban_checksum,:iban_length => @UsersBank.iban_length,:iban_structure => @UsersBank.iban_structure,:account_checksum => @UsersBank.account_checksum,:currency=> @UsersBank.currency)
            
            #a status of the balance withdraw
            #BalancesWithdrawsStatus.create(:balances_withdraws_id => @balances_withdraw.id,:status => 'created', :user_id =>@balances_withdraw.user_id,:user_bank_id=>@balances_withdraw.user_bank_id  )
  
       end


       
    #    **************************************************
   
    #   Here The Other Case When Select to add new user Bank
       
    #    ***************************************************

        #######################################new account########################################################
       #if a previous user bank wasn't selected so we create a new user bank
if (paramss[:balances_withdraw][:previous_user_bank] == '')

            @UsersBank.userd_id = @user.id
            @balances_withdraw=BalancesWithdraw.new

            @UsersBank.currencies_withdraw_ratios_id = paramss[:balances_withdraw][:currencies_withdraw_ratios_id]
            @UsersBank.relation = paramss[:balances_withdraw][:relation]
            @UsersBank.personal_name = paramss[:balances_withdraw][:personal_name]
            @UsersBank.business_name = paramss[:balances_withdraw][:business_name]
            @UsersBank.nickname = paramss[:balances_withdraw][:nickname]
            @UsersBank.nationalid = paramss[:balances_withdraw][:nationalid]
            @UsersBank.birth_date = paramss[:balances_withdraw][:birth_date]
            
            #differentiate between two types of user banks .. banks of withdraw & banks of charging the balance with carts
            @UsersBank.bank_type = "withdraw"

            if (paramss[:balances_withdraw][:country]== 'EG')   #Egyption_Pound local case
              @UsersBank.bank_name = paramss[:balances_withdraw][:bankegname]
              @UsersBank.branch = paramss[:balances_withdraw][:brancheg]
              @UsersBank.branchnumber = paramss[:balances_withdraw][:branchnumber]
              @UsersBank.account_number = paramss[:balances_withdraw][:accountnumbereg]
              @UsersBank.country_code = "EG"
              @UsersBank.currency = 'EGP'

            end
          if (paramss[:balances_withdraw][:swiftcode].present?) #Swift Countries
        @UsersBank.assign_attributes(:bank_name => paramss[:balances_withdraw][:banksw], 
                                     :country => paramss[:balances_withdraw][:countrysw],
                                     :country_code => paramss[:balances_withdraw][:countrycodesw],
                                     :city => paramss[:balances_withdraw][:citysw],
                                     :swiftcode => paramss[:balances_withdraw][:swiftcode],
                                     :valid_swift_code => paramss[:balances_withdraw][:validsw],
                                     :account_number => paramss[:balances_withdraw][:account_number],
                                     :branch => paramss[:balances_withdraw][:branchsw],
                                     :address => paramss[:balances_withdraw][:addresssw],
                                     :currency => 'USD',

                                     )


        elsif (paramss[:balances_withdraw][:iban].present?) #iban countries

        @UsersBank.assign_attributes(:iban_validity => paramss[:balances_withdraw][:iban_validity],
                                    :iban_checksum => paramss[:balances_withdraw][:iban_checksum],
                                    :iban_length => paramss[:balances_withdraw][:iban_length],
                                    :iban_structure => paramss[:balances_withdraw][:iban_structure],
                                    :account_checksum => paramss[:balances_withdraw][:account_checksum],
                                    :iban => paramss[:balances_withdraw][:iban],
                                    :bban => paramss[:balances_withdraw][:bban],
                                    :checksum => paramss[:balances_withdraw][:checksum],
                                    :check_digit => paramss[:balances_withdraw][:check_digit],
                                    :sepa => paramss[:balances_withdraw][:sepa],
                                    :currencies_withdraw_ratios_id => @currencies_withdraw_ratios.id,
                                    :country => paramss[:balances_withdraw][:countryib],                                    
                                    :country_code => paramss[:balances_withdraw][:countrycodeib],
                                    :account_number => paramss[:balances_withdraw][:account_numberib],
                                    :bank_name => paramss[:balances_withdraw][:bankib],
                                    :bank_code => paramss[:balances_withdraw][:bank_codeib]

                                    )
        @UsersBank.currency = 'USD'
        if (paramss[:balances_withdraw][:country]== 'SA')
          @UsersBank.currency = 'SAR'
        end

       
          end
        end
#############################################end new account#############################################
           
################################################################################
            if @UsersBank.save

                Notification.create(:user_id => @user.id,:controller => 'users_bank',:foreign_id => @UsersBank.id.to_s ,:stats => 'you have added new bank for/' + @UsersBank.bank_name.to_s )
                Watchdog.create(:user_id => @user.id,:added_by => @user.id,:controller => 'users_bank', :action_page =>"save",:foreign_id=>@UsersBank.id.to_s ,:details=> 'you have added new bank for/' + @UsersBank.bank_name.to_s)
                
                @balances_withdraw.transaction_id = @transaction_id
                @balances_withdraw.user_id = @user.id
                @balances_withdraw.user_bank_id = @UsersBank.id
                @balances_withdraw.thesum = paramss[:balances_withdraw][:thesum]
                @expenses = BalancesWithdrawsRatio.getexpenses(paramss[:balances_withdraw][:currencies_withdraw_ratios_id].to_i,paramss[:balances_withdraw][:thesum].to_f,@user)
                @balances_withdraw.netsum = (paramss[:balances_withdraw][:thesum].to_f) - (@expenses.to_f)
                @balances_withdraw.wallet_id=@wallet.id
                @balances_withdraw.expect_date = Time.now + @withdraw_date.value.to_i.days
                @balances_withdraw.save

                 #save the data of the bank the withdraw happened on it so that if the user bank is edited the actual data of withdraw bank still found              
                WithdrawsBank.create(:user_id => @UsersBank.userd_id,:bank_id => @UsersBank.id,:withdraw_id=> @balances_withdraw.id,:currencies_withdraw_ratios_id => @UsersBank.currencies_withdraw_ratios_id,:personal_name => @UsersBank.personal_name,:business_name => @UsersBank.business_name ,:nationalid => @UsersBank.nationalid ,:birth_date => @UsersBank.birth_date ,:relation => @UsersBank.relation ,:nickname => @UsersBank.nickname ,:bank_name => @UsersBank.bank_name,:country => @UsersBank.country ,:country_code => @UsersBank.country_code,:branch => @UsersBank.branch,:branchnumber => @UsersBank.branchnumber,:phone => @UsersBank.phone,:account_name => @UsersBank.account_name,:account_email => @UsersBank.account_email,:account_number => @UsersBank.account_number,:visa_cvv => @UsersBank.visa_cvv,:swiftcode => @UsersBank.swiftcode,:valid_swift_code => @UsersBank.valid_swift_code,:expire_date => @UsersBank.expire_date,:status => @UsersBank.status ,:city => @UsersBank.city,:address => @UsersBank.address ,:postal_code => @UsersBank.postal_code,:iban => @UsersBank.iban,:bban => @UsersBank.bban,:checksum => @UsersBank.checksum,:bank_code => @UsersBank.bank_code,:check_digit => @UsersBank.check_digit,:sepa => @UsersBank.sepa,:iban_validity => @UsersBank.iban_validity,:iban_checksum => @UsersBank.iban_checksum,:iban_length => @UsersBank.iban_length,:iban_structure => @UsersBank.iban_structure,:account_checksum => @UsersBank.account_checksum,:currency=> @UsersBank.currency)
                
                #a status of the balance withdraw               
                BalancesWithdrawsStatus.create(:balances_withdraws_id => @balances_withdraw.id,:status => 'created', :user_id =>@balances_withdraw.user_id,:user_bank_id=>@balances_withdraw.user_bank_id  )
            end

                Transaction.create(:transaction_id => @transaction_id , :foreign_id => @balances_withdraw.id ,:user_id => @user.id,:value_from => @balances_withdraw.thesum.to_f, :w_from_name=> @wallet.currency,:w_from_balance_before => @oldbalance,:w_from_balance_after => @wallet.balance,:value_to =>@balances_withdraw.netsum.to_f , :controller => 'balances_withdraws/created',:description => 'withdraw request/'+ @balances_withdraw.thesum.to_s + '  '+ @wallet.currency+ '/Transaction ID/' +@transaction_id.to_s )
            @tran_id = Transaction.where("transaction_id = ?", @transaction_id).first
            @req_id = BalancesWithdraw.where("id =?", @tran_id.foreign_id).first
            @ubank = UsersBank.where("id =?",@req_id.user_bank_id).first
                if (@ubank != nil)
                @curr_id = CurrenciesWithdrawRatio.where("id = ?",@ubank.currencies_withdraw_ratios_id).first
                @tran_id.update(:w_to_name => @curr_id.name)
                end
            
            CompanyBalance.create(:user_id => @user.id,:operation_id => '4',:trans_id => @tran_id.id,:balance => @expenses,:currency => @wallet.currency ,:status => '1')
            
            #notify user of new balancw with draw added to his account
            Notification.create(:user_id => @user.id,:trans_id => @tran_id.id,:controller => 'balances_withdraws',:foreign_id => @balances_withdraw.id.to_s,:stats => 'withdraw request/'+ @balances_withdraw.thesum.to_s + '  '+ @wallet.currency+ '/Transaction ID/' +@transaction_id.to_s  )
            
            #log of new  balancw withdraw to our systems
            Watchdog.create(:user_id => @user.id,:added_by => @user.id,:controller => 'balances_withdraws', :action_page =>"save",:foreign_id=> @balances_withdraw.id.to_s ,:details=> 'withdraw request/'+ @balances_withdraw.thesum.to_s + '  '+ @wallet.currency+ '/Transaction ID/' +@transaction_id.to_s)
            
            #there is a setting to have maximum no of withdraw process each day and a maximum value to be withdrawed so this update the limit
            @user.update(d_withdraw_count: (@user.d_withdraw_count+1),d_withdraw_value: (@user.d_withdraw_value.to_f+@balances_withdraw.thesum.to_f * @rate_value),m_withdraw_count: (@user.m_withdraw_count+1),m_withdraw_value: (@user.m_withdraw_value.to_f+@balances_withdraw.thesum.to_f * @rate_value))



                #notify user by email about new balance withdraw
                BalancewithdrawMail.withdraw(@user,@balances_withdraw,@wallet.wallet_name,"create").deliver

            end
#################################################################

        return @balances_withdraw

end




##it include a withdraw for these categories 
# 1- Western Union Bank
# 2- Money Gram Bank
# 3- Egyptian Mail
     def  self.balancewithdraw(paramss,user_id,transaction_id,currency,country)


        ActiveRecord::Base.transaction do

            
            @user = Userd.where(:id => user_id).lock('LOCK IN SHARE MODE').first
            
            #the current wallet to withdraw from according to currency
            @wallet = UsersWalletsBalance.where("user_id = ? and currency = ?",@user.id,currency).lock('LOCK IN SHARE MODE').first

            @key_rate =  currency.to_s + ' '+'TO USD'
            #the rate from the coming currency to USD
            @therate =    Setting.where(key: @key_rate).first
            if @therate != nil 
                @rate_value = @therate.value.to_f
            else
                @rate_value = 1 
        
            end



            #the selected bank to withdraw on it
            @currencies_withdraw_ratios = CurrenciesWithdrawRatio.where("id = ?",paramss[:balances_withdraw][:currencies_withdraw_ratios_id].to_i).first
            
            @UsersBank=UsersBank.new
            @balances_withdraw=BalancesWithdraw.new   
            
            # a setting for the expected date the amount increased set from this setting
            @withdraw_date = Setting.where(key:'withdraw_date').first
            @transaction_id = transaction_id


            ###start_with draw the balance from you wallet
            @balances_withdraw.wallet_id=@wallet.id
            @oldbalance=@wallet.balance.to_f
            @newbalance=(@wallet.balance.to_f)-(paramss[:balances_withdraw][:thesum].to_f)
            @wallet.update(balance: @newbalance.to_f )
            ####end_with draw the balance from you wallet

                     
            @balances_withdraw.thesum = paramss[:balances_withdraw][:thesum].to_f
            @balances_withdraw.user_id = @user.id
            @balances_withdraw.transaction_id = @transaction_id
            @expenses = BalancesWithdrawsRatio.getexpenses(paramss[:balances_withdraw][:currencies_withdraw_ratios_id].to_i,paramss[:balances_withdraw][:thesum].to_f,@user)
            @balances_withdraw.netsum = (paramss[:balances_withdraw][:thesum].to_f) - (@expenses.to_f)


                #if you add new user bank
            if (paramss[:balances_withdraw][:previous_user_bank] == '')

            #the data set to the new user bank
            @UsersBank.assign_attributes(:userd_id => @user.id, 
                           :personal_name => paramss[:balances_withdraw][:personal_name],
                           :relation => paramss[:balances_withdraw][:relation],
                           :nickname => paramss[:balances_withdraw][:nickname],
                           :business_name => paramss[:balances_withdraw][:business_name],
                           :nationalid => paramss[:balances_withdraw][:nationalid],
                           :birth_date => paramss[:balances_withdraw][:birth_date],
                           :branch => paramss[:balances_withdraw][:brancheg],
                           :account_number => paramss[:balances_withdraw][:account_numbersw],
                           :currency => currency,
                           :country => country,
                           :bank_type => "withdraw",
                           :bank_name => @currencies_withdraw_ratios.name,
                           :currencies_withdraw_ratios_id => @currencies_withdraw_ratios.id,
                               )

                
            if @UsersBank.save

                #after adding the user bank it's time for the balance with draw which is connected to the new user bank created above
                @balances_withdraw.user_bank_id = @UsersBank.id
                @balances_withdraw.transaction_id = @transaction_id
                @balances_withdraw.expect_date = Time.now + @withdraw_date.value.to_i.days
                @balances_withdraw.save
               
                #save the data of the bank the withdraw happened on it so that if the user bank is edited the actual data of withdraw bank still found
                WithdrawsBank.create(:user_id => @UsersBank.userd_id,:bank_id => @UsersBank.id,:withdraw_id =>@balances_withdraw.id, :bank_name => @UsersBank.bank_name,:currencies_withdraw_ratios_id => @UsersBank.currencies_withdraw_ratios_id,:personal_name => @UsersBank.personal_name,:business_name => @UsersBank.business_name ,:nationalid => @UsersBank.nationalid ,:branch => @UsersBank.branch ,:account_number => @UsersBank.account_number,:birth_date => @UsersBank.birth_date,:nickname => @UsersBank.nickname,:relation => @UsersBank.relation ,:country => @UsersBank.country,:currency => @UsersBank.currency )    
                
                #notify user of new bank added to his account
                Notification.create(:user_id => @user.id,:controller => 'users_bank',:foreign_id => @UsersBank.id ,:stats => 'you have added new bank for/' + @UsersBank.bank_name.to_s )
                
                #log of new bank to our systems
                Watchdog.create(:user_id => @user.id,:added_by => @user.id,:controller => 'users_bank', :action_page =>"save_western_gram",:foreign_id=>@UsersBank.id ,:details=> 'you have added new bank for/' + @UsersBank.bank_name.to_s)
                
                #a status of the balance withdraw
                BalancesWithdrawsStatus.create(:balances_withdraws_id => @balances_withdraw.id,:status => 'created', :user_id =>@balances_withdraw.user_id,:user_bank_id=> @UsersBank.id)
               
            end
                #end if you add new user bank


           
          #case of previous bank selected
              elsif (paramss[:balances_withdraw][:previous_user_bank] != '')
            @balances_withdraw.user_bank_id = paramss[:balances_withdraw][:previous_user_bank]
            @balances_withdraw.transaction_id = @transaction_id
            @balances_withdraw.expect_date = Time.now + @withdraw_date.value.to_i.days
      
            @balances_withdraw.save
            # session[:balancewithdraw] = 1
            @UsersBank= UsersBank.find_by_id(paramss[:balances_withdraw][:previous_user_bank])
            WithdrawsBank.create(:user_id => @UsersBank.userd_id,:bank_id => @UsersBank.id,:withdraw_id =>@balances_withdraw.id, :bank_name => @UsersBank.bank_name,:currencies_withdraw_ratios_id => @UsersBank.currencies_withdraw_ratios_id,:personal_name => @UsersBank.personal_name,:business_name => @UsersBank.business_name,:nationalid => @UsersBank.nationalid ,:branch => @UsersBank.branch ,:account_number => @UsersBank.account_number,:birth_date => @UsersBank.birth_date,:nickname => @UsersBank.nickname,:relation => @UsersBank.relation ,:country => @UsersBank.country,:currency => @UsersBank.currency )  
            BalancesWithdrawsStatus.create(:balances_withdraws_id => @balances_withdraw.id,:status => 'created', :user_id =>@balances_withdraw.user_id,:user_bank_id=>paramss[:balances_withdraw][:previous_user_bank] )
             end
           #end case of previous bank selected

           #transaction of balancw withdraw process
           @tran_id = Transaction.create(:w_to_name => @currencies_withdraw_ratios.name,:transaction_id => @transaction_id , :foreign_id => @balances_withdraw.id ,:user_id => @user.id,:value_from => @balances_withdraw.thesum.to_f, :w_from_name=> currency,:w_from_balance_before => @oldbalance,:w_from_balance_after => @wallet.balance,:value_to =>@balances_withdraw.netsum.to_f , :controller => 'balances_withdraws/created',:description => 'withdraw request/'+ @balances_withdraw.thesum.to_s + '  '+currency + " "+ '/Transaction ID/' + @transaction_id.to_s )
          
           #responsible for company profit     
           CompanyBalance.create(:user_id => @user.id,:operation_id => '4',:trans_id => @tran_id.id,:balance => @expenses,:currency => currency ,:status => '1')
           
           #notify user for new balance withdraw  
           Notification.create(:user_id => @user.id,:trans_id => @tran_id.id,:controller => 'balances_withdraws',:foreign_id => @balances_withdraw.id.to_s,:stats => 'withdraw request/'+ @balances_withdraw.thesum.to_s + currency.to_s + '/Transaction ID/' +@transaction_id.to_s)
          
           #log for the system
           Watchdog.create(:user_id => @user.id,:added_by => @user.id,:controller => 'balances_withdraws', :action_page =>"save_western_gram",:foreign_id=> @balances_withdraw.id.to_s ,:details=> 'withdraw request/'+ @balances_withdraw.thesum.to_s + currency.to_s + '/Transaction ID/' +@transaction_id.to_s)
           

           if currency == 'EGP'
             @currency='Egyptian Pound'

            elsif currency == 'USD'
            @currency = 'Dollar'

            end
            #there is a setting to have maximum no of withdraw process each day and a maximum value to be withdrawed so this update the limit
            @user.update(d_withdraw_count: (@user.d_withdraw_count+1),d_withdraw_value: (@user.d_withdraw_value.to_f + (@balances_withdraw.thesum.to_f * @rate_value)),m_withdraw_count: (@user.m_withdraw_count+1),m_withdraw_value: (@user.m_withdraw_value.to_f + (@balances_withdraw.thesum.to_f * @rate_value)))  
            
            #notify user by email about new balance withdraw
            BalancewithdrawMail.withdraw(@user,@balances_withdraw,@currency,"create").deliver
        end

         return @balances_withdraw
        

     end


#cancel withdraw by user if it was in creation status only
def self.cancelwithdraw(params,user_id)

   
    ActiveRecord::Base.transaction do

    #select user 
    @user = Userd.where(:id => user_id).lock('LOCK IN SHARE MODE').first
    
    

    #select balancewithdraw
    @balancewithdraw = BalancesWithdraw.find_by(id: params[:id])  
    #select balancewithdraw status 
    @balancewithdrawstatus = BalancesWithdrawsStatus.find_by(user_id: @balancewithdraw.user_id,balances_withdraws_id: @balancewithdraw.id )
    #select wallet of user
    @wallet=UsersWalletsBalance.where(id:@balancewithdraw.wallet_id ).lock('LOCK IN SHARE MODE').first
    
    @key_rate =  @wallet.currency.to_s + ' '+'TO USD'
    #the rate from the coming currency to USD
    @therate =    Setting.where(key: @key_rate).first
    if @therate != nil 
        @rate_value = @therate.value.to_f
    else
        @rate_value = 1 

    end

    @oldbalance = @wallet.balance
    

    #return back balance after cancel
    @backbalance=@wallet.balance.to_f+@balancewithdraw.thesum.to_f
    @wallet.update(balance:@backbalance)
    
    #create transaction of cancel the balancewithdraw
    @transaction_id = Transaction.gettransaction
    @tran_id = Transaction.create(:transaction_id => @transaction_id , :foreign_id => @balancewithdraw.id ,:user_id => @user.id,:value_from => @balancewithdraw.thesum.to_f, :w_from_name=> @wallet.currency,:w_from_balance_before => @oldbalance,:w_from_balance_after => @backbalance,:value_to =>@balancewithdraw.netsum.to_f , :controller => 'balances_withdraws/canceled',:description => 'balancewithdraw canceled/'+ @balancewithdraw.thesum.to_s + '  '+ @wallet.currency+ '/Transaction ID/ ' +@transaction_id.to_s )      
    #update status of balancewithdraw to canceled
    @balancewithdraw.update(:status => "canceled")
    #create new balancewithdraw with the new status
    BalancesWithdrawsStatus.create(:balances_withdraws_id =>@balancewithdraw.id,:user_id => @user.id,:user_bank_id => @balancewithdraw.user_bank_id ,:status => "canceled")
    #return the profit of the company
    @company_ratio =  @balancewithdraw.thesum.to_f - @balancewithdraw.netsum.to_f
    CompanyBalance.create(:user_id => @balancewithdraw.user_id,:operation_id => '4',:trans_id => @tran_id.id,:balance => @company_ratio,:currency =>  @wallet.currency ,:status => '0')
    #notify user with the new balancewithdraw
    Notification.create(:user_id => @user.id,:added_by => @user.id ,:trans_id => @tran_id.id,:controller => 'balances_withdraws',:foreign_id => @balancewithdraw.id,:stats => 'balancewithdraw canceled/'+ @balancewithdraw.thesum.to_s + '  '+ @wallet.currency+ '/Transaction ID/' +@balancewithdraw.transaction_id    )
    #save the cancel operation in our log
    Watchdog.create(:user_id => @user.id,:added_by => @user.id,:controller => 'balances_withdraws', :action_page =>"cancel",:foreign_id=> @balancewithdraw.id ,:details=> 'balancewithdraw canceled/'+ @balancewithdraw.thesum.to_s + '  '+ @wallet.currency+ '/Transaction ID/' +@balancewithdraw.transaction_id)
    #notify user about cancel through email   
    BalancewithdrawMail.cancelwithdraw(@user,@balancewithdraw,@wallet.wallet_name,"cancel").deliver  
    
    
    
    @create_date = Date.parse(@balancewithdraw.created_at.strftime("%_m/%d"))
    @cancel_date =  Date.parse(@balancewithdraw.updated_at.strftime("%_m/%d"))
    @com = @cancel_date - @create_date
    if @com.to_i != 0
      @user.update(m_withdraw_count: @user.m_withdraw_count-1,
                   m_withdraw_value: @user.m_withdraw_value.to_f-(@balancewithdraw.thesum.to_f *@rate_value.to_f))
     else
      @user.update(d_withdraw_count: (@user.d_withdraw_count-1),d_withdraw_value: (@user.d_withdraw_value.to_f-(@balancewithdraw.thesum.to_f*@rate_value.to_f)),m_withdraw_count: (@user.m_withdraw_count-1),m_withdraw_value: (@user.m_withdraw_value.to_f-(@balancewithdraw.thesum.to_f*@rate_value.to_f)))
      end 
      
    end
    return @balancewithdraw



end





end

