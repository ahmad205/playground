class UsersWalletsBalance < ActiveRecord::Base

	belongs_to :user
	has_many :balance_action
	attr_accessor :type_conversion_from ,:type_conversion_to,:value

	def self.wallettransfer( userid, from, to, value)

		@userid = userid
		@from = from.to_s
		@to = to.to_s
		@value = value.to_f

		@usdtoegp = Setting.where(key:'USD TO EGP').first
		@usdtors = Setting.where(key:'USD TO SAR').first

	    @egptousd = Setting.where(key:'EGP TO USD').first
	    @egptors = Setting.where(key:'EGP TO SAR').first
	                          
	    @rstoegp = Setting.where(key:'SAR TO EGP').first
	    @rstousd = Setting.where(key:'SAR TO USD').first

	    @uni_usdtoegp = Setting.where(key:'Universal USD TO EGP').first
	    @uni_usdtors = Setting.where(key:'Universal USD TO SAR').first

        @uni_egptousd = Setting.where(key:'Universal EGP TO USD').first
        @uni_egptors = Setting.where(key:'Universal EGP TO SAR').first

        @uni_rstoegp = Setting.where(key:'Universal SAR TO EGP').first
        @uni_rstousd = Setting.where(key:'Universal SAR TO USD').first

		if @from == 'USD' && @to == 'EGP'
	  		@ratio = @usdtoegp.value.to_f
	  		@univ_ratio = @uni_usdtoegp.value.to_f

	  	elsif @from == 'USD' && @to == 'SAR'
	      	@ratio = @usdtors.value.to_f
	      	@univ_ratio = @uni_usdtors.value.to_f


	    elsif @from == 'EGP' && @to == 'USD'
	      	@ratio = @egptousd.value.to_f
	      	@univ_ratio = @uni_egptousd.value.to_f

	    elsif @from == 'EGP' && @to == 'SAR'
	      	@ratio = @egptors.value.to_f
	      	@univ_ratio = @uni_egptors.value.to_f


	    elsif @from == 'SAR' && @to == 'EGP'
	      	@ratio = @rstoegp.value.to_f
	      	@univ_ratio = @uni_rstoegp.value.to_f

	    elsif @from == 'SAR' && @to == 'USD'
	      	@ratio = @rstousd.value.to_f
	      	@univ_ratio = @uni_rstousd.value.to_f

	    end

	    loop do
      		@trans_id = "WA"+[*('A'..'Z'),*('0'..'9')].to_a.shuffle[0,16].join
      		break @trans_id unless Transaction.where(transaction_id: @trans_id).exists?
    	end 
    	@user = Userd.find_by_id(@userid)
	    @wallet_from = UsersWalletsBalance.where(["user_id = ? and currency = ?" , @userid , @from]).lock('LOCK IN SHARE MODE').first
    	@wallet_to = UsersWalletsBalance.where(["user_id = ? and currency = ?" , @userid , @to]).lock('LOCK IN SHARE MODE').first
    	@bal_wal = @wallet_from.balance

    	# condition currency_from or currency_to not selected
	    if @from == ''  or @to == ''
      		@message  = I18n.t("select wallet from and wallet to")

    	# condition value to be transfered less than 1
    	elsif @value < 1
    		@message  = I18n.t("the value must be greater than one")
    	
    	# condition balance of wallet will transfered from greater than value to be transfered	
    	elsif @bal_wal >= @value
			
    		@wallet_from_before = @wallet_from.balance
    		@wallet_to_before = @wallet_to.balance
    		@value_from = (@wallet_from.balance.to_f) - (@value)
      		@value_to = @value * (@ratio)
      		@ubalanceEg = (@wallet_to.balance.to_f) + @value_to.to_f
      		@message  = I18n.t('you have replaced') + " #{@value.round(2)} " + " #{@from} " + I18n.t('with a value of') + " #{@value_to.round(2)} " + " #{@to} " + I18n.t('Transaction ID') + " #{@trans_id}" 

      		ActiveRecord::Base.transaction do
        		@wallet_from.update(:balance => @value_from)
        		@wallet_to.update(:balance => @ubalanceEg)
        	end

      		@tran_id = Transaction.create(:transaction_id => @trans_id ,:user_id => @userid,:value_from => @value,:w_from_name => @from,
          		:w_from_balance_before => @wallet_from_before,:w_from_balance_after => @wallet_from.balance ,:w_to_name => @to,:value_to => @value_to.to_f,
          		:w_to_balance_before => @wallet_to_before,:w_to_balance_after => @wallet_to.balance,:controller => 'users_wallets_balances/walletstransferpost',:description => 'you have replaced/' + @value.round(2).to_s + ' ' + @from + ' /with a value of/' + @value_to.round(2).to_s + ' ' + @to + ' /Transaction ID/' + @trans_id.to_s)
        
        	Notification.create(:user_id => @userid, :added_by => Userd.current.id,:trans_id => @tran_id.id, :controller => 'User_wallet_ballence/walletstransferpost',:stats => 'you have replaced/' +@value.round(2).to_s + ' ' + @from +  ' /with a value of/' + @value_to.round(2).to_s + ' ' + @to + ' /Transaction ID/' + @trans_id.to_s )
        	Watchdog.create(:user_id => @userid, :added_by => Userd.current.id , :controller => 'User_wallet_ballence',:foreign_id => @tran_id.id, :action_page => "walletstransferpost", :details => 'you have replaced/' +@value.round(2).to_s + ' ' + @from +  '/with a value of/' + @value_to.round(2).to_s + ' ' + @to + ' /Transaction ID/' + @trans_id.to_s )
        	@company_ratio = @value*(@univ_ratio - @ratio )
        
        	CompanyBalance.create(:user_id => @userid,:operation_id => '2',:trans_id => @tran_id.id,:balance => @company_ratio,:currency =>  @to ,:status => '1')
        	Wallet.balancetransfer(@user,@value,@from,@to,@value_to).deliver

    	else
        	@message  = I18n.t("your balance is less than the value")

    	end

	    return @message
	end


    def self.walletname(wallet_currency)

    	@wallet_currency = wallet_currency

        if @wallet_currency == "USD"
      		@wallet_name = 'American Dollar'

    	elsif @wallet_currency == "SAR"
      		@wallet_name = 'Saudi Riyal'

   		elsif @wallet_currency == "EGP"
      		@wallet_name = 'Egyptian Pound'
    
    	end

    	return @wallet_name

	end


	def self.searchwallettransfer( wallet_searchs, wallet_currency, searchdate )

		@wallet_searchs = wallet_searchs
		@wallet_currency = wallet_currency
		@searchdate = searchdate

		if @wallet_searchs != ""
      		@reports = Transaction.where("transaction_id = ?", @wallet_searchs ).all
    
    	else
      		if @searchdate != "" and @wallet_currency != ""
        		@reports = Transaction.where("created_at >= ? And controller = ? AND user_id = ? AND (w_to_name = ? or w_from_name = ?)",
        			@searchdate, "users_wallets_balances/walletstransferpost", Userd.current.id, @wallet_currency, @wallet_currency ).all.order('transactions.created_at DESC')

      		elsif @searchdate != "" and @wallet_currency == ""
    			@reports = Transaction.where("created_at >= ? And controller = ? AND user_id = ?", @searchdate, "users_wallets_balances/walletstransferpost", Userd.current.id).all.order('transactions.created_at DESC')

    		elsif @searchdate == "" and @wallet_currency != ""
    			@reports = Transaction.where("w_to_name = ? or w_from_name = ? And controller = ? AND user_id = ?", @wallet_currency, @wallet_currency, "users_wallets_balances/walletstransferpost", Userd.current.id ).all.order('transactions.created_at DESC')
      		end
    	end

	    return @reports

	end

	def self.searchcartcharge( cart_searchs, searchdate )

		@cart_searchs = cart_searchs
		@searchdate = searchdate

		@transactions = Transaction.joins("INNER JOIN carts u3 ON transactions.foreign_id = u3.id ")
                        .select("transactions.transaction_id as '0', transactions.w_to_name as '1', transactions.value_from as '2'
                                ,transactions.beforecharge as '3', transactions.w_to_balance_after as '4', transactions.created_at as '5'")
    
    	if @cart_searchs != ""
      		@rr = @transactions.where("transactions.user_id = ? AND transactions.transaction_id = ?", Userd.current.id, @cart_searchs ).all.order('transactions.created_at DESC')
                        
    	else
     		@rr = @transactions.where("transactions.user_id = ? AND transactions.created_at >= ? AND transactions.controller LIKE ?", Userd.current.id, @searchdate, 'carts/cartbalancetouser').all.order('transactions.created_at DESC')
    	end

	    return @rr

	end

	def self.searchbalancewithdraw( withdraw_type, withdraw_searchs, searchdate )

		@withdraw_type = withdraw_type
		@withdraw_searchs = withdraw_searchs
		@searchdate = searchdate

		if @withdraw_type == "recieved" or @withdraw_type == "workon" or @withdraw_type == "exception"
      		@status = "created"
    	else
      		@status = @withdraw_type
    	end

    	@transactions = Transaction.joins("INNER JOIN balances_withdraws u3 ON transactions.foreign_id = u3.id")
                        .select("transactions.transaction_id as '0', transactions.w_from_name as '1', transactions.w_to_name as '2'
                                ,transactions.value_from as '3', transactions.value_to as '4', transactions.w_from_balance_before as '5'
                                ,transactions.w_from_balance_after as '6', u3.status as '7', transactions.created_at as '8'")

	    if @withdraw_searchs != ""
	      	@rr = @transactions.where("transactions.user_id = ? AND transactions.transaction_id = ?", Userd.current.id, @withdraw_searchs).all.order('transactions.created_at DESC')
	                       
	    else
	    	if @searchdate != "" and @withdraw_type != ""
	        	@rr = @transactions.where("transactions.user_id = ? AND transactions.created_at >= ? AND u3.status = ? AND transactions.controller LIKE ?", Userd.current.id, @searchdate, @withdraw_type, "balances_withdraws/#{@status}").all.order('transactions.created_at DESC')
	      
	      	elsif @searchdate != "" and @withdraw_type == ""
	        	@rr = @transactions.where("transactions.user_id = ? AND transactions.created_at >= ? AND transactions.controller LIKE ?", Userd.current.id, @searchdate, "%balances_withdraws%").all.order('transactions.created_at DESC')
	      
	      	elsif @searchdate == "" and @withdraw_type != ""
	        	@rr = @transactions.where("transactions.user_id = ? AND u3.status = ? AND transactions.controller LIKE ?", Userd.current.id, @withdraw_type, "balances_withdraws/#{@status}").all.order('transactions.created_at DESC')
	                        
	      	end
	    end

	    return @rr

	end

	def self.searchbalancetransfer( balance_searchs, searchdate, balance_type )

		@balance_searchs = balance_searchs
		@searchdate = searchdate
		@balance_type = balance_type

		if @balance_type == "Confirmed Transfer" 
          	@controller_name = "users_wallets_ballences_frozens/approvedbalance"

        elsif @balance_type == "Rejected Transfer" 
			@controller_name = "users_wallets_ballences_frozens/create"

        elsif @balance_type == "UnConfirmed Transfer" 
          	@controller_name = "users_wallets_ballences_frozens/create"

        elsif @balance_type == "Received transfers"
        	@controller_name = "users_wallets_ballences_frozens/approvedbalance"

        elsif @balance_type == "Frozen transfers"
        	@controller_name = "users_wallets_ballences_frozens/create"
        end


    	@transactions = Transaction.joins("INNER JOIN users_wallets_balances_frozens u3 ON transactions.foreign_id = u3.id")
                              .joins("INNER JOIN userds u1 ON transactions.user_id = u1.id")
                              .joins("INNER JOIN userds u2 ON transactions.user_id_to = u2.id")
                              .select("transactions.transaction_id as '0', u3.currency as '1', transactions.w_from_balance_before as '2', transactions.w_from_balance_after as '3', transactions.w_to_balance_before as '4'
                                      , transactions.w_to_balance_after as '5', u3.balance as '6', u1.username as '7', u2.username as '8', u3.ratio_company as '9', u3.netbalance as '10', u3.approve as '11'
                                      , transactions.created_at as '12', transactions.value_from as '13', transactions.w_from_name as '14', transactions.value_to as '15', transactions.w_to_name as '16', transactions.beforecharge as '17'
                                      , transactions.user_id as '18', transactions.user_id_to as '19', u3.deleted as '20'")
      
	    if @balance_searchs != ""
	      	@rr = @transactions.where("transactions.transaction_id = ? AND (transactions.user_id = ? OR transactions.user_id_to = ?) ", @balance_searchs, Userd.current.id, Userd.current.id).all.order('transactions.created_at DESC')
	                        
	    else
	      	if @searchdate != "" and @balance_type != ""

	        	if @balance_type == "Confirmed Transfer" 
	          		@rr = @transactions.where("transactions.user_id = ? AND u3.approve = ? AND u3.deleted = ? AND transactions.created_at >= ? AND transactions.controller LIKE ?", Userd.current.id, 1, 0, @searchdate, "%#{@controller_name}%").all.order('transactions.created_at DESC')

	        	elsif @balance_type == "Rejected Transfer" 
	          		@rr = @transactions.where("(transactions.user_id = ? OR transactions.user_id_to = ?) AND u3.deleted != ? AND transactions.created_at >= ? AND transactions.controller LIKE ?", Userd.current.id, Userd.current.id, 0, @searchdate, "%#{@controller_name}%").all.order('transactions.created_at DESC')

	        	elsif @balance_type == "UnConfirmed Transfer" 
	          		@rr = @transactions.where("transactions.user_id = ? AND u3.approve = ? AND u3.deleted = ? AND transactions.created_at >= ? AND transactions.controller LIKE ?", Userd.current.id, 0, 0, @searchdate, "%#{@controller_name}%").all.order('transactions.created_at DESC')

	        	elsif @balance_type == "Received transfers"
	          		@rr = @transactions.where("transactions.user_id_to = ? AND u3.approve = ? AND u3.deleted = ? AND transactions.created_at >= ? AND transactions.controller LIKE ?", Userd.current.id, 1, 0, @searchdate, "%#{@controller_name}%").all.order('transactions.created_at DESC')

	        	elsif @balance_type == "Frozen transfers"
	          		@rr = @transactions.where("transactions.user_id_to = ? AND u3.approve = ? AND u3.deleted = ? AND transactions.created_at >= ? AND transactions.controller LIKE ?", Userd.current.id, 0, 0, @searchdate, "%#{@controller_name}%").all.order('transactions.created_at DESC')
	        	end


	      	elsif @searchdate != "" and @balance_type == ""
	        	@rr = @transactions.where("transactions.created_at >= ? AND (transactions.user_id = ? OR transactions.user_id_to = ?)", @searchdate, Userd.current.id, Userd.current.id).all.order('transactions.created_at DESC')

	      	elsif @searchdate == "" and @balance_type != ""
	        
	        	if @balance_type == "Confirmed Transfer"
	          		@rr = @transactions.where("transactions.user_id = ? AND u3.approve = ? And u3.deleted = ?  AND transactions.controller LIKE ?", Userd.current.id, 1, 0, "%#{@controller_name}%").all.order('transactions.created_at DESC')

	        	elsif @balance_type == "UnConfirmed Transfer"          
	          		@rr = @transactions.where("transactions.user_id = ? AND u3.approve = ? AND u3.deleted = ? AND transactions.controller LIKE ?", Userd.current.id, 0, 0, "%#{@controller_name}%").all.order('transactions.created_at DESC')

	        	elsif @balance_type == "Received transfers"
	          		@rr = @transactions.where("transactions.user_id_to = ? AND u3.approve = ? AND u3.deleted = ? AND transactions.controller LIKE ?", Userd.current.id, 1, 0,  "%#{@controller_name}%").all.order('transactions.created_at DESC')
	                           
	        	elsif @balance_type == "Frozen transfers"
	          		@rr = @transactions.where("transactions.user_id_to = ? AND u3.approve = ? AND u3.deleted = ? AND transactions.controller LIKE ?", Userd.current.id, 0, 0, "%#{@controller_name}%").all.order('transactions.created_at DESC')
	                            
	        	elsif @balance_type == "Rejected Transfer"
	          		@rr = @transactions.where("(transactions.user_id = ? OR transactions.user_id_to = ?) AND u3.deleted != ? AND transactions.controller LIKE ?", Userd.current.id, Userd.current.id, 0, "%#{@controller_name}%").all.order('transactions.created_at DESC')
	        	end

	      	end
	    
	    end

	    return @rr

	end

	def self.createwallet( wallet_currency, users_wallets_balance, isrepeated, wallet_name )

		@wallet_currency = wallet_currency
		@users_wallets_balance = users_wallets_balance
		@isrepeated = isrepeated
		@wallet_name = wallet_name

		@users_wallets_balance.wallet_name = @wallet_name

	    # condition user wallet balance not existed
	    if (@isrepeated == nil)

	        if @users_wallets_balance.save
	          Notification.create(:user_id => Userd.current.id,:added_by => Userd.current.id,:foreign_id => @users_wallets_balance.id, :controller => 'User_wallet_ballence',:stats => 'you have created/' + @users_wallets_balance.wallet_name)
	          Wallet.newwallet(Userd.current, @users_wallets_balance.wallet_name).deliver
	          Watchdog.create(:user_id => Userd.current.id,:added_by => Userd.current.id,:controller => 'User_wallet_ballence', :action_page =>"create new wallet",:foreign_id=> @users_wallets_balance.id)
	          
	          if @users_wallets_balance.currency == "USD"
	          	@message  = I18n.t('Dollar wallet was successfully created')
	          
	          elsif @users_wallets_balance.currency == "EGP"
	            @message  = I18n.t('Egyptian Pound wallet was successfully created')
	          
	          elsif @users_wallets_balance.currency == "SAR"
	            @message  = I18n.t('Riyal wallet was successfully created')	          
	          end
	        
	        else
				@message  = I18n.t('Wallet can not be created')
	        end

	    else
	      # condition user wallet balance exist and deleted
	      if(@isrepeated.is_deleted == 1)
	        @isrepeated.update(:is_deleted => 0)
	        Notification.create(:user_id => Userd.current.id,:foreign_id => @isrepeated.id, :controller => 'User_wallet_ballence',:stats => 'you have created/' + @isrepeated.wallet_name)
	        Watchdog.create(:user_id => Userd.current.id,:added_by => Userd.current.id,:controller => 'User_wallet_ballence', :action_page =>"create new wallet",:foreign_id=> @isrepeated.id,:details => 'you have created/' + @isrepeated.wallet_name)
	        Wallet.newwallet(Userd.current, @isrepeated.wallet_name).deliver
	       
	        if @isrepeated.currency == "USD"
	        	@message  = I18n.t('Dollar wallet was successfully created')
	         
	        elsif @isrepeated.currency == "EGP"
	          @message  = I18n.t('Egyptian Pound wallet was successfully created')
	         
	        elsif @isrepeated.currency == "SAR"
	          @message  = I18n.t('Riyal wallet was successfully created')	           
	        end

		    else
		      	@message  = I18n.t('this wallet was created previously')		      
		    end

	    end

	    return @message

	end

	def self.deleteewallet( users_wallets_balance, walletbalance, balancefrozen )

		@users_wallets_balance = users_wallets_balance
		@walletbalance = walletbalance
		@balancefrozen = balancefrozen

		# condition user who delete the wallet is the same who have it
	    if (Userd.current.id == @walletbalance.user_id )
	      
	      	# condition there is balance in the wallet
	      	if (@walletbalance.balance != 0 )
	      		@message  = I18n.t('you can not delete this wallet')
	      
	      	# condition there is frozen balance in the wallet
	      	elsif (@balancefrozen != nil)
	      		@message  = I18n.t('you can not delete this wallet')
	      
	      	else
		        @walletbalance.update(:is_deleted => 1)
		        Notification.create(:user_id => Userd.current.id,:foreign_id => @walletbalance.id, :controller => 'User_wallet_ballence',:stats => 'you have deleted/' +@walletbalance.wallet_name.to_s)
		        Watchdog.create(:user_id => Userd.current.id,:added_by => Userd.current.id,:controller => 'User_wallet_ballence', :action_page =>"delete / User_wallet_ballence",:foreign_id=>@walletbalance.id ,:details=> 'you have deleted/' +@walletbalance.wallet_name.to_s )
		        Wallet.deletewallet(Userd.current, @walletbalance.wallet_name).deliver
		        @message  = I18n.t('you have deleted this wallet')
	      	end
	    end

	end	

end