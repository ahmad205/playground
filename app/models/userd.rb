
require "net/http"
require "uri"
require 'mailgun'

class Userd < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # devise :database_authenticatable, :registerable,
  #        :recoverable, :rememberable, :trackable, :validatable
  # devise :two_factor_authenticatable,
  #        :otp_secret_encryption_key => ENV['Sv6p3vdOSA']

  #acts_as_google_authenticated :google_secret_column => :gauth_secret
  
  # mount_uploader :avatar, AvatarUploader

  belongs_to :role
  belongs_to :users_group ,:foreign_key => "group_id"
    has_many :ticket
    has_many :users_bank
    has_many :watchdog
    
    has_many :users_wallets_balances_frozen , foreign_key: "user_id"
    
    #belongs_to :users_wallets_balances_frozen , foreign_key: "foreign_id"
    has_one :user_verification , foreign_key: "user_id"
    has_many :notification , foreign_key: "user_id"
    #has_one  :users_balance
    has_many :balances_operation
  has_many :service_operation
  validates_uniqueness_of :username,:case_sensitive => false
  #validates_inclusion_of :time_zone, in: ActiveSupport::TimeZone.zones_map(&:name)


  #validates_uniqueness_of :tel
  
  validates :username, :presence => true


  attr_accessor :verification_code
  attr_accessor :verification_codee
    # validates :f_name,:l_name,:tel,:postal_code,:country,:city, :presence => true

  
    
  
  # attr_accessor :avatar ,:nationalidphoto ,:attachments
  # attr_accessible  :email, :password, :password_confirmation, :remember_me,:avatar_file_name ,:nationalidphoto_file_name ,:attachments_file_name,:password ,:regactive,:username,:authy_id,:name,:tel,:email,:avatar,:nationalidphoto,:attachments,:role_id,:group_id,:loginips,:nationalid,:vodafonecash,:country,:currency
  # #attr_accessible :gauth_secret,:gauth_token,:gauth_enabled, :gauth_tmp, :gauth_tmp_datetime,
  

  
# has_one_time_password
#   enum otp_module: {disabled:0, enabled: 1}
  # has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  # validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

# has_attached_file :nationalidphoto, :styles => { :medium => "350x350>", :thumb => "200x200>" }, :default_url => "/images/:style/missing.png"
#   validates_attachment_content_type :nationalidphoto, :content_type => /\Aimage\/.*\Z/
  
#  has_attached_file :attachments

 
#  geocoded_by :current_sign_in_ip,
#   :latitude => :lat, :longitude => :lon

# after_validation :geocode


  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable,:authy_authenticatable

  # devise :two_factor_authenticatable,
  # # Set a unique encryption key for your app.
  # # Store your key as an ENV variable and
  # # remember to add it to .gitignore
  # # if you plan to share your code publicly.
  # otp_secret_encryption_key: "f537aed557dbac2cf4e17831e2b3e0edddfbfbbad7d9f17a16d7902f3442cfc4531d4f64f5037fcf80e3ca2e98a6547a3c71768ea191dc24aec98d6f1862dbad"
  
#:authy_authenticatable,
#:google_authenticatable,
#:confirmable
#:two_factor_authentication
#:invitable
# devise :two_factor_authenticatable,
# :otp_secret_encryption_key => ENV['OTP_SECRET_ENCRYPTION_KEY']
#:session_limitable
#,:traceable
   devise  :google_authenticatable,:confirmable,:invitable,:database_authenticatable,:registerable,:recoverable, :rememberable,:two_factor_authenticatable, :trackable, :validatable,:lockable,:timeoutable,:password_expirable, :secure_validatable, :expirable
  # devise  :database_authenticatable, :registerable,
  # :recoverable, :rememberable, :trackable, :validatable

  # devise :two_factor_authenticatable, :database_authenticatable, :registerable,
  # :recoverable, :rememberable, :trackable, :validatable
  #has_one_time_password(encrypted: true)  
   has_one_time_password
  #has_one_time_password(encrypted: true)
  
  # devise  :registerable,
  # :trackable, :validatable

#   devise :two_factor_authenticatable,
#   otp_secret_encryption_key: ENV['OTP_SECRET_ENCRYPTION_KEY']  

# devise :registerable, :recoverable, 
#   :rememberable, :trackable, :validatable

  # devise  :google_authenticatable, :registerable,
  # :recoverable, :rememberable, :trackable, :validatable

  def self.current
    Thread.current[:userd]
  end
  def self.current=(userd)
    Thread.current[:userd] = userd
  end

  def send_new_otp
    # @code =otp_code
    # @cc =otp_secret_key
    # @email=email
    #   mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'
    # message_params = {:from    => 'Payerz@payerz.com',
    #                   :to      => @email,
    #                   :subject => 'كود التفعيل',
    #                   :text    => otp_code}
    # mg_client.send_message 'sandboxd398e72047544c559f4ba47aebd39f7a.mailgun.org', message_params
    
    
  end

  def send_reset_password_instructions
  token = set_reset_password_token
  send_reset_password_instructions_notification(token)

  token
end

def set_reset_password_token
  raw, enc = Devise.token_generator.generate(self.class, :reset_password_token)

  self.reset_password_token   = enc
  self.reset_password_sent_at = Time.now.utc
  save(validate: false)
  raw
end

def send_reset_password_instructions_notification(token)
  send_devise_notification(:reset_password_instructions, token, {})
end

  def generate_password_token!
  user.reset_password_token = generate_token
  user.reset_password_sent_at = Time.now.utc
  save!
end

def password_token_valid?
  (user.reset_password_sent_at + 4.hours) > Time.now.utc
end

def reset_password!(password)
  user.reset_password_token = nil
  user.password = password
  save!
end





  def send_two_factor_authentication_code

    @code =otp_code
    @cc =otp_secret_key
    @email=email


    
    # mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'
    # message_params = {:from    => 'Payerz@payerz.com',
    #                   :to      => current_userd.email,
    #                   :subject => 'إنشاء محفظة',
    
    #                   :text    => '  تم إنشاء محفظة لل '+ @users_wallets_balance.wallet_name}
    #                    mg_client.send_message 'postmaster@clients.payers.net', message_params

                       



    #   mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'
    # message_params = {:from    => 'Payerz@payerz.com',
    #                   :to      => @email,
    #                   :subject => 'كود التفعيل',
    #                   :text    => otp_code}
    # mg_client.send_message 'postmaster@clients.payers.net', message_params

    @useremail = Userd.where(:email => @email).first
    
    if @useremail.sms_active == 0 && @useremail.active_otp_secret == 1
      
    Emailcode.sendcode(@useremail,otp_code).deliver
    end
    

    
    # if @useremail.sms_active == 1

     
      
    #   @phone = @useremail.tel
    #   url = URI("https://api.infobip.com/sms/1/text/single")
      
    #   http = Net::HTTP.new(url.host, url.port)
    #   http.use_ssl = true
    #   http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      
    #   request = Net::HTTP::Post.new(url)
    #   request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
    #   request["content-type"] = 'application/json'
    #   request["accept"] = 'application/json'
      
    #   request.body = "{\"from\":\"Payers\",\"to\":\"#{@phone}\",   \"text\":\" كود التفعيل #{@code}\"}"
      
      
    #   response = http.request(request)
    #   puts response.read_body
    
    # end
      

  end

  def resend_code
    
        @code =otp_code
        @cc =otp_secret_key
        @email=email
        mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'
        message_params = {:from    => 'Payerz@payerz.com',
                          :to      => @email,
                          :subject => 'كود التفعيل',
                          :text    => otp_code}
        mg_client.send_message 'postmaster@clients.payers.net', message_params
        
        @useremail = Userd.find_by_email( email )
        if @useremail.sms_active == 1
        
        end
          
    
      end
    

  

  

  def need_two_factor_authentication?(request)
    
    @useremail = Userd.find_by_email( email )
 if @useremail != nil
  
  #@useremail.active_otp_secret.nil? 
  @useremail.active_otp_secret.to_i == 1


end

#not otp_secret_key.nil?
    

  end
  
  
   
# instead of deleting, indicate the user requested a delete & timestamp it  
  def soft_delete  
    update_attribute(:deleted_at, Time.current)  
  end  
  
  # ensure user account is active  
  def active_for_authentication?  
    super && !deleted_at  
  end  
  
  # provide a custom message for a deleted account   
  def inactive_message   
    !deleted_at ? super : :deleted_account  
  end  
#######################otp#######################
  def activate_two_factor params
    otp_params = { otp_secret: unconfirmed_otp_secret }
    if !valid_password?(params[:password])
      errors.add :password, :invalid
      false
    elsif !validate_and_consume_otp!(params[:otp_attempt], otp_params)
      errors.add :otp_attempt, :invalid
      false
    else
      activate_two_factor!
    end
  end
  
  def deactivate_two_factor params
    if !valid_password?(params[:password])
      errors.add :password, :invalid
      false
    else
      self.otp_required_for_login = false
      self.otp_secret = nil
      save
    end
  end
  
  private
  
  def activate_two_factor!
    self.otp_required_for_login = true
    self.otp_secret = current_admin_user.unconfirmed_otp_secret
    self.unconfirmed_otp_secret = nil
    save
  end

  def generate_token
  SecureRandom.hex(10)
end
   
end
