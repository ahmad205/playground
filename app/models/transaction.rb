class Transaction < ActiveRecord::Base
	belongs_to :users_wallets_balances_frozen 
	# before_create :set_access_token
	
	def self.to_csv(options = {})
	
		CSV.generate(options) do |csv|
			csv << column_names

			all.each do|post|
			csv << post.attributes.values_at(*column_names)
			end

		end
	end


  def self.gettransaction
      loop do
      @transaction_id = "WB"+[*('A'..'Z'),*('0'..'9')].to_a.shuffle[0,16].join
      break @transaction_id unless Transaction.where(transaction_id: @transaction_id).exists?
      	end
	return @transaction_id
end

end