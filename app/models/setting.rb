class Setting < ActiveRecord::Base
	
	validates :key, :presence => true
	attr_accessible :key, :value,:name,:description
	
end
