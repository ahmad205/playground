class UsersBank < ActiveRecord::Base
    belongs_to :user  
    belongs_to  :banks_list
    attr_accessor :account_numbersw,:language, :selectedcountry, :type , :type2 ,:bankegname,:brancheg,:accountnumbereg,:countrysw ,:countrycodesw ,:citysw ,:addresssw ,:banksw ,:branchsw ,:postalcodesw ,:account_type,:countryib ,:account_numberib ,:bankib ,:bank_codeib ,:countrycodeib,:currencyib  
  validates :userd_id, :presence => true


  


  def self.newuserbank(params,user_id,currency)

    #################################################################
    @UsersBank=UsersBank.new
    @currencies_withdraw_ratios = CurrenciesWithdrawRatio.where(:id => params[:users_bank][:currencies_withdraw_ratios_id].to_i).first
    
    ActiveRecord::Base.transaction do
    @user = Userd.where(:id => user_id).lock('LOCK IN SHARE MODE').first    
    @UsersBank.userd_id = user_id
    @UsersBank.personal_name = params[:users_bank][:personal_name]
    @UsersBank.business_name = params[:users_bank][:business_name]
    @UsersBank.nickname = params[:users_bank][:nickname]
    @UsersBank.relation = params[:users_bank][:relation]
    @UsersBank.nationalid = params[:users_bank][:nationalid]
    @UsersBank.currencies_withdraw_ratios_id= params[:users_bank][:currencies_withdraw_ratios_id].to_i
    @UsersBank.bank_type = "withdraw" 
    @UsersBank.birth_date = params[:users_bank][:birth_date]
    @UsersBank.currency = currency  
    @UsersBank.country = params[:users_bank][:country]
    @UsersBank.bank_name = @currencies_withdraw_ratios.name
    @bankname = @UsersBank.bank_name.to_s  
    ##############EGP Banks##################
    if (params[:users_bank][:country]== 'EG' && params[:users_bank][:currencies_withdraw_ratios_id].to_i == 13)   #Egyption_Pound
      @UsersBank.bank_name = params[:users_bank][:bankegname]
      @UsersBank.branch = params[:users_bank][:brancheg]
      @UsersBank.branchnumber = params[:users_bank][:branchnumber]
      @UsersBank.account_number = params[:users_bank][:accountnumbereg]
      @UsersBank.country_code = "EG"
      @UsersBank.currency = 'EGP'
    end 
    ###############EGP Banks####


    ###############USD Swift######
    if (params[:users_bank][:swiftcode].present?) #Swift Countries
    
      @UsersBank.bank_name = params[:users_bank][:banksw]
      @UsersBank.country = params[:users_bank][:countrysw]
      @UsersBank.country_code = params[:users_bank][:countrycodesw]
      @UsersBank.city = params[:users_bank][:citysw]
      @UsersBank.swiftcode = params[:users_bank][:swiftcode]
      @UsersBank.valid_swift_code=params[:users_bank][:validsw]
      @UsersBank.account_number = params[:users_bank][:account_numbersw]
      @UsersBank.branch = params[:users_bank][:branchsw]
      @UsersBank.address = params[:users_bank][:addresssw]
      @UsersBank.currency = 'USD'
     #################USD Swift######
    
         ###################USD Ibann######
      elsif (params[:users_bank][:iban].present?)
      @UsersBank.iban_validity = params[:users_bank][:iban_validity]
      @UsersBank.iban_checksum = params[:users_bank][:iban_checksum]
      @UsersBank.iban_length = params[:users_bank][:iban_length]
      @UsersBank.iban_structure = params[:users_bank][:iban_structure]
      @UsersBank.account_checksum = params[:users_bank][:account_checksum]
      @UsersBank.iban = params[:users_bank][:iban]
      @UsersBank.bban = params[:users_bank][:bban]
      @UsersBank.checksum = params[:users_bank][:checksum]
      @UsersBank.check_digit = params[:users_bank][:check_digit]
      @UsersBank.sepa = params[:users_bank][:sepa]
      #@UsersBank.currency = params[:users_bank][:currencyib]
      @UsersBank.currency = 'USD'
      if (params[:users_bank][:country]== 'SA' && params[:users_bank][:currencies_withdraw_ratios_id].to_i ==  15) 
       @UsersBank.currency = 'SAR'     
     end        
      @UsersBank.country=params[:users_bank][:countryib]
      @UsersBank.country_code=params[:users_bank][:countrycodeib]
      @UsersBank.account_number = params[:users_bank][:account_numberib]
      @UsersBank.bank_code = params[:users_bank][:bank_codeib]
      @UsersBank.bank_name = params[:users_bank][:bankib]               
    
        end 
      ###################USD Ibann######

    ###################################


    @UsersBank.save
    Notification.create(:user_id => @user.id,:controller => 'users_bank',:foreign_id => @UsersBank.id ,:stats => 'you have added new bank for/' + @UsersBank.bank_name.to_s )
    Watchdog.create(:user_id => @user.id,:added_by => @user.id,:controller => 'users_bank', :action_page =>"new/#{@UsersBank.bank_name}",:foreign_id=>@UsersBank.id ,:details=> 'you have added new bank for/' + @UsersBank.bank_name.to_s)
    AddUserbankMailer.userbank(@user,@bankname).deliver
    
    end
    return @UsersBank

  end

  
   
 
end
