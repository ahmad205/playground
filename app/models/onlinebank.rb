class Onlinebank < ActiveRecord::Base
# 	has_attached_file :avatar
# do_not_validate_attachment_file_type :avatar,:preserve_files => true
# mount_uploader :attachments, BankAvatarUploader

def self.user_reply(params,user_id,cart_usdtoegp,cart_usdtors)
    @cart_usdtoegp = cart_usdtoegp
    @cart_usdtors = cart_usdtors

	@user = Userd.where(:id => user_id).first
	@id = params[:user_reply][:id].to_s
    @note = params[:user_reply][:reply]
    @excep_id = params[:user_reply][:excep_id]
    @attachment = params[:user_reply][:reply_attachment]
    @payment_id = Onlinebank.where("id = ?",@id).first
    @payment_id.update(status: "Processing")
    @tran_id = Transaction.where(["foreign_id = ? and controller in (?) " , @payment_id.id,['Onlinebanks/banktransfer_post']]).first
    
    Watchdog.create(:user_id => @payment_id.user_id,:added_by => @user.id,:controller => 'Onlinebanks', :action_page =>"exception_reply",:foreign_id=>@payment_id.id ,:details=> "reply to exception/ " + @payment_id.transaction_id.to_s + "/addvalue/" + @payment_id.netsum.to_s )
    @notif_id = Notification.create(:user_id => @payment_id.user_id,:added_by => @user.id,:trans_id => @tran_id.id,:controller => 'Onlinebanks/exception_reply',:foreign_id => @payment_id.id.to_s,:stats => 'reply to exception/ ' + @payment_id.transaction_id.to_s + "/addvalue/" + @payment_id.netsum.to_s)   
    
    @att =  Attachment.create(:uuid => SecureRandom.uuid , :content => @attachment,:user_id => @payment_id.user_id,:file_ext => @attachment.content_type,:file_name => @attachment.original_filename )
    @ticket = Ticket.create(:foreign_id => @id,:userd_id =>@payment_id.user_id,:ticket_number => rand(100000..999999),:body => @note,:status => "customer reply",:predefined_reply_id => @notif_id.id,:ticket_id => @excep_id,:reply_attachment => @att.id )




end



  
end
