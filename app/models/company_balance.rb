class CompanyBalance < ActiveRecord::Base
  belongs_to :balances_operation
  belongs_to :balances_withdraw


before_create :add_partner_balance



private
def add_partner_balance


        # CompanyBalance.create(:partner_id => current_userd.id,:operation_id => '4',:trans_id => 11,:balance => 100,:currency => 'USD' ,:status => '7')



        @partners = Partner.all


        @partnersettings =  PartnersSetting.where(:key => 'profit_status').first
        @company_partner = Partner.where(:id => 1).first
        if @partnersettings.value  == '1'  && self.operation_id != 5
          # && self.operation_id == 17
          # && self.operation_id != 5
          @last = CompanyBalance.maximum(:id).to_i
          @company_balance_id = @last.to_i + 1 

              # PartnersWithdraw.create(:partner_id => p.id,:value => params[:value] ,:currency => 'USD', :details => 'p.ratio', :status => 0)

                        if self.currency == 'USD'  ##currency

                        @partners.each do |p|   ##foreach

                            @increased_value = self.balance.to_f * (p.ratio.to_f/100) #100
                          
                         ###################################جزئية الربح #########################################
                            if self.status == true 

                              # @check_withdraw_limit = p.withdraw_limit.to_f + @increased_value.to_f
                              @final_value = p.usd_balance.to_f + @increased_value.to_f
                              @withdraw_balance = p.withdraw_balance.to_f + @increased_value.to_f

                              
                              if   ( @withdraw_balance.to_f < p.withdraw_limit.to_f || p.profit_infinite == 1  || @withdraw_balance.to_f == p.withdraw_limit.to_f)  &&  p.profit_status == 1  && p.id != 1  && p.id != 0 
                              p.usd_balance = @final_value 
                              p.withdraw_balance = p.withdraw_balance.to_f + @increased_value.to_f
                              p.save
                              PartnersReport.create(:partner_id => p.id,:company_balance_id =>  @company_balance_id  ,:currency => 'USD', :current_ratio => p.ratio, :balance_before => p.usd_balance.to_f - @increased_value.to_f  ,:balance_after => p.usd_balance)
                              end

                              if  @withdraw_balance.to_f > p.withdraw_limit.to_f  &&  p.withdraw_balance.to_f != p.withdraw_limit.to_f &&  p.profit_infinite == 0 &&  p.profit_status == 1  && p.id != 1  && p.id != 0 
                                @normalize =  p.withdraw_limit.to_f  - p.withdraw_balance.to_f 

                                @remaining_company =  @increased_value.to_f -  @normalize.to_f
                                @company_partner.withdraw_balance =  @company_partner.withdraw_balance.to_f +  @remaining_company.to_f
                                @company_partner.usd_balance =  @company_partner.usd_balance.to_f +  @remaining_company.to_f
                                @company_partner.save
                                PartnersReport.create(:partner_id => 1,:company_balance_id =>  @company_balance_id  ,:currency => 'USD', :current_ratio => @company_partner.ratio, :balance_before => @company_partner.usd_balance.to_f -  @remaining_company.to_f   ,:balance_after => @company_partner.usd_balance.to_f)

                                
                                p.usd_balance =  p.usd_balance.to_f + @normalize.to_f 
                                p.withdraw_balance = p.withdraw_limit.to_f 
                                p.save

                                PartnersReport.create(:partner_id => p.id,:company_balance_id => @company_balance_id ,:currency => 'USD', :current_ratio => p.ratio, :balance_before => p.usd_balance.to_f - @normalize.to_f  ,:balance_after => p.usd_balance)
                              elsif  (@withdraw_balance.to_f > p.withdraw_limit.to_f  &&  p.withdraw_balance.to_f == p.withdraw_limit.to_f) || p.profit_status != 1  && p.id != 1 && p.id != 0  &&  p.profit_infinite == 0
                               
                                @remaining_company =  @increased_value.to_f 
                                @company_partner.withdraw_balance =  @company_partner.withdraw_balance.to_f +  @remaining_company.to_f
                                @company_partner.usd_balance =  @company_partner.usd_balance.to_f +  @remaining_company.to_f
                                @company_partner.save
                                PartnersReport.create(:partner_id => 1,:company_balance_id =>  @company_balance_id  ,:currency => 'USD', :current_ratio => @company_partner.ratio, :balance_before => @company_partner.usd_balance.to_f -  @remaining_company.to_f   ,:balance_after => @company_partner.usd_balance.to_f)
                          
                                elsif (p.id == 1 || p.id == 0)  &&  self.status == true  ##add company balance
                              @company_partner.usd_balance = @company_partner.usd_balance + @increased_value.to_f
                              @company_partner.withdraw_balance =  @company_partner.withdraw_balance.to_f + @increased_value.to_f
                              @company_partner.save
                              # @last = CompanyBalance.maximum(:id).to_i
                              # @company_balance_id = @last.to_i + 1 
  
  
                              PartnersReport.create(:partner_id => 1,:company_balance_id => @company_balance_id ,:currency => 'USD', :current_ratio =>  @company_partner.ratio, :balance_before =>  @company_partner.usd_balance.to_f - @increased_value.to_f  ,:balance_after =>  @company_partner.usd_balance)
  
                   
                              end

                            
                            end   ##true

                         ###################################جزئية الربح نهاية#########################################

                      
                      
                         ###################################جزئية الخسارة #########################################

                            if self.status == false && p.id != 1 && p.id != 0  #partners refund


                              @main_operation = CompanyBalance.where(:foreign_id => self.foreign_id , :operation_id => self.operation_id).first
                              self.parent_id = @main_operation.id
                              @reports_to_refunded = PartnersReport.where(:company_balance_id => @main_operation.id.to_i,:partner_id => p.id).all
                              if  @reports_to_refunded != nil 
                              @reports_to_refunded.each do |re|
                                
                                @refund_balance = re.balance_after.to_f - re.balance_before.to_f
                                p.usd_balance = p.usd_balance.to_f - @refund_balance.to_f
                                p.withdraw_balance = p.withdraw_balance.to_f - @refund_balance.to_f

                                p.save
                           PartnersReport.create(:partner_id => p.id,:company_balance_id =>  @company_balance_id  ,:currency => 'USD', :current_ratio => p.ratio, :balance_before => p.usd_balance.to_f + @refund_balance.to_f  ,:balance_after => p.usd_balance)
                              end
                              end
 
                            elsif  self.status == false &&  ( p.id == 1 || p.id == 0)   #company refund

                              @main_operation = CompanyBalance.where(:foreign_id => self.foreign_id , :operation_id => self.operation_id).first
                              self.parent_id = @main_operation.id
                              @reports_to_refunded = PartnersReport.where(:company_balance_id => @main_operation.id.to_i,:partner_id => 1).all
                              @reports_to_refunded.each do |re|
                                
                                @refund_balance = re.balance_after.to_f - re.balance_before.to_f
                                p.usd_balance = p.usd_balance.to_f - @refund_balance.to_f
                                p.withdraw_balance = p.withdraw_balance.to_f - @refund_balance.to_f

                                p.save
                           PartnersReport.create(:partner_id => 1,:company_balance_id =>  @company_balance_id  ,:currency => 'USD', :current_ratio => p.ratio, :balance_before => p.usd_balance.to_f + @refund_balance.to_f  ,:balance_after => p.usd_balance)

                              end



                              # p.usd_balance = p.usd_balance.to_f - @increased_value.to_f
                              # p.withdraw_balance = p.withdraw_balance.to_f - @increased_value.to_f

                              # #  if  p.usd_balance < 0
                              # #    p.usd_balance = 0
                              # #  end
                              # PartnersReport.create(:partner_id => p.id,:company_balance_id =>  @company_balance_id  ,:currency => 'USD', :current_ratio => p.ratio, :balance_before => p.usd_balance.to_f + @increased_value.to_f  ,:balance_after => p.usd_balance)

                            end  ##false
                         ###################################جزئية الخسارة نهاية#########################################

                           # p.save

                         
                          # end   ## end user nil

                        end ##end foreach

                      end ##end currency



                   
     if self.currency == 'SAR'

      @partners.each do |p|

          @increased_value = self.balance.to_f * (p.ratio.to_f/100) #100000
          @sar_rate = Setting.where(:key => 'Universal SAR TO USD').first ##0.266
          @usdtosar = Setting.where(:key => 'Universal USD TO SAR').first ##0.266
        #   if p.id != nil

             ###################################جزئية الربح #########################################

          if self.status == true

            # @check_withdraw_limit = p.withdraw_limit.to_f + @increased_value.to_f
            @final_value = p.sar_balance.to_f + @increased_value.to_f  #in sar
            @withdraw_balance = p.withdraw_balance.to_f + (@increased_value.to_f * @sar_rate.value.to_f ) #in usd


            if   ( @withdraw_balance.to_f < p.withdraw_limit.to_f || p.profit_infinite == 1   || @withdraw_balance.to_f == p.withdraw_limit.to_f)  &&  p.profit_status == 1  && p.id != 1  && p.id != 0 

            p.sar_balance = @final_value  #in sar
            p.withdraw_balance = p.withdraw_balance.to_f + (@increased_value.to_f * @sar_rate.value.to_f ) #in usd
            p.save
            PartnersReport.create(:partner_id => p.id,:company_balance_id =>  @company_balance_id  ,:currency => 'SAR', :current_ratio => p.ratio, :balance_before => p.sar_balance.to_f - @increased_value.to_f  ,:balance_after => p.sar_balance)

            end

           if  @withdraw_balance.to_f > p.withdraw_limit.to_f  &&  p.withdraw_balance.to_f != p.withdraw_limit.to_f &&  p.profit_infinite == 0  &&  p.profit_status == 1  && p.id != 1  && p.id != 0 
            @normalize =  p.withdraw_limit.to_f  - p.withdraw_balance.to_f  #in usd
           
            # p.sar_balance =  p.sar_balance.to_f + (@normalize.to_f * @usdtosar.value.to_f ) #in sar
            #  p.withdraw_balance = p.withdraw_limit.to_f 
            #  PartnersReport.create(:partner_id => p.id,:company_balance_id => CompanyBalance.maximum('id') +1 ,:currency => 'SAR', :current_ratio => p.ratio, :balance_before => p.sar_balance.to_f - ( @normalize.to_f * @usdtosar.value.to_f )    ,:balance_after => p.sar_balance)
            
            

             @remaining_company =  @increased_value.to_f -  ( @normalize.to_f * @usdtosar.value.to_f )  #in sar
             @company_partner.withdraw_balance =  @company_partner.withdraw_balance.to_f + (@remaining_company.to_f * @sar_rate.value.to_f)  #in usd
             @company_partner.sar_balance =  @company_partner.sar_balance.to_f +  @remaining_company.to_f #in sar
             @company_partner.save
             PartnersReport.create(:partner_id => 1,:company_balance_id =>  @company_balance_id  ,:currency => 'SAR', :current_ratio => @company_partner.ratio, :balance_before => @company_partner.sar_balance.to_f -  @remaining_company.to_f   ,:balance_after => @company_partner.sar_balance.to_f)         
             p.sar_balance =  p.sar_balance.to_f + (@normalize.to_f * @usdtosar.value.to_f )
             p.withdraw_balance = p.withdraw_limit.to_f 
             p.save

             PartnersReport.create(:partner_id => p.id,:company_balance_id => @company_balance_id ,:currency => 'SAR', :current_ratio => p.ratio, :balance_before => p.sar_balance.to_f - (@normalize.to_f * @usdtosar.value.to_f )  ,:balance_after => p.sar_balance)


            elsif  (@withdraw_balance.to_f > p.withdraw_limit.to_f  &&  p.withdraw_balance.to_f == p.withdraw_limit.to_f) || p.profit_status != 1  && p.id != 1 &&  p.profit_infinite == 0
                               
                @remaining_company =  @increased_value.to_f  #in sar
                @company_partner.withdraw_balance =  @company_partner.withdraw_balance.to_f +  (@remaining_company.to_f * @sar_rate.value.to_f)  #in usd
                @company_partner.sar_balance =  @company_partner.sar_balance.to_f +  @remaining_company.to_f
                @company_partner.save
                PartnersReport.create(:partner_id => 1,:company_balance_id =>  @company_balance_id  ,:currency => 'SAR', :current_ratio => @company_partner.ratio, :balance_before => @company_partner.sar_balance.to_f -  @remaining_company.to_f   ,:balance_after => @company_partner.sar_balance.to_f)
            

              elsif (p.id == 1 || p.id == 0 )  &&  self.status == true  ##add company balance
                @company_partner.sar_balance = @company_partner.sar_balance + @increased_value.to_f #in sar
                @company_partner.withdraw_balance =  @company_partner.withdraw_balance.to_f + (@increased_value.to_f * @sar_rate.value.to_f)  #in usd
                @company_partner.save
                # @last = CompanyBalance.maximum(:id).to_i
                # @company_balance_id = @last.to_i + 1 
    
    
                PartnersReport.create(:partner_id => 1,:company_balance_id => @company_balance_id ,:currency => 'SAR', :current_ratio =>  @company_partner.ratio, :balance_before =>  @company_partner.sar_balance.to_f - @increased_value.to_f  ,:balance_after =>  @company_partner.sar_balance)
    
     
            end


           



            
          end
                         ###################################جزئية الربح نهاية#########################################



                         ###################################جزئية الخسارة #########################################

                         if self.status == false &&  p.id != 1  && p.id != 0   #partners refund


                            @main_operation = CompanyBalance.where(:foreign_id => self.foreign_id , :operation_id => self.operation_id).first
                            self.parent_id = @main_operation.id
                            @reports_to_refunded = PartnersReport.where(:company_balance_id => @main_operation.id.to_i,:partner_id => p.id).all
                            if  @reports_to_refunded != nil 
                            @reports_to_refunded.each do |re|
                              
                              @refund_balance = re.balance_after.to_f - re.balance_before.to_f #in sar
                              p.sar_balance = p.sar_balance.to_f - @refund_balance.to_f #in sar
                              p.withdraw_balance = p.withdraw_balance.to_f - (@refund_balance.to_f * @sar_rate.value.to_f) #in usd

                              p.save
                         PartnersReport.create(:partner_id => p.id,:company_balance_id =>  @company_balance_id  ,:currency => 'SAR', :current_ratio => p.ratio, :balance_before => p.sar_balance.to_f + @refund_balance.to_f  ,:balance_after => p.sar_balance)
                            end
                            end

                          elsif  self.status == false &&  ( p.id == 1 || p.id == 0 )  #company refund

                            @main_operation = CompanyBalance.where(:foreign_id => self.foreign_id , :operation_id => self.operation_id).first
                            self.parent_id = @main_operation.id
                            @reports_to_refunded = PartnersReport.where(:company_balance_id => @main_operation.id.to_i,:partner_id => 1).all
                            @reports_to_refunded.each do |re|
                              
                              @refund_balance = re.balance_after.to_f - re.balance_before.to_f #in sar
                              p.sar_balance = p.sar_balance.to_f - @refund_balance.to_f #in sar
                              p.withdraw_balance = p.withdraw_balance.to_f - (@refund_balance.to_f * @sar_rate.value.to_f) #in usd

                              p.save
                         PartnersReport.create(:partner_id => 1,:company_balance_id =>  @company_balance_id  ,:currency => 'SAR', :current_ratio => p.ratio, :balance_before => p.sar_balance.to_f + @refund_balance.to_f  ,:balance_after => p.sar_balance)

                            end



                            # p.usd_balance = p.usd_balance.to_f - @increased_value.to_f
                            # p.withdraw_balance = p.withdraw_balance.to_f - @increased_value.to_f

                            # #  if  p.usd_balance < 0
                            # #    p.usd_balance = 0
                            # #  end
                            # PartnersReport.create(:partner_id => p.id,:company_balance_id =>  @company_balance_id  ,:currency => 'USD', :current_ratio => p.ratio, :balance_before => p.usd_balance.to_f + @increased_value.to_f  ,:balance_after => p.usd_balance)

                          end  ##false
                       ###################################جزئية الخسارة نهاية#########################################
                    

                end ##end foreach

            end ##end currency

      

            if self.currency == 'EGP'  ##currency

              @partners.each do |p|   ##foreach

                  @increased_value = self.balance.to_f * (p.ratio.to_f/100) #100 #inegp
                  @egp_rate = Setting.where(:key => 'Universal EGP TO USD').first ##0.266
                  @usdtoegp = Setting.where(:key => 'Universal USD TO EGP').first ##0.266
                  
               ###################################جزئية الربح #########################################
                  if self.status == true 

                    # @check_withdraw_limit = p.withdraw_limit.to_f + @increased_value.to_f
                    @final_value = p.egp_balance.to_f + @increased_value.to_f
                    @withdraw_balance = p.withdraw_balance.to_f + (@increased_value.to_f * @egp_rate.value.to_f )

                    if   ( @withdraw_balance.to_f < p.withdraw_limit.to_f  || @withdraw_balance.to_f == p.withdraw_limit.to_f || p.profit_infinite == 1)  &&  p.profit_status == 1  && p.id != 1  && p.id != 0 
                    p.egp_balance = @final_value 
                    p.withdraw_balance = p.withdraw_balance.to_f + (@increased_value.to_f * @egp_rate.value.to_f ) #in usd
                    p.save
                    PartnersReport.create(:partner_id => p.id,:company_balance_id =>  @company_balance_id  ,:currency => 'EGP', :current_ratio => p.ratio, :balance_before => p.egp_balance.to_f - @increased_value.to_f  ,:balance_after => p.egp_balance)
                    end

                    if  @withdraw_balance.to_f > p.withdraw_limit.to_f  &&  p.withdraw_balance.to_f != p.withdraw_limit.to_f  &&  p.profit_status == 1  && p.id != 1  && p.id != 0 &&  p.profit_infinite == 0  
                      @normalize =  p.withdraw_limit.to_f  - p.withdraw_balance.to_f 

                      @remaining_company =  (@increased_value.to_f * @egp_rate.value.to_f)  -  @normalize.to_f #in usd
                      @company_partner.withdraw_balance =  @company_partner.withdraw_balance.to_f + @remaining_company.to_f  #in usd
                      @company_partner.egp_balance =  @company_partner.egp_balance.to_f +  (@remaining_company.to_f * @usdtoegp.to_f)  #in egp
                      @company_partner.save
                      PartnersReport.create(:partner_id => 1,:company_balance_id =>  @company_balance_id  ,:currency => 'EGP', :current_ratio => @company_partner.ratio, :balance_before => @company_partner.egp_balance.to_f -  @remaining_company.to_f   ,:balance_after => @company_partner.egp_balance.to_f)

                      
                      p.egp_balance =  p.egp_balance.to_f +  (@normalize.to_f * @egp_rate.value.to_f) 
                      p.withdraw_balance = p.withdraw_limit.to_f 
                      p.save

                      PartnersReport.create(:partner_id => p.id,:company_balance_id => @company_balance_id ,:currency => 'EGP', :current_ratio => p.ratio, :balance_before => p.egp_balance.to_f - ( @normalize.to_f * @egp_rate.to_f)   ,:balance_after => p.egp_balance)
                    elsif  (@withdraw_balance.to_f > p.withdraw_limit.to_f  &&  p.withdraw_balance.to_f == p.withdraw_limit.to_f) || p.profit_status != 1  && p.id != 1 && p.id != 0  &&  p.profit_infinite == 0
                     
                      @remaining_company =  @increased_value.to_f  #in egp
                      @company_partner.withdraw_balance =  @company_partner.withdraw_balance.to_f +  (@remaining_company.to_f * @egp_rate.value.to_f) 
                      @company_partner.egp_balance =  @company_partner.egp_balance.to_f +  @remaining_company.to_f
                      @company_partner.save
                      PartnersReport.create(:partner_id => 1,:company_balance_id =>  @company_balance_id  ,:currency => 'EGP', :current_ratio => @company_partner.ratio, :balance_before => @company_partner.egp_balance.to_f -  @remaining_company.to_f   ,:balance_after => @company_partner.egp_balance.to_f)
                
                      elsif (p.id == 1 || p.id == 0)  &&  self.status == true  ##add company balance
                    @company_partner.egp_balance = @company_partner.egp_balance + @increased_value.to_f
                    @company_partner.withdraw_balance =  @company_partner.withdraw_balance.to_f + ( @increased_value.to_f * @egp_rate.value.to_f) 
                    @company_partner.save
                    # @last = CompanyBalance.maximum(:id).to_i
                    # @company_balance_id = @last.to_i + 1 


                    PartnersReport.create(:partner_id => 1,:company_balance_id => @company_balance_id ,:currency => 'EGP', :current_ratio =>  @company_partner.ratio, :balance_before =>  @company_partner.egp_balance.to_f - @increased_value.to_f  ,:balance_after =>  @company_partner.egp_balance)

         
                    end

                  
                  end   ##true

               ###################################جزئية الربح نهاية#########################################

            
            
                ###################################جزئية الخسارة #########################################

                if self.status == false &&  p.id != 1 && p.id != 0    #partners refund


                  @main_operation = CompanyBalance.where(:foreign_id => self.foreign_id , :operation_id => self.operation_id).first
                  self.parent_id = @main_operation.id
                  @reports_to_refunded = PartnersReport.where(:company_balance_id => @main_operation.id.to_i,:partner_id => p.id).all
                  if  @reports_to_refunded != nil 
                  @reports_to_refunded.each do |re|
                    
                    @refund_balance = re.balance_after.to_f - re.balance_before.to_f #in egp
                    p.egp_balance = p.egp_balance.to_f - @refund_balance.to_f #in egp
                    p.withdraw_balance = p.withdraw_balance.to_f - (@refund_balance.to_f * @egp_rate.value.to_f) #in usd

                    p.save
               PartnersReport.create(:partner_id => p.id,:company_balance_id =>  @company_balance_id  ,:currency => 'EGP', :current_ratio => p.ratio, :balance_before => p.egp_balance.to_f + @refund_balance.to_f  ,:balance_after => p.egp_balance)
                  end
                  end

                elsif  self.status == false &&  ( p.id == 1 || p.id == 0 )    #company refund

                  @main_operation = CompanyBalance.where(:foreign_id => self.foreign_id , :operation_id => self.operation_id).first
                  self.parent_id = @main_operation.id
                  @reports_to_refunded = PartnersReport.where(:company_balance_id => @main_operation.id.to_i,:partner_id => 1).all
                  @reports_to_refunded.each do |re|
                    
                    @refund_balance = re.balance_after.to_f - re.balance_before.to_f #in egp
                    p.egp_balance = p.egp_balance.to_f - @refund_balance.to_f #in egp
                    p.withdraw_balance = p.withdraw_balance.to_f - (@refund_balance.to_f * @egp_rate.value.to_f) #in usd

                    p.save
               PartnersReport.create(:partner_id => 1,:company_balance_id =>  @company_balance_id  ,:currency => 'EGP', :current_ratio => p.ratio, :balance_before => p.egp_balance.to_f + @refund_balance.to_f  ,:balance_after => p.egp_balance)

                  end



                  # p.usd_balance = p.usd_balance.to_f - @increased_value.to_f
                  # p.withdraw_balance = p.withdraw_balance.to_f - @increased_value.to_f

                  # #  if  p.usd_balance < 0
                  # #    p.usd_balance = 0
                  # #  end
                  # PartnersReport.create(:partner_id => p.id,:company_balance_id =>  @company_balance_id  ,:currency => 'USD', :current_ratio => p.ratio, :balance_before => p.usd_balance.to_f + @increased_value.to_f  ,:balance_after => p.usd_balance)

                end  ##false
             ###################################جزئية الخسارة نهاية#########################################

                 # p.save

               
                # end   ## end user nil

              end ##end foreach

            end ##end currency

end


end



end
