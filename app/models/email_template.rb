class EmailTemplate < ActiveRecord::Base

    def self.get_template(user,key)

        @email_template = EmailTemplate.where(:key => key ).first
        

        @greeting = "Hi"
        # @username=user.username
        @content = ''
        @subject = 'wallet'



        if user.locale == 'en'
          @subject = @email_template.subject_en

          @status = @subject
          @greeting = @email_template.greeting_en
          @content = @email_template.en_content

        end

        if user.locale == 'ar'
          @subject = @email_template.subject_ar
          @status = @subject
          @greeting = @email_template.greeting_ar
          @content = @email_template.ar_content


        end

        
        return @greeting,@subject,@content
    
    end
end
