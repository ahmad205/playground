class CreditCard < ActiveRecord::Base

    has_attached_file :photo,:preserve_files => true
do_not_validate_attachment_file_type :photo
validates :photo, attachment_presence: true

has_attached_file :selfie,:preserve_files => true
do_not_validate_attachment_file_type :selfie
validates :selfie, attachment_presence: true

validate :expiration_date_cannot_be_in_the_past

# attr_accessor :card_no
  # include ActiveModel::Validations
  # validates :card_no, credit_card_number: {brands: [:amex, :maestro]}
  validates :card_no, presence: true, credit_card_number: true
  validates_uniqueness_of :card_no, :allow_nil => true, :allow_blank => true


def expiration_date_cannot_be_in_the_past
    if expire_date.present? && expire_date.at_beginning_of_month.next_month < Date.today
      
      # errors.add(:expire_date, "لا يمكن ان يكون في الماضي")


      errors.add('تاريخ الانتهاء','لا يمكن ان يكون في الماضي')


    end
  end   


end
