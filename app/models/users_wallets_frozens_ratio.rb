class UsersWalletsFrozensRatio < ActiveRecord::Base
belongs_to :users_group ,:foreign_key => "users_group_id"

belongs_to :users_wallets_frozens_purpose ,:foreign_key => "user_wallet_purpose_ratio_id"

def self.getexpense(ratioid, value,user,currency,usdtors,usdtoegp)
      
        
        @ratioid=ratioid
        @userdata = user
        @user = Userd.find_by_id(@userdata)
        @sum=  value.to_f
        @currency= currency
        @usdtors = usdtors
        @usdtoegp = usdtoegp
        @ratio = UsersWalletsFrozensRatio.where(users_group_id: @user.group_id,user_wallet_purpose_ratio_id: @ratioid).first

        #@expenses = (@sum * @ratio.ratio/100)
        if  @currency=="USD"         
       @expenses = (@sum * @ratio.ratio/100) + @ratio.fees
        elsif @currency=="EGP"
       @expenses = (@sum * @ratio.ratio/100) + (@ratio.fees * @usdtoegp.to_f)
        elsif @currency=="SAR"
       @expenses = (@sum * @ratio.ratio/100) + (@ratio.fees * @usdtors.to_f)
        end

           return  @expenses
           
end


def self.checkexpenses(compared_val,currency,dollar_wallet,egp_wallet,sar_wallet, purpose_id,userid,userto)
      
    @sum=  compared_val.to_f
    @purpose_id = purpose_id
    @current_user = Userd.find_by_id(userid.to_i)
    @usergroup_limit= UsersGroup.find_by_id(@current_user.group_id.to_i)

    @user_verification = UserVerification.where("user_id = ?",@current_user.id).first
    @user_verification_to = UserVerification.where("user_id = ?",userto.to_i).first

    @userto = Userd.find_by_id(userto)
    @usergroup_limit_to = UsersGroup.find_by_id(@userto.group_id.to_i)

    @ratio = UsersWalletsFrozensRatio.where(users_group_id: @current_user.group_id,user_wallet_purpose_ratio_id: @purpose_id.to_i).first



@messege = ''


if userto.to_i == userid.to_i
     @messege  = @messege +   I18n.t("you can not transfer balance to yourself") 

end

if @userto == nil

     @messege  = @messege +   I18n.t("The User Wasn't Stored in Our Database") 

end

if compared_val.to_f <= 0

    @messege  = @messege +   I18n.t("the value must be greater than zero") 

end

if currency == ""

    @messege  = @messege +   I18n.t("you should select transfer currency") 

    end

if dollar_wallet != nil 

    if currency == "USD"
   
        @check_limit =  compared_val.to_f +  @current_user.current_family_limit.to_f;
        @check_limit_recieve =  compared_val.to_f + @userto.current_max_family_recieve.to_f 
        @expenses = (@sum * @ratio.ratio/100) + @ratio.fees


        if compared_val.to_f >  dollar_wallet.balance.to_f 
        
            @messege  = @messege + ' - ' +   I18n.t("your balance is less than the value") 

        end

        if compared_val.to_f <   @usergroup_limit.minimum_value.to_f 

            @messege  = @messege + ' - ' + I18n.t("the value must be greater than") + @usergroup_limit.minimum_value.to_s + I18n.t('dollar') 

        end

        if compared_val.to_f <  @expenses.to_f 
            
            @messege  = @messege + ' - ' +   I18n.t("the transfer balance is less than the website ratio and fees") 

        end

        if ( @check_limit_recieve.to_f >  @usergroup_limit_to.max_family_recieve.to_f  && @user_verification_to.all_verified != 1)

            @messege =   @messege + ' - ' + I18n.t("You have exceeded the prescribed rate Reciever")

        end

        if ( @check_limit.to_f >  @usergroup_limit.family_limit.to_f  && @user_verification.all_verified != 1)

            @messege =   @messege + ' - ' + I18n.t("You have exceeded the prescribed rate Sender")
         
        end


        if ( @check_limit_recieve.to_f >  @usergroup_limit_to.max_family_recieve.to_f  && purpose_id == 3 && @user_verification_to.all_verified == 1)

            @messege =   @messege + ' - '  + I18n.t("You have exceeded the prescribed rate Reciever")

        end

        if ( @check_limit.to_f >  @usergroup_limit.family_limit.to_f  && purpose_id == 3 && @user_verification.all_verified == 1)

            @messege =   @messege + ' ' +   ' '  + I18n.t("You have exceeded the prescribed rate Sender")
          
        end
    
    end

end 


if egp_wallet != nil 

     if currency == "EGP"

          @usdtoegp =    Setting.where(key:'USD TO EGP').first 
          @expenses = (@sum * @ratio.ratio/100) + (@ratio.fees * @usdtoegp.value.to_f)


          #the family limit of current user in EGP
          @egp_current_limit = @current_user.current_family_limit.to_f * @usdtoegp.value.to_f  

         #the minimum limit of current user group in EGP
          @egp_minimum_limit = @usergroup_limit.minimum_value.to_f * @usdtoegp.value.to_f 

         #the family limit of current user group in EGP
          @egp_family_limit = @usergroup_limit.family_limit.to_f * @usdtoegp.value.to_f  
          
         #the value of current user  compare with 
          @check_limit = compared_val.to_f + @egp_current_limit.to_f

          #the user to group family limit
          @egp_family_recieve = @usergroup_limit_to.max_family_recieve.to_f * @usdtoegp.value.to_f 

         #the value of  user to  compare with 
          @check_limit_recieve =  compared_val.to_f + (@userto.current_max_family_recieve.to_f * @usdtoegp.value.to_f ) 



            if compared_val.to_f <  @egp_minimum_limit.to_f 

                @messege = @messege + ' '+' '+  I18n.t("the value must be greater than") + @egp_minimum_limit.to_s + I18n.t('egyptian pound') 
             end

            if compared_val.to_f <  @expenses.to_f 
                
                @messege  = @messege + ' - ' +   I18n.t("the transfer balance is less than the website ratio and fees") 

            end

            if compared_val.to_f >  egp_wallet.balance.to_f 
            
             @messege = @messege + ' '+ ' '+ I18n.t("your balance is less than the value") 

             
             end



            if ( @check_limit_recieve.to_f >  @egp_family_recieve.to_f && @user_verification.all_verified != 1)

                @messege =   @messege + ' ' +   ' '  + I18n.t("You have exceeded the prescribed rate Reciever")


            end

           if ( @check_limit.to_f >  @egp_family_recieve.to_f && @user_verification.all_verified != 1)

                @messege =   @messege + ' ' +   ' '  + I18n.t("You have exceeded the prescribed rate Sender")

               
            end



           if ( @check_limit.to_f >  @egp_family_limit.to_f  && purpose_id == 3 && @user_verification.all_verified == 1 )


                    
            @messege =   @messege + ' ' +   ' '  + I18n.t("You have exceeded the prescribed rate Sender")

            end


            if ( @check_limit_recieve.to_f > @egp_family_recieve.to_f  && purpose_id == 3 && @user_verification.all_verified == 1)

                @messege =   @messege + ' ' +   ' '  + I18n.t("You have exceeded the prescribed rate Reciever")


            end


    end
end 






if sar_wallet != nil 

    if currency == "SAR"

         @usdtors =    Setting.where(key:'USD TO SAR').first   
         @expenses = (@sum * @ratio.ratio/100) + (@ratio.fees * @usdtors.value.to_f)


         #the family limit of current user in SAR
         @sar_current_limit = @current_user.current_family_limit.to_f * @usdtors.value.to_f  

        #the minimum limit of current user group in SAR
         @sar_minimum_limit = @usergroup_limit.minimum_value.to_f * @usdtors.value.to_f 

        #the family limit of current user group in SAR
         @sar_family_limit = @usergroup_limit.family_limit.to_f * @usdtors.value.to_f  
         
        #the value of current user  compare with 
         @check_limit = compared_val.to_f + @egp_current_limit.to_f

         #the user to group family limit
         @sar_family_recieve = @usergroup_limit_to.max_family_recieve.to_f * @usdtors.value.to_f 

        #the value of  user to  compare with 
         @check_limit_recieve =  compared_val.to_f + (@userto.current_max_family_recieve.to_f * @usdtors.value.to_f ) 



           if compared_val.to_f <  @sar_minimum_limit.to_f 

               @messege = @messege + ' '+' '+  I18n.t("the value must be greater than") + @sar_minimum_limit.to_s + I18n.t('saudi riyal') 
            end


            if compared_val.to_f <  @expenses.to_f 
                
                @messege  = @messege + ' - ' +   I18n.t("the transfer balance is less than the website ratio and fees") 

            end

           if compared_val.to_f >  sar_wallet.balance.to_f 
           
            @messege = @messege + ' '+ ' '+ I18n.t("your balance is less than the value") 

            
            end


            if ( @check_limit_recieve.to_f >  @sar_family_limit.to_f && @user_verification.all_verified != 1)

                @messege =   @messege + ' ' +   ' '  + I18n.t("You have exceeded the prescribed rate Reciever")


            end

           if ( @check_limit.to_f >  @sar_family_limit.to_f && @user_verification.all_verified != 1)

                @messege =   @messege + ' ' +   ' '  + I18n.t("You have exceeded the prescribed rate Sender")

               
            end


          if ( @check_limit.to_f >  @sar_family_limit.to_f  && purpose_id == 3 && @user_verification.all_verified == 1 )


                   
           @messege =   @messege + ' ' +   ' '  + I18n.t("You have exceeded the prescribed rate Sender")

           end


           if ( @check_limit_recieve.to_f > @sar_family_recieve.to_f  && purpose_id == 3 && @user_verification.all_verified == 1  )

               @messege =   @messege + ' ' +   ' '  + I18n.t("You have exceeded the prescribed rate Reciever")


           end


   end
end 






# if @sar_wallet != nil 

#      if (@currency == "SAR")


#         @usdtors =    Setting.where(key:'USD TO SAR').first
#         @riyal_family_limit = @usergroup_limit.family_limit.to_f * @usdtors.value.to_f 
#         @riyal_minimum_limit = @usergroup_limit.minimum_value.to_f * @usdtors.value.to_f
#         @riyal_current_limit = @current_user.current_family_limit.to_f * @usdtors.value.to_f

    
#          @check_limit = compared_val.to_f + @riyal_current_limit.to_f 

#             if compared_val.to_f < @riyal_minimum_limit

#                 @messege =   t("the value must be greater than") + @riyal_minimum_limit.to_s + t('saudi riyal') 

#             end
#             if compared_val.to_f > @sar_wallet.balance 
            
#                 @messege =   t("your balance is less than the value") 
#             end

#             if @check_limit >  @riyal_family_limit  && @trans_purpose == 3
#                 @messege =   t("You have exceeded the prescribed rate")

#             end
#     end
# end 




#         if @compared_val <= 0
        
#             @messege =   t("the value must be greater than zero") 

#         end

        


#################################
#     @ratioid=ratioid
#     @userdata = user
#     @user = Userd.find_by_id(@userdata)
#     @sum=  value.to_f
#     @ratio = UsersWalletsFrozensRatio.where(users_group_id: @user.group_id,user_wallet_purpose_ratio_id: @ratioid).first

#     #@expenses = (@sum * @ratio.ratio/100) 
     
#    @expenses = (@sum * @ratio.ratio/100) + @ratio.fees
#####################################################

       return   @messege
       
end




end
