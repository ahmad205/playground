module ApplicationHelper
	def show_field_error(model, field)
    s=""

    if !model.errors[field].empty?
      s =
        <<-EOHTML
           <div id="error_message">
             #{model.errors[field][0]}
           </div>
        EOHTML
    end

    s.html_safe
  end


def get_attachment(request,id,type)
  request.protocol + request.host + "/get_attachment/" + id.to_s + "?type=" + type.to_s
end

def admin_attachment(request,id,type)
  request.protocol + request.host + "/admin_attach/" + id.to_s + "?type=" + type.to_s
end


end
