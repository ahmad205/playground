module BalancesWithdrawsStatusesHelper

    def status_image(id)
        request.protocol + request.host + "/withdraw_status_image?id=" + id.to_s
    end

end
