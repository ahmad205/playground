json.extract! currencies_withdraw_ratio, :id, :name, :withdraw_type, :currency_code, :hint, :ratio_bank, :ratio_company, :additional_expenses, :status, :created_at, :updated_at
json.url currencies_withdraw_ratio_url(currencies_withdraw_ratio, format: :json)
