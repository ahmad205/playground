json.extract! email_template, :id, :key, :greeting_en, :greeting_ar, :subject_en, :subject_ar, :en_content, :ar_content, :class, :action, :created_at, :updated_at
json.url email_template_url(email_template, format: :json)
