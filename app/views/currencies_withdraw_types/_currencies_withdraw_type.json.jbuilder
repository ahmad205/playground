json.extract! currencies_withdraw_type, :id, :name, :currencies_withdraw_ratios_id, :created_at, :updated_at
json.url currencies_withdraw_type_url(currencies_withdraw_type, format: :json)
