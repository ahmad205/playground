json.array!(@company_banks) do |company_bank|
  json.extract! company_bank, :id, :bank_key, :bank_name, :country, :branch, :phone, :account_name, :account_email, :account_number, :visa_cvv, :swiftcode, :expire_date
  json.url company_bank_url(company_bank, format: :json)
end
