json.extract! @company_bank, :id, :bank_key, :bank_name, :country, :branch, :phone, :account_name, :account_email, :account_number, :visa_cvv, :swiftcode, :expire_date, :created_at, :updated_at
