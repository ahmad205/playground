json.array!(@frontendpages) do |frontendpage|
  json.extract! frontendpage, :id
  json.url frontendpage_url(frontendpage, format: :json)
end
