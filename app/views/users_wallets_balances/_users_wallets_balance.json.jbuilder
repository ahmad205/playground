json.extract! users_wallets_balance, :id, :user_id, :user_id_to, :currency, :wallet_name, :balance, :ratio_company, :status, :created_at, :updated_at
json.url users_wallets_balance_url(users_wallets_balance, format: :json)
