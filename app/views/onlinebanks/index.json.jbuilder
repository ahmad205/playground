json.array!(@onlinebanks) do |onlinebank|
  json.extract! onlinebank, :id, :user_id, :service_id, :tansaction, :currency, :value, :refund, :approved, :created_at
  json.url onlinebank_url(onlinebank, format: :json)
end
