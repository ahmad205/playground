json.array!(@users_groups) do |users_group|
  json.extract! users_group, :id, :name, :description, :order_ratio, :transfer_ratio, :add_balance_ratio, :withdraw_sum
  json.url users_group_url(users_group, format: :json)
end
