json.array!(@company_balances) do |company_balance|
  json.extract! company_balance, :id, :user_id, :balance_operation_id, :balance_withdraw_id, :balance, :status, :hint, :balances_operation_id, :balances_withdraw_id
  json.url company_balance_url(company_balance, format: :json)
end
