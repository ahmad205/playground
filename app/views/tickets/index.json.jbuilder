json.array!(@tickets) do |ticket|
  json.extract! ticket, :titleuser, :bodyuser
  json.url user_url(ticket, format: :json)
end
