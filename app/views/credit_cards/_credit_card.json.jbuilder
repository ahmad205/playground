json.extract! credit_card, :id, :user_id, :name, :card_no, :card_type, :expire_date, :created_at, :updated_at
json.url credit_card_url(credit_card, format: :json)
