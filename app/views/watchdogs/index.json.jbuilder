json.array!(@watchdogs) do |whatchdog|
  json.extract! whatchdog, :user_id, :action ,:created_at
  json.url watchdog_url(Watchdog, format: :json)
end
