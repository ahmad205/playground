json.array!(@users_balance) do |department|
  json.extract! users_balance, :id
  json.url banks_list_url(users_balance, format: :json)
end
