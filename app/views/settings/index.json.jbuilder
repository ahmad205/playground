json.array!(@settings) do |setting|
  json.extract! setting, :name, :key ,:value,:description
  json.url setting_url(Setting, format: :json)
end
