json.array!(@balances_withdraws) do |balances_withdraw|
  json.extract! balances_withdraw, :id, :thesum, :user_id, :netsum, :company_sum, :bank_sum, :status, :hint, :expect_date, :user_bank_id, :users_bank_id
  json.url balances_withdraw_url(balances_withdraw, format: :json)
end
