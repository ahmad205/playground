json.extract! balances_withdraws_status, :id, :balances_withdraws_id, :status, :user_id, :user_bank_id, :description, :created_at, :updated_at
json.url balances_withdraws_status_url(balances_withdraws_status, format: :json)
