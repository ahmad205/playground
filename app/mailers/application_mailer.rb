class ApplicationMailer < ActionMailer::Base

  include ApplicationHelper

  default from: "Payers <no-reply@payers.net>"
  layout 'mailer'
  # @host = request.protocol + request.host

end
