class MassMailer < ApplicationMailer
    default from: "Payers <no-reply@payers.net>"


    def mass_mail(user)

        @result = EmailTemplate.get_template(user,'mass_mail')
        @greeting = @result[0].to_s
        @subject = @result[1].to_s
        @content = @result[2].to_s
    
        @username=user.username
       
          mail to: user.email, subject: @subject
      end
end
