class Chargebalance < ApplicationMailer

 # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.chargebalance.charge.subject
  #
  default from: "Payers <no-reply@payers.net>"
  
  def payforcart(user,cartvalue,currentbalance)


    @result = EmailTemplate.get_template(user,'payforcart')

    @greeting = @result[0].to_s
    @subject = @result[1].to_s
    @content = @result[2].to_s


    @username=user.username
    @cartvalue=cartvalue
    @value=cartvalue.value
    @expiredate=cartvalue.expire_date
    
   @currentbalance=currentbalance

   @pre= AESCrypt.decrypt(@cartvalue.prefix, 'ServHight')  
   @cartnn=    AESCrypt.decrypt(@cartvalue.cart_number, 'ServHight') 

      mail to: user.email, subject: @subject
  end


def charge(user,cartvalue,currentbalance)
  

  @result = EmailTemplate.get_template(user,'charge')

  @greeting = @result[0].to_s
  @subject = @result[1].to_s
  @content = @result[2].to_s


  @username=user.username
  @cartvalue=cartvalue
  @value=cartvalue.value
  @expiredate=cartvalue.expire_date
  
 @currentbalance=currentbalance

 @pre= AESCrypt.decrypt(@cartvalue.prefix, 'ServHight')  
 @cartnn=    AESCrypt.decrypt(@cartvalue.cart_number, 'ServHight') 

    mail to: user.email, subject: @subject
    
  end


end