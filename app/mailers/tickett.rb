class Tickett < ApplicationMailer
default from: "Payers <no-reply@payers.net>"
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.wallet.activate_deactivate.subject
  #
  def ticket(user,ticket_number)

    @result = EmailTemplate.get_template(user,'ticket')
    @greeting = @result[0].to_s
    @subject = @result[1].to_s
    @content = @result[2].to_s

    @username=user.username
    @ticket_number=ticket_number
   
      mail to: user.email, subject: @subject
  end

  def ticket_reply(user,ticket_number)
    @result = EmailTemplate.get_template(user,'ticket_reply')
    @greeting = @result[0].to_s
    @subject = @result[1].to_s
    @content = @result[2].to_s

    @username=user.username
    @ticket_number=ticket_number
   
      mail to: user.email, subject: @subject
  end
  def ticket_close(user,ticket_number)
    @result = EmailTemplate.get_template(user,'ticket_close')
    @greeting = @result[0].to_s
    @subject = @result[1].to_s
    @content = @result[2].to_s

    @username=user.username
    @ticket_number=ticket_number
   
      mail to: user.email, subject: @subject
  end
end
 