class Payment < ApplicationMailer

    # Subject can be set in your I18n file at config/locales/en.yml
     # with the following lookup:
     #
     #   en.payment_request.charge.subject
     #
     default from: "Payers <no-reply@payers.net>"
     
     def new_payment(user,onlinebank)
       @result = EmailTemplate.get_template(user,'new_payment')
       @greeting = @result[0].to_s
       @subject = @result[1].to_s
       @content = @result[2].to_s
       @username = user.username
       @onlinebank = onlinebank
       @trans = onlinebank.transaction_id
       @netsum = onlinebank.netsum      
             
         mail to: user.email, subject: @subject
     end
   
     def add_payment_data(user,onlinebank)
      @result = EmailTemplate.get_template(user,'add_payment_data')
      @greeting = @result[0].to_s
      @subject = @result[1].to_s
      @content = @result[2].to_s
      @username = user.username
      @onlinebank = onlinebank
      @trans = onlinebank.transaction_id
      @netsum = onlinebank.netsum      
            
        mail to: user.email, subject: @subject
    end

    def cancel_payment_request(user,onlinebank)
      @result = EmailTemplate.get_template(user,'cancel_payment_request')
      @greeting = @result[0].to_s
      @subject = @result[1].to_s
      @content = @result[2].to_s
      @username = user.username
      @onlinebank = onlinebank
      @trans = onlinebank.transaction_id
      @netsum = onlinebank.netsum      
            
        mail to: user.email, subject: @subject
    end
   
   
   
   end