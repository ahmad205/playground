class Emailcode < ApplicationMailer

  default from: "Payers <no-reply@payers.net>"
  
  
      
      def sendcode(useremail,otp_code)
  
        @result = EmailTemplate.get_template(useremail,'sendcode')
        @greeting = @result[0].to_s
        @subject = @result[1].to_s
        @content = @result[2].to_s

        @email=useremail.email
          @code=otp_code    
         mail to: @email, subject: @subject
  
        end
      

        def backupcode(useremail,code)

          @result = EmailTemplate.get_template(useremail,'backupcode')
          @greeting = @result[0].to_s
          @subject = @result[1].to_s
          @content = @result[2].to_s

          @email=useremail.email

          @code=code    
          mail to: @email, subject: @subject


        end

      def failed_signin(useremail,messege)

       
        @result = EmailTemplate.get_template(useremail,'failed_signin')
        @greeting = @result[0].to_s
        @subject = @result[1].to_s
        @content = @result[2].to_s

        @test2 = messege.to_s.split('/') 
        @shown_msg = t(@test2[0])+' ' + 'IP'+' ' + @test2[1] +' '+ t('Device Type') + ' '+ @test2[2]+' ' + t('Operating System') + ' '+ @test2[3]+' '+ @test2[4] +' '+t('Browser')+' '+ @test2[5] 
       
        @code= @shown_msg

        @email=useremail.email

       mail to: @email, subject: @subject

      end
end
