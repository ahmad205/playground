class BalancewithdrawMail < ApplicationMailer
  default from: "Payers <no-reply@payers.net>"
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.balancewithdraw_mail.withdraw.subject
  #
  def withdraw(user,balancewithdraw,currency,status)

    @result = EmailTemplate.get_template(user,'balancewithdraw')

    @greeting = @result[0].to_s
    @subject = @result[1].to_s
    @content = @result[2].to_s
    @username=user.username
    @currency=currency
    @status = status
    #@balancewithdraw=balancewithdraw
    @sum=balancewithdraw.thesum
    @trans=balancewithdraw.transaction_id
    mail to: user.email, subject: @subject
  end

  def cancelwithdraw(user,balancewithdraw,currency,status)

    @result = EmailTemplate.get_template(user,'cancelwithdraw')

    @greeting = @result[0].to_s
    @subject = @result[1].to_s
    @content = @result[2].to_s
    @username=user.username
    @currency=currency
    @status = status
    #@balancewithdraw=balancewithdraw
    @sum=balancewithdraw.thesum
    @trans=balancewithdraw.transaction_id
    mail to: user.email, subject: @subject
  end

end
