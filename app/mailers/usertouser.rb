class Usertouser < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.usertouser.transfer.subject
  #
  default from: "Payers <no-reply@payers.net>"
  
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.wallet.activate_deactivate.subject
  #
  def transfer(user,userto,walletfrozen,tran_id)
    @result = EmailTemplate.get_template(user,'transfer')
    @greeting = @result[0].to_s
    @subject = @result[1].to_s
    @content = @result[2].to_s


    @username=user.username
    @walletfrozen=walletfrozen
    @trans_id = tran_id
   @userto=userto
      mail to: user.email, subject: @subject
  end
  def transferto(user,userto,walletfrozen,tran_id)
    @result = EmailTemplate.get_template(userto,'transferto')
    @greeting = @result[0].to_s
    @subject = @result[1].to_s
    @content = @result[2].to_s

    @username=userto.username
    @userfrom=user.username
    @walletfrozen=walletfrozen
    @trans_id = tran_id
   
      mail to: userto.email, subject: @subject
  end
  
def confirmtransfer(user,userto,walletfrozen,tran_id,status)
  @result = EmailTemplate.get_template(user,'confirmtransfer')
  @greeting = @result[0].to_s
  @subject = @result[1].to_s
  @content = @result[2].to_s

  @username=user.username
    @userto=userto
    @walletfrozen=walletfrozen
    @trans_id = tran_id
    @status = status
   
      mail to: user.email, subject: @subject
  end

  def confirmtransfertransferto(user,userto,walletfrozen,tran_id,status)
    @result = EmailTemplate.get_template(userto,'confirmtransfertransferto')
    @greeting = @result[0].to_s
    @subject = @result[1].to_s
    @content = @result[2].to_s

    @username=userto.username
    @userfrom=user.username
    @walletfrozen=walletfrozen
    @trans_id = tran_id
    @status = status
   
      mail to: userto.email, subject: @subject
  end

  def cancel_transfer_mail(user,userto,userswalletsbalancesfrozen,tran_id,status)
    @result = EmailTemplate.get_template(user,'cancel_transfer_mail')
    @greeting = @result[0].to_s
    @subject = @result[1].to_s
    @content = @result[2].to_s
  
      @username = user.username
      @userto = userto
      @userswalletsbalancesfrozen = userswalletsbalancesfrozen
      @status = status
      @trans_id = tran_id

      
  
        mail to: user.email, subject: @subject
    end
  
    def cancel_transfer_to_mail(user,userto,userswalletsbalancesfrozen,tran_id,status)
      @result = EmailTemplate.get_template(userto,'cancel_transfer_to_mail')
      @greeting = @result[0].to_s
      @subject = @result[1].to_s
      @content = @result[2].to_s
  
      @username = userto.username
      @userfrom = user.username
      @userswalletsbalancesfrozen = userswalletsbalancesfrozen
      @status = status
      @trans_id = tran_id
      
     
        mail to: userto.email, subject: @subject
    end


  
end
