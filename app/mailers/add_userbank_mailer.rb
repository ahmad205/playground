class AddUserbankMailer < ApplicationMailer
  default from: "Payers <no-reply@payers.net>"
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.add_userbank_mailer.userbank.subject
  #
  

  def userbank(user, bankname)

    @result = EmailTemplate.get_template(user,'userbank')

    @greeting = @result[0].to_s
    @subject = @result[1].to_s
    @status = @subject
    @content = @result[2].to_s
    @username=user.username
    @bankname=bankname
    
    mail to: user.email, subject: @subject
  end

end
