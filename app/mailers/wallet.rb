class Wallet < ApplicationMailer
  default from: "Payers <no-reply@payers.net>"
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.wallet.activate_deactivate.subject
  #

  def newwallet(user,walletname)   
    @username=user.username
    @walletname=walletname

      @result = EmailTemplate.get_template(user,'wallet_create')
            @greeting = @result[0].to_s
            @subject = @result[1].to_s
            @status = @subject
            @content = @result[2].to_s

   
      mail to: user.email, subject: @subject.to_s
  end
  def activate (user,walletname)
    # @greeting = "Hi"
    # @subject = 'wallet'
    # @content = ''

    @username=user.username
    @walletname=walletname

      @result = EmailTemplate.get_template(user,'wallet_activate')
            @greeting = @result[0].to_s
            @subject = @result[1].to_s
            @status = @subject
            @content = @result[2].to_s

   
      mail to: user.email, subject: @subject.to_s
  end




  def deactivate(user,walletname)
    # @greeting = "Hi"
    # @subject = 'wallet'
    # @content = ''

    @username=user.username
    @walletname=walletname
   

      @result = EmailTemplate.get_template(user,'wallet_deactivate')
      @greeting = @result[0].to_s
      @subject = @result[1].to_s
      @status = @subject
      @content = @result[2].to_s
      
      
      mail to: user.email, subject: @subject.to_s
  end






   def deletewallet(user,walletname)

            @username=user.username
            @walletname=walletname

            @result = EmailTemplate.get_template(user,'wallet_delete')
            @greeting = @result[0].to_s
            @subject = @result[1].to_s
            @status = @subject
            @content = @result[2].to_s




       mail to: user.email, subject: @subject.to_s
    
  end

  def balancetransfer(user,value,from,to,netvalue)


    @result = EmailTemplate.get_template(user,'balancetransfer')
    @greeting = @result[0].to_s
    @subject = @result[1].to_s
    @status = @subject
    @content = @result[2].to_s

    
    @username=user.username
    @value=value
    @from_wallet=from
    @to_wallet=to
    @net_value=netvalue

    mail to: user.email, subject: @subject
    
  end
end
